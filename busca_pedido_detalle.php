<?php

include ("conexion.php");
include ("formatos.php");


$idpedido = $_POST["idpedido"];
$total = 0;

$sql="SELECT pd.idpedido,a.nombreart,pd.precio,pd.cantidad,pd.articulo,pd.subtotal FROM pedido_det pd
      join articulos a on a.idarticulo=pd.articulo 
      WHERE pd.idpedido=$idpedido";

$res = mysqli_query($conexion,$sql);
$con = mysqli_num_rows($res);

if ($con>0) {
   echo "<div class='box box-primary'>";
   echo "<div class='box-header with-border'>";
   echo "</div><br>";
   echo "<div class='box-body'>";
   echo "<table class='table table-bordered table-hover' id='tabla_pedido'>";
   echo "<thead>";
   echo "<tr>";
   echo "<th>Articulo</th>";
   echo "<th>Cantidad</th>";
   echo "<th>Precio</th>";
   echo "<th>Subtotal</th>";
   echo "</tr>";
   echo "</thead>";
   echo "<tbody>";
    while($row = mysqli_fetch_array($res)){

      $total += $row['subtotal'];

      $data=$row['idpedido']."|".$row["articulo"];
      
      echo "<tr>";
      echo "<td>".$row['nombreart']."</td>";
      echo "<td>".$row['cantidad']."</td>";
      echo "<td>".formatearNumero($row['precio'])."</td>";
      echo "<td>".formatearNumero($row['subtotal'])."</td>";
      echo "</tr>";
    }

   echo "<tr>";
   echo "<td><b>TOTAL: <b></td>";
   echo "<td><b>".formatearNumero($total)." Gs.</b></td>";
   echo "</tr>";


   echo '</tbody>';
   echo "</table>";
   echo "</div>";
   echo "</div>";
}else{
   echo "Sin detalle...";
}


?>