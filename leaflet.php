<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<head>
	
	<link rel="stylesheet" href="css/leaflet.css" />
 	<script src="js/leaflet.js"></script>
 	<script src="js/jquery.min.js"></script>
 	<script src='js/leaflet-src.js'></script>

 <style>
  #map { 
  widh: 50px;
  height: 600px; }
 </style>
 
 </head>
  <body>
   <input type="text" name="latitud" id="latitud">
   <input type="text" name="longitud" id="longitud">
   <div id="map"></div>

   <?php 

   $latitud= "-25.346607";
   $longitud= "-57.628693";

   ?>
 
 </body> 
 </html>

<script>
		var map = L.map('map').
		setView([<?php echo $latitud ?>,<?php echo $longitud ?>], 
		20);
		 
		L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		    attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>',
		    maxZoom: 18
		}).addTo(map);

		L.control.scale().addTo(map);
		L.marker([<?php echo $latitud ?>,<?php echo $longitud ?>], {draggable: true}).addTo(map);

		var popup = L.popup();

		function onMapClick(e) {
		    popup
		        .setLatLng(e.latlng) // Sets the geographical point where the popup will open.
		        .setContent("Has hecho click en la coordenada:<br> " +  e.latlng.lat.toString() + "," +  e.latlng.lng.toString()) // Sets the HTML content of the popup.
		        .openOn(map); // Adds the popup to the map and closes the previous one. 
		}

		map.on('click', onMapClick);

</script>