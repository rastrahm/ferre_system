<!DOCTYPE html>
<html lang="en">
	<?php include("header.php"); ?>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<?php 

			include("header_top.php"); 

			?>

			
			<!-- /TOP HEADER -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container" >
				
				<!-- row -->
				<div class="card-body">

					<h2>Contacto</h2>

					<form action="guardarMensaje.php" method="post">
                            <div class="row">
							<div class="col-md-6">
							<div id="ubicacion"></div>
							</div><br><br>
                                <div class="col-12 col-lg-6">
                                	<span>Nombres</span>
                                    <input class="input" type="text"  name="nombres" id="nombres" class="form-control">
                                </div><br>
                                <div class="col-12 col-lg-6">
                                	<span>Apellidos</span>
                                    <input class="input" type="text" name="apellidos" id="apellidos" class="form-control">
                                </div><br>
                                <div class="col-12 col-lg-6">
                                	<span>Correo</span>
                                    <input class="input" type="email" name="correo" id="correo" class="form-control">
                                </div><br>
                                 <div class="col-12 col-lg-6">
                                 	<span>Telefono</span>
                                    <input class="input" type="text" name="telefono" id="telefono" class="form-control">
                                </div><br> 
                                <div class="col-12 col-lg-6"><br>
                                    <textarea name="mensaje" id="mensaje" class="form-control" placeholder="Su Mensaje"></textarea>
                                </div><br><br>
                                <div class="col-12 text-center"><br>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-envelope"></i> Enviar</button>
                                </div>
                            </div>
                               
                        </form>
                        </div>
				<!-- /row -->
			</div>
			<!-- /container -->
			
		</div>
		<!-- /SECTION -->
		<!-- FOOTER -->
		<?php include("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php"); ?>

	</body>
</html>

<script type="text/javascript">
	
	 window.onload = function() {
          $.ajax({
            url:'ubicacion2.php',
            success:function(data){
              $("#ubicacion").html(data).fadeIn('slow');
            }
          })
        };

    $(document).on("ready", function(){
      guardar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarMensaje.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Mensaje Enviado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "VACIO" ){
          swal({
              title: 'Atención!',
              text: 'No se cargo ninguna imagen',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

</script>