<?php

include ("conexion.php");
include ("./seguridad/formatos.php");


$idcliente = $_POST["idcliente"];
$total = 0;

$sql="SELECT pd.idpedido,tp.desc_tip_ped,p.fecha,p.hora,p.estado,a.nombreart,pd.precio,pd.cantidad,pd.articulo,pd.subtotal FROM pedido_det pd
   join pedido p on p.idpedido=pd.idpedido
   join tipo_pedido tp on tp.idtipo_ped=pd.tipo_pedido
   join articulos a on a.idarticulo=pd.articulo 
   WHERE p.idcliente=$idcliente";

$res = mysqli_query($conexion,$sql);
$con = mysqli_num_rows($res);

if ($con>0) {
   echo "<div class='box box-primary'>";
   echo "<div class='box-header with-border'>";
   echo "</div><br>";
   echo "<div class='box-body'>";
   echo "<table class='table table-bordered table-hover'>";
   echo "<thead>";
   echo "<tr>";
   echo "<th>Nro Pedido</th>";
   echo "<th>Articulo</th>";
   echo "<th>Cantidad</th>";
   echo "<th>Precio</th>";
   echo "<th>Subtotal</th>";
   echo "</tr>";
   echo "</thead>";
   echo "<tbody>";
    while($row = mysqli_fetch_array($res)){

      $total += $row['subtotal'];

      $data=$row['idpedido']."|".$row["articulo"];
      
      echo "<tr>";
      echo "<td>".$row['articulo']."</td>";
      echo "<td>".$row['nombreart']."</td>";
      echo "<td>".$row['cantidad']."</td>";
      echo "<td>".formatearNumero($row['precio'])."</td>";
      echo "<td>".formatearNumero($row['subtotal'])."</td>";
      echo '<td><button class="btn btn-primary" onclick="editar(this)"><i class="fa fa-edit"></i></button></td>';
      echo '<td><button class="btn btn-success" id="'.$row['articulo'].'" title="Ver disponibilidad" onclick="consultar_stock(this.id)"><i class="fa fa-eye"></i></button></td>';
      echo '<td><button class="btn btn-danger" title="eliminar" id="'.$data.'" onclick="eliminar(this.id)"><i class="fa fa-trash"></i></button></td>';
      echo "</tr>";
    }

   echo "<tr>";
   echo "<td><b>TOTAL: <b></td>";
   echo "<td><b>".formatearNumero($total)." Gs.</b></td>";
   echo "</tr>";


   echo '</tbody>';
   echo "</table>";
   echo "</div>";
   echo "</div>";
}else{
   echo "Sin pedidos...";
}


?>