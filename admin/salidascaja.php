<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");

    date_default_timezone_set('America/Asuncion');
    include ("conexion.php");
    include ("verificarlogeo4.php");

    $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

    $hoy = date("Y-m-d");

    $sql = "SELECT * from caja where estado=0 and fechaapertura like '$hoy'";

    $res = mysqli_query($conexion,$sql);

    while ($row = mysqli_fetch_array($res)){
        $estado = $row['estado'];
    }
    
    if(isset($estado)=='' || $estado==1){
      echo "<script type='text/javascript'>
        location.href='aperturacaja.php';
      </script>";
    }
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
         
              <ol class="breadcrumb">
        <li>
            <small><?php echo $fecha; ?></small>
        </li>
      </ol>
    <div class="card-body">
          <!-- Your Page Content Here -->
      <div class='row'>
        <div class="card mb-3">
          <div class="col-md-12"><br>
              <form action="" method="POST">
                    <h3 class='box-title'>Salida de caja</h3><br>
                    <div class='box-body'>
                    <div class='input-group'>
                    <span class='input-group-addon'>Fecha:</span>
                    <input type='date' id='fecha' name="fecha" class='form-control' value="<?php echo date("Y-m-d") ?>" readonly="readonly">
                    </div>
                    <br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Hora:</span>
                    <input type='text' id='hora' name="hora" class='form-control' value="<?php echo strftime("%H:%M")?>" readonly="readonly">
                    </div>
                    <br>
                    <div class='input-group'>
                    <span class='input-group-addon'>N° Caja:</span>
                    <input type='text' id="nrocaja" name="nrocaja" class='form-control' value="001" readonly="readonly">
                    </div>
                    <br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Monto:</span>
                    <input type='number' id='monto' name="monto" class='form-control'>
                    </div>
                    <br>
                     <div class='input-group'>
                    <span class='input-group-addon'>Observacion:</span>
                    <textarea id="observacion" name="observacion" class="form-control"></textarea>
                    </div>
                    <br>
                    <button type="button" id="mostrar" class="btn btn-primary"><i class="fa fa-check"></i> Guardar</button>
                </div>
              </form>
          </div><br>
        </div>
      </div><br><br>
    </div>

        </div>     

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
    <?php include("seguridad.php");  ?>
    <?php include("scripts.php");  ?>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

    $("#mostrar").click(function(){
      $('#verificar').modal('show');
    });

    $("#ocultar").click(function(){
      $('#verificar').modal('hide');
    });
     
function procesarsalida(){
        var monto = $("#monto").val();
        var caja = $("#nrocaja").val();
        var opcion = 'registrar';
        var observacion = $("#observacion").val();
        var contrasena = $("#contrasena").val();
        $.ajax({
          method: "POST",
          url: "guardarSalidas.php",
          data: 'monto='+monto+'&observacion='+observacion+'&caja='+caja+'&contrasena='+contrasena+'&opcion='+opcion
        }).done( function( info ){   
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
        });
    }

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "AUTORIZADO"){
          swal({
              title: 'Atención!',
              text: 'No esta autorizado!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ABIERTA"){
          swal({
              title: 'Atención!',
              text: 'La caja ya esta abierta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "CERRADA"){
          swal({
              title: 'Atención!',
              text: 'La caja ya esta cerrada!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }
    
</script>
