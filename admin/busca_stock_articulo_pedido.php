<?php

include ("conexion.php");
include ("./seguridad/formatos.php");


$idarticulo = $_POST["idarticulo"];
$total = 0;

$sql="SELECT s.desc_suc,b.descbodega,bd.articulo,bd.stock FROM bodegas_det bd
      join articulos a on a.idarticulo=bd.articulo 
      join bodegas b on b.idbodega=bd.bodega
      join sucursal s on s.idsucursal=b.sucursal
      WHERE bd.articulo=$idarticulo";

$res = mysqli_query($conexion,$sql);
$con = mysqli_num_rows($res);

if ($con>0) {
   echo "<div class='box box-primary'>";
   echo "<div class='box-header with-border'>";
   echo "</div><br>";
   echo "<div class='box-body'>";
   echo "<table class='table table-bordered table-hover' id='tabla_ventas_detalle'>";
   echo "<thead>";
   echo "<tr>";
   echo "<th>SUCURSAL</th>";
   echo "<th>BODEGA</th>";
   echo "<th>STOCK</th>";
   echo "</tr>";
   echo "</thead>";
   echo "<tbody>";
    while($row = mysqli_fetch_array($res)){

      $total += $row['stock'];
      
      echo "<tr>";
      echo "<td>".$row['desc_suc']."</td>";
      echo "<td>".$row['descbodega']."</td>";
      echo "<td>".$row['stock']."</td>";
      echo "</tr>";
    }

   echo "<tr>";
   echo "<td><b>TOTAL: <b></td>";
   echo "<td><b>".formatearNumero($total)."</b></td>";
   echo "</tr>";


   echo '</tbody>';
   echo "</table>";
   echo "</div>";
   echo "</div>";
}else{
   echo "Sin stock...";
}


?>