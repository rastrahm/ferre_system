<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");

     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

    include ("conexion.php");
    include ("./seguridad/formatos.php");
    
    $usuario = $_SESSION['nombreUsuario'];

    $sql = "SELECT max(h.idhabilitacion) as idhabilitacion from habilitaciones h
            JOIN usuarios u on u.idusuario=h.usuario where u.usuario='$usuario' and h.estado=0";
    $res = mysqli_query($conexion, $sql);
    $numcaja = mysqli_num_rows($res);

    if ($numcaja>0) {
       while($row=mysqli_fetch_array($res)) {
        $nrohabilitacion= $row['idhabilitacion'];
      }

    }else{

      $nrohabilitacion = "";

    }

    $sql = "SELECT u.idusuario,u.nrocaja,u.idsucursal,s.desc_suc from usuarios u 
            join sucursal s on s.idsucursal=u.idsucursal where usuario='$usuario' and estado=0";
    $res = mysqli_query($conexion, $sql);
    $numusu = mysqli_num_rows($res);

    if ($numusu>0) {
     while($row=mysqli_fetch_array($res)) {
      $nrocaja= $row['nrocaja'];
      $desc_suc = $row['desc_suc'];
      $idsucursal = $row['idsucursal'];
      $idusuario = $row['idusuario'];
    }
    }
    


?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
            <div class="container-fluid"><br>
                <ol class="breadcrumb">
                  <li>
                      <small><?php echo $fecha; ?></small>
                  </li>
                </ol>
    <div class="card-body">
          <!-- Your Page Content Here -->
      <div class='row'>
        <div class="card">
          <div class="card-header">
              <strong>Apertura de caja</strong> 
          </div>
              <form action="" method="POST">

                <div class="card-body card-block">
                          
                    <div class='input-group'>
                    <span class='input-group-addon'>Sucursal:</span>
                    <input type='hidden' id='idsucursal' name="idsucursal" class='form-control' value="<?php echo $idsucursal; ?>" readonly="readonly" >
                    <input type='text' id='sucursal' name="sucursal" class='form-control' value="<?php echo $desc_suc; ?>" readonly="readonly" >
                    </div><br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Nro Caja:</span>
                    <input type='text' id='nrocaja' name="nrocaja" class='form-control' value="<?php echo $nrocaja; ?>" readonly="readonly" >
                    </div><br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Usuario:</span>
                    <input type='hidden' id='idusuario' name="idusuario" class='form-control' value="<?php echo $idusuario; ?>" readonly="readonly">
                    <input type='text' id='usuario' name="usuario" class='form-control' value="<?php echo $_SESSION['nombreUsuario']; ?>" readonly="readonly">
                    </div>
                    <br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Fecha:</span>
                    <input type='date' id='fecha' name="fecha" class='form-control' value="<?php echo date("Y-m-d") ?>" readonly="readonly">
                    </div>
                    <br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Hora:</span>
                    <input type='text' id='hora' name="hora" class='form-control' value="<?php echo strftime("%H:%M")?>" readonly="readonly">
                    </div>
                    <br>
                    <div class='input-group'>
                    <span class='input-group-addon'>Nro Habilitacion:</span>
                    <input type='text' id='nrohabilitacion' name="nrohabilitacion" value="<?php echo $nrohabilitacion ?>" class='form-control' readonly="readonly">
                    </div>
                    <br>
              </div> 
                
              
               <div class="card-footer" align="right">
                            <button type="button" onclick="procesarapertura()" class="btn btn-primary"><i class="fa fa-check"></i> Habilitar</button>
                  </div>
                </form>
        </div>

      </div><br><br>
    </div>

        </div>     

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>

     <?php  include ("seguridad.php") ?>

    <?php include("scripts.php");  ?>

</body>

</html>
<!-- end document-->

<script type="text/javascript">
     
function procesarapertura(){
        var idusuario = $("#idusuario").val();
        var nrocaja = $("#nrocaja").val();
        var sucursal = $("#idsucursal").val();
        var opcion = $("#opcion").val();
        $.ajax({
          method: "POST",
          url: "guardarApertura.php",
          data: 'idusuario='+idusuario+'&nrocaja='+nrocaja+'&sucursal='+sucursal+'&opcion='+opcion
        }).done( function( info ){   
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          nrohabilitacion();
        });
    }

    function nrohabilitacion() {
        $.ajax({
              url: 'busca_data_habilitacion.php',
              dataType: 'json',
              type: 'POST',
              success: function(data){
                $("#nrohabilitacion").val(parseInt(data[0].idhabilitacion));
        },
        });
    }

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "AUTORIZADO"){
          swal({
              title: 'Atención!',
              text: 'No esta autorizado!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ABIERTA"){
          swal({
              title: 'Atención!',
              text: 'La caja ya esta abierta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }
    
</script>
