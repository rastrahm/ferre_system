<?php
	include ("conexion.php");

		$sql = "SELECT p.idpedido,p.barrio_fac,p.cliente,tp.desc_tip_ped,c.razonsocial,p.tipo_pedido,p.latitud,p.longitud,p.fecha,p.hora, p.fecha_entrega, p.hora_entrega,p.total, p.estado from pedido p 
	    JOIN clientes c on c.idcliente=p.cliente
	    JOIN tipo_pedido tp on tp.idtipo_ped=p.tipo_pedido 
	    where p.estado in (4,5,6)
	    order by p.idpedido desc";

	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>