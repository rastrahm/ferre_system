<!DOCTYPE html>
<html lang="es">
<?php include ("header.php");?>

<style type="text/css">
  
  .table-wrapper {
  width: 100%;
  height: 600px; /* Altura de ejemplo */
  overflow: auto;
}

.table-wrapper table {
  border-collapse: separate;
  border-spacing: 0;
}

.table-wrapper table thead {
  position: -webkit-sticky; /* Safari... */
  position: sticky;
  top: 0;
  left: 0;
}

.table-wrapper table thead th,
.table-wrapper table tbody td {
  border: 1px solid #000;
  /*background-color: #000aff;*/
}

.derecha   { float: right; }

</style>

<body class="animsition">
  <?php

  include ("conexion.php");
  include ("buscar_articulo.php");

  $usuario = $_SESSION['nombreUsuario'];

  include("navbar.php");

?>
  <!-- Navigation-->
  <div class="content-wrapper" id="cuerpo">
   <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
    <div class="container-fluid" align="center"><br>
        <!-- Example DataTables Card-->
      
        <div class="card-body">

          <div class="card">
              <div class="card-header">
                <strong>Inventario de Articulos</strong>
                <button class="btn btn-success" onclick="nuevo();"><i class="fa fa-plus-circle"></i> Nuevo</button>
                <button class="btn btn-primary" onclick="consultar();"><i class="fa fa-search"></i> Consultar</button>
                <button class="btn btn-danger" onclick="eliminar()"><i class="fa fa-trash"></i> Eliminar</button>
              </div>
            <div class="card-body">
            <div class="row">
               <div class='col-md-1'>
              <label>Comprobante:</label>
              <input type="number" name="idinv" id="idinv" class="form-control" onchange="buscar_inv()" disabled="">
            </div>
             <div class='col-md-2'>
              <label>Sucursal:</label>
                    <select id="sucursal" name="sucursal" class="form-control" onchange="bodega(this.value);">
                      <option>Seleccione la sucursal</option>
                      <?php
                        $res = mysqli_query($conexion, "SELECT * FROM sucursal");

                        foreach ($res as $row) {
                          echo "<option value='".$row["idsucursal"]."'> ".$row["desc_suc"]."</option>";
                        }
                       

                      ?>

                   </select><br>
              </div>

                <div class='col-md-2'>
              <label>Bodega:</label>
                    <select id="bodega" name="bodega" class="form-control">
                      <option>Seleccione la bodega</option>
                      <?php

                        $res = mysqli_query($conexion, "SELECT * FROM bodegas group by descbodega");

                        foreach ($res as $row) {
                          echo "<option value='".$row["idbodega"]."'> ".$row["descbodega"]."</option>";
                        }
                       

                      ?>

                   </select><br>
              </div>

              <div class='col-md-3'>
              <label>Observacion:</label>
                    <input type="text" name="observacion" id="observacion" class="form-control" onkeypress="saltar2(event,'idart')">
              </div>

               <div class='col-md-2'>
                <label>Fecha:</label>
                <input type="date" name="fecha" id="fecha" class="form-control" value="<?php echo date("Y-m-d") ?>" disabled>
               </div>

               <div class='col-md-2'>
                  <label>Diferencia:</label>
                    <div class='input-group'>
                    <input type="text" name="total" id="total" class="form-control" disabled="" style="color:red;text-align: center;font-size: 30px;font-weight: bold;">
                  </div>
               </div>

                </div>
              </div>            
         <div class="card-footer" align="right">
          <button class="btn btn-success" id="guardar" onclick="guardar_inventario()">
               <i class="fa fa-check"></i> Guardar
          </button>                                  
        </div>
        </div>
         

            <div align="center">
              <div class="col-md-12">
                <div class="table-wrapper table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning" id="tabla_articulos" width="100%" cellspacing="0">
                 <thead>
                 <tr align="center">
                 <th class='center'>Codigo</th>
                 <th class='center'>Articulo</th>
                 <th class='center'>Cant Sis</th>
                 <th class='center'>Cant Fis</th>
                 <th class='center'>Diferencia</th>
                 <th class='center'>Costo</th>
                 <th class='center'>Subtotal</th>
                 </tr>
                 </thead>
                 <tbody>
                 </tbody>
                </table>
                </div>
              </div>
            </div><br>

        </div>
    </div>
  </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php  include ("footer.php") ?>
    <!-- Logout Modal-->

   
  </div>
  
   <!-- Bootstrap core JavaScript-->
    
<?php  include ("seguridad.php") ?>
<?php  include ("scripts.php") ?>

</body>
  
</html>
<script type="text/javascript">

  function consultar(){
      $("#idinv").attr("disabled", false);
      $("#idinv").val("");
      $("#idinv").focus();
  }

  function nuevo() {

    $.ajax({
              url: 'busca_ultimo_inv.php',
              dataType: 'json',
              type: 'POST',
              success: function(data){

                if (data=='0') {
                  $("#idinv").val("1");
                }else{
                  $("#idinv").val(parseInt(data[0].id_inv)+1);
                }
                    $("#idinv").attr("disabled", true);
                    $("#fecha").attr("disabled", true);
                    $("#guardar").attr("disabled", false);

                    limpiar_campos();

                    $("#tabla_articulos>tbody").append('<tr style="text-align:center"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>'+
                     '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"'+
                     'disabled></td><td class="cantsis" style="width:150px"><input type="text" class="form-control" name="cantsis" id="cantsis" size="5"'+
                     'disabled></td><td class="cantfis" style="width:150px"><input type="text" class="form-control" name="cantfis" onkeyup="cal_sub()" id="cantfis" onkeypress="saltar(event)" size="5"'+
                     '></td><td style="width:150px"><input type="text" class="form-control" name="diferencia" id="diferencia" size="5"'+
                     '></td><td class="costo" style="width:200px"><input type="text" class="form-control" name="cos" id="cos" size="6"'+
                     '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"'+
                      'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

                         $("#sucursal").focus();

                },
            });
  }

  function buscar_inv() {
    $("#tabla_articulos > tbody:last").children().remove();
     $.ajax({
              url: 'busca_data_inv.php',
              dataType: 'json',
              type: 'POST',
              data: 'idinv='+$("#idinv").val(),
              success: function(data){
                  
                  if(data==0){
                  alertify.error("No existe el numero de comprobante!!!");
                  }else{

                    $("#idinv").val(data[0].id_inv);
                    $("#sucursal").val(data[0].sucursal);
                    $("#bodega").val(data[0].bodega); 
                    $("#observacion").val(data[0].observacion);
                    $("#total").val(parseInt(data[0].total_diferencia).toLocaleString());
                    $("#fecha").val(data[0].fecha);
                    
                      for (var i = 0; i < data.length; i++) {

                        var diferencia = parseInt(data[i].cantfis-data[i].cantsis);

                      $("#tabla_articulos > tbody").append("<tr><td class='datos'>"+data[i].articulo+
                      "</td><td>"+data[i].nombreart+
                      "</td><td>"+data[i].cantsis+
                      "</td><td>"+data[i].cantfis+
                      "</td><td>"+diferencia+
                      "</td><td>"+parseInt(data[i].costo).toLocaleString()+
                      "</td><td>"+parseInt(data[i].subtotal).toLocaleString()+
                      "</td></tr>");
                      }
                    }
                },
            });
      
  }

  function bodega(str){
    var xmlhttp;
     
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("bodega").innerHTML=xmlhttp.responseText;
    }
    }
    xmlhttp.open("POST","filtro_bodega.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("sucursal="+str);
}

     $(document).ready(function() {

         buscar_inv();

    });

     function agregar(){
         
            var idarticulo = $("#idart").val();
            var nombreart = $("#nom").val();
            var cantsis = parseInt($("#cantsis").val());
            var cantfis = parseInt($("#cantfis").val());
            var costo = $("#cos").val().replace(/(?!-)[^\d]/g, '');
            var subtotal = $("#sub").val().replace(/(?!-)[^\d]/g, '');
            var diferencia = cantfis-cantsis;

              $("#tabla_articulos > tbody").prepend("<tr style='font-weight:bold;color:black;font-size:20px;text-align:center'><td class='idarticulo'>"+idarticulo+
                        "</td><td class='nombreart'>"+nombreart+"</td><td class='cantsis'>"+cantsis+
                        "</td><td class='cantfis'>"+cantfis+
                        "</td><td>"+diferencia+
                        "</td><td class='costo'>"+parseInt(costo).toLocaleString()+"</td><td class='subtotal'>"+parseInt(subtotal).toLocaleString()+
                        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
              
            resumen();
            agregar_fila();

    }

     function agregar_fila() {

        $("#tabla_articulos>tbody").prepend('<tr style="text-align:center"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>'+
       '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"'+
       'disabled></td><td class="cantsis" style="width:150px"><input type="text" class="form-control" name="cantsis" id="cantsis" size="5"'+
       'disabled></td><td class="cantfis" style="width:150px"><input type="text" class="form-control" name="cantfis" onkeyup="cal_sub()" id="cantfis" onkeypress="saltar(event)" size="5"'+
       '></td><td style="width:150px"><input type="text" class="form-control" name="diferencia" id="diferencia" size="5"'+
       '></td><td class="costo" style="width:200px"><input type="text" class="form-control" name="cos" id="cos" size="6"'+
       '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"'+
        'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

        $("#idart").focus();

     }

     function resumen(){
            var monto=0;
            $('#tabla_articulos > tbody > tr').each(function(){
            monto+=parseInt($(this).find('td').eq(6).html().replace(/(?!-)[^\d]/g, ''));
            });
            $("#total").val(parseInt(monto+5).toLocaleString());
          }


    function saltar(e){
      (e.keyCode)?k=e.keyCode:k=e.which;
      if(k==13){
          $("#confirmar").focus();
        }
    }

    function saltar2(e,id){
      (e.keyCode)?k=e.keyCode:k=e.which;
      if(k==13){
          document.getElementById(id).focus();
        }
    }

    function cal_sub(){
      var cantfis = $("#cantfis").val();
      var cantsis = $("#cantsis").val();
      var costo = $("#cos").val().replace(/[^\d]/g, '');
      $("#sub").val(parseInt((cantfis-cantsis)*costo).toLocaleString());
      $("#diferencia").val(cantfis-cantsis);
    }

    function busca_articulo(){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_articulo.php',
          dataType: 'json',
          type: 'POST',
          data: 'idarticulo='+$("#idart").val()+'&idsucursal='+$("#sucursal").val()+'&idbodega='+$("#bodega").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el articulo!!!");
            $("#idart").val("");
            $("#idart").focus();
            }else{
            $("#cantfis").focus();
            $("#idart").val(data[0].idarticulo);
            $("#nom").val(data[0].nombreart);
            $("#cantsis").val(data[0].stock);
            $("#cos").val(parseInt(data[0].costo).toLocaleString());
            }
           },
          });
        });
  }



  function busqueda_art(){
   $("#modal_busqueda_arts").modal("show");
   $('#modal_busqueda_arts').on('shown.bs.modal', function () {
   $("#lista_articulos").html("");
   $("#articulo_buscar").val("");
   $("#articulo_buscar").focus();
   busca();
   });
}


function busca(){
    $.ajax({
        beforeSend: function(){
          $("#lista_articulos").html("");
          },
        url: 'busca_articulos_ayuda.php',
        type: 'POST',
        data: 'articulo='+$("#articulo_buscar").val()+'&idsucursal='+$("#sucursal").val()+'&idbodega='+$("#bodega").val(),
        success: function(x){
         $("#lista_articulos").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_articulos").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}


function add_art(art){
  //alert(art);
  $("#modal_busqueda_arts").modal("toggle");
  $("#idart").val(art);
  busca_articulo();
}

  $(function(){
         // Evento que selecciona la fila y la elimina
        $(document).on("click",".delete",function(){
            var parent = $(this).parents().parents().get(0);
          $(parent).remove();
          resumen();
        });
       });

function guardar_inventario(){
             let articulos = [];
            var sucursal = $("#sucursal").val();
            var bodega = $("#bodega").val();
            var observacion=$("#observacion").val();
            var total=$("#total").val().replace(/(?!-)[^\d]/g, '');
            var opcion = "registrar";

                    document.querySelectorAll('#tabla_articulos tbody tr').forEach(function(e){
                      let fila = {
                        idarticulo: e.querySelector('.idarticulo').innerText,
                        nombreart: e.querySelector('.nombreart').innerText,
                        cantsis: e.querySelector('.cantsis').innerText,
                        cantfis: e.querySelector('.cantfis').innerText,
                        costo: e.querySelector('.costo').innerText.replace(/(?!-)[^\d]/g, ''),
                        subtotal: e.querySelector('.subtotal').innerText.replace(/(?!-)[^\d]/g, '')
                      };
                      articulos.push(fila);
                    });

                $.ajax({
                      url: 'guardarInventario.php',
                      dataType: 'json',
                      type: 'POST',
                     data: {'articulos': JSON.stringify(articulos), 'observacion': observacion, 'idsucursal': sucursal, 'bodega': bodega, 'total': total, 'opcion': opcion}
                   }).done( function( info ){   
                      var json_info = JSON.parse( JSON.stringify( info ));
                      mostrar_mensaje( json_info );
                      limpiar_campos();
                      agregar_fila();
                });
    
}

function eliminar(){
    var idinv = $("#idinv").val();
    var opcion = "eliminar";
    $.ajax({
                      url: 'guardarInventario.php',
                      dataType: 'json',
                      type: 'POST',
                     data: {'idinv': idinv, 'opcion': opcion}
                   }).done( function( info ){   
                      var json_info = JSON.parse( JSON.stringify( info ));
                      mostrar_mensaje( json_info );
                      location.reload(true);
                });

}


function limpiar_campos() {
  $("#tabla_articulos > tbody:last").children().remove();
  $("#total").val("");
  $("#observacion").val("");
  $("#sucursal").val("Seleccione la sucursal");
  $("#bodega").val("Seleccione la bodega");
}

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

</script>