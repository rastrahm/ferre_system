<!DOCTYPE html>
<html lang="en">

<?php
include("header.php");

$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$fecha = $dias[date('w')] . " " . date('d') . " de " . $meses[date('n') - 1] . " del " . date('Y');

$nivel = $_SESSION['nivel'];

include("conexion.php");
include("seguridad/formatos.php");

$hoy = date("Y-m-d");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php
        if ($nivel == 1) {
            include("navbar.php");
        } else {
            include("navbar_logistica.php");
        }
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
            <div class="container-fluid"><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 style="color: white" class="title-1"><?php echo $fecha; ?></h2>
                        </div>
                    </div>
                </div>

                <?php if ($nivel == 1) { ?>
                    <div class="row m-t-25">
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c1">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <a href="reporte_pedido.php">
                                                <i class="zmdi zmdi-shopping-cart"></i></a>
                                        </div>
                                        <div class="text">
                                            <h2><? php/* echo $pedidos;*/ ?></h2>
                                            <span>Pedidos</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c2">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <a href="pedidospendientes.php">
                                                <i class="zmdi zmdi-alert-circle-o"></i></a>
                                        </div>
                                        <div class="text">
                                            <h2><?php /*echo $pedidosp;*/ ?></h2>
                                            <span>Pedidos Pendientes</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c3">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <a href="reporte_compra.php">
                                                <i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                        </div>
                                        <div class="text">
                                            <h2><?php /*echo $compras;*/ ?> Gs.</h2>
                                            <span>Compras</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c4">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <a href="reporte_venta.php">
                                                <i class="zmdi zmdi-money"></i></a>
                                        </div>
                                        <div class="text">
                                            <h2><?php /*echo $total2; */ ?> Gs.</h2>
                                            <span>Ventas</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php   
                 } else {
                }  ?>

            </div>
            <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
            <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>
    </div>

    <?php include("seguridad.php") ?>
    <?php include("scripts.php");  ?>


</body>

</html>
<!-- end document-->

<script type="text/javascript">

</script>