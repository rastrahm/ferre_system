<?php

include ("conexion.php");
include ("./seguridad/formatos.php");

    //main query to fetch the data
    $sql="SELECT p.idpedido,p.barrio_fac,p.cliente,tp.desc_tip_ped,c.razonsocial,p.tipo_pedido,p.latitud,p.longitud,p.fecha,p.hora, p.fecha_entrega, p.hora_entrega,p.total, p.estado from pedido p 
    JOIN clientes c on c.idcliente=p.cliente
    JOIN tipo_pedido tp on tp.idtipo_ped=p.tipo_pedido 
    order by p.idpedido desc";
    $query = mysqli_query($conexion, $sql);
    $numrows = mysqli_num_rows($query);

if($numrows>0){
    echo '<div class="table-responsive table--no-card m-b-30">';
    echo '<table class="table table-borderless table-striped table-earning" id="tablaclientes" width="100%" cellspacing="0">';
    echo "<thead>";
    echo "<tr>";
    echo "<th></th>";
    echo "<th>N° Pedido</th>";
    echo "<th>Cliente</th>";
    echo "<th>Barrio</th>";
    echo "<th>Tipo Pedido</th>";
    echo "<th>Fecha Pedido</th>";
    echo "<th>Hora</th>";
    echo "<th>Fecha Entrega</th>";
    echo "<th>Hora Entrega</th>";
    echo "<th>Total</th>";
    echo "<th>Estado</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($query)){

    $data = $row['idpedido']."|".$row['latitud']."|".$row['longitud'];

    echo "<tr>";
    if ($row['tipo_pedido']==1) {
      echo "<td><button class='btn btn-success' title='ver detalles' id='".$row['idpedido']."' onclick='verdetalles(this.id)'><i class='fa fa-eye'></i></button> <button class='btn btn-primary' title='ubicacion' id='".$data."' onclick='verubicacion(this.id)'><i class='fa fa-map-marker'></i></button> <button class='btn btn-warning' title='Ver ruta' id='".$row['idpedido']."' onclick='verruta(this.id)'><i class='fa fa-road'></i></button></td>";
    }else{
      echo "<td><button class='btn btn-success' title='ver detalles' id='".$data."' onclick='verdetalles(this.id)'><i class='fa fa-eye'></i></button></td>";
    
    }
    echo "<td><h4>".$row['idpedido']."</h4></td>";
    echo "<td><h4>".$row['razonsocial']."</h4></td>";
    echo "<td><h4>".$row['barrio_fac']."</h4></td>";
    echo "<td><h4>".$row['desc_tip_ped']."</h4></td>";
    echo "<td><h4>".formatearFecha($row['fecha'])."</h4></td>";
    echo "<td><h4>".$row['hora']."</h4></td>";

    if ($row['fecha_entrega']=='') {
      echo "<td></td>";
    }else{
      echo "<td><h4>".formatearFecha($row['fecha_entrega'])."</h4></td>";
    }
    
    echo "<td><h4>".$row['hora_entrega']."</h4></td>";
    echo "<td><h4>".formatearNumero($row['total'])."</h4></td>";

    if ($row['estado']==0) {
                          echo "<td style='color:blue;font-weight: bold;'><i class='fa fa-clock-o'></i> Pendiente</td>";
                        }else if ($row['estado']==1) {
                          echo "<td style='color:red;font-weight: bold;'><i class='fa fa-ban'></i> Anulado</td>";
                        }else if ($row['estado']==2) {
                          echo "<td style='color:yellow;font-weight: bold;'>En proceso de verificacion</td>";
                        }else if ($row['estado']==3) {
                          echo "<td style='color:yellow;font-weight: bold;'><i class='fa fa-print'></i> En proceso de facturacion</td>";
                        }else if ($row['estado']==4) {
                          echo "<td style='color:green;font-weight: bold;'><i class='fa fa-check-circle'></i> Facturado</td>";
                        }else if ($row['estado']==5) {
                          echo "<td style='color:blue;font-weight: bold;'><i class='fa fa-truck'></i> En proceso de entrega</td>";
                        }else if ($row['estado']==6) {
                          echo "<td style='color:green;font-weight: bold;'><i class='fa fa-check-circle'></i> Entregado</td>";
                        }
      echo "</tr>";
    

    
  }
  echo "</tbody>";
  echo "</table>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}
?>