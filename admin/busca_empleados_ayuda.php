<?php

include ("conexion.php");

$idempleado = $_POST['idempleado'];
$sql = "SELECT e.idempleado,e.documento,e.nombre,c.desc_cargo,e.estado from empleados e 
        join cargos c on c.idcargo=e.cargo where e.nombre like '%$idempleado%' and e.cargo in (3,5,6)";
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Documento</th>";
    echo "<th>Nombre</th>";
    echo "<th>Cargo</th>";
    echo "<th>Estado</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($res)){

    echo "<tr>";
    echo "<td>".$row['documento']."</td>";
    echo "<td>".$row['nombre']."</td>";
    echo "<td>".$row['desc_cargo']."</td>";
    if ($row['estado']==0) {
      echo "<td>Disponible</td>";
    }else{
      echo "<td>En proceso de entrega</td>";
    }
    
    echo "<td><button type='button' id='".$row['documento']."' class='btn btn-primary btn-xs' onclick='add_emp(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}
?>