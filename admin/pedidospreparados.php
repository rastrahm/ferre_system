<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;


?>
<link href="css/loader.css" rel="stylesheet">
<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <div class="container-fluid"><br>
              <div align="center">
                <div class="card-body">
                   <div align="left">
                  <?php //include("mostrar_pedidos.php");?>
                  <div id="mostrar_pedidos" class="row">
                    
                  </div>

                  </div><br>
                </div>
               </div>
            </div>
                                   <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   

</body>

</html>
<!-- end document-->

<script type="text/javascript">

    $(document).on("ready", function(){
      buscar_pedidos();
    });

   //mostrar reservas de parque

    function buscar_pedidos(){

              $.ajax({
              beforeSend: function(){
                $("#mostrar_pedidos").html("<div align='center'> <div class='bt-spinner'></div></div");
               },
              url: 'mostrar_pedidos_preparados.php',
              type: 'POST',
              success: function(data){
      
                $("#mostrar_pedidos").html(data).fadeIn('slow');
               },
               error: function(jqXHR,estado,error){
                 $("#mostrar_pedidos").html(estado+"    "+error);
               }
               });
    }

    function hablar(){

        var Jarvis = new Artyom();
 
        Jarvis.say("¡Hay nuevos pedidos!");

  }

    //setInterval('buscar_pedidos()',5000);

     function entregar(id){
        var de=id.split("|");
        var idpedido=de[0];
        var color=de[1];
        var opcion = 'entregar';
        var cargo = 2;
          $.ajax({
              url: 'accionespedidos.php',
              type: 'POST',
              data: 'idpedido='+idpedido+'&color='+color+'&opcion='+opcion+'&cargo='+cargo
              }).done( function( info ){   
                var json_info = JSON.parse( info );
                mostrar_mensaje( json_info );
                $('#mostrar_pedidos').load('mostrar_pedidos_preparados.php');
              });
    }

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADO"){
          swal({
              title: 'Atención!',
              text: 'El pedido ha sido anulado!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ABIERTA"){
          swal({
              title: 'Atención!',
              text: 'La caja ya esta abierta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }


</script>
