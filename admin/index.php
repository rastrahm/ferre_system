<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Acceso a Ferresystem</title>
  <!-- Bootstrap core CSS-->
 <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
<link href="css/sb-admin.css" rel="stylesheet">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
<script src="alertifyjs/alertify.js"></script>

</head>

<body>
  <?php
    if (isset($_SESSION["exito"])){
     if ($_SESSION['exito']==3){
                echo '
                    <script>
                        alertify.error("Usuario y/o contraseña incorrectos!");
                    </script>
                ';
            }
            unset($_SESSION["exito"]);
    }
  ?>
  <div class="container-login100" style="background-image: url('img/fondo_ferreteria.jpg');">
    <div class="card card-login mx-auto mt-5" align="center">
      <span class="login100-form-title p-t-20 p-b-45" style="color:black">
            Iniciar Sesión
          </span>
      <div align="center">
         <img id="profile-img" height="200px" width="200px" class="profile-img-card" src="img/logo_ferresystem.jpg"/>
      </div>
     
      <div class="card-body">
        <form class="login100-form validate-form" action="./seguridad/control.php" method="post">
         <div class="wrap-input100 validate-input m-b-10" data-validate = "Nombre de usuario requerido">
            <input class="input100" type="text" name="usuario" id="usuario" placeholder="Usuario">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input m-b-10" data-validate = "Contraseña requerida">
            <input class="input100" type="password" id="contrasena" name="contrasena" placeholder="Contraseña">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock"></i>
            </span>
          </div>
                    <div class="container-login100-form-btn p-t-10">
            <button class="login100-form-btn">
              Ingresar
            </button>
          </div><br><br>
        </form><br>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/bloquear.js"></script>
</body>

</html>
<script type="text/javascript">

  function soloLetras(e){
                       key = e.keyCode || e.which;
                       tecla = String.fromCharCode(key).toLowerCase();
                       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                       especiales = "8-37-39-46";

                       tecla_especial = false
                       for(var i in especiales){
                            if(key == especiales[i]){
                                tecla_especial = true;
                                break;
                            }
                        }

                        if(letras.indexOf(tecla)==-1 && !tecla_especial){
                            return false;
                        }
                    }

</script>
