<?php
	include ("conexion.php");

	$id_proveedor = $_POST["idproveedor"];
	$opcion = $_POST["opcion"];
	$informacion = [];

	switch ($opcion) {
		case 'eliminar':
			eliminar($id_proveedor, $conexion);
			break;
	}

	function eliminar($id_proveedor, $conexion){
		$query = "delete from proveedores
					where idproveedor='$id_proveedor'";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado($resultado);
		cerrar($conexion);
	}

	function verificar_resultado($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "ELIMINADO";
			
			echo json_encode($informacion);
		}

	function cerrar($conexion){
		mysqli_close($conexion);
	}

?>