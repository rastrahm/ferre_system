<?php
	include ("conexion.php");

	$sql = "SELECT p.idlista,pl.descripcion,p.articulo,a.nombreart,p.precio,p.fecha_alta from precios p
			join precio_lista pl on pl.idlista=p.idlista
			join articulos a on a.idarticulo=p.articulo";
	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>