<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<head>
	
	<link rel="stylesheet" href="css/leaflet.css" />
	<link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
 	<script src="js/leaflet.js"></script>
 	<script src="js/jquery.min.js"></script>
 	<script src='js/leaflet-src.js'></script>
 	<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="css/leaflet-routing-machine.css" />
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <link rel="stylesheet" href="css/Control.FullScreen.css" />
	<script src="js/Control.FullScreen.js"></script>
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
	<script src="js/sweetalert.min.js"></script>

 <style>
  #map { 
  widh: 50px;
  height: 600px; }
 </style>
 
 </head>
  <body>

   <span>IDRUTA</span>
   <input type="text" name="idruta" id="idruta">
   <input type="text" name="latlong_ini" id="latlong_ini">
   <input type="text" name="distancia" id="distancia" value="625.1 m" onkeyup="calcular_velocidad()">
   <input type="text" name="tiempo" id="tiempo" value="1 m 30 s" onkeyup="calcular_velocidad()">
   <input type="text" name="velocidad" id="velocidad" value="" onkeyup="calcular_velocidad()">
   <input type="text" name="tiempoaux" id="tiempoaux" value="">

   <div id="map"></div>
 
 </body> 
 </html>

<script type="text/javascript">

		function guardarViaje() {

			   var idruta = $("#idruta").val();
			   var distancia = $("#distancia").val();
			   var tiempo = $("#tiempo").val();
         var velocidad = $("#velocidad").val();
         var latlong_ini = $("#latlong_ini").val();

			   $.ajax({
                      url: 'guardarViaje.php',
                      dataType: 'json',
                      type: 'GET',
                     data: {'idruta': idruta, 'distancia': distancia, 'tiempo': tiempo, 'velocidad': velocidad, 'latlong_ini': latlong_ini}
                  }).done( function( info ){ 
                    var json_info = JSON.parse( JSON.stringify(info) );
          			    mostrar_mensaje( json_info );
                  });
		}


 var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "MODIFICADO" ){
         
      }
    }

		function guardar_ruta(coordenadas,latlong) {

			var idruta = $("#idruta").val();
      //var latlong_ini = $("#latlong_i").val();

			   $.ajax({
                      url: 'guardarRuta.php',
                      dataType: 'json',
                      type: 'GET',
                     data: {'coordenadas': coordenadas, 'idruta': idruta, 'latlong_fin': latlong}
                  }).done( function( info ){ 

                      buscar_ruta();
                      
                  });
		}

function buscar_ruta() {

          $.ajax({
          url: 'busca_data_ruta.php',
          dataType: 'json',
          type: 'POST',
          success: function(data){

            $("#idruta").val(data[0].idruta);

           },
          });
}

window.onload = function() {

  calcular_velocidad();

};     

function calcular_velocidad() {
     var condicion = /(\d+)/g;

      var tiem=$("#tiempo").val().match(condicion);
      $("#tiempoaux").val(tiem);
      var tiempo=$("#tiempoaux").val().replace(',', '.');
      var t = $("#tiempo").val().indexOf("m") > -1;

      if (t.toString()==="true") {
        console.log("encontrado");
      }
      
      console.log(tiempo);
      var distancia=$("#distancia").val().match(condicion);
      console.log(parseInt(distancia)*tiempo);
      var calculo = (((parseInt(distancia)*1000)/(tiempo*60))*3600)/1000;
      $("#velocidad").val(parseInt(calculo.toLocaleString())+" km/h");

     }   

		/*var map = L.map('map').
		setView([<?php echo $latitud ?>,<?php echo $longitud ?>], 
		20);*/
		

		
		

</script>
