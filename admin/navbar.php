
<?php
$nivel = $_SESSION['nivel'];
?>

<header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35" id="mainNav">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="menu.php">
                            <h3 class="text-white">FS</h3>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
                           
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-shopping-cart"></i>
                                    <span class="bot-line"></span>Pedidos</a>
                                <ul class="header3-sub-list list-unstyled">
                                     <?php

                            if ($nivel == 1) {
                                
                            ?>
                                   <!-- <li>
                                        <a href="pedidos.php">Pedidos</a>
                                    </li>-->
                                    <li>
                                        <a href="pedidospendientes.php">Pedidos Pendientes</a>
                                    </li>
                                    <li>
                                        <a href="pedidosconfirmados.php">Pedidos Confirmados</a>
                                    </li>
                                    <li>
                                        <a href="orden_de_entrega.php">Orden de entrega</a>
                                    </li>
                            <?php

                            }else if ($nivel == 3){

                            ?>

                            <li>
                                <a href="pedidospendientes.php">Pedidos Pendientes</a>
                            </li>
                            <?php

                            }
                                
                            ?>
                                </ul>
                            </li>
                            
                             <!--<li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-road"></i>
                                    <span class="bot-line"></span>Viajes</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="viajes.php">Nuevo Viaje</a>
                                    
                                    <li>
                                        <a href="viajes_en_curso.php">Viajes en curso</a>
                                    </li>
                                    
                                </ul>
                            </li></li>-->

                            <?php

                            if ($nivel == 1) {
                                
                            ?>

                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-dollar"></i>
                                    <span class="bot-line"></span>Ventas</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="ventas.php">Nueva Venta</a>
                                    </li>
                                    <li>
                                        <a href="credito_venta.php">Cuentas a Cobrar</a>
                                    </li>
                                    <li>
                                        <a href="clientes.php">Clientes</a>
                                    </li>
                                </ul>
                            </li>
                             <li class="has-sub">
                                <a href="#">
                                    <i class="zmdi zmdi-shopping-cart-plus"></i>
                                    <span class="bot-line"></span>Compras</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="pedido_compra.php">Nuevo Pedido</a>
                                    </li>
                                    <li>
                                        <a href="presupuesto_compra.php">Nuevo Presupuesto</a>
                                    </li>
                                    <li>
                                        <a href="orden_compra.php">Orden de Compra</a>
                                    </li>
                                    <li>
                                        <a href="compras.php">Nueva Compra</a>
                                    </li>
                                   <!--  <li>
                                        <a href="orden_compra.php">Orden de Compra</a>
                                    </li>-->
                                    <li>
                                        <a href="credito_compra.php">Cuentas a Pagar</a>
                                    </li>
                                      <li>
                                        <a href="proveedores.php">Proveedores</a>
                                    </li>
                                </ul>
                            </li>
                           <li class="has-sub">
                                <a href="#">
                                    <i class="zmdi zmdi-widgets"></i>
                                    <span class="bot-line"></span>Productos</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="productos.php">Lista de productos</a>
                                    </li>
                                    <li>
                                        <a href="precios_lista.php">Lista de precios</a>
                                    </li>
                                      <li>
                                        <a href="precios.php">Precios definitivos</a>
                                    </li>
                                    <li>
                                        <a href="categoria.php">Categoria</a>
                                    </li>
                                    <li>
                                        <a href="inventario.php">Inventario</a>
                                    </li>
                                     <li>
                                        <a href="transferencias.php">Transferencia</a>
                                    </li>
                                    <li>
                                        <a href="movimientos.php">Movimientos</a>
                                    </li>
                                     <li>
                                        <a href="mot_ent_sal.php">Motivos de entrada y salida</a>
                                    </li>
                                </ul>
                            </li>
                           <li class="has-sub">
                                <a href="#">
                                    <i class="zmdi zmdi-settings"></i>
                                    <span class="bot-line"></span>Mantenimiento</a>
                                <ul class="header3-sub-list list-unstyled">
                                     <li>
                                        <a href="cajas.php">Cajas</a>
                                    </li>
                                    <li>
                                        <a href="empleados.php">Empleados</a>
                                    </li>
                                     <li>
                                        <a href="vehiculos.php">Vehiculos</a>
                                    </li>
                                    <li>
                                        <a href="usuarios.php">Usuarios</a>
                                    </li>
                                    <!--<li>
                                        <a href="vehiculo.php">Vehiculo</a>
                                    </li>-->
                                </ul>
                            </li>
                            <?php
                                }
                            ?>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-key"></i>
                                    <span class="bot-line"></span>Caja</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="aperturacaja.php">Apertura</a>
                                    </li>
                                     <li>
                                        <a href="cierrecaja.php">Cierre</a>
                                    </li>
                                    <!--<li>
                                    <a href="salidascaja.php">Salidas</a>
                                    </li>-->
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-bar-chart-o"></i>
                                    <span class="bot-line"></span>Reportes</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="reporte_compra.php">Compras</a>
                                    </li>
                                    <li>
                                        <a href="reporte_stock.php">Stock</a>
                                    </li>
                                    <li>
                                        <a href="reporte_venta.php">Ventas</a>
                                    </li>
                                     <li>
                                    <a href="reporte_pedido.php">Pedidos</a>
                                    </li>
                                    <li>
                                        <a href="reportecaja.php">Caja</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="content">
                                    <a class="js-acc-btn" href="#"><i class="fa fa-user"></i>  <?php
                                        if (isset($_SESSION['nombreUsuario'])){
                                            echo $_SESSION['nombreUsuario'];
                                        }
                                      ?> </a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a onclick="cerrarsesion();">
                                            <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

<header class="header-mobile header-mobile-2 d-block d-lg-none" id="mainNav">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="menu.php">
                            <h3 class="text-white">FS</h3>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-shopping-cart"></i>Pedidos</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="pedidos.php">Ver Pedidos</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-dollar"></i>Ventas</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="ventas.php">Nueva Venta</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="zmdi zmdi-shopping-cart-plus"></i>Compras</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="compras.php">Nueva Compra</a>
                                </li>
                                <li>
                                    <a href="detallecompras.php">Ver Compras</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="ingredientes.php">
                                <i class="fas fa-cubes"></i>Ingredientes</a>
                        </li>
                        <li>
                            <a href="proveedores.php">
                                <i class="fas fa-users"></i>Proveedores</a>
                        </li>
                        <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-key"></i>Caja</a>
                                <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                    <li>
                                        <a href="aperturacaja.php">Apertura</a>
                                    </li>
                                     <li>
                                        <a href="cierrecaja.php">Cierre</a>
                                    </li>
                                     <li>
                                    <a href="salidascaja.php">Salidas</a>
                                </li>
                                </ul>
                            </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-bar-chart-o"></i>Reportes</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="reporte_compra.php">Compras</a>
                                </li>
                                <li>
                                    <a href="reporte_venta.php">Ventas</a>
                                </li>
                                <li>
                                    <a href="reporte_pedido.php">Pedidos</a>
                                </li>
                                   <li>
                                        <a href="reportecaja.php">Caja</a>
                                    </li>
                                    <li>
                                        <a href="reportesalidascaja.php">Salidas</a>
                                    </li>
                            </ul>
                        </li>
                         <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i> <?php
                                if (isset($_SESSION['nombreUsuario'])){
                                    echo $_SESSION['nombreUsuario'];
                                }
                              ?> </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a onclick="cerrarsesion();" href="#">
                                            <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>


<div class="modal fade" id="cerrarsesion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Estas seguro?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Cerrar sesión" para finalizar su sesión actual.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="./seguridad/salir.php">Cerrar Sesión</a>
          </div>
        </div>
      </div>
    </div>