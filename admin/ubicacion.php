<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<head>
	
	<link rel="stylesheet" href="css/leaflet.css" />
 	<script src="js/leaflet.js"></script>
 	
 	<script src='js/leaflet-src.js'></script>
 	<link rel="stylesheet" href="css/leaflet-routing-machine.css" />
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <link rel="stylesheet" href="css/Control.FullScreen.css" />
	<script src="js/Control.FullScreen.js"></script>
		
 <style>
  #map { 
  widh: 50px;
  height: 600px; }
 </style>
 
 </head>
  <body>

   <label> Latitud</label>
   <input type="text" class="form-control" name="latitud" id="latitud">
   <label> Longitud</label>
   <input type="text" class="form-control" name="longitud" id="longitud">
   <br>

   <div id="map"></div>
 
 </body> 
 </html>

<script type="text/javascript">

		navigator.geolocation.getCurrentPosition(function(position){

			//para obtener ubicacion actual
		    var latitude = position.coords.latitude;
		    var longitude = position.coords.longitude;

		    	var map = L.map('map', {
				    center: [latitude, longitude],
				    zoom: 12,
				    fullscreenControl: true,
					 fullscreenControlOptions: {
					 position: 'topleft'
					 }
				});

				map.locate({setView: true});

				 $("#latitud").val(latitude);
				 $("#longitud").val(longitude);

				
		    	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			    attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>'
				}).addTo(map);

				L.control.scale().addTo(map);
				var ubicacion = L.marker([latitude,longitude], {draggable: true}).addTo(map);
				 	ubicacion.bindPopup('Por favor seleccione su ubicacion').openPopup();

				//mostrar coordenadas al hacer click en el mapa

				var popup = L.popup();

				function onMapClick(e) {
					ubicacion.remove();

				    popup
				        .setLatLng(e.latlng) // Sets the geographical point where the popup will open.
				        .setContent("Solo se tendra en cuenta el ultimo marcador colocado, Favor sea lo mas preciso posible") // Sets the HTML content of the popup.
				        .openOn(map); // Adds the popup to the map and closes the previous one. 
				        var marker = L.marker([e.latlng.lat.toString(),e.latlng.lng.toString()], {draggable: true}).addTo(map);
				        
				        $("#latitud").val(e.latlng.lat.toString());
				        $("#longitud").val(e.latlng.lng.toString());
				}

				map.on('click', onMapClick);

		});

		/*var map = L.map('map').
		setView([<?php echo $latitud ?>,<?php echo $longitud ?>], 
		20);*/
		

		
		

</script>