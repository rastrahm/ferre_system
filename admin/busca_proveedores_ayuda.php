<?php

include ("conexion.php");

     $codigo = $_POST['proveedor'];
     $aColumns = array('ruc', 'proveedor');//Columnas de busqueda
     $sTable = "proveedores";
     $sWhere = "";
    
      $sWhere = "WHERE (";
      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {
        $sWhere .= $aColumns[$i]." LIKE '%".$codigo."%' OR ";
      }
      $sWhere = substr_replace( $sWhere, "", -3 );
      $sWhere .= ')';
    
    $count_query   = mysqli_query($conexion, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
    $row= mysqli_fetch_array($count_query);
    $numrows = $row['numrows'];
    //main query to fetch the data
    $sql="SELECT * FROM  $sTable $sWhere";
    $query = mysqli_query($conexion, $sql);

if($numrows>0){
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Cedula</th>";
    echo "<th>Nombre</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($query)){

    echo "<tr>";
    echo "<td>".$row['ruc']."</td>";
    echo "<td>".$row['proveedor']."</td>";
    echo "<td><button type='button' id='".$row['ruc']."' class='btn btn-primary btn-xs' onclick='add_pro(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}
?>