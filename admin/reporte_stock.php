<!DOCTYPE html>
<html lang="es">

<?php

date_default_timezone_set('America/Asuncion');
include("header.php");
include("conexion.php");
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$fecha = $dias[date('w')] . " " . date('d') . " de " . $meses[date('n') - 1] . " del " . date('Y');
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php
        include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <div class="container-fluid"><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">Reporte de Stock</h2>
                        </div>
                    </div>
                </div><br>
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2"><br>
                                <div class='box box-primary'>
                                    <div class='box-body'>
                                        <div class="form-group">
                                            <label for="clasificacion">Bodega</label>
                                            <select class="bodega form-control" name="bodega" id="bodega" class="form-control">
                                                <option>Seleccione una bodega</option>
                                                <?php
                                                $sql = "SELECT * FROM `bodegas` b GROUP by b.descbodega;                                                ";
                                                $res = mysqli_query($conexion, $sql);
                                                while ($row = mysqli_fetch_array($res)) {
                                                    echo "<option value='" . $row["idbodega"] . "'>" . $row["descbodega"] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="clasificacion">Sucursal</label>
                                            <select class="sucursal form-control" name="sucursal" id="sucursal" class="form-control">
                                                <option>Seleccione una sucursal</option>
                                                <?php
                                                $sql = "SELECT * FROM `sucursal`;";
                                                $res = mysqli_query($conexion, $sql);
                                                while ($row = mysqli_fetch_array($res)) {
                                                    echo "<option value='" . $row["idsucursal"] . "'>" . $row["desc_suc"] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="clasificacion">Articulo</label>
                                            <select class="articulo form-control" name="articulo" id="articulo" class="form-control">
                                                <option>Seleccione un articulo</option>
                                                <?php
                                                $sql = "SELECT * FROM `articulos`;";
                                                $res = mysqli_query($conexion, $sql);
                                                while ($row = mysqli_fetch_array($res)) {
                                                    echo "<option value='" . $row["idarticulo"] . "'>" . $row["nombreart"] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!--   <div class="form-group">
                                            <label for="clasificacion">Tipo de reporte</label>
                                            <select id="tiporeporte" name="tiporeporte" class="form-control">
                                                <option value="Completo">Completo</option>
                                                <option value="Detalle">Detalle</option>
                                            </select>
                                        </div>-->
                                    </div>
                                    <div class='box-footer'>
                                        <button class='btn btn-primary pull-right' onclick='trae_stock();' id='btn-sig'><i class='fa fa-search'></i> Generar Reporte</button>
                                    </div>
                                </div>
                            </div><br>
                            <div class="col-md-8"><br>
                                <div id='data'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br><br>
            <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
            <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
    <?php include("seguridad.php");  ?>
    <?php include("scripts.php");  ?>


</body>

</html>
<!-- end document-->

<script type="text/javascript">
    $(function() {
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Este dia': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Los ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                    'Los ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                    'Este mes': [moment().startOf('month'), moment().endOf('month')],
                    'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function(start, end) {
                $('.fe').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                var xstart = start.format('YYYY-MM-DD');
                var xend = end.format('YYYY-MM-DD');
                $("#fi").val(xstart);
                $("#ff").val(xend);
                //alert(start.format('YYYY-MM-DD')+'    '+end.format('YYYY-MM-DD'));
            }
        );


        listado();
    });
    $(document).ready(function() {
        $('.articulo').select2();
        $('.bodega').select2();
        $('.sucursal').select2();
    });

    function trae_stock() {
       
        $(document).ready(function() {
            var bodega = $("#bodega").val();
        var sucursal = $("#sucursal").val();
        var articulo = $("#articulo").val();
            $.ajax({
                beforeSend: function() {
                    $("#data").html("Buscando ventas, un momento...");
                },
                url: 'busca_stock_reporte.php',
                type: 'POST',
                data: {
                    bodega: bodega,
                    sucursal: sucursal,
                    articulo: sucursal,
                },
                success: function(datares) {
                    $("#data").html(datares);
                },
                error: function(jqXHR, estado, error) {
                    $("#data").html(error + "    " + estado);
                }
            });
        })
    }

    function listado() {
        $(document).ready(function() {
            $.ajax({
                beforeSend: function() {
                    $("#data").html("Buscando ventas, un momento...");
                },
                url: 'busca_stock_reporte.php',
                type: 'POST',
                //data: data_enviar,
                success: function(datares) {
                    $("#data").html(datares);
                },
                error: function(jqXHR, estado, error) {
                    $("#data").html(error + "    " + estado);
                }
            });

        })
    }

    function print1() {
        $(".print1").printArea();
    }
</script>