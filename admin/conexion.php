<?php
	$server = "localhost";
	$user = "root";
	$password = "";
	$bd = "ferresystem";

    date_default_timezone_set('America/Asuncion');
    
	$conexion = mysqli_connect ($server, $user, $password, $bd);
	mysqli_set_charset($conexion, "utf8");

	if(!$conexion){
		die('Error de Conexión:' . mysqli_connect_errno());
	}

?>