<?php

include ("conexion.php");
include ("./seguridad/formatos.php");

$fi=$_POST['fechai'];
$ff=$_POST['fechaf'];
$estado=$_POST['estado'];
$tabonos = 0;

$sql="SELECT * from habilitaciones WHERE fechaapertura BETWEEN '$fi' AND  '$ff' and estado like '%$estado%' ORDER BY fechaapertura DESC";

$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

     /*ventas de credito*/
     if($resul>0){
        echo "<div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
        echo "<div class='box box-danger print1'>";
        echo "<div class='box-header with-border'><br>";
        echo "<h4 class='box-title'>Movimientos de Caja | ".formatearFecha($fi)." al ".formatearFecha($ff)." <div id='total_credito'></div><br></h4>";
        echo "</div>";
        echo "<div class='box-body'>";
        echo "<div class='box-body table-responsive'>";
        echo "<table id='tabla_ventas_credito' class='table table-bordered table-hover'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Fecha Apertura</th>";
        echo "<th>Hora Apertura</th>";
        echo "<th>Contado</th>";
        echo "<th>Credito</th>";
        echo "<th>Valor Cierre</th>";
        echo "<th>Diferencia</th>";
        echo "<th>Fecha Cierre</th>";
        echo "<th>Hora Cierre</th>";
        echo "<th>Estado</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
           while($row = mysqli_fetch_array($res)){
            echo "<tr>";
            $tabonos+=$row['valorcierre'];
            echo "<td>".formatearFecha($row['fechaapertura'])."</td>";
            echo "<td>".$row['horaapertura']."</td>";
            echo "<td>".formatearNumero($row['contado'])."</td>";
            echo "<td>".formatearNumero($row['credito'])."</td>";
            echo "<td>".formatearNumero($row['valorcierre'])."</td>";
            echo "<td>".formatearNumero($row['diferencia'])."</td>";
            if ($row['fechacierre']=='') {
              echo "<td></td>";
            }else{
            echo "<td>".formatearFecha($row['fechacierre'])."</td>";
            }
            echo "<td>".$row['horacierre']."</td>";

            if ($row['estado']==0) {
               echo "<td>Abierta</td>";
            }else{
                echo "<td>Cerrada</td>";
            }
          
          }
          echo "<tr>";
          echo "<td><b>TOTAL: Gs.<b></td>";
          echo "<td><b>".formatearNumero($tabonos)." Gs.</b></td>";
          echo "</tr>";
          echo "</tbody>";
          echo "</table>";
        echo "</div>";
      echo "</div>";
      echo "</div>";
      echo "</div><br><br>";
     }else{
      echo '<script>alertify.error("Ninguna caja registrada!!!");</script>';
     }

  
?>