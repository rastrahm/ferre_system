<?php
	include ("conexion.php");

	$sql = "SELECT c.idcliente,c.documento,c.razonsocial,c.telefono,c.correo,c.barrio,c.direccion,c.latitud,c.longitud,cc.idciudad,cc.ciudad from clientes c
			join ciudades cc on cc.idciudad=c.ciudad";
	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>