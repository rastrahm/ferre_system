<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<?php 
  include("header2.php");
  include ("conexion.php");

  $usuario = $_SESSION['nombreUsuario'];
  $idusuario = $_SESSION['idusuario'];
  $idsucursal = $_SESSION['idsucursal'];

  if (isset($_GET["idpedido"])=='') {
              $idpedido = "";
            }else{
              $idpedido = $_GET["idpedido"];
            }

  $sql = "SELECT concat(latitud,',',longitud) as coordenadas from pedido where idpedido=$idpedido";

  $resultado = mysqli_query($conexion,$sql);
  foreach ($resultado as $row) {
      $latlong_fin = $row["coordenadas"];
  }


?>

 <style>
  #map { 
  widh: 50px;
  height: 600px; }
 </style>
 
<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>

               <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Viajes</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                      <div class="row">
                                         <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">ID RUTA</label>
                                                
                                                    <input type="text" id="idruta" name="idruta" class="form-control" required="" disabled="">
                                            </div>
                                           <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">NRO PEDIDO</label>
                                                
                                                    <input type="text" class="form-control" name="idpedido" id="idpedido" style="text-align:center;" value="<?php echo $idpedido; ?>" disabled>
                                            </div>
                                                <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Fecha Salida</label>
                                                
                                                    <input type="date" id="fecha_salida" name="fecha_salida" class="form-control" required="" value="<?php echo date("Y-m-d") ?>" disabled="">
                                            </div>
                                           <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Hora Salida</label>
                                                
                                                    <input type="time" id="hora_salida" value="<?php echo strftime("%H:%M:%S"); ?>" name="hora_salida" class="form-control" disabled="">
                                            </div>
                                                
                                              <input type="hidden" id="latlong_ini" name="latlong_ini" class="form-control" required="" disabled="">
                                             <input type="hidden" id="latlong_fin" name="latlong_fin" value="<?php echo $latlong_fin ?>" class="form-control" required="" disabled="">
                          

                                         </div>
                                      </div>
                                      <div class="card-footer" align="right">
                                                <button type="button" onclick="guardarViaje()" class="btn btn-success"><i class="fa fa-check"></i> Guardar Viaje</button>
                                      </div>
                                    </div>
                                    <div id="map"></div>
                                </div>
                </div>

             
          </div>
      </div>

      <?php include("footer.php"); ?>
    </div>


    <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>

    <script src="js/leaflet.js"></script>
    <script src='js/leaflet-src.js'></script>
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <script src="js/Control.FullScreen.js"></script>

 </body> 

 </html>

<script type="text/javascript">


		function guardarViaje() {

			   var idruta = $("#idruta").val();
         var idpedido = $("#idpedido").val();
			   var distancia = $("#distancia").val();
			   var tiempo = $("#tiempo").val();
         var latlong_ini = $("#latlong_ini").val();
         var fecha_salida = $("#fecha_salida").val();
         var hora_salida = $("#hora_salida").val();

			             $.ajax({
                      url: 'guardarRutaDet.php',
                      dataType: 'json',
                      type: 'GET',
                     data: {'idruta': idruta, 'idpedido': idpedido, 'distancia': distancia, 'tiempo': tiempo, 'latlong_ini': latlong_ini, 'fecha_salida': fecha_salida, 'hora_salida': hora_salida}
                  }).done( function( info ){ 
                    var json_info = JSON.parse( JSON.stringify(info) );
          			    mostrar_mensaje( json_info );
                  });
		}


 var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "MODIFICADO" ){
         
      }
    }

		function guardar_ruta(coordenadas,latlong) {

			var idruta = $("#idruta").val();
      //var latlong_ini = $("#latlong_i").val();

			   $.ajax({
                      url: 'guardarRuta.php',
                      dataType: 'json',
                      type: 'GET',
                     data: {'coordenadas': coordenadas, 'idruta': idruta, 'latlong_fin': latlong}
                  }).done( function( info ){ 

                      buscar_ruta();
                      
                  });
		}

function buscar_ruta() {

          $.ajax({
          url: 'busca_data_ruta.php',
          dataType: 'json',
          type: 'POST',
          success: function(data){

            $("#idruta").val(data[0].idruta);

           },
          });
}

		navigator.geolocation.getCurrentPosition(function(position){

			//para obtener ubicacion actual
		    var latitude = position.coords.latitude;
		    var longitude = position.coords.longitude;

        //console.log(latitude+","+longitude);

		    	var map = L.map('map', {
				    center: [latitude, longitude],
				    zoom: 12
				});

		    	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			    attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>',
			    maxZoom: 18
				}).addTo(map);

				L.control.scale().addTo(map);
				/*var ubicacion = L.marker([latitude,longitude], {draggable: true}).addTo(map);
				 	ubicacion.bindPopup('Tu estas aqui').openPopup();*/

        $("#latlong_ini").val(latitude+','+longitude);

				L.Routing.control({
				  waypoints: [
						    L.latLng(latitude, longitude), //dirección obtenida del usuario
				        L.latLng(<?php echo $latlong_fin; ?>) //dirección fija de destino
						],
						language : 'es',
						geocoder: L.Control.Geocoder.nominatim(),//ruta alternativa
						routeWhileDragging: true,
						reverseWaypoints: true,
						showAlternatives: true,
						altLineOptions: {
							styles: [
								{color: 'black', opacity: 0.15, weight: 9},
								{color: 'white', opacity: 0.8, weight: 6},
								{color: 'blue', opacity: 0.5, weight: 2}
							]
						}
				}).addTo(map);

		});



/*window.onload = function() {

  calcular_velocidad();

};     

function calcular_velocidad() {

      var tiempo=$("#tiempo").val().replace('min', '');
      var distancia=$("#distancia").val().replace('km', '');
      var calculo = (((distancia*1000)/(tiempo*60))*3600)/1000;
      $("#velocidad").val(parseInt(calculo.toLocaleString())+" km/h");

     }   */



</script>
