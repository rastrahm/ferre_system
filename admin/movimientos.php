<!DOCTYPE html>
<html lang="es">
<?php include ("header.php");?>

<style type="text/css">
  
  .table-wrapper {
  width: 100%;
  height: 600px; /* Altura de ejemplo */
  overflow: auto;
}

.table-wrapper table {
  border-collapse: separate;
  border-spacing: 0;
}

.table-wrapper table thead {
  position: -webkit-sticky; /* Safari... */
  position: sticky;
  top: 0;
  left: 0;
}

.table-wrapper table thead th,
.table-wrapper table tbody td {
  border: 1px solid #000;
  /*background-color: #000aff;*/
}

.derecha   { float: right; }

</style>

<body class="animsition">
  <?php

  include ("conexion.php");
  include ("buscar_articulo.php");

  $usuario = $_SESSION['nombreUsuario'];

  include("navbar.php");

?>
  <!-- Navigation-->
  <div class="content-wrapper" id="cuerpo">
   <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
    <div class="container-fluid" align="center"><br>
        <!-- Example DataTables Card-->
      
        <div class="card-body">

          <div class="card">
              <div class="card-header">
                <strong>Movimientos de Articulos</strong>
                <button class="btn btn-success" onclick="nuevo();"><i class="fa fa-plus-circle"></i> Nuevo</button>
                <button class="btn btn-primary" onclick="consultar();"><i class="fa fa-search"></i> Consultar</button>
                <button class="btn btn-danger" onclick="eliminar()"><i class="fa fa-trash"></i> Eliminar</button>
              </div>
            <div class="card-body">
            <div class="row">
            <div class='col-md-1'>
              <label>Comprobante:</label>
              <input type="number" name="idmov" id="idmov" class="form-control" onchange="buscar_mov()" disabled="">
            </div>
             <div class='col-md-2'>
              <label>Sucursal:</label>
                    <select id="sucursal" name="sucursal" class="form-control" onchange="bodega(this.value);">
                      <option>Seleccione la sucursal</option>
                      <?php

                        $res = mysqli_query($conexion, "SELECT * FROM sucursal");

                        foreach ($res as $row) {
                          echo "<option value='".$row["idsucursal"]."'> ".$row["desc_suc"]."</option>";
                        }
                       
                      ?>

                   </select><br>
              </div>

                <div class='col-md-2'>
              <label>Bodega:</label>
                    <select id="bodega" name="bodega" class="form-control">
                      <option>Seleccione la bodega</option>
                      <?php

                        $res = mysqli_query($conexion, "SELECT * FROM bodegas group by descbodega");

                        foreach ($res as $row) {
                          echo "<option value='".$row["idbodega"]."'> ".$row["descbodega"]."</option>";
                        }
                       

                      ?>

                   </select><br>
              </div>

             <div class='col-md-3'>
              <label>Motivo:</label>
                    <select id="motivo" name="motivo" class="form-control">
                      <option>Seleccione el motivo</option>
                      <?php

                        $res = mysqli_query($conexion, "SELECT * FROM motivos_mov");

                        foreach ($res as $row) {
                          echo "<option value='".$row["tipo"]."'> ".$row["desc_motivo"]."</option>";
                        }
                       

                      ?>

                   </select><br>
              </div>


               <div class='col-md-2'>
                <label>Fecha:</label>
                <input type="date" name="fecha" id="fecha" class="form-control" value="<?php echo date("Y-m-d"); ?>" onchange="buscar_mov()">
               </div>

               <div class='col-md-2'>
                  <label>Total:</label>
                    <div class='input-group'>
                    <input type="text" name="total" id="total" class="form-control" disabled="" style="color:red;text-align: center;font-size: 30px;font-weight: bold;">
                  </div>
               </div>

                </div>
              </div>            
         <div class="card-footer" align="right">
          <button class="btn btn-success" id="guardar" onclick="guardar_movimiento()" disabled="">
               <i class="fa fa-check"></i> Guardar
          </button>                                  
        </div>
        </div>
         

            <div align="center">
              <div class="col-md-10">
                <div class="table-wrapper table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning" id="tabla_articulos" width="100%" cellspacing="0">
                 <thead>
                 <tr align="center">
                 <th class='center'>Codigo</th>
                 <th class='center'>Articulo</th>
                 <th class='center'>Cantidad</th>
                 <th class='center'>Costo</th>
                 <th class='center'>Subtotal</th>
                 </tr>
                 </thead>
                 <tbody>
                 </tbody>
                </table>
                </div>
              </div>
            </div><br>

        </div>
    </div>
  </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php  include ("footer.php") ?>
    <!-- Logout Modal-->

   
  </div>
  
   <!-- Bootstrap core JavaScript-->
    
<?php  include ("seguridad.php") ?>
<?php  include ("scripts.php") ?>

</body>
  
</html>
<script type="text/javascript">

  function nuevo() {

    $.ajax({
              url: 'busca_ultimo_mov.php',
              dataType: 'json',
              type: 'POST',
              success: function(data){

                if (data=='0') {
                  $("#idmov").val("1");
                }else{
                  $("#idmov").val(parseInt(data[0].id_mov)+1);
                }
                    $("#idmov").attr("disabled", true);
                    $("#fecha").attr("disabled", true);
                    $("#guardar").attr("disabled", false);

                    limpiar_campos();

                    $("#tabla_articulos>tbody").append('<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>'+
                       '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"'+
                       'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" onkeypress="saltar(event)" onkeyup="cal_sub()" size="5"'+
                       '></td><td class="costo" style="width:200px"><input type="text" class="form-control" name="cos" id="cos" size="6"'+
                       '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"'+
                        'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

                         $("#sucursal").focus();

                },
            });
  }

  function buscar_mov() {
    $("#tabla_articulos > tbody:last").children().remove();
     $.ajax({
              url: 'busca_data_mov.php',
              dataType: 'json',
              type: 'POST',
              data: 'idmov='+$("#idmov").val(),
              success: function(data){
                  
                  if(data==0){
                  alertify.error("No existe el numero de comprobante!!!");
                  }else{

                    $("#idmov").val(data[0].id_mov);
                    $("#sucursal").val(data[0].sucursal);
                    $("#bodega").val(data[0].bodega); 
                    $("#motivo").val(data[0].motivo);
                    $("#total").val(parseInt(data[0].total_movimiento).toLocaleString());
                    $("#fecha").val(data[0].fecha);
                    
                      for (var i = 0; i < data.length; i++) {
                      $("#tabla_articulos > tbody").append("<tr><td class='datos'>"+data[i].articulo+
                      "</td><td>"+data[i].nombreart+
                      "</td><td>"+data[i].cantidad+
                      "</td><td>"+parseInt(data[i].costo).toLocaleString()+
                      "</td><td>"+parseInt(data[i].subtotal).toLocaleString()+
                      "</td></tr>");
                      }
                    }
                },
            });
      
  }

  function consultar(){
      $("#idmov").attr("disabled", false);
      $("#fecha").attr("disabled", false);
      $("#idmov").focus();
  }

  function bodega(str){
    var xmlhttp;
     
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("bodega").innerHTML=xmlhttp.responseText;
    }
    }
    xmlhttp.open("POST","filtro_bodega.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("sucursal="+str);
}

     $(document).ready(function() {

         buscar_mov();

    });

     function agregar(){
         
            var idarticulo = $("#idart").val();
            var nombreart = $("#nom").val();
            var cantidad = parseInt($("#cant").val());
            var costo = $("#cos").val().replace(/(?!-)[^\d]/g, '');
            var subtotal = $("#sub").val().replace(/(?!-)[^\d]/g, '');

              $("#tabla_articulos > tbody").prepend("<tr style='font-weight:bold;color:black;font-size:20px;text-align:center'><td        class='idarticulo'>"+idarticulo+
                        "</td><td class='nombreart'>"+nombreart+
                        "</td><td class='cantidad'>"+cantidad+
                        "</td><td class='costo'>"+parseInt(costo).toLocaleString()+
                        "</td><td class='subtotal'>"+parseInt(subtotal).toLocaleString()+
                        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
              
            resumen();
            agregar_fila();

    }

     function agregar_fila() {

        $("#tabla_articulos>tbody").prepend('<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>'+
       '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"'+
       'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" onkeypress="saltar(event)" onkeyup="cal_sub()" size="5"'+
       '></td><td class="costo" style="width:200px"><input type="text" class="form-control" name="cos" id="cos" size="6"'+
       '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"'+
        'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

        $("#idart").focus();

     }

     function resumen(){
            var monto=0;
            $('#tabla_articulos > tbody > tr').each(function(){
            monto+=parseInt($(this).find('td').eq(4).html().replace(/(?!-)[^\d]/g, ''));
            });
            $("#total").val(parseInt(monto+5).toLocaleString());
          }


    function saltar(e){
      (e.keyCode)?k=e.keyCode:k=e.which;
      if(k==13){
          $("#confirmar").focus();
        }
    }

    function saltar2(e,id){
      (e.keyCode)?k=e.keyCode:k=e.which;
      if(k==13){
          document.getElementById(id).focus();
        }
    }

    function cal_sub(){
      var cantidad = $("#cant").val();
      var costo = $("#cos").val().replace(/[^\d]/g, '');
      $("#sub").val(parseInt(cantidad*costo).toLocaleString());
    }

    function busca_articulo(){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_articulo.php',
          dataType: 'json',
          type: 'POST',
          data: 'idarticulo='+$("#idart").val()+'&idsucursal='+$("#sucursal").val()+'&idbodega='+$("#bodega").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el articulo!!!");
            $("#idart").val("");
            $("#idart").focus();
            }else{
            $("#cant").focus();
            $("#idart").val(data[0].idarticulo);
            $("#nom").val(data[0].nombreart);
            $("#cos").val(parseInt(data[0].costo).toLocaleString());
            }
           },
          });
        });
  }



  function busqueda_art(){
   $("#modal_busqueda_arts").modal("show");
   $('#modal_busqueda_arts').on('shown.bs.modal', function () {
   $("#lista_articulos").html("");
   $("#articulo_buscar").val("");
   $("#articulo_buscar").focus();
   busca();
   });
}


function busca(){
    $.ajax({
        beforeSend: function(){
          $("#lista_articulos").html("");
          },
        url: 'busca_articulos_ayuda.php',
        type: 'POST',
        data: 'articulo='+$("#articulo_buscar").val()+'&idsucursal='+$("#sucursal").val()+'&idbodega='+$("#bodega").val(),
        success: function(x){
         $("#lista_articulos").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_articulos").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}


function add_art(art){
  //alert(art);
  $("#modal_busqueda_arts").modal("toggle");
  $("#idart").val(art);
  busca_articulo();
}

  $(function(){
         // Evento que selecciona la fila y la elimina
        $(document).on("click",".delete",function(){
            var parent = $(this).parents().parents().get(0);
          $(parent).remove();
          resumen();
        });
       });

function guardar_movimiento(){
             let articulos = [];
            var sucursal = $("#sucursal").val();
            var bodega = $("#bodega").val();
            var motivo=$("#motivo").val();
            var total=$("#total").val().replace(/(?!-)[^\d]/g, '');
            var opcion = "registrar";

                    document.querySelectorAll('#tabla_articulos tbody tr').forEach(function(e){
                      let fila = {
                        idarticulo: e.querySelector('.idarticulo').innerText,
                        nombreart: e.querySelector('.nombreart').innerText,
                        cantidad: e.querySelector('.cantidad').innerText,
                        costo: e.querySelector('.costo').innerText.replace(/(?!-)[^\d]/g, ''),
                        subtotal: e.querySelector('.subtotal').innerText.replace(/(?!-)[^\d]/g, '')
                      };
                      articulos.push(fila);
                    });

                $.ajax({
                      url: 'guardarMovimientos.php',
                      dataType: 'json',
                      type: 'POST',
                     data: {'articulos': JSON.stringify(articulos), 'motivo': motivo, 'idsucursal': sucursal, 'bodega': bodega, 'total': total, 'opcion': opcion}
                   }).done( function( info ){   
                      var json_info = JSON.parse( JSON.stringify( info ));
                      mostrar_mensaje( json_info );
                      limpiar_campos();
                      agregar_fila();
                });
    
}

function eliminar(){
    var idmov = $("#idmov").val();
    var opcion = "eliminar";
    $.ajax({
                      url: 'guardarMovimientos.php',
                      dataType: 'json',
                      type: 'POST',
                     data: {'idmov': idmov, 'opcion': opcion}
                   }).done( function( info ){   
                      var json_info = JSON.parse( JSON.stringify( info ));
                      mostrar_mensaje( json_info );
                      location.reload(true);
                });

}

function limpiar_campos() {
  $("#tabla_articulos > tbody:last").children().remove();
  $("#sucursal").val("Seleccione la sucursal");
  $("#bodega").val("Seleccione la bodega");
  $("#motivo").val("Seleccione el motivo");
  $("#total").val("");
}

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })

          $("#guardar").attr("disabled", true);
          $("#fila1").remove();

      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

</script>