<?php

include ("conexion.php");
include ("seguridad/formatos.php");

$cliente = $_POST['idcliente'];
$nrofactura = $_POST['nrofactura'];
$sql = "SELECT cv.id_credito_venta,v.tipo_comprobante,
v.nrofactura,cv.saldo,cv.fecha_credito 
from credito_venta cv join ventas_cab v on v.idventa=cv.venta 
where cv.cliente=$cliente and v.nrofactura like '%$nrofactura%'  and cv.estado=0";
//echo $sql;
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){
    echo "<div class='table-responsive'>";
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Tipo</th>";
    echo "<th>Fecha</th>";
    echo "<th>Nro Factura</th>";
    echo "<th>Saldo</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($res)){

    echo "<tr>";
    echo "<td>".$row['tipo_comprobante']."</td>";
    echo "<td>".formatearFecha($row['fecha_credito'])."</td>";
    echo "<td>".$row['nrofactura']."</td>";
    echo "<td>".formatearNumero($row['saldo'])."</td>";
    echo "<td><button type='button' id='".$row['nrofactura']."' class='btn btn-primary btn-xs' onclick='add_art(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
  echo "</div>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}

?>