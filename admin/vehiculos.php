
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     include ("conexion.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>
               
                             <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Vehiculo</strong> 
                                    </div>
                                     <form name="formulario" id="guardarDatos" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="card-body card-block">
                                      <div class="row">
                                          <input type="hidden" id="idvehiculo" name="idvehiculo" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Sucursal</label>
                                                    <select class="form-control" name="sucursal" id="sucursal" class="form-control" required="">
                                                      <option>Seleccione un sucursal</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from sucursal";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idsucursal"]."'>".$row["desc_suc"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                             <div class="col-md-3">
                                                    <label for="text-input" class=" form-control-label">Marca</label>
                                                
                                                    <input type="text" id="marca" name="marca" class="form-control" required="">
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Modelo</label>
                                                
                                                    <input type="text" id="modelo" name="modelo" class="form-control" required="">
                                            </div>
                                            <div class="col-md-1">
                                                    <label for="textarea-input" class=" form-control-label">Año</label>
                                                    <input type="text" id="año" name="año" class="form-control">
                                             </div>
                                              <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Matricula</label>
                                                    <input type="text" id="matricula" name="matricula" class="form-control">
                                             </div>
                                              <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Estado</label>
                                                    <select class="form-control" name="estado" id="estado" class="form-control" required="">
                                                    <option value="0">Disponible</option>
                                                    <option value="1">En proceso de entrega</option>
                                                  </select>

                                            </div>
                                            </div>
                                          </div>
                                            <div class="card-footer" align="right">
                                                          <button type="submit" class="btn btn-success">
                                                              <i class="fa fa-check"></i> Guardar
                                                          </button>    
                                                  </div> 
                                            </form>
                                            </div><br>

                                        
                   

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablavehiculos" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th class="hidden">ID</th>
                                            <th>ID SUCURSAL</th>
                                            <th>SUCURSAL</th>
                                            <th>MARCA</th>
                                            <th>MODELO</th>
                                            <th>AÑO</th>
                                            <th>MATRICULA</th>
                                            <th>ESTADO</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div> 

                               </div>

                            </div>
        </div>     

          <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idvehiculo" name="idvehiculo" value="">
              <input type="hidden" id="opcion2" name="opcion2" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Vehiculo</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el vehiculo?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
            
            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/vehiculos.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">


    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarVehiculos.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablavehiculos').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula o Ruc ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idvehiculo = $("#frmEliminarArticulo #idvehiculo").val(),
          opcion = $("#frmEliminarArticulo #opcion2").val();
        $.ajax({
          method:"POST",
          url: "guardarVehiculos.php",
          data: {"idvehiculo": idvehiculo, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablavehiculos').DataTable().ajax.reload();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idvehiculo").val("");
      $("#marca").val("");
      $("#modelo").val("");
      $("#año").val("");
      $("#matricula").val("");
    }


    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idvehiculo = $("#idvehiculo").val( data.idvehiculo ),
            sucursal = $("#sucursal").val( data.sucursal ),
            marca = $("#marca").val( data.marca ),
            modelo = $("#modelo").val( data.modelo ),
            año = $("#año").val( data.año ),
            matricula = $("#matricula").val( data.matricula ),
            estado = $("#estado").val( data.estado ),
            opcion = $("#opcion").val("modificar");
             window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idvehiculo = $("#frmEliminarArticulo #idvehiculo").val( data.idvehiculo );
      });
    }

    
</script>
