<?php

include ("conexion.php");
include ("./seguridad/formatos.php");

$reporte = 'Completo';

$fi=$_POST['fechai'];
$ff=$_POST['fechaf'];

$tiporeporte = $_POST['tiporeporte'];

$proveedor = $_POST['proveedor'];
$tabonos=0;
$tabonos2=0;

if (strcmp($tiporeporte, $reporte)==0) {

    $sql="SELECT cc.idcompra,p.proveedor,cc.nrofactura,cc.fecha,cc.hora,cc.total_comprobante_c_iva,cc.estado from compra_cab cc
      join proveedores p on p.idproveedor=cc.proveedor
      where p.proveedor like '%$proveedor%' and cc.fecha between '$fi' and '$ff' 
      ORDER BY cc.fecha DESC";

$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

     /*ventas de credito*/
     if($resul>0){
        echo "<div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
        echo "<div class='box box-danger print1'>";
        echo "<div class='box-header with-border'><br>";
        echo "<h4 class='box-title'>Compras | ".formatearFecha($fi)." al ".formatearFecha($ff)." <div id='total_credito'></div><br></h4>";
        echo "</div>";
        echo "<div class='box-body'>";
        echo "<div class='box-body table-responsive'>";
        echo "<table id='tabla_ventas_credito' class='table table-bordered table-hover'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Proveedor</th>";
        echo "<th>N° Factura</th>";
        echo "<th>Fecha</th>";
        echo "<th>Total</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
           while($row = mysqli_fetch_array($res)){
            echo "<tr>";
            $tabonos+=$row['total_comprobante_c_iva'];
            echo "<td>".$row['proveedor']."</td>";
            echo "<td>".$row['nrofactura']."</td>";
            echo "<td>".formatearFecha($row['fecha'])."</td>";
            echo "<td align='right'>".formatearNumero($row['total_comprobante_c_iva'])."</td>";
            
          }
          echo "<tr>";
          echo "<td><b>TOTAL: <b></td>";
          echo "<td><b>".formatearNumero($tabonos)." Gs.</b></td>";
          echo "</tr>";
          echo "</tbody>";
          echo "</table>";
        echo "</div>";
      echo "</div>";
      echo "</div>";
      echo "</div><br><br>";
     }else{
      echo '<script>alertify.error("Ninguna compra registrada!!!");</script>';
     }

}else{
    $sql="SELECT cc.nrofactura,a.nombreart,cd.cantidad,cd.costo,cd.subtotal_c_iva from compra_cab cc
      join compra_det cd on cd.idcompra=cc.idcompra
      join proveedores p on p.idproveedor=cc.proveedor
      join articulos a on a.idarticulo=cd.articulo
      where p.proveedor like '%$proveedor%' and cc.fecha between '$fi' and '$ff' 
      ORDER BY cc.fecha DESC";

$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

     /*ventas de credito*/
     if($resul>0){
        echo "<div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
        echo "<div class='box box-danger print1'>";
        echo "<div class='box-header with-border'><br>";
        echo "<h4 class='box-title'>Compras | ".formatearFecha($fi)." al ".formatearFecha($ff)." <div id='total_credito'></div><br></h4>";
        echo "</div>";
        echo "<div class='box-body'>";
        echo "<table id='tabla_ventas_credito' class='table table-bordered table-hover'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>N° Factura</th>";
        echo "<th>Articulo</th>";
        echo "<th>Cantidad</th>";
        echo "<th>Costo</th>";
        echo "<th>Subtotal</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
           while($row = mysqli_fetch_array($res)){
            echo "<tr>";
            $tabonos+=$row['subtotal_c_iva'];
            echo "<td>".$row['nrofactura']."</td>";
            echo "<td>".$row['nombreart']."</td>";
            echo "<td>".$row['cantidad']."</td>";
            echo "<td>".$row['costo']."</td>";
            echo "<td align='right'>".formatearNumero($row['subtotal_c_iva'])."</td>";
            
          }
          echo "<tr>";
          echo "<td><b>TOTAL: <b></td>";
          echo "<td><b>".formatearNumero($tabonos)." Gs.</b></td>";
          echo "</tr>";
          echo "</tbody>";
          echo "</table>";
        echo "</div>";
      echo "</div>";
      echo "</div><br><br>";
     }else{
      echo '<script>alertify.error("Ninguna compra registrada!!!");</script>';
     }
}

?>