<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<?php 
  include("header2.php");
  include ("conexion.php");
  $usuario = $_SESSION['nombreUsuario'];
  $idusuario = $_SESSION['idusuario'];
  $idsucursal = $_SESSION['idsucursal'];
  if (isset($_GET["idpedido"])=='') {
              $idpedido = "";
            }else{
              $idpedido = $_GET["idpedido"];
            }

  $sql = "SELECT concat(latitud,',',longitud) as coordenadas from pedido where idpedido=$idpedido";

  $resultado = mysqli_query($conexion,$sql);
  foreach ($resultado as $row) {
      $latlong_fin = $row["coordenadas"];
  }


?>

 <style>
  #map { 
  widh: 50px;
  height: 600px; }
 </style>
 
<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
              include ("buscar_empleado.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>

               <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Viajes</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                      <input type="hidden" id="nombre" name="nombre" class="form-control" required="">
                                      <input type="hidden" id="cargo" name="cargo" class="form-control" required="">
                                      <input type="hidden" id="idempleado" name="idempleado" class="form-control" required="">
                                      <div class="row">
                                         <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">ID RUTA</label>
                                                
                                                    <input type="text" id="idruta" name="idruta" class="form-control" required="" disabled="">
                                            </div>
                                           <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">NRO PEDIDO</label>
                                                
                                                    <input type="text" class="form-control" name="idpedido" id="idpedido" style="text-align:center;" value="<?php echo $idpedido; ?>" disabled>
                                            </div>
                                            <div class="col-md-2">   
                                              <label for="text-input" class=" form-control-label">Empleado</label>
                                            <div class="input-group">
                                              <div class="input-group-btn">
                                                <button class="btn btn-primary" onclick="busqueda_emp()"><i class="fa fa-search"></i>
                                                </button>
                                              </div>
                                                <input type="text" class="form-control" onchange="busca_empleado();" name="documento" id="documento" style="text-align:center;" required="">
                                              </div>    
                                              </div> 
                                          
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Vehiculo</label>
                                                
                                                     <select id="vehiculo" name="vehiculo" class="form-control">
                                                  <?php
                                                    $res = mysqli_query($conexion, "SELECT * FROM vehiculos where sucursal=$idsucursal");

                                                    foreach ($res as $row) {
                                                      echo "<option value='".$row["idvehiculo"]."'> ".$row["marca"]." - ".$row["modelo"]."</option>";
                                                    }
                                                   

                                                  ?>

                                               </select>
                                            </div>
                                           <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Fecha Salida</label>
                                                
                                                    <input type="date" id="fecha_salida" name="fecha_salida" class="form-control" required="">
                                            </div>
                                           <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">Hora Salida</label>
                                                
                                                    <input type="time" id="hora_salida" name="hora_salida" class="form-control" required="">
                                            </div>
                                                
                                                    <input type="hidden" id="latlong_ini" name="latlong_ini" class="form-control" required="" disabled="">
                                                
                                                    <input type="hidden" id="latlong_fin" name="latlong_fin" value="<?php echo $latlong_fin ?>" class="form-control" required="" disabled="">
                              <div class="col-md-6" align="center"><br>
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablaempleados" width="100%" cellspacing="0">
                                    <thead>
                                         <tr align="center">
                                            <th>CI</th>
                                            <th>NOMBRE</th>
                                            <th>ROL</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>
                              </div>

                                         </div>
                                      </div>
                                      <div class="card-footer" align="right">
                                                <button type="button" onclick="guardarViaje()" class="btn btn-success"><i class="fa fa-check"></i> Guardar Viaje</button>
                                      </div>
                                    </div>
                                    <div id="map"></div>
                                </div>
                </div>

             
          </div>
      </div>

      <?php include("footer.php"); ?>
    </div>


    <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>

    <script src="js/leaflet.js"></script>
    <script src='js/leaflet-src.js'></script>
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <script src="js/Control.FullScreen.js"></script>

 </body> 

 </html>

<script type="text/javascript">

  function agregar(){
         
            var documento = $("#documento").val();
            var nombre = $("#nombre").val();
            var cargo = $("#cargo").val();
            var idempleado = $("#idempleado").val();

              $("#tablaempleados > tbody").prepend("<tr style='text-align:center'><td class='documento'>"+documento+
                        "</td><td class='nombre'>"+nombre+
                        "</td><td class='cargo'>"+cargo+
                        "</td><td class='idempleado' style='display:none'>"+idempleado+
                        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
              
    }

      $(function(){
         // Evento que selecciona la fila y la elimina
        $(document).on("click",".delete",function(){
            var parent = $(this).parents().parents().get(0);
          $(parent).remove();
        });
       });

  function busca_empleado(){
          $.ajax({
          url: 'busca_data_empleado.php',
          dataType: 'json',
          type: 'POST',
          data: 'documento='+$("#documento").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el empleado!!!");
            $("#documento").val("");
            $("#documento").focus();
            }else{
            $("#documento").val(data[0].documento);
            $("#idempleado").val(data[0].idempleado);
            $("#nombre").val(data[0].nombre);
            $("#cargo").val(data[0].desc_cargo);
            agregar();
            }
           },
          });
}

function busqueda_emp(){
   $("#modal_empleados").modal("show");
   $('#modal_empleados').on('shown.bs.modal', function () {
   $("#lista_empleados").html("");
   $("#empleado_buscar").val("");
   $("#empleado_buscar").focus();
   busca_e();
   });
}

function busca_e(){
    $.ajax({
        beforeSend: function(){
          $("#lista_empleados").html("");
          },
        url: 'busca_empleados_ayuda.php',
        type: 'POST',
        data: 'idempleado='+$("#empleado_buscar").val(),
        success: function(x){
         $("#lista_empleados").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_empleados").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
function add_emp(cli){
  //alert(art);
  $("#modal_empleados").modal("toggle");
  $("#documento").val(cli);
  busca_empleado();
}

		function guardarViaje() {

			   var idruta = $("#idruta").val();
         var idpedido = $("#idpedido").val();
         var vehiculo = $("#vehiculo").val();
         var fecha_salida = $("#fecha_salida").val();
         var hora_salida = $("#hora_salida").val();
			   var distancia = $("#distancia").val();
			   var tiempo = $("#tiempo").val();
         var velocidad = $("#velocidad").val();
         var latlong_ini = $("#latlong_ini").val();

          let empleados = [];

                    document.querySelectorAll('#tablaempleados tbody tr').forEach(function(e){
                      let fila = {
                        idempleado: e.querySelector('.idempleado').innerText
                      };
                      empleados.push(fila);
                    });

			             $.ajax({
                      url: 'guardarRutaDet.php',
                      dataType: 'json',
                      type: 'GET',
                     data: {'empleados': JSON.stringify(empleados), 'idruta': idruta, 'idpedido': idpedido, 'vehiculo': vehiculo, 'fecha_salida': fecha_salida, 'hora_salida': hora_salida, 'distancia': distancia, 'tiempo': tiempo, 'velocidad': velocidad, 'latlong_ini': latlong_ini}
                  }).done( function( info ){ 
                    var json_info = JSON.parse( JSON.stringify(info) );
          			    mostrar_mensaje( json_info );
                  });
		}


 var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "MODIFICADO" ){
         
      }
    }

		function guardar_ruta(coordenadas,latlong) {

			var idruta = $("#idruta").val();
      //var latlong_ini = $("#latlong_i").val();

			   $.ajax({
                      url: 'guardarRuta.php',
                      dataType: 'json',
                      type: 'GET',
                     data: {'coordenadas': coordenadas, 'idruta': idruta, 'latlong_fin': latlong}
                  }).done( function( info ){ 

                      buscar_ruta();
                      
                  });
		}

function buscar_ruta() {

          $.ajax({
          url: 'busca_data_ruta.php',
          dataType: 'json',
          type: 'POST',
          success: function(data){

            $("#idruta").val(data[0].idruta);

           },
          });
}

		navigator.geolocation.getCurrentPosition(function(position){

			//para obtener ubicacion actual
		    var latitude = position.coords.latitude;
		    var longitude = position.coords.longitude;

        //console.log(latitude+","+longitude);

		    	var map = L.map('map', {
				    center: [latitude, longitude],
				    zoom: 12
				});

		    	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			    attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>',
			    maxZoom: 18
				}).addTo(map);

				L.control.scale().addTo(map);
				/*var ubicacion = L.marker([latitude,longitude], {draggable: true}).addTo(map);
				 	ubicacion.bindPopup('Tu estas aqui').openPopup();*/

        $("#latlong_ini").val(latitude+','+longitude);

				L.Routing.control({
				  waypoints: [
						    L.latLng(latitude, longitude), //dirección obtenida del usuario
				        L.latLng(<?php echo $latlong_fin; ?>) //dirección fija de destino
						],
						language : 'es',
						geocoder: L.Control.Geocoder.nominatim(),//ruta alternativa
						routeWhileDragging: true,
						reverseWaypoints: true,
						showAlternatives: true,
						altLineOptions: {
							styles: [
								{color: 'black', opacity: 0.15, weight: 9},
								{color: 'white', opacity: 0.8, weight: 6},
								{color: 'blue', opacity: 0.5, weight: 2}
							]
						}
				}).addTo(map);

		});



window.onload = function() {

  calcular_velocidad();

};     

function calcular_velocidad() {
     var condicion = /(\d+)/g;

      var tiempo=$("#tiempo").val().match(condicion);
      var distancia=$("#distancia").val().replace('km', '');
      var calculo = (((distancia*1000)/(tiempo*60))*3600)/1000;
      $("#velocidad").val(parseInt(calculo.toLocaleString())+" km/h");

     }   

		/*var map = L.map('map').
		setView([<?php echo $latitud ?>,<?php echo $longitud ?>], 
		20);*/
		

		
		

</script>
