<?php
	include ("conexion.php");

	$codigo = $_POST["codigo"];
	$opcion = $_POST["opcion"];
	$informacion = [];

	switch ($opcion) {
		case 'eliminar':
			eliminar($codigo, $conexion);
			
			break;
		case 'habilitar':
			habilitar($codigo, $conexion);
			
			break;
	}

	function eliminar($codigo, $conexion){
		$query = "UPDATE ingredientes set estado=1
					where idingrediente='$codigo'";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado($resultado);
		cerrar($conexion);
	}

	function habilitar($codigo, $conexion){
		$query = "UPDATE ingredientes set estado=0
					where idingrediente='$codigo'";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado2($resultado);
		cerrar($conexion);
	}

	function verificar_resultado($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "ELIMINADO";
			
			echo json_encode($informacion);
		}

	function verificar_resultado2($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "HABILITADO";
			
			echo json_encode($informacion);
		}

	function cerrar($conexion){
		mysqli_close($conexion);
	}
?>