<?php

include ("conexion.php");
include ("./seguridad/formatos.php");

$completo = "Completo";

$fi=$_POST['fechai'];
$ff=$_POST['fechaf'];
//$grupo=$_POST['grupo'];
$tiporeporte=$_POST['tiporeporte'];
$tabonos=0;
$tabonos2=0;

$sql="SELECT c.combo, pd.cantidad, pc.total, pc.fecha 
      FROM pedido pc
      JOIN pedido_det pd on pd.pedido=pc.idpedido
      JOIN combos c ON c.idcombo = pd.comboid 
      where pc.fecha BETWEEN '$fi' and '$ff' and pc.estado=3
      GROUP BY pc.idpedido ORDER BY pc.fecha DESC";
      
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

     /*ventas de credito*/
     if($resul>0){
        echo "<div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
        echo "<div class='box box-danger print1'>";
        echo "<div class='box-header with-border'><br>";
        echo "<h4 class='box-title'>Lista de Pedidos | ".formatearFecha($fi)." al ".formatearFecha($ff)." <div id='total_credito'></div><br></h4>";
        echo "</div>";
        echo "<div class='box-body'>";
        echo "<div class='box-body table-responsive'>";
        echo "<table id='tabla_ventas_credito' class='table table-bordered table-hover'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Combo</th>";
        echo "<th>Cantidad</th>";
        echo "<th>Total</th>";
        echo "<th>Fecha</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
           while($row = mysqli_fetch_array($res)){
            echo "<tr>";
            $tabonos+=$row['cantidad'];
            $tabonos2+=$row['total'];
            echo "<td>".$row['combo']."</td>";
            echo "<td>".$row['cantidad']."</td>";
            echo "<td>".formatearNumero($row['total'])."</td>";
            echo "<td>".formatearFecha($row['fecha'])."</td>";
          }
          echo "<tr>";
          echo "<td><b>TOTAL: <b></td>";
          echo "<td><b>".formatearNumero($tabonos2)." Gs.</b></td>";
           echo "<td><b>TOTAL VENDIDOS:<b></td>";
          echo "<td><b>".formatearNumero($tabonos)."</b></td>";
          echo "</tr>";
          echo "</tbody>";
          echo "</table>";
        echo "</div>";
        echo "</div>";
      echo "</div>";
      echo "</div><br><br>";
     }else{
      echo '<script>alertify.error("Ningun pedido registrado!!!");</script>';
     }

?>