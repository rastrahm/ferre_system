<?php

include ("conexion.php");
include ("seguridad/formatos.php");

$proveedor = $_POST['proveedor'];
$sql = "SELECT ccp.idcab_pedido, p.proveedor, ccp.concepto from compra_cab_pedido ccp 
    join proveedores p on p.idproveedor=ccp.proveedor
    where p.proveedor like '%$proveedor%'";
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){
    echo "<div class='table-responsive'>";
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>ID</th>";
    echo "<th>Proveedor</th>";
    echo "<th>Concepto</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($res)){

    echo "<tr>";
    echo "<td>".$row['idcab_pedido']."</td>";
    echo "<td>".$row['proveedor']."</td>";
    echo "<td>".$row['concepto']."</td>";
    echo "<td><button type='button' id='".$row['idcab_pedido']."' class='btn btn-primary btn-xs' onclick='add_ped(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";

  }
  echo "</tbody>";
  echo "</table>";
  echo "</div>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}
