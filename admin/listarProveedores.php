<?php
	include ("conexion.php");

	$sql = "select idproveedor,proveedor,ruc,direccion, telefono, ciudad FROM proveedores";
	$res = mysqli_query($conexion, $sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>