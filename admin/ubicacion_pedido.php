<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<head>
	
	<link rel="stylesheet" href="css/leaflet.css" />
 	<script src="js/leaflet.js"></script>
 	
 	<script src='js/leaflet-src.js'></script>
 	<link rel="stylesheet" href="css/leaflet-routing-machine.css" />
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <link rel="stylesheet" href="css/Control.FullScreen.css" />
	<script src="js/Control.FullScreen.js"></script>
		
 <style>
  #map { 
  widh: 50px;
  height: 600px; }
 </style>
 
 </head>
  <body>

   <?php  

   	include ("conexion.php");

   	$idpedido = $_GET["idpedido"];

   	$sql = "SELECT latitud,longitud from pedido where idpedido=$idpedido";

        $resultado = mysqli_query($conexion,$sql);
        foreach ($resultado as $row) {

        	$latitud = $row["latitud"];
        	$longitud = $row["longitud"];
        }

   ?>

   <div id="map"></div>
 
 </body> 
 </html>

<script type="text/javascript">

		navigator.geolocation.getCurrentPosition(function(position){

		    	var map = L.map('map', {
				    center: [<?php echo $latitud ?>, <?php echo $longitud ?>],
				    zoom: 12,
				    fullscreenControl: true,
					 fullscreenControlOptions: {
					 position: 'topleft'
					 }
				});
				
		    	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			    attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>'
				}).addTo(map);

				L.control.scale().addTo(map);
				var ubicacion = L.marker([<?php echo $latitud ?>, <?php echo $longitud ?>], {draggable: false}).addTo(map);
				ubicacion.bindPopup('Ubicacion de entrega').openPopup();

		});

</script>