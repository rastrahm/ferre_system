<?php
	include ("conexion.php");

	$sql = "SELECT a.idarticulo,p.proveedor,a.nombreart,c.nombrecla,a.descripcion,a.iva,a.costo,a.estado,a.fecha_alta,a.fec_ult_compra,a.codigobarras from articulos a
		join clasificaciones c on c.idclasificacion=a.clasificacion
		join proveedores p on p.idproveedor=a.proveedor";
	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>