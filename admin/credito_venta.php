<!DOCTYPE html>
<html lang="es">
<?php include("header.php"); ?>

<style type="text/css">
  .table-wrapper {
    width: 100%;
    height: 200px;
    /* Altura de ejemplo */
    overflow: auto;
  }

  .table-wrapper table {
    border-collapse: separate;
    border-spacing: 0;
  }

  .table-wrapper table thead {
    position: -webkit-sticky;
    /* Safari... */
    position: sticky;
    top: 0;
    left: 0;
  }

  .table-wrapper table thead th,
  .table-wrapper table tbody td {
    border: 1px solid #000;
    /*background-color: #000aff;*/
  }

  .derecha {
    float: right;
  }
</style>

<body class="animsition">
  <?php
  include("buscar_cliente_credito.php");
  include("buscar_comprobante.php");

  include("conexion.php");
  include("navbar.php");

  $idusuario = $_SESSION['idusuario'];
  ?>
  <!-- Navigation-->
  <div class="page-wrapper" id="cuerpo">

    <div class="page-content">

      <div class="container-fluid"><br>
        <div class="card-body">
          <div class="card">
            <div class="card-header">
              <strong>Pagos</strong>
            </div>
            <div class="card-body card-block">
              <div class="row">
                <input type='hidden' id='idusuario' name="idusuario" value="<?php echo $idusuario; ?>">
                <div class='col-md-1'>
                  <label>N° Pago</label>
                  <input type='text' id='idpago' name="idpago" class='form-control' placeholder='' style="text-align: center;" disabled="">
                </div>
                <div class='col-md-3'>
                  <label>cliente</label>
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button class="btn btn-primary" onclick="busqueda_cli()"><i class="fa fa-search"></i></button>
                    </div>
                    <input type="text" class="form-control" name="cliente" id="cliente" style="text-align: center;" disabled="">
                    <input type="hidden" name="idcliente" id="idcliente">
                  </div>
                </div>
                <div class='col-md-2'>
                  <label>Fecha</label>
                  <input type='date' id='fecha' value="<?php echo date("Y-m-d"); ?>" name="fecha" class='form-control' placeholder=''>
                </div>
                <div class='col-md-2' align="center">
                  <label>Total</label>
                  <input type="text" id="total" name="total" class="form-control" readonly="readonly" style="font-size:30px; text-align:center; color:red; font-weight: bold;">
                </div>
              </div><br>
              <div class="row">

                <div align="center"><br>
                  <div class="col-md-12">
                    <div class="table-wrapper table-responsive">
                      <table class="table table-borderless table-striped table-earning" id="tabla_detalle" width="100%" cellspacing="0">
                        <thead>
                          <tr align="center">
                            <th class='center'>Nro Comprobante</th>
                            <th class='center'>Tipo</th>
                            <th class='center'>Fecha</th>
                            <th class='center'>Total</th>
                            <th class='center'>Monto Pagado</th>
                            <th class='center'>Saldo</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer" align="right">
              <button type="button" id="guardar" class="btn btn-success" onclick="guardar_pago();"><i class="fa fa-check"></i> Guardar

              </button>
            </div>
          </div>
          <div class="row" align="center">
            <div class="col-md-12">
              <div class="table-wrapper table-responsive">
                <table class="table table-borderless table-striped table-earning" id="tabla_pagos" width="100%" cellspacing="0">
                  <thead>
                    <tr align="center">
                      <th class='center'>Forma Pago</th>
                      <th class='center'>Banco</th>
                      <th class='center'>Fec. Emision</th>
                      <th class='center'>Fec. Ven.</th>
                      <th class='center'>Nro valor</th>
                      <th class='center'>Monto</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
            <div class='col-md-2 offset-md-9 col-sm-6' align="center">
              <label>Total</label>
              <input type="text" id="total_importe" name="total_importe" class="form-control" readonly="readonly" style="font-size:30px; text-align:center; color:red; font-weight: bold;">
            </div>

          </div>
        </div>
      </div>
    </div>

    <?php include("footer.php") ?>
    <!-- Scroll to Top Button-->
    <!-- Logout Modal-->
  </div>

  <?php include("seguridad.php") ?>

  <!-- Bootstrap core JavaScript-->
  <?php include("scripts.php") ?>

  <script src="funciones/credito_venta.js"></script>

</body>

</html>
<script type="text/javascript">
  $(document).on("ready", function() {

    busca_nro_comprobante();

    $("#tabla_detalle>tbody").append('<tr style="text-align:center" id="fila1"><td class="idcredito" style="display:none"><input type="text" class="form-control" name="idcredito" id="idcredito"></td>' +
      '<td class="comprobante" style="width:300px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_comprobante()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_factura();" name="comprobante" id="comprobante" size="2"></div>' +
      '</td><td class="tipo_comprobante" style="width:150px"><input type="text" class="form-control" name="tipo_comprobante" id="tipo_comprobante" size="5"' +
      'disabled></td><td class="fecha_credito" style="width:200px"><input type="date" class="form-control" name="fecha_credito" id="fecha_credito" size="5"' +
      'disabled></td><td class="total_credito" style="width:200px"><input type="text" class="form-control" name="total_credito" id="total_credito" size="5"' +
      'disabled></td><td class="monto" style="width:200px"><input type="text" class="form-control" name="monto" id="monto" onkeyup="format(this)" onkeypress="saltar(event)" size="5"' +
      '></td><td class="saldo" style="width:200px"><input type="text" class="form-control" name="saldo" id="saldo" size="5"' +
      'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

    $("#tabla_pagos>tbody").prepend('<tr style="text-align:center" id="fila1">' +
      '<td class="formapago" style="width:250px"><select name="formapago" id="formapago" class="form-control"><option value="">Seleccionar</option><?php include('conexion.php');
                                                                                                                                                    $sql = "SELECT * from formapago where idpago in (1,4)";
                                                                                                                                                    $res = mysqli_query($conexion, $sql);
                                                                                                                                                    while ($row = mysqli_fetch_array($res)) {
                                                                                                                                                      echo '<option value="' . $row["despago"] . '">' . $row["despago"] . '</option>';
                                                                                                                                                    } ?></select></td>' +
      '<td class="banco" style="width:280px"><select name="banco" id="banco" class="form-control"><option value="">Seleccionar</option><?php include('conexion.php');
                                                                                                                                        $sql = "SELECT * from bancos";
                                                                                                                                        $res = mysqli_query($conexion, $sql);
                                                                                                                                        while ($row = mysqli_fetch_array($res)) {
                                                                                                                                          echo '<option value="' . $row["desbanco"] . '">' . $row["desbanco"] . '</option>';
                                                                                                                                        } ?></select></td>' +
      '<td class="fecha_emi" style="width:200px"><input type="date" value="<?php echo date("Y-m-d") ?>" class="form-control" name="fecha_emi" id="fecha_emi" size="5"></td>' +
      '<td class="fecha_ven" style="width:200px"><input type="date" value="<?php echo date("Y-m-d") ?>"  class="form-control" name="fecha_ven" id="fecha_ven" size="5"></td>' +
      '<td class="nrovalor" style="width:200px"><input type="text" class="form-control" name="nrovalor" id="nrovalor" size="5"></td>' +
      '<td class="importe" style="width:200px"><input type="text" class="form-control" name="importe" id="importe" onkeyup="format(this)" onkeypress="saltar2(event)" size="5"' +
      '></td><td><button class="btn btn-success delete" id="confirmarpago" onclick="agregar_pago()"><i class="fa fa-check-circle"></i></button></td></tr>');


    $("#cliente").focus();

  });

  function busca_factura() {

    $.ajax({
      url: 'busca_data_venta_pagos.php',
      dataType: 'json',
      type: 'POST',
      data: 'nrofactura=' + $("#comprobante").val() + '&idcliente=' + $("#idcliente").val(),
      success: function(data) {
        if (data == 0) {
          alertify.error("No existe el numero de comprobante o ya fue dado de baja!!!");
          $("#idart").val("");
          $("#idart").focus();
        } else {
          $("#monto").focus();
          $("#idcredito").val(data[0].idcredito);
          $("#tipo_comprobante").val(data[0].tipo_comprobante);
          $("#fecha_credito").val(data[0].fecha_credito);
          $("#comprobante").val(data[0].nrofactura);
          $("#total_credito").val(parseInt(data[0].saldo).toLocaleString());
          $("#monto").val(parseInt(data[0].saldo).toLocaleString());
        }
      },
    });
  }

  function busqueda_comprobante() {
    $("#modal_busqueda_comp").modal("show");
    $('#modal_busqueda_comp').on('shown.bs.modal', function() {
      $("#lista_comprobantes").html("");
      $("#comprobante_buscar").val("");
      $("#comprobante_buscar").focus();
      busca_comp();
    });
  }


  function busca_comp() {

    var nrofactura = $("#comprobante").val();
    var idcliente = $("#idcliente").val();
   // console.log(nrofactura, idcliente);
    $.ajax({
      beforeSend: function() {
        $("#lista_comprobantes").html("");
      },
      url: 'busca_credito_venta_ayuda.php',
      type: 'POST',
      data: 'nrofactura=' + $("#comprobante").val() + '&idcliente=' + $("#idcliente").val(),
      success: function(x) {
        $("#lista_comprobantes").html(x);
      },
      error: function(jqXHR, estado, error) {
        $("#lista_comprobantes").html("Error en la peticion AJAX..." + estado + "      " + error);
      }
    });
  }


  function add_art(art) {
    //alert(art);
    $("#modal_busqueda_comp").modal("toggle");
    $("#comprobante").val(art);
    busca_factura();
  }

  function agregar() {

    var idcredito = $("#idcredito").val();
    var comprobante = $("#comprobante").val();
    var tipo_comprobante = $("#tipo_comprobante").val();
    var fecha = new Date($("#fecha_credito").val() + "GMT-0400");
    var total_credito = $("#total_credito").val().replace(/(?!-)[^\d]/g, '');
    var monto = $("#monto").val().replace(/(?!-)[^\d]/g, '');
    var saldo = total_credito - monto;

    var fecha_credito = fecha.getDate() + '/' + (fecha.getMonth() + 1) + '/' + fecha.getFullYear();

    $("#tabla_detalle > tbody").prepend("<tr style='text-align:center'><td class='idcredito' style='display:none'>" + idcredito +
      "</td><td class='comprobante'>" + comprobante +
      "</td><td class='tipo_comprobante'>" + tipo_comprobante +
      "</td><td class='fecha_credito'>" + fecha_credito +
      "</td><td class='total_credito'>" + parseInt(total_credito).toLocaleString() +
      "</td><td class='monto'>" + parseInt(monto).toLocaleString() +
      "</td><td class='saldo'>" + parseInt(saldo).toLocaleString() +
      "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");

    resumen();
    agregar_fila();

  }

  function agregar_pago() {

    var formapago = $("#formapago").val();
    var banco = $("#banco").val();
    var fecha_emi = new Date($("#fecha_emi").val() + "GMT-0400");
    var fecha_ven = new Date($("#fecha_ven").val() + "GMT-0400");
    var nrovalor = $("#nrovalor").val();
    var importe = $("#importe").val().replace(/(?!-)[^\d]/g, '');

    var fecha_emision = fecha_emi.getDate() + '/' + (fecha_emi.getMonth() + 1) + '/' + fecha_emi.getFullYear();
    var fecha_venc = fecha_ven.getDate() + '/' + (fecha_ven.getMonth() + 1) + '/' + fecha_ven.getFullYear();

    if (formapago == 'CHEQUE' && banco == '') {
      alertify.error("Debe elegir un banco!");
      agregar_fila2();
    } else {

      $("#tabla_pagos > tbody").prepend("<tr style='text-align:center'>" +
        "<td class='formapago'>" + formapago +
        "</td><td class='banco'>" + banco +
        "</td><td class='fecha_emi'>" + fecha_emision +
        "</td><td class='fecha_ven'>" + fecha_venc +
        "</td><td class='nrovalor'>" + nrovalor +
        "</td><td class='importe'>" + parseInt(importe).toLocaleString() +
        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");

      resumen2();
      agregar_fila2();

    }
  }

  function agregar_fila() {

    $("#tabla_detalle>tbody").prepend('<tr style="text-align:center" id="fila1"><td class="idcredito" style="display:none"><input type="text" class="form-control" name="idcredito" id="idcredito"></td>' +
      '<td class="comprobante" style="width:300px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_comprobante()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_factura();" name="comprobante" id="comprobante" size="2"></div>' +
      '</td><td class="tipo_comprobante" style="width:150px"><input type="text" class="form-control" name="tipo_comprobante" id="tipo_comprobante" size="5"' +
      'disabled></td><td class="fecha_credito" style="width:200px"><input type="date" class="form-control" name="fecha_credito" id="fecha_credito" size="5"' +
      'disabled></td><td class="total_credito" style="width:200px"><input type="text" class="form-control" name="total_credito" id="total_credito" size="5"' +
      'disabled></td><td class="monto" style="width:200px"><input type="text" class="form-control" name="monto" id="monto" onkeyup="format(this)" onkeypress="saltar(event)" size="5"' +
      '></td><td class="saldo" style="width:200px"><input type="text" class="form-control" name="saldo" id="saldo" size="5"' +
      'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

    $("#comprobante").focus();
  }

  function agregar_fila2() {

    $("#tabla_pagos>tbody").prepend('<tr style="text-align:center" id="fila1">' +
      '<td class="formapago" style="width:250px"><select name="formapago" id="formapago" class="form-control"><option value="">Seleccionar</option><?php include('conexion.php');
                                                                                                                                                    $sql = "SELECT * from formapago where idpago in (1,4)";
                                                                                                                                                    $res = mysqli_query($conexion, $sql);
                                                                                                                                                    while ($row = mysqli_fetch_array($res)) {
                                                                                                                                                      echo '<option value="' . $row["despago"] . '">' . $row["despago"] . '</option>';
                                                                                                                                                    } ?></select></td>' +
      '<td class="banco" style="width:280px"><select name="banco" id="banco" class="form-control"><option value="">Seleccionar</option><?php include('conexion.php');
                                                                                                                                        $sql = "SELECT * from bancos";
                                                                                                                                        $res = mysqli_query($conexion, $sql);
                                                                                                                                        while ($row = mysqli_fetch_array($res)) {
                                                                                                                                          echo '<option value="' . $row["desbanco"] . '">' . $row["desbanco"] . '</option>';
                                                                                                                                        } ?></select></td>' +
      '<td class="fecha_emi" style="width:200px"><input type="date" value="<?php echo date("Y-m-d") ?>" class="form-control" name="fecha_emi" id="fecha_emi" size="5"></td>' +
      '<td class="fecha_ven" style="width:200px"><input type="date" value="<?php echo date("Y-m-d") ?>"  class="form-control" name="fecha_ven" id="fecha_ven" size="5"></td>' +
      '<td class="nrovalor" style="width:200px"><input type="text" class="form-control" name="nrovalor" id="nrovalor" size="5"></td>' +
      '<td class="importe" style="width:200px"><input type="text" class="form-control" name="importe" id="importe" onkeyup="format(this)" onkeypress="saltar2(event)" size="5"' +
      '></td><td><button class="btn btn-success delete" id="confirmarpago" onclick="agregar_pago()"><i class="fa fa-check-circle"></i></button></td></tr>');
  }

  function busca_nro_comprobante() {
    $.ajax({
      url: 'busca_ultimo_pago.php',
      dataType: 'json',
      type: 'POST',
      success: function(data) {

        if (data == 0) {

          $("#idpago").val("1");

        } else {
          $("#idpago").val(parseInt(data[0].idpago) + 1);
        }

      },
    });
  }



  $(function() {
    // Evento que selecciona la fila y la elimina
    $(document).on("click", ".delete", function() {
      var parent = $(this).parents().parents().get(0);
      $(parent).remove();
      resumen();
      resumen2();
    });
  });

  function resumen() {
    var montoncr = 0;
    var montofac = 0;
    var monto = 0;
    var tipo = 0;

    $('#tabla_detalle > tbody > tr').each(function() {
      tipo = $(this).find('td').eq(2).html();
      if (tipo == 'NC') {
        montoncr += parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
      } else {
        montofac += parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
      }

      monto = montofac - montoncr;

    });
    $("#total").val(parseInt(monto + 5).toLocaleString());
  }

  function resumen2() {
    var monto = 0;
    var num = 0;

    $('#tabla_pagos > tbody > tr').each(function() {
      monto += parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
      num += parseInt($(this).find('td').eq(4).html().replace(/(?!-)[^\d]/g, ''));
    });
    $("#total_importe").val(parseInt(monto + 25).toLocaleString());
  }

  function guardar_pago() {

    var total_importe = parseInt($("#total_importe").val().replace(/(?!-)[^\d]/g, ''));
    var total = parseInt($("#total").val().replace(/(?!-)[^\d]/g, ''));

    if (total_importe != total) {

      alertify.error("Los totales no coinciden!");

    } else {

      var idpago = $("#idpago").val();
      var idcliente = $("#idcliente").val();
      var usuario = $("#idusuario").val();
      var fecha = $("#fecha").val();
      var total = $("#total").val().replace(/(?!-)[^\d]/g, '');

      let detalle = [];
      let pagos = [];

      document.querySelectorAll('#tabla_detalle tbody tr').forEach(function(e) {
        let fila = {
          idcredito: e.querySelector('.idcredito').innerText,
          tipo_comprobante: e.querySelector('.tipo_comprobante').innerText,
          comprobante: e.querySelector('.comprobante').innerText,
          total_credito: e.querySelector('.total_credito').innerText.replace(/[^\d]/g, ''),
          monto: e.querySelector('.monto').innerText.replace(/[^\d]/g, '')
        };
        detalle.push(fila);
      });

      document.querySelectorAll('#tabla_pagos tbody tr').forEach(function(e) {
        let fila2 = {
          formapago: e.querySelector('.formapago').innerText,
          banco: e.querySelector('.banco').innerText,
          fecha_emi: e.querySelector('.fecha_emi').innerText,
          fecha_ven: e.querySelector('.fecha_ven').innerText,
          nrovalor: e.querySelector('.nrovalor').innerText,
          importe: e.querySelector('.importe').innerText.replace(/[^\d]/g, '')
        };
        pagos.push(fila2);
      });

      $.ajax({
        url: 'guardarPagosventa.php',
        dataType: 'json',
        type: 'POST',
        data: {
          'detalle': JSON.stringify(detalle),
          'pagos': JSON.stringify(pagos),
          'idpago': idpago,
          'idcliente': idcliente,
          'fecha': fecha,
          'total': total,
          'usuario': usuario
        }
      }).done(function(info) {
        var json_info = JSON.parse(JSON.stringify(info));
        mostrar_mensaje(json_info);
        limpiar_campos();
        busca_nro_comprobante();
      });

    }



  }

  function limpiar_campos() {
    $("#tabla_detalle > tbody:last").children().remove();
    $("#tabla_pagos > tbody:last").children().remove();
    agregar_fila();
    agregar_fila2();

    $("#idcliente").val("");
    $("#cliente").val("");
    $("#total").val("");
    $("#total_importe").val("");
    $("#fecha").val("");
  }


  function format(input) {
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
      num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
      num = num.split('').reverse().join('').replace(/^[\.]/, '');
      input.value = num;
    } else {
      alertify.error('Solo se permiten numeros!');
      input.value = input.value.replace(/[^\d\.]*/g, '');
    }
  }

  function saltar(e) {
    (e.keyCode) ? k = e.keyCode: k = e.which;
    if (k == 13) {
      $("#confirmar").focus();
    }
  }

  function saltar2(e) {
    (e.keyCode) ? k = e.keyCode: k = e.which;
    if (k == 13) {
      $("#confirmarpago").focus();
    }
  }

  function print1() {
    $(".print1").printArea();
  }
</script>