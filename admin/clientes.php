<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     include ("conexion.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>
               
                              <div class="row">
                                <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Cliente</strong> 
                                    </div> 
                                    <div class="card-body">
                                    <div class="card-body card-block">
                                        <form name="formulario" id="guardarDatos" method="post" action="" class="form-horizontal">
                                          <input type="hidden" id="idcliente" name="idcliente" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                             <div class="form-group">
                                                    <label for="text-input" class=" form-control-label">Nombre Completo</label>
                                                
                                                    <input type="text" id="razonsocial" name="razonsocial" class="form-control" required="">
                                            </div>
                                            <div class="form-group">
                                                    <label for="text-input" class=" form-control-label">Cedula de Identidad</label>
                                                
                                                    <input type="text" id="documento" name="documento" class="form-control" required="">
                                            </div>
                                           
                                            <div class="form-group">
                                                    <label for="textarea-input" class=" form-control-label">Numero de Contacto</label>
                                                    <input type="text" id="telefono" name="telefono" class="form-control">
                                             </div>
                                            <div class="form-group">
                                                    <label for="textarea-input" class=" form-control-label">Correo</label>
                                                    <input type="email" id="correo" name="correo" class="form-control">
                                             </div>
                                              <div class="form-group">
                                                    <label for="textarea-input" class=" form-control-label">Barrio</label>
                                                    <input type="text" id="barrio" name="barrio" class="form-control">
                                             </div>
                                              <div class="form-group">
                                                    <label for="textarea-input" class=" form-control-label">Direccion</label>
                                                    <input type="text" id="direccion" name="direccion" class="form-control">
                                             </div>
                                             <div class="form-group">
                                                    <label for="text-input" class=" form-control-label">Ciudad</label>
                                                    <select class="form-control" name="ciudad" id="ciudad" class="form-control" required="">
                                                      <option>Seleccione una ciudad</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from ciudades";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idciudad"]."'>".$row["ciudad"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>

                                            </form>
                                            </div><br>

                                          </div>
                                          <div class="card-footer" align="right">
                                                          <button type="button" onclick="guardarClientes()" class="btn btn-success">
                                                              <i class="fa fa-check"></i> Guardar
                                                          </button>    
                                                  </div> 
                                          </div>
                                          </div>

                                <div class="col-lg-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <strong>Ubicacion</strong> 
                                            </div> 
                                            <div class="card-body">
                                                    <div class="card-body card-block">
                                                        
                                                              <div class='mostrar'></div><!-- Carga los datos ajax -->
                                                                <!-- END USER DATA-->
                                                    
                                                    </div><br>
                                                  </div>
                                                 
                                                  </div>
                                  </div>

                            </div>
                                          
                   

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablaclientes" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th class="hidden">ID</th>
                                            <th>DOCUMENTO</th>
                                            <th>RAZON SOCIAL</th>
                                            <th>TELEFONO</th>
                                            <th>CORREO</th>
                                            <th>BARRIO</th>
                                            <th>DIRECCION</th>
                                            <th class="hidden">LATITUD</th>               
                                            <th class="hidden">LONGITUD</th>
                                            <th>CIUDAD</th>
                                            <th class="hidden">IDCIUDAD</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>

                            </div>
        </div>     
          <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idcliente2" name="idcliente2" value="">
              <input type="hidden" id="opcion2" name="opcion2" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Cliente</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el cliente?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
            
          

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/clientes.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

  window.onload = function() {
          $.ajax({
            url:'ubicacion.php',
             beforeSend: function(objeto){
             $('#loader').html('');
            },
            success:function(data){
              $(".mostrar").html(data).fadeIn('slow');
              $('#loader').html('');
            }
          })
        };


    $(document).on("ready", function(){
      listar();
      eliminar();
    });

    function guardarClientes(){
        
        var datos='documento='+$("#documento").val()+'&razonsocial='
        +$("#razonsocial").val()+'&telefono='+$("#telefono").val()
        +'&correo='+$("#correo").val()+'&direccion='+$("#direccion").val()+'&barrio='+$("#barrio").val()
        +'&lgt='+$("#longitud").val()+'&ltt='+$("#latitud").val()+'&ciudad='+$("#ciudad").val()
        +'&opcion='+$("#opcion").val()+'&idcliente='+$("#idcliente").val();

        $.ajax({
          method: "POST",
          url: "guardarClientes.php",
          data: datos
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          $('#tablaclientes').DataTable().ajax.reload();
        });

    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
                   limpiar_datos();
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula o Ruc ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idcliente = $("#frmEliminarArticulo #idcliente2").val(),
          opcion = $("#frmEliminarArticulo #opcion2").val();
        $.ajax({
          method:"POST",
          url: "guardarClientes.php",
          data: {"idcliente": idcliente, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          listar();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idcliente").val("");
      $("#razonsocial").val("");
      $("#direccion").val("");
      $("#documento").val("");
      $("#correo").val("");
      $("#celular").val("");
    }


    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var documento = $("#documento").val( data.documento),
            idcliente = $("#idcliente").val( data.idcliente ),
            razonsocial = $("#razonsocial").val( data.razonsocial ),
            barrio = $("#barrio").val( data.barrio ),
            direccion = $("#direccion").val( data.direccion ),
            telefono = $("#telefono").val( data.telefono ),
            latitud = $("#latitud").val( data.latitud ),
            longitud = $("#longitud").val( data.longitud ),
            correo = $("#correo").val( data.correo ),
            ciudad = $("#ciudad").val( data.idciudad ),
            opcion = $("#opcion").val("modificar");
             window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idcliente = $("#frmEliminarArticulo #idcliente2").val( data.idcliente );
      });
    }

    
</script>
