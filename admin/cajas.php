
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     include ("conexion.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>
               
                             <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Cajas</strong> 
                                    </div>
                                     <form name="formulario" id="guardarDatos" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="card-body card-block">
                                      <div class="row">
                                          <input type="hidden" id="idcaja" name="idcaja" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                             <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">Nro caja</label>
                                                
                                                    <input type="text" id="nrocaja" name="nrocaja" class="form-control" required="">
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Serie Timbrado</label>
                                                
                                                    <input type="text" id="serie_timbrado" name="serie_timbrado" class="form-control" required="">
                                            </div>
                                             <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Nro Timbrado</label>
                                                
                                                    <input type="text" id="nro_timbrado" name="nro_timbrado" class="form-control" required="">
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Fecha Inicio</label>
                                                    <input type="date" id="fec_ini" name="fec_ini" class="form-control" required="">
                                             </div>
                                             <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Fecha Final</label>
                                                    <input type="date" id="fec_fin" name="fec_fin" class="form-control" required="">
                                             </div>
                                            </div>
                                          </div>
                                            <div class="card-footer" align="right">
                                                          <button type="submit" class="btn btn-success">
                                                              <i class="fa fa-check"></i> Guardar
                                                          </button>    
                                                  </div> 
                                            </form>
                                            </div><br>

                                        
                   

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablacajas" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th class="hidden">ID</th>
                                            <th>NRO CAJA</th>
                                            <th>SERIE</th>
                                            <th>NRO TIMBRADO</th>
                                            <th>FECHA INICIO</th>
                                            <th>FECHA FIN</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div> 

                               </div>

                            </div>
        </div>     

          <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idcaja" name="idcaja" value="">
              <input type="hidden" id="opcion2" name="opcion2" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Caja</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar la caja?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
            
            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/cajas.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">


    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarCajas.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablacajas').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'La informacion de caja ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idcaja = $("#frmEliminarArticulo #idcaja").val(),
          opcion = $("#frmEliminarArticulo #opcion2").val();
        $.ajax({
          method:"POST",
          url: "guardarCajas.php",
          data: {"idcaja": idcaja, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablacajas').DataTable().ajax.reload();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idcaja").val("");
      $("#nrocaja").val("");
      $("#serie_timbrado").val("");
      $("#nro_timbrado").val("");
      $("#fec_ini").val("");
      $("#fec_fin").val("");
    }


    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idcaja = $("#idcaja").val( data.idcaja ),
            nrocaja = $("#nrocaja").val( data.nro_caja ),
            serie_timbrado = $("#serie_timbrado").val( data.serie_timbrado ),
            nro_timbrado = $("#nro_timbrado").val( data.nro_timbrado ),
            fec_ini = $("#fec_ini").val( data.fec_ini ),
            fec_fin = $("#fec_fin").val( data.fec_fin ),
            opcion = $("#opcion").val("modificar");
             window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idcaja = $("#frmEliminarArticulo #idcaja").val( data.idcaja );
      });
    }

    
</script>
