<?php
	include ("conexion.php");

	$informacion = [];

	switch ($_POST["opcion"]) {
		case 'nuevo':    
		$idpedido = isset($_POST["idpedido"]);//AGG - VALIDACION DE CAMPOS VACIOS
		$sucursal = $_POST["sucursal"];
		$idcliente = $_POST["cliente"];
		$documento = $_POST["documento"];
		$nombre_ocacional = $_POST["nombreocacional"];
		$nrocaja = $_POST["nrocaja"];
		$tipofactura = $_POST["tipofactura"];
		$tipo_comprobante = $_POST["tipo_comprobante"];
		$fecha = $_POST["fecha"];
		$fecha_alta= $_POST["fecha_alta"];
		$hora = strftime("%H:%M:%S");
		$total_iva = $_POST["totaliva"];
		$total_gravada5 = $_POST["total5"];
		$total_gravada10 = $_POST["total10"];
		$total_exenta = $_POST["totalexenta"];
		$total_comprobante_c_iva = $_POST["total"];
		$total_comprobante = $total_gravada10+$total_gravada5;
		$usuario = $_POST["usuario"];
		$idlista = $_POST["lista_precio"];
		$idhabilitacion = $_POST["idhabilitacion"];

		$sqlid = "SELECT idventa from ventas_cab order by idventa desc LIMIT 1";
        $resid = mysqli_query($conexion,$sqlid);

        foreach ($resid as $rowid) {

        	$id = $rowid["idventa"];
        	
        }

        if (isset($id)=="") {
        		$idventa = 1;
        }else{
        		$idventa = $id+1;
        }

        $sqlid2 = "SELECT nrofactura from ventas_cab where id_caja=$nrocaja
           order by nrofactura desc LIMIT 1";
        $resid2 = mysqli_query($conexion,$sqlid2);

        foreach ($resid2 as $rowid2) {

        	$nro = $rowid2["nrofactura"];
        	
        }

        if (isset($nro)=="") {
        		$nrofactura = 1;
        }else{
        		$nrofactura = $nro+1;
        }

        mysqli_autocommit($conexion,FALSE);

		$query = "INSERT INTO ventas_cab (idventa, sucursal, id_caja, nrofactura, cliente, documento, nombre_ocacional,tipo_comprobante ,tipofactura ,fecha, fecha_alta, hora, total_comprobante, total_iva, total_gravada5, total_gravada10, total_exenta, total_comprobante_c_iva, lista_precio, usuario, idhabilitacion) 
		VALUES ('$idventa', '$sucursal', '$nrocaja', '$nrofactura', '$idcliente', '$documento', '$nombre_ocacional','$tipo_comprobante', '$tipofactura', '$fecha', '$fecha_alta', '$hora', '$total_comprobante', '$total_iva', '$total_gravada5', '$total_gravada10', '$total_exenta', '$total_comprobante_c_iva', '$idlista', '$usuario', '$idhabilitacion');";
			
		 
		 $resultado = mysqli_query($conexion, $query);	
			
		verificar_resultado($resultado);

		$pedido = "UPDATE pedido set estado=4 where idpedido='$idpedido'";
		$respedido = mysqli_query($conexion, $pedido);

		$querycre = "INSERT INTO credito_venta (venta, cliente, fecha_credito, total, saldo) 
				VALUES ('$idventa', '$idcliente', '$fecha', '$total_comprobante_c_iva', '$total_comprobante_c_iva');";
		$rescre = mysqli_query($conexion, $querycre);

		if ($tipofactura==1) {
				$sql = "SELECT contado from habilitaciones where idhabilitacion='$idhabilitacion'";
								$res = mysqli_query($conexion, $sql);
						
								foreach ($res as $row) {
									$contado = $row['contado'];
						        }

						        $entradasuma = $contado+$total_comprobante_c_iva;

					$query = "UPDATE habilitaciones set contado='$entradasuma' where idhabilitacion='$idhabilitacion'";
					$resultado = mysqli_query($conexion, $query);

				}else{

					$meses = 0;
                    $mes = 0;
                    $sumano = 0;

                     for ($i=1; $i <= 1; $i++) { 

                            $dia = date('d', strtotime($fecha));
                            $meses = date('m', strtotime($fecha));
                            $ano = date('Y', strtotime($fecha));

                            $meses = $meses+$i;

                               if ($meses==12) {
                                  $fecha_ven = $ano."-".$meses."-".$dia."\n<br>";

                               }else if ($meses>12) {
                                  $mes = $mes+1;
                                  $sumano = $ano+1;
                                  $fecha_ven = $sumano."-".$mes."-".$dia."\n<br>";

                               }else if ($meses<12) {

                                  $fecha_ven = $ano."-".$meses."-".$dia."\n<br>";

                               }

                     }
				

					$sql = "SELECT credito from habilitaciones where idhabilitacion='$idhabilitacion'";
								$res = mysqli_query($conexion, $sql);
						
								foreach ($res as $row) {
									$credito = $row['credito'];
						        }

						        $entradacredito = $credito+$total_comprobante_c_iva;

						$query = "UPDATE habilitaciones set credito='$entradacredito' where idhabilitacion='$idhabilitacion'";
						$resultado = mysqli_query($conexion, $query);
				}

		$data = json_decode($_POST['articulos']);

		$sql = "SELECT idbodega from bodegas where sucursal=$sucursal and descbodega='SALON'";
		$res = mysqli_query($conexion, $sql);
		foreach ($res as $row) {
			$idbodega = $row["idbodega"];
		}

		foreach($data as $value){
			
			  $idarticulo = $value->idarticulo;
			  $cantidad = $value->cantidad;
			  $costo = $value->costo;
			  $precio = $value->precio;
			  $subtotal_c_iva = $value->subtotal;
			  $iva = $value->iva;

			  if ($idarticulo=='') {
			  	
			  }else{

			  if ($iva==10) {
			  	$totaliva = round($subtotal_c_iva/11);
			  	$total10 = $subtotal_c_iva-$totaliva;

			  	$query = "INSERT INTO ventas_det (idventa, articulo, precio, costo, cantidad, subtotal, total_iva, subtotal_c_iva, total_gravada10) 
				VALUES ('$idventa', '$idarticulo', '$precio', '$costo', '$cantidad', '$total10', '$totaliva', '$subtotal_c_iva', '$total10');";

			  }else if ($iva==5){
			  	$totaliva = round($subtotal_c_iva/21);
			  	$total5 = $subtotal_c_iva-$totaliva;

			  	$query = "INSERT INTO ventas_det (idventa, articulo, precio, costo, cantidad, subtotal, total_iva, subtotal_c_iva, total_gravada5) 
				VALUES ('$idventa', '$idarticulo', '$precio', '$costo', '$cantidad', '$total5', '$totaliva', '$subtotal_c_iva', '$total5');";

			  }else{
			  	$query = "INSERT INTO ventas_det (idventa, articulo, precio, costo, cantidad, subtotal, subtotal_c_iva, total_exenta) 
				VALUES ('$idventa', '$idarticulo', '$precio', '$costo', '$cantidad', '$subtotal_c_iva', '$subtotal_c_iva', '$subtotal_c_iva');";
			  }

				$resultado = mysqli_query($conexion, $query);	

				$sql = "SELECT bd.stock from articulos a
						join bodegas_det bd on bd.articulo=a.idarticulo
						join bodegas b on b.idbodega=bd.bodega
						where b.sucursal=$sucursal and b.idbodega=$idbodega and a.idarticulo=$idarticulo";
				
				echo $sql;
					$res = mysqli_query($conexion, $sql);
					foreach ($res as $row) {
			        	
			        	$existencia = $row['stock'];

			        }

			    $stock = $existencia-$cantidad;

				$query = "UPDATE articulos a join bodegas_det bd on bd.articulo=a.idarticulo
								join bodegas b on b.idbodega=bd.bodega
								set bd.stock=$stock
								where b.sucursal=$sucursal and b.idbodega=$idbodega and bd.articulo=$idarticulo";
				$resultado = mysqli_query($conexion, $query);

				}
			}

			if (!mysqli_commit($conexion)) {
				  echo "Commit transaction failed";
				  exit();
				}else{
				}
		break;

		case 'anular':

						$idventa = $_POST["idventa"];	

						$sql = "UPDATE ventas_cab set estado = 1 where idventa='$idventa'";
						$res = mysqli_query($conexion, $sql);

						$sql = "SELECT * from ventas_cab where idventa = '$idventa'";
						$res = mysqli_query($conexion, $sql);
						$venta = mysqli_fetch_array($res);
						$idsucursal = $venta['sucursal'];

						$sql = "SELECT idbodega from bodegas where sucursal=$idsucursal and descbodega='SALON'";
							$res = mysqli_query($conexion, $sql);
							foreach ($res as $row) {
								$idbodega = $row["idbodega"];
							}

						$sql = "SELECT * from ventas_det where idventa=$idventa";
						$res = mysqli_query($conexion,$sql);

						foreach ($res as $row) {
							$idarticulo = $row["articulo"];
							$cantidad = $row["cantidad"];

							$sql = "SELECT bd.stock from articulos a
								join bodegas_det bd on bd.articulo=a.idarticulo
								join bodegas b on b.idbodega=bd.bodega
								where b.sucursal=$idsucursal and b.idbodega=$idbodega and a.idarticulo=$idarticulo";
							$res = mysqli_query($conexion, $sql);
							foreach ($res as $row) {
					        		$existencia = $row['stock'];
					        }

					    	$stock = $existencia+$cantidad;

							$sqlstock = "UPDATE articulos a join bodegas_det bd on bd.articulo=a.idarticulo
								join bodegas b on b.idbodega=bd.bodega
								set bd.stock=$stock
								where b.sucursal=$idsucursal and b.idbodega=$idbodega and bd.articulo=$idarticulo";
							$resstock = mysqli_query($conexion, $sqlstock);

						}

						verificar_resultado2($res);
				
			break;
		default:
			$informacion["respuesta"] = "OPCION_VACIA";
			echo json_encode($informacion);
			break;
	}


	function verificar_resultado($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "BIEN";
			
			echo json_encode($informacion);
		}

		function verificar_resultado2($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "ELIMINADO";
			
			echo json_encode($informacion);
		}

	function cerrar($conexion){
		mysqli_close($conexion);
	}
