<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");

    date_default_timezone_set('America/Asuncion');

     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

    
    include ("conexion.php");
    include ("./seguridad/formatos.php");
    
    $total = 0;
    $hoy=date("Y-m-d");

    $usuario = $_SESSION['nombreUsuario'];

    $sqlusuario = "SELECT * from usuarios where usuario = '$usuario'";
    $resusuario = mysqli_query($conexion, $sqlusuario);
    while($row=mysqli_fetch_array($resusuario)) {
      $idusuario= $row['idusuario'];
    }   

    $sql = "SELECT max(idhabilitacion) as idcaja from habilitaciones
     where usuario='$idusuario' and estado=0";
    $res = mysqli_query($conexion, $sql);
    while($row=mysqli_fetch_array($res)) {
      $idcaja= $row['idcaja'];
    }

    $query = "SELECT sum(vd.subtotal_c_iva) as subtotales,vc.fecha,vc.hora,vc.estado,vc.total_comprobante_c_iva from ventas_det vd 
      join ventas_cab vc on vc.idventa=vd.idventa 
      where vc.tipofactura=1 and vc.estado=0 and vc.idhabilitacion=$idcaja";
    $resul = mysqli_query($conexion, $query);
    while($fila=mysqli_fetch_array($resul)) {
      $contado = $fila['subtotales'];
    }

    $query = "SELECT sum(vd.subtotal_c_iva) as subtotales,vc.fecha,vc.hora,vc.estado,vc.total_comprobante_c_iva from ventas_det vd 
      join ventas_cab vc on vc.idventa=vd.idventa 
      where vc.tipofactura=2 and vc.estado=0 and vc.idhabilitacion=$idcaja";
    $resul = mysqli_query($conexion, $query);
    while($fila=mysqli_fetch_array($resul)) {
      $credito = $fila['subtotales'];
    }

    $query = "UPDATE caja set entrada=$contado, credito = $credito where idcaja=$idcaja";
    $resultado = mysqli_query($conexion, $query);

?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
             
            $nivel = $_SESSION['nivel'];

            if ($nivel==1) {
              include("navbar.php");
            }else{
              include("navbarcajero.php");
            }

        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content"><br>

        <div class="card-body">
          <!-- Your Page Content Here -->
        <div class='row'>
          <div class="card mb-3">
            <div class='col-md-10'><br>
                <div class='input-group imprimir'>
                <input type='hidden' id='usuario' class='form-control' style="text-align: center;" value="<?php echo $idusuario?>" disabled=""> 
              </div>
               <div class='input-group'>
                <input type='hidden' id='totalcaja' class='form-control' style="text-align: center;" 
                value="<?php echo $contado; ?>">
                <input type='hidden' id='idcaja' class='form-control' style="text-align: center;" 
                value="<?php echo $idcaja; ?>">
              </div>
              <div class='box-header with-border'><PRE>Billetes           Cantidad           Subtotal</PRE></div><br>
              <div class='input-group'>
              <span class='input-group-addon'>100.000:</span>
              <input type='text' id='bill1' class='form-control' value="" style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'bill2')">
              <input type='text' id='sub1' class='form-control' style="text-align: center;" value="0" readonly="readonly">
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>50.000:</span>
              <input type='text' id='bill2' class='form-control'  style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'bill3')">
              <input type='text' id='sub2' class='form-control' style="text-align: center;" value="0" readonly="readonly">
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>20.000:</span>
              <input type='text' id='bill3' class='form-control'  style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'bill4')">
              <input type='text' id='sub3' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>10.000:</span>
              <input type='text' id='bill4' class='form-control'  style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'bill5')">
              <input type='text' id='sub4' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>5.000:</span>
              <input type='text' id='bill5' class='form-control'  style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'bill6')">
              <input type='text' id='sub5' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>2.000:</span>
              <input type='text' id='bill6' class='form-control'  style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'mon1')">
              <input type='text' id='sub6' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
            </div>
          </div>
          <div class="card mb-3">
            <div class='col-md-10'><br>
              <div class='box-header with-border'><PRE>Monedas        Cantidad        Subtotal</PRE></div><br>
               <div class='input-group'>
              <span class='input-group-addon'>1000:</span>
              <input type='text' id='mon1'  class='form-control' style="text-align: center;" onkeyup="calcula_sub();" onkeypress="saltar(event,'mon2')">
              <input type='text' id='sub7' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>500:</span>
              <input type='text' id='mon2' class='form-control' style="text-align: center;"  onkeyup="calcula_sub();" onkeypress="saltar(event,'mon3')">
              <input type='text' id='sub8' class='form-control' style="text-align: center;" value="0"  readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>100:</span>
              <input type='text' id='mon3' class='form-control' style="text-align: center;"  onkeyup="calcula_sub();" onkeypress="saltar(event,'mon4')">
              <input type='text' id='sub9' class='form-control' style="text-align: center;" value="0"  readonly="readonly" >
              </div>
              <br>
               <div class='input-group'>
              <span class='input-group-addon'>50:</span>
              <input type='text' id='mon4' class='form-control' style="text-align: center;"  onkeyup="calcula_sub();" onkeypress="saltar(event,'mostrar')">
              <input type='text' id='sub10' class='form-control' style="text-align: center;" value="0"  readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>Total Billetes:</span>
              <input type='text' id='totalb' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>Total Monedas:</span>
              <input type='text' id='totalm' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <div class='input-group'>
              <span class='input-group-addon'>Total en caja:</span>
              <input type='text' id='total' class='form-control' style="text-align: center;" value="0" readonly="readonly" >
              </div>
              <br>
              <button class='btn btn-success btn-md' onclick="procesarcierre();" id="mostrar"><i class='fa fa fa-check'></i> Guardar</button>
              
            </div><br>
            </div><br>
          </div>
        </div>

        </div>     

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

$(document).on("ready", function(){
    $("#bill1").focus();
    })

    function calcula_sub(){
     var m1=$("#bill1").val();
     var m2=$("#bill2").val();
     var m3=$("#bill3").val();
     var m4=$("#bill4").val();
     var m5=$("#bill5").val();
     var m6=$("#bill6").val();
     var m7=$("#mon1").val();
     var m8=$("#mon2").val();
     var m9=$("#mon3").val();
     var m10=$("#mon4").val();
     var change1=m1*100000;
     var change2=m2*50000;
     var change3=m3*20000;
     var change4=m4*10000;
     var change5=m5*5000;
     var change6=m6*2000;
     var totalb = change1+change2+change3+change4+change5+change6;
     var change7=m7*1000;
     var change8=m8*500;
     var change9=m9*100;
     var change10=m10*50;
     var totalm = change7+change8+change9+change10;
     var total = totalb+totalm;
     $("#sub1").val("Gs. "+change1);
     $("#sub2").val("Gs. "+change2);
     $("#sub3").val("Gs. "+change3);
     $("#sub4").val("Gs. "+change4);
     $("#sub5").val("Gs. "+change5);
     $("#sub6").val("Gs. "+change6);
     $("#totalb").val("Gs. "+parseInt(totalb).toLocaleString());
     $("#sub7").val("Gs. "+change7);
     $("#sub8").val("Gs. "+change8);
     $("#sub9").val("Gs. "+change9);
     $("#sub10").val("Gs. "+change10);
     $("#totalm").val("Gs. "+parseInt(totalm).toLocaleString());
     $("#total").val(parseInt(total).toLocaleString());
     }

     function procesarcierre(){
        var usuario = $("#usuario").val();
        var idcaja = $("#idcaja").val();
        var totalsistema= $("#totalcaja").val();
        var totalcajafisica = $("#total").val().replace(/[^\d]/g, '');
        var contrasena = $("#contrasena").val();
        var opcion = 'registrar';
        $.ajax({
          method: "POST",
          url: "guardarCierre.php",
          data: 'totalcajafisica='+totalcajafisica+'&totalsistema='+totalsistema+'&usuario='+usuario+'&idcaja='+idcaja+'&opcion='+opcion
        }).done( function( info ){   
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
        });
    }

 var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "CERRADO" ){
          swal({
              title: 'Atención!',
              text: 'Ninguna caja abierta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "AUTORIZADO"){
          swal({
              title: 'Atención!',
              text: 'No esta autorizado!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

     function saltar(e,id){
      (e.keyCode)?k=e.keyCode:k=e.which;
      if(k==13){
          document.getElementById(id).focus();
        }
    }


    
</script>
