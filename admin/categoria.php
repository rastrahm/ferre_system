
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");

?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
            <div class="container-fluid"><br>
                <div class="card-body">
                  <div class="row"> 
                    <div class="col-md-5">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Categoria</strong> 
                                    </div>
                               
                                <form name="formulario" id="guardarDatos" method="post" class="form-horizontal">
                                    <div class="card-body card-block">
                                      
                                          <input type="hidden" id="idcategoria" name="idcategoria" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                          <div class="row">
                                             <div class="col-md-8">
                                                    <label for="text-input" class=" form-control-label">Descripcion</label>
                                                   <input type="text" id="nombrecla" name="nombrecla" class="form-control">
                                            </div>
                                             <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Porcentaje</label>
                                                   <input type="text" id="porcentaje" name="porcentaje" class="form-control">
                                            </div>
                                            </div>
                                            <br>
                                            
                                        
                                    </div> 
                                    <div class="card-footer" align="right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            
                                            </div>
                                   </form>
                                 </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablacategoria" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th>ID</th>            
                                            <th>CATEGORIA</th>
                                            <th>PORCENTAJE</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>
                            </div>
                              </div>

                               

                            </div>
        </div>     
            
           <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idcategoria" name="idcategoria" value="">
              <input type="hidden" id="opcion" name="opcion" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Categoria</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar la categoria?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/categoria.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

  $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarCategoria.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablacategoria').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "VACIO" ){
          swal({
              title: 'Atención!',
              text: 'No se cargo ninguna imagen',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idcategoria = $("#frmEliminarArticulo #idcategoria").val(),
          opcion = $("#frmEliminarArticulo #opcion").val();
        $.ajax({
          method:"POST",
          url: "guardarCategoria.php",
          data: {"idcategoria": idcategoria, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablacategoria').DataTable().ajax.reload();
        });
      });
    }

  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#nombrecla").val("");
      $("#porcentaje").val("");
    }

  var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idclasificacion = $("#idcategoria").val( data.idclasificacion ),
            nombrecla = $("#nombrecla").val( data.nombrecla),
            porcentaje = $("#porcentaje").val( data.porc_ganancia),
            opcion = $("#opcion").val("modificar");
            window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idclasificacion = $("#frmEliminarArticulo #idcategoria").val( data.idclasificacion );
      });
    }

    
</script>
