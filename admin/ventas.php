<!DOCTYPE html>
<html lang="en">

<?php 
    include("header.php");
?>

<style type="text/css">
.table-wrapper {
    width: 100%;
    height: 500px;
    /* Altura de ejemplo */
    overflow: auto;
}

.table-wrapper table {
    border-collapse: separate;
    border-spacing: 0;
}

.table-wrapper table thead {
    position: -webkit-sticky;
    /* Safari... */
    position: sticky;
    top: 0;
    left: 0;
}

.table-wrapper table thead th,
.table-wrapper table tbody td {
    border: 1px solid #000;
    /*background-color: #000aff;*/
}

.derecha {
    float: right;
}
</style>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">

        <?php
            include ("conexion.php");
            include ("./seguridad/formatos.php");
            include ("buscar_articulo.php");
            include ("buscar_cliente.php");
            include("navbar.php");

            $usuario = $_SESSION['nombreUsuario'];
            $idusuario = $_SESSION['idusuario'];
            $idsucursal = $_SESSION['idsucursal'];
            $nrocaja = $_SESSION['nrocaja'];

            $sql = "SELECT serie_timbrado from cajas where nro_caja=$nrocaja order by fec_fin desc limit 1";
            $res = mysqli_query($conexion,$sql);

            foreach ($res as $row) {
                $serie_timbrado = $row["serie_timbrado"];
            }

            $sql = "SELECT max(idhabilitacion) as idhabilitacion from habilitaciones where usuario=$idusuario and estado=0";
            $res = mysqli_query($conexion, $sql);
            while($row=mysqli_fetch_array($res)) {
              $idhabilitacion= $row['idhabilitacion'];
            }

            if (isset($_GET["idpedido"])=='') {
              $idpedido = "";
            }else{
              $idpedido = $_GET["idpedido"];
            }

            


        ?>

        <!-- PAGE CONTAINER-->
        <div class="page-content">
            <!-- STATISTIC-->
            <div class="container-fluid" align="center">
                <!-- HEADER DESKTOP-->
                <div class="card-body">
                    <div class="card">
                        <div class="card-header">
                            <strong>Ventas</strong>
                            <button class="btn btn-primary" onclick="consultar();"><i class="fa fa-search"></i>
                                Consultar</button>
                            <button class="btn btn-success" id="botonreimprimir" onclick="reimprimir_factura();"
                                disabled=""><i class="fa fa-print"></i> Reimpresion</button>
                            <button class="btn btn-warning" id="botonanular" onclick="anular();" disabled=""><i
                                    class="fa fa-ban"></i> Anular</button>
                            <!--<button class="btn btn-danger" id="botoneliminar" onclick="eliminar()" disabled=""><i class="fa fa-trash"></i> Eliminar</button>-->
                        </div>
                        <div class="card-body card-block">
                            <div class="row">
                                <input type="hidden" name="opcion" id="opcion" value="nuevo">
                                <input type="hidden" name="idventa" id="idventa">
                                <input type="hidden" name="idhabilitacion" id="idhabilitacion"
                                    value="<?php echo $idhabilitacion ?>">
                                <input type="hidden" name="usuario" id="usuario" value="<?php echo $idusuario ?>">
                                <input type="hidden" name="nrocaja" id="nrocaja" value="<?php echo $nrocaja ?>">
                                <input type="hidden" name="stock" id="stock">
                                <div class="col-md-1">
                                    <label for="pago">Nro Pedido</label>
                                    <input type="text" class="form-control" id="idpedido" name="idpedido"
                                        onchange="buscar_pedido()" value="<?php echo $idpedido; ?>" disabled>
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">FACTURA</label>
                                    <select class="form-control" name="tipo_comprobante" id="tipo_comprobante" onchange="nota_credito()" class="form-control" required="">
                                        <option value="FAC">VENTA</option>
                                        <option value="NC">NOTA DE CREDITO</option>
                                    </select>
                                </div>
                                <div class='col-md-2'>
                                    <label>Sucursal:</label>
                                    <select id="sucursal" name="sucursal" class="form-control" disabled="">
                                        <?php
                                                    $res = mysqli_query($conexion, "SELECT * FROM sucursal where idsucursal=$idsucursal");

                                                    foreach ($res as $row) {
                                                      echo "<option value='".$row["idsucursal"]."'> ".$row["desc_suc"]."</option>";
                                                    }
                                                   

                                                  ?>

                                    </select><br>
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">Serie / Nro Factura</label>
                                    <div class='input-group'>
                                        <span class='input-group-addon'><?php echo $serie_timbrado;  ?></span>
                                        <input type='number' id='nrofactura' name="nrofactura" class='form-control'
                                            onchange="buscar_factura()" disabled="">
                                    </div><br>
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">Cliente</label>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" onclick="busqueda_cli()"><i
                                                    class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <input type="text" class="form-control" onchange="busca_cliente();"
                                            name="codigo" id="codigo" style="text-align:center;" value="1" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="text-input" class=" form-control-label">Razon Social</label>
                                    <input type="text" name="razonsocial" id="razonsocial" class="form-control"
                                        value="GENERICO" disabled="" style="text-align:center;">
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">Lista de Precio</label>
                                    <select class="form-control" name="lista" id="lista" class="form-control">
                                        <?php
                                                          $sql = "SELECT * from precio_lista";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idlista"]."'>".$row["descripcion"]."</option>";
                                                          }
                                                      ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">Condicion</label>
                                    <select class="form-control" name="tipofactura" id="tipofactura"
                                        class="form-control" required="">
                                        <option>Seleccione una opcion</option>
                                        <?php
                                                          $sql = "SELECT * from tipo_factura";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idtipo"]."'>".$row["factura"]."</option>";
                                                          }
                                                      ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="pago">Fecha</label>
                                    <input type="date" class="form-control" id="fecha" name="fecha"
                                        value="<?php echo date('Y-m-d'); ?>">
                                </div>
                                <div class="col-md-2">
                                    <label for="pago">Fecha alta</label>
                                    <input type="date" class="form-control" id="fecha_alta" name="fecha_alta"
                                        value="<?php echo date('Y-m-d'); ?>" disabled="">
                                </div>
                                <div class="col-md-2" align="center">
                                    <h3><label for="textarea-input" class=" form-control-label">Total</label></h3>
                                    <input type="text" id="total" name="total" class="form-control"
                                        onkeyup="format(this);" onchange="format(this);" readonly="readonly"
                                        style="font-size:30px; text-align:center; color:red; font-weight: bold;">
                                </div>

                                <div class="col-md-1">
                                    <label for="pago">Total Iva</label>
                                    <input type="text" class="form-control" id="totaliva" name="totaliva" disabled="">
                                </div>
                                <div class="col-md-1">
                                    <label for="pago">Total Exenta</label>
                                    <input type="text" class="form-control" id="totalexenta" name="totalexenta"
                                        disabled="">
                                </div>
                                <div class="col-md-1">
                                    <label for="pago">Total 5%</label>
                                    <input type="text" class="form-control" id="total5" name="total5" disabled="">
                                </div>
                                <div class="col-md-1">
                                    <label for="pago">Total 10%</label>
                                    <input type="text" class="form-control" id="total10" name="total10" disabled="">
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">Documento</label>

                                    <input type="text" class="form-control" name="documento" id="documento">
                                </div>
                                <div class="col-md-3">
                                    <label for="text-input" class=" form-control-label">Nombre Ocacional</label>
                                    <input type="text" name="nombreocacional" id="nombreocacional" class="form-control">
                                </div>

                            </div><br>

                        </div>
                        <div class="card-footer" align="right">

                            <button type="button" id="guardar" disabled class="btn btn-success"
                                onclick="procesa_venta();"><i class="fa fa-check"></i> Guardar
                            </button>
                        </div>
                    </div>

                    <div align="center">
                        <div class="col-md-10">
                            <div class="table-wrapper table-responsive table--no-card m-b-30">
                                <table class="table table-borderless table-striped table-earning" id="tabla_articulos"
                                    width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th class='center'>Codigo</th>
                                            <th class='center'>Articulo</th>
                                            <th class='center'>Cantidad</th>
                                            <th class='center'>Precio</th>
                                            <th class='center'>Subtotal</th>
                                            <th class='center'>Iva</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <?php  
            include("footer.php");
        ?>
                <!-- END PAGE CONTAINER-->
            </div>
        </div>
    </div>

    <?php  include ("seguridad.php") ?>
    <?php  
    include("scripts.php");
   ?>

</body>

</html>
<!-- end document-->
<script type="text/javascript">
$(document).on("ready", function() {
    nuevo();
    buscar_pedido();
});

function nuevo() {

    limpiar_campos();
    tabla();
    /*$("#tabla_articulos>tbody").append(
        '<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
        '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
        'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" onkeypress="saltar(event)" onkeyup="cal_sub()" size="5"' +
        '></td><td class="costo" style="display:none"><input type="text" class="form-control" name="cos" id="cos" size="6"' +
        '></td><td class="precio" style="width:200px"><input type="text" class="form-control" name="pre" id="pre" size="6"' +
        '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"' +
        'disabled></td><td class="iva" style="width:150px"><input type="text" class="form-control" name="iva" id="iva" size="5"' +
        'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>'
    );*/


}

function buscar_pedido() {
    if ($("#idpedido").val() == '') {

    } else {

        $("#tabla_articulos > tbody:last").children().remove();
        $.ajax({
            url: 'busca_data_pedido.php',
            dataType: 'json',
            type: 'POST',
            data: 'idpedido=' + $("#idpedido").val(),
            success: function(data) {

                if (data == 0) {
                    alertify.error("No existe el numero de pedido!!!");
                } else {

                    $("#codigo").val(data[0].cliente);
                    $("#razonsocial").val(data[0].razonsocial);
                    $("#tipofactura").val("1");
                    $("#guardar").attr("disabled", false);

                    for (var i = 0; i < data.length; i++) {
                        $("#tabla_articulos > tbody").append("<tr><td class='idarticulo'>" + data[i]
                            .articulo +
                            "</td><td class='nombreart'>" + data[i].nombreart +
                            "</td><td class='cantidad'>" + data[i].cantidad +
                            "</td><td class='costo' style='display:none'>" + data[i].costo +
                            "</td><td class='precio'>" + parseInt(data[i].precio).toLocaleString() +
                            "</td><td class='subtotal'>" + parseInt(data[i].subtotal).toLocaleString() +
                            "</td><td class='iva'>" + data[i].iva +
                            "</td></tr>");
                    }
                    resumen2()

                }

            },
        });

    }

}

function buscar_factura() {

    $.ajax({
        url: 'busca_data_venta.php',
        dataType: 'json',
        type: 'POST',
        data: 'nrofactura=' + $("#nrofactura").val(),
        success: function(data) {

            if (data == 0) {
                alertify.error("No existe el numero de comprobante!!!");
            } else {

                $("#guardar").attr("disabled", true);

                $("#idventa").val(data[0].idventa);
                $("#usuario").val(data[0].usuario);
                $("#nrocaja").val(data[0].id_caja);
                //$("#idhabilitacion").val(data[0].idhabilitacion);
                $("#lista").val(data[0].lista_precio).trigger('change');
                $("#sucursal").val(data[0].sucursal);
                $("#total").val(parseInt(data[0].total_comprobante_c_iva).toLocaleString());
                $("#fecha").val(data[0].fecha);
                $("#fecha_alta").val(data[0].fecha_alta);
                $("#nrofactura").val(data[0].nrofactura);
                $("#cliente").val(data[0].cliente).trigger('change');
                $("#tipofactura").val(data[0].tipofactura).trigger('change');                
                $("#documento").val(data[0].documento);
                $("#nombreocacional").val(data[0].nombre_ocacional);
                $("#totaliva").val(parseInt(data[0].total_iva).toLocaleString());
                $("#total10").val(parseInt(data[0].total_gravada10).toLocaleString());
                $("#total5").val(parseInt(data[0].total_gravada5).toLocaleString());
                $("#totalexenta").val(parseInt(data[0].total_exenta).toLocaleString());
                var tipo_comprobante = $("#tipo_comprobante").val();

                if (tipo_comprobante == 'NC') {
                    $("#totaliva").val('');
                    $("#total10").val('');
                    $("#total5").val('');
                    $("#totalexenta").val('');
                    $("#total").val('');
                    $("#documento").val('');
                    $("#nombreocacional").val('');
                    $("#lista").val(data[0].lista_precio).trigger('change');
                    //console.log('es nota de credito');

                } else {
                    $("#tabla_articulos > tbody:last").children().remove();
                    for (var i = 0; i < data.length; i++) {
                        $("#tabla_articulos > tbody").append("<tr><td class='idarticulo'>" + data[i]
                            .articulo +
                            "</td><td class='nombreart'>" + data[i].nombreart +
                            "</td><td class='cantidad'>" + data[i].cantidad +
                            "</td><td class='precio'>" + parseInt(data[i].precio).toLocaleString() +
                            "</td><td class='subtotal'>" + parseInt(data[i].subtotal_c_iva)
                            .toLocaleString() +
                            "</td><td class='iva'>" + data[i].iva +
                            "</td></tr>");
                    }
                }


            }
        },
    });
}

function tabla() {
    $("#tabla_articulos>tbody").append(
        '<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
        '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
        'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" onkeypress="saltar(event)" onkeyup="cal_sub()" size="5"' +
        '></td><td class="costo" style="display:none"><input type="text" class="form-control" name="cos" id="cos" size="6"' +
        '></td><td class="precio" style="width:200px"><input type="text" class="form-control" name="pre" id="pre" size="6"' +
        '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"' +
        'disabled></td><td class="iva" style="width:150px"><input type="text" class="form-control" name="iva" id="iva" size="5"' +
        'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>'
    );
    //  idarticulo
    // $("#idarticulo").attr("display", 'none');
    $("#idart").focus();
}

//funcion para agg nota de credito
function nota_credito() {
    console.log(1);
    $("#nrofactura").attr("disabled", false);
    $("#codigo").attr("disabled", true);
    //tabla()
}

function consultar() {
    $("#nrofactura").attr("disabled", false);
    $("#nrofactura").val("");
    $("#botonreimprimir").attr("disabled", false);
    $("#botonanular").attr("disabled", false);
    $("#nrofactura").focus();
}

function agregar() {

    var idarticulo = $("#idart").val();
    var nombreart = $("#nom").val();
    var cantidad = parseInt($("#cant").val());
    var costo = $("#cos").val().replace(/(?!-)[^\d]/g, '');
    var precio = $("#pre").val().replace(/(?!-)[^\d]/g, '');
    var subtotal = $("#sub").val().replace(/(?!-)[^\d]/g, '');
    var iva = $("#iva").val();

    $("#tabla_articulos > tbody").prepend(
        "<tr style='font-weight:bold;color:black;font-size:20px;text-align:center'><td        class='idarticulo'>" +
        idarticulo +
        "</td><td class='nombreart'>" + nombreart +
        "</td><td class='cantidad'>" + cantidad +
        "</td><td class='costo' style='display:none'>" + costo +
        "</td><td class='precio'>" + parseInt(precio).toLocaleString() +
        "</td><td class='subtotal'>" + parseInt(subtotal).toLocaleString() +
        "</td><td class='iva'>" + iva +
        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");

    resumen();
    agregar_fila();

}

function agregar_fila() {

    $("#tabla_articulos>tbody").prepend(
        '<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
        '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
        'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" onkeypress="saltar(event)" onkeyup="cal_sub()" size="5"' +
        '></td><td class="costo" style="display:none"><input type="text" class="form-control" name="cos" id="cos" size="6"' +
        '></td><td class="precio" style="width:200px"><input type="text" class="form-control" name="pre" id="pre" size="6"' +
        '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"' +
        'disabled></td><td class="iva" style="width:150px"><input type="text" class="form-control" name="iva" id="iva" size="5"' +
        'disabled></td><td><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>'
    );

    $("#idart").focus();

}

function resumen2() {
    var monto = 0;
    var iva = 0;
    var sum10 = 0;
    var sum5 = 0;
    var totaliva10 = 0;
    var totaliva5 = 0;
    var total10 = 0;
    var total5 = 0;
    $('#tabla_articulos > tbody > tr').each(function() {
        monto += parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
        subtotal = parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
        iva = parseInt($(this).find('td').eq(6).html());

        if (iva == 10) {
            sum10 = Math.round(subtotal / 11);
            total10 += sum10;
            totaliva10 += Math.round(subtotal - sum10);
        } else if (iva == 5) {
            sum5 = Math.round(subtotal / 21);
            total5 += sum5;
            totaliva5 += Math.round(subtotal - sum5);
        }

    });

    $("#total").val(parseInt(monto).toLocaleString());
    $("#totaliva").val(parseInt(total5 + total10).toLocaleString());
    $("#total10").val(parseInt(totaliva10).toLocaleString());
    $("#total5").val(parseInt(totaliva5).toLocaleString());
}

function resumen() {
    var monto = 0;
    var iva = 0;
    var sum10 = 0;
    var sum5 = 0;
    var totaliva10 = 0;
    var totaliva5 = 0;
    var total10 = 0;
    var total5 = 0;
    var articulo = 0;
    var cont = 0;
    $('#tabla_articulos > tbody > tr').each(function() {
        cont++;
        monto += parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
        //articulo += parseInt($(this).find('td').eq(2).html().replace(/(?!-)[^\d]/g, ''));
        subtotal = parseInt($(this).find('td').eq(5).html().replace(/(?!-)[^\d]/g, ''));
        iva = parseInt($(this).find('td').eq(6).html());

        console.log(cont)
        if (cont > 0) {
            $("#guardar").attr("disabled", false);
        } else {
            $("#guardar").attr("disabled", true);
        }

        if (iva == 10) {
            sum10 = Math.round(subtotal / 11);
            total10 += sum10;
            totaliva10 += Math.round(subtotal - sum10);
        } else if (iva == 5) {
            sum5 = Math.round(subtotal / 21);
            total5 += sum5;
            totaliva5 += Math.round(subtotal - sum5);
        }

    });

    $("#total").val(parseInt(monto + 5).toLocaleString());
    $("#totaliva").val(parseInt(total5 + total10).toLocaleString());
    $("#total10").val(parseInt(totaliva10).toLocaleString());
    $("#total5").val(parseInt(totaliva5).toLocaleString());
}


function saltar(e) {
    (e.keyCode) ? k = e.keyCode: k = e.which;
    if (k == 13) {
        $("#confirmar").focus();
    }
}

function saltar2(e, id) {
    (e.keyCode) ? k = e.keyCode: k = e.which;
    if (k == 13) {
        document.getElementById(id).focus();
    }
}

function cal_sub() {
    var cantidad = $("#cant").val();
    var stock = parseInt($("#stock").val());
    var precio = $("#pre").val().replace(/[^\d]/g, '');
    $("#sub").val(parseInt(cantidad * precio).toLocaleString());
    var tipo_comprobante = $("#tipo_comprobante").val();

    if (tipo_comprobante == 'NC') {

    } else {
        if (cantidad > stock) {

            alertify.error("No hay existencia suficiente para esta venta!");

            var aux = cantidad - stock;

            $("#cant").val(cantidad - aux);

            $("#sub").val(parseInt($("#cant").val() * precio).toLocaleString());

        } else {

        }
    }


}

function busca_articulo() {
    $(document).ready(function() {
        $.ajax({
            url: 'busca_data_articulo_ven.php',
            dataType: 'json',
            type: 'POST',
            data: 'idarticulo=' + $("#idart").val() + '&sucursal=' + $("#sucursal").val() + '&lista=' +
                $("#lista").val(),
            success: function(data) {
                if (data == 0) {
                    alertify.error("No existe el articulo!!!");
                    $("#idart").val("");
                    $("#idart").focus();
                } else {
                    $("#cant").focus();
                    $("#nom").val(data[0].nombreart);
                    $("#stock").val(data[0].stock);
                    $("#cos").val(data[0].costo);
                    $("#pre").val(parseInt(data[0].precio).toLocaleString());
                    $("#iva").val(data[0].iva);
                }
            },
        });
    });
}

function busqueda_art() {
    $("#modal_busqueda_arts").modal("show");
    $('#modal_busqueda_arts').on('shown.bs.modal', function() {
        $("#lista_articulos").html("");
        $("#articulo_buscar").val("");
        $("#articulo_buscar").focus();
        busca();
    });
}


function busca() {
    $.ajax({
        beforeSend: function() {
            $("#lista_articulos").html("");
        },
        url: 'busca_articulos_ayuda_ven.php',
        type: 'POST',
        data: 'articulo=' + $("#articulo_buscar").val(),
        success: function(x) {
            $("#lista_articulos").html(x);
        },
        error: function(jqXHR, estado, error) {
            $("#lista_articulos").html("Error en la peticion AJAX..." + estado + "      " + error);
        }
    });
}


function add_art(art) {
    //alert(art);
    $("#modal_busqueda_arts").modal("toggle");
    $("#idart").val(art);
    busca_articulo();
}

function busca_cliente() {
    $.ajax({
        url: 'busca_data_cliente.php',
        dataType: 'json',
        type: 'POST',
        data: 'idcliente=' + $("#codigo").val(),
        success: function(data) {
            if (data == 0) {
                alertify.error("No existe el cliente!!!");
                $("#codigo").val("");
                $("#codigo").focus();
            } else {
                $("#codigo").val(data[0].idcliente);
                $("#razonsocial").val(data[0].razonsocial);
            }
        },
    });
}

function busqueda_cli() {
    $("#modal_tabla_clientes").modal("show");
    $('#modal_tabla_clientes').on('shown.bs.modal', function() {
        $("#lista_clientes").html("");
        $("#cliente_buscar").val("");
        $("#cliente_buscar").focus();
        busca_c();
    });
}

function busca_c() {
    $.ajax({
        beforeSend: function() {
            $("#lista_clientes").html("");
        },
        url: 'busca_clientes_ayuda.php',
        type: 'POST',
        data: 'cliente=' + $("#cliente_buscar").val(),
        success: function(x) {
            $("#lista_clientes").html(x);
        },
        error: function(jqXHR, estado, error) {
            $("#lista_clientes").html("Error en la peticion AJAX..." + estado + "      " + error);
        }
    });
}
/*********************************************************************************************/
function add_cli(cli) {
    //alert(art);
    $("#modal_tabla_clientes").modal("toggle");
    $("#codigo").val(cli);
    busca_cliente();
}

$(function() {
    // Evento que selecciona la fila y la elimina
    $(document).on("click", ".delete", function() {
        var parent = $(this).parents().parents().get(0);
        $(parent).remove();
        resumen();
    });
});

$(document).ready(function() {
    $('.cliente').select2();
    $('.lista').select2();
});

function format(input) {
    var num = input.value.replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        input.value = num;
    } else {
        alertify.error('Solo se permiten numeros!');
        input.value = input.value.replace(/[^\d\.]*/g, '');
    }
}


function procesa_venta() {

    var idpedido = $("#idpedido").val();
    var idventa = $("#idventa").val();
    var tipofactura = $("#tipofactura").val();
    var tipo_comprobante = $("#tipo_comprobante").val();
    var sucursal = $("#sucursal").val();
    var nrocaja = $("#nrocaja").val();
    var usuario = $("#usuario").val();
    var idhabilitacion = $("#idhabilitacion").val();
    var idlista = $("#lista").val();
    var cliente = $("#codigo").val();
    var documento = $("#documento").val();
    var nombreocacional = $("#nombreocacional").val();

    var fecha = $("#fecha").val();
    var fecha_alta = $("#fecha_alta").val();
    var total = $("#total").val().replace(/[^\d]/g, '');
    var totaliva = $("#totaliva").val().replace(/[^\d]/g, '');
    var total10 = $("#total10").val().replace(/[^\d]/g, '');
    var total5 = $("#total5").val().replace(/[^\d]/g, '');
    var totalexenta = $("#totalexenta").val().replace(/[^\d]/g, '');
    var opcion = $("#opcion").val();

    let articulos = [];

    document.querySelectorAll('#tabla_articulos tbody tr').forEach(function(e) {
        let fila = {
            idarticulo: e.querySelector('.idarticulo').innerText,
            cantidad: e.querySelector('.cantidad').innerText,
            costo: e.querySelector('.costo').innerText.replace(/[^\d]/g, ''),
            precio: e.querySelector('.precio').innerText.replace(/[^\d]/g, ''),
            subtotal: e.querySelector('.subtotal').innerText.replace(/[^\d]/g, ''),
            iva: e.querySelector('.iva').innerText
        };
        articulos.push(fila);
    });

    if (tipofactura == 1) {

        $.ajax({
            url: 'guardarVenta.php',
            dataType: 'json',
            type: 'POST',
            data: {
                'articulos': JSON.stringify(articulos),
                'idpedido': idpedido,
                'idventa': idventa,
                'tipofactura': tipofactura,
                'tipo_comprobante': tipo_comprobante,
                'sucursal': sucursal,
                'cliente': cliente,
                'documento': documento,
                'nombreocacional': nombreocacional,
                'fecha': fecha,
                'fecha_alta': fecha_alta,
                'totaliva': totaliva,
                'total10': total10,
                'total5': total5,
                'totalexenta': totalexenta,
                'total': total,
                'nrocaja': nrocaja,
                'usuario': usuario,
                'lista_precio': idlista,
                'idhabilitacion': idhabilitacion,
                'opcion': opcion
            }
        }).done(function(info) {
            var json_info = JSON.parse(JSON.stringify(info));
            console.log(json_info);
            mostrar_mensaje(json_info);
            limpiar_campos();
            nuevo();
            if (tipo_comprobante != 'NC') {
                busca_nro_comprobante();
            }
          
            imprimir_factura();
        });

    } else if (tipofactura == 2 && $("#razonsocial").val() != 'GENERICO') {

    $.ajax({
        url: 'guardarVenta.php',
        dataType: 'json',
        type: 'POST',
        data: {
            'articulos': JSON.stringify(articulos),
            'idventa': idventa,
            'tipofactura': tipofactura,
            'tipo_comprobante': tipo_comprobante,
            'sucursal': sucursal,
            'cliente': cliente,
            'documento': documento,
            'nombreocacional': nombreocacional,
            'fecha': fecha,
            'fecha_alta': fecha_alta,
            'totaliva': totaliva,
            'total10': total10,
            'total5': total5,
            'totalexenta': totalexenta,
            'total': total,
            'nrocaja': nrocaja,
            'usuario': usuario,
            'lista_precio': idlista,
            'idhabilitacion': idhabilitacion,
            'opcion': opcion
        }
    }).done(function(info) {
        var json_info = JSON.parse(JSON.stringify(info));
        mostrar_mensaje(json_info);
        limpiar_campos();
        nuevo();
        if (tipo_comprobante != 'NC') {
                busca_nro_comprobante();
            }
        imprimir_factura();
    });

} else {
    alertify.error("Seleccione el cliente y/o el tipo de venta!");
}


}

function imprimir_factura() {
    window.open("factura.php");
}

function reimprimir_factura() {
    window.open("reimprimir_factura.php?nrofactura=" + $("#nrofactura").val());
}

function busca_nro_comprobante() {
    var nrocaja = $("#nrocaja").val();
    $.ajax({
        url: 'busca_ultimo_venta.php',
        dataType: 'json',
        type: 'POST',
        data: 'nrocaja=' + nrocaja,
        success: function(data) {

            var nro1 = parseInt(data[0].nrofactura);

            $("#nrofactura").val(zfill(nro1, 7));
        },
    });
}

function zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (width <= length) {
        if (number < 0) {
            return ("-" + numberOutput.toString());
        } else {
            return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
}

function anular() {
    var idventa = $("#idventa").val();
    var opcion = "anular";
    $.ajax({
        url: 'guardarVenta.php',
        dataType: 'json',
        type: 'POST',
        data: {
            'idventa': idventa,
            'opcion': opcion
        }
    }).done(function(info) {
        var json_info = JSON.parse(JSON.stringify(info));
        mostrar_mensaje(json_info);
        location.reload(true)
    });

}

function limpiar_campos(argument) {
    $("#tabla_articulos > tbody:last").children().remove();
    $("#idventa").val("");
    $("#nrofactura").val("");
    $("#razonsocial").val("GENERICO");
    $("#codigo").val("1");
    $("#tipofactura").val("Seleccione una opcion");
    $("#total").val("");
    $("#totaliva").val("");
    $("#total10").val("");
    $("#total5").val("");
    $("#totalexenta").val("");
    $("#documento").val("");
    $("#nombreocacional").val("");
}

var mostrar_mensaje = function(informacion) {
    if (informacion.respuesta == "BIEN") {
        swal({
            title: 'Bien!',
            text: 'Registro Guardado',
            type: 'success',
            timer: 2000,
            showConfirmButton: false
        })
    } else if (informacion.respuesta == "ERROR") {
        swal({
            title: 'Error!',
            text: 'No se ejecuto la consulta',
            type: 'warning',
            timer: 2000,
            showConfirmButton: false
        })
    } else if (informacion.respuesta == "ELIMINADO") {
        swal({
            title: 'Atención!',
            text: 'Registro Anulado',
            type: 'warning',
            timer: 2000,
            showConfirmButton: false
        })
    } else if (informacion.respuesta == "VACIO") {
        swal({
            title: 'Atención!',
            text: 'No se cargo ninguna imagen',
            type: 'warning',
            timer: 2000,
            showConfirmButton: false
        })
    } else if (informacion.respuesta == "EXISTE") {
        swal({
            title: 'Atención!',
            text: 'Cedula ya existe',
            type: 'warning',
            timer: 2000,
            showConfirmButton: false
        })
    } else if (informacion.respuesta == "HABILITADO") {
        swal({
            title: 'Bien!',
            text: 'Ingrediente Disponible',
            type: 'success',
            timer: 2000,
            showConfirmButton: false
        })
    }
}

/*window.onload = function() {
       var usuario=$("#usuario").val();
       $.ajax({
             url: 'comprobarcaja.php',
             dataType: 'json',
             type: 'POST',
             data: 'usuario='+usuario,
             success: function(data){
                   if (data=="0") {
                     swal({
                       title: 'Atención!',
                       text: 'Caja Cerrada',
                       type: 'warning',
                       timer: 2000,
                       showConfirmButton: false
                     })

                     location.href='aperturacaja.php';
                   }else{
                       
                   }
               },
           });
   };*/
</script>