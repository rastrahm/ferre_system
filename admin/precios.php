
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
    include ("conexion.php");
?>

<body class="animsition">


    <div class="page-wrapper" id="cuerpo">

         <?php
                  include("navbar.php");
                  include ("buscar_articulo.php");

          ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
            <div class="container-fluid"><br>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Articulos</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                          <input type="hidden" id="categoria" name="categoria">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                          <div class="row">         
                                         
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">COD LISTA</label>
                                                    <select id="idlista" name="idlista" class="idlista form-control" required="">
                                                          <option value="">SELECCIONE LA LISTA</option>
                                                            <?php 
                                                                
                                                                $sql = "SELECT * FROM precio_lista";
                                                                $res = mysqli_query($conexion,$sql);
                                                                while($row = mysqli_fetch_array($res)){
                                                                    echo "<option value='".$row["idlista"]."'>".$row["descripcion"]."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    
                                            </div>     
                                            <div class="col-md-1">   
                                              <label for="text-input" class=" form-control-label">ARTICULO</label>
                                            <div class="input-group">
                                              <div class="input-group-btn">
                                                <button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i>
                                                </button>
                                              </div>
                                                <input type="text" class="form-control" onchange="busca_articulo();" name="codigo" id="codigo" required="">
                                              </div>    
                                              </div> 
                                           <div class="col-md-3">
                                                    <label for="text-input" class=" form-control-label">DESCRIPCION</label>
                                                     <input type="text" id="nombreart" name="nombreart" class="form-control" disabled="">
                                                    
                                            </div>          
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">PRECIO</label>
                                                
                                                    <input type="text" id="precio" name="precio" class="form-control" onkeyup="format(this)" required="">
                                            </div>
                                          </div>
                                           
                                         
                                          </div>
                                          
                                            <div class="card-footer" align="right">
                                                <button type="button" onclick="guardar()" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            
                                            </div>
                                    </div>
                                    
                                        
                                </div>

                                <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning" id="tablaprecios" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th>ID</th>
                                            <th>DESCRIPCION</th>
                                            <th>COD ARTICULO</th>
                                            <th>ARTICULO</th>
                                            <th>PRECIO</th>
                                            <th>FECHA</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>

                            </div>
            
        <?php  
            include("footer.php");
        ?>
            <!-- END PAGE CONTAINER-->
        </div>

        <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idlistaprecio" name="idlistaprecio" value="">
              <input type="hidden" id="idarticulo" name="idarticulo" value="">
              <input type="hidden" id="opcion" name="opcion" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Precio</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el precio?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>

    </div>
    <?php include("scripts.php");  ?>
   <script src="funciones/precios.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

  function busca_articulo(){
          $.ajax({
          url: 'busca_data_articulo_pre.php',
          dataType: 'json',
          type: 'POST',
          data: 'idarticulo='+$("#codigo").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el articulo!!!");
            $("#codigo").focus();
            $("#codigo").val();
            }else{
            $("#nombreart").val(data[0].nombreart);
            var precio = parseInt(data[0].costo*data[0].porc_ganancia/100)+parseInt(data[0].costo);
            $("#precio").val(parseInt(precio).toLocaleString());
            $("#precio").focus();
            }
           },
          });
  }

   function busqueda_art(){
     $("#modal_busqueda_arts").modal("show");
     $('#modal_busqueda_arts').on('shown.bs.modal', function () {
       $("#lista_articulos").html("");
       $("#articulo_buscar").val("");
       $("#articulo_buscar").focus();
       busca(); 
     });
  }


function busca(){
    $.ajax({
        beforeSend: function(){
          $("#lista_articulos").html("");
          },
        url: 'busca_articulos_ayuda_pre.php',
        type: 'POST',
        data: 'articulo='+$("#articulo_buscar").val(),
        success: function(x){
         $("#lista_articulos").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_articulos").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}


function add_art(art){
  //alert(art);
  $("#modal_busqueda_arts").modal("toggle");
  $("#codigo").val(art);
  busca_articulo();
}

  $(document).ready(function() {
              $('.articulo').select2();
              $('.idlista').select2();
          });

    $(document).on("ready", function(){
      listar();
      eliminar();
    });

    function guardar(){

          var idarticulo = $("#codigo").val();
          var idlista = $("#idlista").val();
          var precio = $("#precio").val().replace(/(?!-)[^\d]/g, '');
          var opcion = $("#opcion").val();

        $.ajax({
          method: "POST",
          url: "guardarPrecios.php",
          data: {'idarticulo': idarticulo, 'idlista': idlista, 'precio': precio, 'opcion': opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablaprecios').DataTable().ajax.reload();
        });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "NUEVO" ){
          swal({
              title: 'Bien!',
              text: 'Un producto nuevo agregado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula o Ruc ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }


function format(input) {
  var num = input.value.replace(/\./g, '');
  if (!isNaN(num)) {
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/, '');
    input.value = num;
  }
  else {
    alertify.error('Solo se permiten numeros!');
    input.value = input.value.replace(/[^\d\.]*/g, '');
  }
}

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idlista = $("#frmEliminarArticulo #idlistaprecio").val(),
            articulo = $("#frmEliminarArticulo #idarticulo").val()
          opcion = $("#frmEliminarArticulo #opcion").val();
        $.ajax({
          method:"POST",
          url: "guardarPrecios.php",
          data: {"idlista": idlista, "idarticulo": articulo, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablaprecios').DataTable().ajax.reload();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idlista").val('').trigger('change');
      $("#nombreart").val("");
      $("#precio").val("");
      $("#codigo").val("");
    }


   var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idlista = $("#idlista").val( data.idlista).trigger('change'),
            nombreart = $("#nombreart").val( data.nombreart ),
            codigo = $("#codigo").val( data.articulo ),
            precio = $("#precio").val( parseInt(data.precio).toLocaleString()),
            opcion = $("#opcion").val("modificar");
            window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idlista = $("#frmEliminarArticulo #idlistaprecio").val( data.idlista ),
          articulo = $("#frmEliminarArticulo #idarticulo").val( data.articulo );
      });
    }
    
</script>
