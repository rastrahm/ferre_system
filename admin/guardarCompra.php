<?php
	include ("conexion.php");


	switch ($_POST["opcion"]) {
		case 'nuevo':

		$sqlid = "SELECT idcompra from compra_cab
           order by idcompra desc LIMIT 1";
        $resid = mysqli_query($conexion,$sqlid);

        foreach ($resid as $rowid) {

        	$id = $rowid["idcompra"];
        	
        }

        if (isset($id)=="") {
        		$idcompra = 1;
        }else{
        		$idcompra = $id+1;
        }

		$sucursal = $_POST["sucursal"];
		$idproveedor = $_POST["proveedor"];
		$nrofactura = $_POST["nrofactura"];
		$timbrado = $_POST["timbrado"];
		$tipo_comprobante = $_POST["tipocomprobante"];
		$tipofactura = $_POST["condicion"];
		$concepto = $_POST["concepto"];
		$fecha = $_POST["fecha"];
		$fecha_alta= $_POST["fecha_alta"];
		$hora = strftime("%H:%M:%S");
		$total_iva = $_POST["totaliva"];
		$total_gravada5 = $_POST["total5"];
		$total_gravada10 = $_POST["total10"];
		$total_exenta = $_POST["totalexenta"];
		$total_comprobante_c_iva = $_POST["total"];
		$total_comprobante = $total_gravada10+$total_gravada5;

		mysqli_autocommit($conexion,FALSE);

		$existe = existe_nrofactura($nrofactura, $conexion);

        if ($existe>0) {
          $informacion["respuesta"] = "EXISTE";
          echo json_encode($informacion);
        }else{  

        	$query = "INSERT INTO compra_cab (idcompra, sucursal, proveedor, nrofactura, timbrado, tipo_comprobante, tipofactura, concepto, fecha, fecha_alta, hora, total_comprobante, total_iva, total_gravada5, total_gravada10, total_exenta, total_comprobante_c_iva) 
							VALUES ('$idcompra', '$sucursal', '$idproveedor', '$nrofactura', '$timbrado', '$tipo_comprobante', '$tipofactura', '$concepto', '$fecha', '$fecha_alta', '$hora', '$total_comprobante', '$total_iva', '$total_gravada5', '$total_gravada10', '$total_exenta', '$total_comprobante_c_iva');";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado($resultado);

		if($tipofactura==2) {

                    $query = "INSERT INTO credito_compra (compra, proveedor, fecha_credito, total, saldo) 
                    VALUES ('$idcompra', '$idproveedor', '$fecha', '$total_comprobante_c_iva', '$total_comprobante_c_iva')";

                    $resultado = mysqli_query($conexion, $query);
    	}

		$data = json_decode($_POST['articulos']);

		$sql = "SELECT idbodega from bodegas where sucursal=$sucursal and descbodega='DEPOSITO'";
		$res = mysqli_query($conexion, $sql);
		foreach ($res as $row) {
			$idbodega = $row["idbodega"];
		}

		foreach($data as $value){
			
			  $idarticulo = $value->idarticulo;
			  $cantidad = $value->cantidad;
			  $costo = $value->costo;
			  $subtotal_c_iva = $value->subtotal;
			  $iva = $value->iva;

			  if ($idarticulo=='') {
			  	
			  }else{

			  if ($iva==10) {
			  	$totaliva = round($subtotal_c_iva/11);
			  	$total10 = $subtotal_c_iva-$totaliva;

			  	$query = "INSERT INTO compra_det (idcompra, articulo, cantidad, costo, subtotal, total_iva, subtotal_c_iva, total_gravada10) 
				VALUES ('$idcompra', '$idarticulo', '$cantidad', '$costo', '$total10', '$totaliva', '$subtotal_c_iva', '$total10');";

			  }else if ($iva==5){
			  	$totaliva = round($subtotal_c_iva/21);
			  	$total5 = $subtotal_c_iva-$totaliva;

			  	$query = "INSERT INTO compra_det (idcompra, articulo, cantidad, costo, subtotal, total_iva, subtotal_c_iva, total_gravada5) 
					VALUES ('$idcompra', '$idarticulo', '$cantidad', '$costo', '$total5', '$totaliva', '$subtotal_c_iva', '$total5');";
			  }else{
			  	$query = "INSERT INTO compra_det (idcompra, articulo, cantidad, costo, subtotal, subtotal_c_iva, total_exenta) 
					VALUES ('$idcompra', '$idarticulo', '$cantidad', '$costo', '$subtotal_c_iva', '$subtotal_c_iva', '$subtotal_c_iva');";
			  }

				$resultado = mysqli_query($conexion, $query);	

				$sql = "SELECT bd.stock from articulos a
						join bodegas_det bd on bd.articulo=a.idarticulo
						join bodegas b on b.idbodega=bd.bodega
						where b.sucursal=$sucursal and b.idbodega=$idbodega and a.idarticulo=$idarticulo";
					$res = mysqli_query($conexion, $sql);
					foreach ($res as $row) {

						if ($row['stock']=='') {
			        		$existencia = 0;
			        	}else{
			        		$existencia = $row['stock'];
			        	}
			        }

			    if ($tipo_comprobante=='COM') {
			    	$stock = $existencia+$cantidad;
			    }else if ($tipo_comprobante=='NC'){
			    	$stock = $existencia-$cantidad;
			    }

				$query = "UPDATE articulos a join bodegas_det bd on bd.articulo=a.idarticulo
								join bodegas b on b.idbodega=bd.bodega
								set bd.stock=$stock
								where b.sucursal=$sucursal and b.idbodega=$idbodega and bd.articulo=$idarticulo";
				$resultado = mysqli_query($conexion, $query);

				}
			}

			if (!mysqli_commit($conexion)) {
				  echo "Commit transaction failed";
				  exit();
			}
        }

		break;
		case 'modificar':

		$idcompra = $_POST["idcompra"];
		$idproveedor = $_POST["proveedor"];
		$nrofactura = $_POST["nrofactura"];
		$timbrado = $_POST["timbrado"];
		$concepto = $_POST["concepto"];
		$fecha = $_POST["fecha"];

		$query = "UPDATE compra_cab set proveedor=$idproveedor,nrofactura='$nrofactura',timbrado=$timbrado,concepto='$concepto',fecha='$fecha'
				  WHERE idcompra=$idcompra";
		$resultado = mysqli_query($conexion, $query);
		verificar_resultado($resultado);

			break;
		case 'eliminar':

						$idcompra = $_POST["idcompra"];	
						$idsucursal = $_POST["sucursal"];	
						$tipocomprobante = $_POST["tipocomprobante"];	

						mysqli_autocommit($conexion,FALSE);

						$sql = "SELECT idbodega from bodegas where sucursal=$idsucursal and descbodega='DEPOSITO'";
							$res = mysqli_query($conexion, $sql);
							foreach ($res as $row) {
								$idbodega = $row["idbodega"];
							}

						$sql = "SELECT * from compra_det where idcompra=$idcompra";
						$res = mysqli_query($conexion,$sql);

						foreach ($res as $row) {
							$idarticulo = $row["articulo"];
							$cantidad = $row["cantidad"];

							$sql = "SELECT bd.stock from articulos a
								join bodegas_det bd on bd.articulo=a.idarticulo
								join bodegas b on b.idbodega=bd.bodega
								where b.sucursal=$idsucursal and b.idbodega=$idbodega and a.idarticulo=$idarticulo";
							$res = mysqli_query($conexion, $sql);
							foreach ($res as $row) {
					        		$exis = $row['stock'];
					        }

					        if (isset($existencia)=='') {
					        	$existencia=0;
					        }else{
					        	$existencia = $exis;
					        }

							if ($tipocomprobante=='COM') {
						    	$stock = $existencia-$cantidad;
						    }else if ($tipocomprobante=='NC'){
						    	$stock = $existencia+$cantidad;
						    }

							$sqlstock = "UPDATE articulos a join bodegas_det bd on bd.articulo=a.idarticulo
								join bodegas b on b.idbodega=bd.bodega
								set bd.stock=$stock
								where b.sucursal=$idsucursal and b.idbodega=$idbodega and bd.articulo=$idarticulo";
							$resstock = mysqli_query($conexion, $sqlstock);

						}

						$query = "DELETE FROM compra_cab where idcompra=$idcompra";
						$resultadoquery = mysqli_query($conexion, $query);	
						verificar_resultado2($resultadoquery);

						if (!mysqli_commit($conexion)) {
							echo "Commit transaction failed";
							exit();
						}
				
			break;
		default:
			$informacion["respuesta"] = "OPCION_VACIA";
			echo json_encode($informacion);
			break;
	}

	
	$informacion = [];

	function existe_nrofactura($nrofactura, $conexion){
	    $query = "SELECT * FROM compra_cab 
	          WHERE nrofactura='$nrofactura'";
	    $resultado = mysqli_query($conexion, $query);
	    $existe = mysqli_num_rows( $resultado );
	    return $existe;
	 }


	function verificar_resultado($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "BIEN";
			
			echo json_encode($informacion);
		}

		function verificar_resultado2($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "ELIMINADO";
			
			echo json_encode($informacion);
		}

	function cerrar($conexion){
		mysqli_close($conexion);
	}
?>