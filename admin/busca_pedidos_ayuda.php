<?php

include ("conexion.php");

$idpedido = $_POST['idpedido'];
$sql = "SELECT p.idpedido,c.razonsocial,p.cliente,p.razonsocial_fac,p.documento_fac,p.barrio_fac,p.direccion_fac,p.total,pd.articulo,a.nombreart,a.costo,pd.cantidad,pd.precio,pd.subtotal,a.iva from pedido p 
  join pedido_det pd on pd.idpedido=p.idpedido
  join articulos a on a.idarticulo=pd.articulo
  join clientes c on c.idcliente=p.cliente
  where p.idpedido like '%$idpedido%' and p.estado=5 group by p.idpedido";
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>N° Pedido</th>";
    echo "<th>Cliente</th>";
    echo "<th>Barrio</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($res)){

    echo "<tr>";
    echo "<td>".$row['idpedido']."</td>";
    echo "<td>".$row['razonsocial_fac']."</td>";
    echo "<td>".$row['barrio_fac']."</td>";
    
    echo "<td><button type='button' id='".$row['idpedido']."' class='btn btn-primary btn-xs' onclick='add_ped(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}
?>