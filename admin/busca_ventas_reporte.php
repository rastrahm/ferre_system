<?php

include ("conexion.php");
include ("./seguridad/formatos.php");

$fi=$_POST['fechai'];
$ff=$_POST['fechaf'];
$tabonos=0;
$tabonos2=0;
$sum = 0;
$documento = '';
$razonsocial = '';

$sql="SELECT vc.idventa, vc.nrofactura, vc.documento as documento_ocacional, vc.nombre_ocacional, c.documento, c.razonsocial,tf.factura,vc.fecha,vc.hora,vc.estado from ventas_cab vc
  join clientes c on c.idcliente=vc.cliente
  join tipo_factura tf on tf.idtipo = vc.tipofactura
  where vc.fecha between '$fi' and '$ff'
  order by vc.fecha desc";

$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

     if($resul>0){
        echo "<div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
        echo "<div class='box box-danger print1'>";
        echo "<div class='box-header with-border'><br>";
        echo "<h4 class='box-title'>Ventas | ".formatearFecha($fi)." al ".formatearFecha($ff)."</h4><br>";
        echo "</div>";
        echo "<div class='box-body'>";
        echo "<div class='box-body table-responsive'>";

        foreach ($res as $cabecera) {

          if ($cabecera['nombre_ocacional']) {
            $razonsocial = $cabecera['nombre_ocacional'];
            $documento = $cabecera['documento_ocacional'];
          } else {
            $razonsocial = $cabecera['razonsocial'];
            $documento = $cabecera['documento'];
          }

        echo "<h4 class='box-title'>Nro Factura: ".$cabecera['nrofactura']." | Documento: ".$documento." | Razon Social: ".$razonsocial." | Tipo Factura: ".$cabecera['factura']."</h4><br>";


        echo "<table id='tabla_ventas_credito' class='table table-bordered table-hover'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Articulo</th>";
        echo "<th>Cantidad</th>";
        echo "<th>Precio</th>";
        echo "<th>Subtotal</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $sqldetalle="SELECT a.nombreart, vd.cantidad, vd.precio, vd.subtotal_c_iva from ventas_det vd 
              join articulos a on a.idarticulo=vd.articulo
              where vd.idventa=".$cabecera['idventa'];

        $resdetalle = mysqli_query($conexion,$sqldetalle);

          $tabonos = 0;
          foreach ($resdetalle as $detalle) {
            echo "<tr>";
            $sum+=$detalle['cantidad'];
            echo "<td>".$detalle['nombreart']."</td>";
            echo "<td>".$detalle['cantidad']."</td>";
            echo "<td>".formatearNumero($detalle['precio'])."</td>";
            echo "<td>".formatearNumero($detalle['subtotal_c_iva'])."</td>";
              $tabonos+=$detalle['subtotal_c_iva'];
            echo "</tr>";
          }

          echo "<tr>";
          echo "<td><b>TOTAL: <b></td>";
          echo "<td><b>".formatearNumero($tabonos)." Gs.</b></td>";
          echo "<td><b>Cantidad Vendida: <b></td>";
          echo "<td><b>".formatearNumero($sum)."</b></td>";
          echo "</tr>";
          echo "</tbody>";
          echo "</table><br>";
         
        }

         echo "</div>";
        echo "</div>";
      echo "</div>";
      echo "</div><br><br>";
           
     }else{
      echo '<script>alertify.error("Ninguna venta registrada!!!");</script>';
     }