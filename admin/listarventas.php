<?php
  include ("conexion.php");

  $sql = "SELECT vc.idventa,c.razonsocial,vc.tipofactura,vc.fecha,vc.hora,vc.total,vc.estado from ventas_cab vc
                join clientes c on c.idcliente=vc.cliente order by vc.fecha";
  $res = mysqli_query($conexion,$sql);
  
  if(!$res){
    die("Error");
  }else{
    while( $data = mysqli_fetch_assoc($res)) {
      $arreglo["data"][] = $data;
    }
    echo json_encode($arreglo);
  } 

  mysqli_free_result($res);
  mysqli_close($conexion);
?>