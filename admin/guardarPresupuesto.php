<?php
include("conexion.php");


switch ($_POST["opcion"]) {
    case 'nuevo':

        $sucursal = $_POST["sucursal"];
        $idpedido = $_POST["idpedido"];
        $concepto = $_POST["concepto"];
        $idproveedor = $_POST["proveedor"];
        $fecha = date("Y-m-d");
        $hora = strftime("%H:%M:%S");
        $total = $_POST["total"];

        mysqli_autocommit($conexion, FALSE);

        $existe = existe_presupuesto($idpedido, $conexion);

        if ($existe > 0) {
            $informacion["respuesta"] = "EXISTE";
            echo json_encode($informacion);
        } else {

            $query = "INSERT INTO presupuesto_cab (sucursal, idcompra_pedido, proveedor, observacion, fecha_alta, total) 
							VALUES ('$sucursal', '$idpedido', '$idproveedor', '$concepto', '$fecha $hora', '$total');";
            $resultado = mysqli_query($conexion, $query);
            verificar_resultado($resultado);

            $sqlid = "SELECT idpresupuesto from presupuesto_cab
           order by idpresupuesto desc LIMIT 1";
            $resid = mysqli_query($conexion, $sqlid);

            foreach ($resid as $rowid) {
                $idpresupuesto = $rowid["idpresupuesto"];
            }

            $data = json_decode($_POST['articulos']);

            foreach ($data as $value) {

                $idarticulo = $value->idarticulo;
                $cantidad = $value->cantidad;
                $costo = $value->costo;
                $subtotal = $value->subtotal;

                if ($idarticulo == '') {
                } else {

                    $query = "INSERT INTO presupuesto_det (idpresupuesto, articulo, cantidad, costo, subtotal) 
                        VALUES ('$idpresupuesto', '$idarticulo', '$cantidad', '$costo', '$subtotal');";
                    $resultado = mysqli_query($conexion, $query);
                }
            }

            if (!mysqli_commit($conexion)) {
                echo "Commit transaction failed";
                exit();
            }
        }

        break;
    case 'modificar':

        $idpresupuesto = $_POST["idpresupuesto"];
        $sucursal = $_POST["sucursal"];
        $idproveedor = $_POST["proveedor"];
        $idpedido = $_POST["idpedido"];
        $concepto = $_POST["concepto"];

        $query = "UPDATE presupuesto_cab set idcompra_pedido=$idpedido,proveedor=$idproveedor,observacion='$concepto'
				WHERE idpresupuesto=$idpresupuesto";
        $resultado = mysqli_query($conexion, $query);
        verificar_resultado($resultado);

        break;
    case 'eliminar':

        $idpresupuesto = $_POST["idpresupuesto"];

        mysqli_autocommit($conexion, FALSE);

        $query = "DELETE FROM presupuesto_cab where idpresupuesto=$idpresupuesto";
        $resultadoquery = mysqli_query($conexion, $query);
        verificar_resultado2($resultadoquery);

        if (!mysqli_commit($conexion)) {
            echo "Commit transaction failed";
            exit();
        }

        break;
    default:
        $informacion["respuesta"] = "OPCION_VACIA";
        echo json_encode($informacion);
        break;
}


$informacion = [];

function existe_presupuesto($idpedido, $conexion)
{
    $query = "SELECT * FROM presupuesto_cab 
	          WHERE idcompra_pedido='$idpedido'";
    $resultado = mysqli_query($conexion, $query);
    $existe = mysqli_num_rows($resultado);
    return $existe;
}


function verificar_resultado($resultado)
{
    if (!$resultado)
        $informacion["respuesta"] = "ERROR";
    else
        $informacion["respuesta"] = "BIEN";

    echo json_encode($informacion);
}

function verificar_resultado2($resultado)
{
    if (!$resultado)
        $informacion["respuesta"] = "ERROR";
    else
        $informacion["respuesta"] = "ELIMINADO";

    echo json_encode($informacion);
}

function cerrar($conexion)
{
    mysqli_close($conexion);
}
