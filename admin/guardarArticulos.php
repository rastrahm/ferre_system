<?php
	include ("conexion.php");

		$eliminar = "eliminar";

	if (strcmp($eliminar, $_POST["opcion"])==0) {
			$idarticulo = $_POST["idarticulo"];
			$opcion = $_POST["opcion"];
	}else{

		$idarticulo = $_POST["idarticulo"];
		$articulo = $_POST["nombreart"];
		$cat = $_POST["clasificacion"];

		$sql = "SELECT idclasificacion from clasificaciones
			 where nombrecla='$cat'";
			$res = mysqli_query($conexion, $sql);
			foreach ($res as $row) {
				$clasificacion= $row['idclasificacion'];
	        }
	    $proveedor = $_POST["proveedor"];

	    $sql = "SELECT idproveedor from proveedores
			 where proveedor='$proveedor'";
			$res = mysqli_query($conexion, $sql);
			foreach ($res as $row) {
				$idproveedor= $row['idproveedor'];
	        }

	        $fecha = date("Y-m-d");
	        $iva = $_POST["iva"];
			$cos = $_POST["costo"];
			$costo = str_replace('.', '', $cos);
			$descripcion =  $_POST["descripcion"];
			$codigobarras = $_POST["codbarra"];


		$opcion = $_POST["opcion"];
	}
	
	$informacion = [];

	switch ($opcion) {
		case 'registrar':

				registrar($idproveedor, $codigobarras, $articulo, $descripcion, $clasificacion, $iva, $costo, $fecha, $conexion);	
	
				$sql = "SELECT max(idarticulo) from articulos";
				$res = mysqli_query($conexion, $sql);
				foreach ($res as $row) {
					$idarticulo= $row['max(idarticulo)'];
				}

				foreach($_FILES["imagen"]['tmp_name'] as $key => $tmp_name){


			//Validamos que el archivo exista
			if($_FILES["imagen"]["name"][$key]) {
				$imagen = $_FILES["imagen"]["name"][$key]; //Obtenemos el nombre original del archivo
				$tamaño = $_FILES["imagen"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo

				$directorio = 'img/'; //Declaramos un  variable con la ruta donde guardaremos los archivos
				
				//Validamos si la ruta de destino existe, en caso de no existir la creamos
				if(!file_exists($directorio)){
					mkdir($directorio, 0777) or die("No se puede crear el directorio de extraccion");	
				}
				
				$dir=opendir($directorio); //Abrimos el directorio de destino
				$cargar = $directorio.'/'.$imagen; //Indicamos la ruta de destino, así como el nombre del archivo
				
				$query = "INSERT INTO imagenes (articulo,imagen) 
				VALUES ('$idarticulo', '$imagen');";
				$resultado = mysqli_query($conexion, $query);	
				//Movemos y validamos que el archivo se haya cargado correctamente
				//El primer campo es el origen y el segundo el destino
				if(move_uploaded_file($tamaño, $cargar)) {	
					//echo "El archivo $imagen se ha almacenado en forma exitosa.<br>";
					} else {	
					echo "Ha ocurrido un error, por favor inténtelo de nuevo.<br>";
				}
				closedir($dir); //Cerramos el directorio de destino
			}
		}
						
			break;

		case 'modificar':


			modificar($idarticulo, $idproveedor, $codigobarras, $articulo, $descripcion, $clasificacion, $iva, $costo, $conexion);
					
					foreach($_FILES["imagen"]['tmp_name'] as $key => $tmp_name){
					//Validamos que el archivo exista
					if($_FILES["imagen"]["name"][$key]) {
						$imagen = $_FILES["imagen"]["name"][$key]; //Obtenemos el nombre original del archivo
						$tamaño = $_FILES["imagen"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo

						$directorio = 'img/'; //Declaramos un  variable con la ruta donde guardaremos los archivos
						
						//Validamos si la ruta de destino existe, en caso de no existir la creamos
						if(!file_exists($directorio)){
							mkdir($directorio, 0777) or die("No se puede crear el directorio de extraccion");	
						}
						
						$dir=opendir($directorio); //Abrimos el directorio de destino
						$cargar = $directorio.'/'.$imagen; //Indicamos la ruta de destino, así como el nombre del archivo
						
						$query = "INSERT INTO imagenes (articulo,imagen) 
						VALUES ('$idarticulo', '$imagen');";
						$resultado = mysqli_query($conexion, $query);	
						//Movemos y validamos que el archivo se haya cargado correctamente
						//El primer campo es el origen y el segundo el destino
						if(move_uploaded_file($tamaño, $cargar)) {	
							//echo "El archivo $imagen se ha almacenado en forma exitosa.<br>";
							} else {	
							echo "Ha ocurrido un error, por favor inténtelo de nuevo.<br>";
						}
						closedir($dir); //Cerramos el directorio de destino
					}
				}

				
			break;
		case 'eliminar':
						eliminar($idarticulo, $conexion);
			break;
		default:
			$informacion["respuesta"] = "OPCION_VACIA";
			echo json_encode($informacion);
			break;
	}

	function registrar($idproveedor, $codigobarras, $articulo, $descripcion, $clasificacion, $iva, $costo, $fecha, $conexion){
		$query = "INSERT INTO articulos (proveedor, codigobarras, nombreart, descripcion, clasificacion, iva, costo, fecha_alta) 
		VALUES ('$idproveedor', '$codigobarras', '$articulo', '$descripcion', '$clasificacion', '$iva', '$costo', '$fecha');";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado($resultado);
		//cerrar($conexion);
	}


	function  modificar($idarticulo, $idproveedor, $codigobarras, $articulo, $descripcion, $clasificacion, $iva, $costo, $conexion){
		$query = "UPDATE articulos SET nombreart='$articulo', 
									clasificacion='$clasificacion',
									codigobarras='$codigobarras',
									iva='$iva',
									proveedor='$idproveedor',
									costo='$costo',
									descripcion='$descripcion'
									WHERE idarticulo='$idarticulo'";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado($resultado);
		//cerrar($conexion);
	}


	function verificar_resultado($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "BIEN";
			
			echo json_encode($informacion);
		}

		function eliminar($idarticulo, $conexion){
		$query = "UPDATE articulos set estado=1
					where idarticulo='$idarticulo'";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado2($resultado);
		cerrar($conexion);
	}
	
	function verificar_resultado2($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "ELIMINADO";
			
			echo json_encode($informacion);
		}

	function cerrar($conexion){
		mysqli_close($conexion);
	}
?>