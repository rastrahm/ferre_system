<?php
	include ("conexion.php");

	$idempleado = $_GET["idempleado"];

	$sql = "SELECT oc.idorden,vi.ruta,r.distancia,od.pedido,concat(v.marca,' ',v.modelo) as vehiculo,oc.fecha_salida,oc.hora_salida,oc.estado FROM orden_entrega oc
		join orden_entrega_det_ped od on od.idorden=oc.idorden
		join orden_entrega_det_emp oe on oe.idorden=oc.idorden
		join vehiculos v on v.idvehiculo=oc.vehiculo
		join viajes vi on vi.pedido=od.pedido
		join ruta r on r.idruta=vi.ruta
		where oe.empleado=$idempleado order by r.distancia";

	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>