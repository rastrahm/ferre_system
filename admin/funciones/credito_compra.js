function busca_proveedor(){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_proveedor.php',
          dataType: 'json',
          type: 'POST',
          data: 'id_proveedor='+$("#proveedor").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el proveedor!!!");
            $("#proveedor").val("");
            $("#proveedor").focus();
            }else{
            $("#proveedor").val(data[0].proveedor);
            $("#idproveedor").val(data[0].idproveedor);
            $("#proveedor").select();
            $("#proveedor").focus();
            }
           },
          });
        });

  }
function busqueda_pro(){
   $("#modalProveedor").modal("show");
   $('#modalProveedor').on('shown.bs.modal', function () {
   $("#lista_proveedores").html("");
   $("#proveedor_buscar").val("");
   $("#proveedor_buscar").focus();
   busca_p();
   });
}
function busca_p(){
    $.ajax({
        beforeSend: function(){
          $("#lista_proveedores").html("");
          },
        url: 'busca_proveedores_ayuda.php',
        type: 'POST',
        data: 'proveedor='+$("#proveedor_buscar").val(),
        success: function(x){
         $("#lista_proveedores").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_proveedores").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}

function add_pro(cli){
  //alert(art);
  $("#modalProveedor").modal("toggle");
  $("#proveedor").val(cli);
  busca_proveedor();
  //busca_cuentas_proveedor();
}

function busca_cuentas_proveedor(){
  $(document).ready(function() {
          if ($("#cliente").val()!='') {
          $.ajax({
          beforeSend: function(){
            $("#cartera_clientes").html("Consultando informacion...");
           },
          url: 'consulta_cuenta_proveedor.php',
          type: 'GET',
          data: 'proveedor='+$("#proveedor").val(),
          success: function(x){
            $("#cartera_clientes").html(x);
           },
           error: function(jqXHR,estado,error){
             $("#cartera_clientes").html(estado+"    "+error);
           }
           });
          }else{
            alertify.error("Debe elegir un cliente!!!");
          }
          });
}

function abona_ticket(ti){
      var de=ti.split("|");
      var idcredito=de[0];
      var monto=de[1];
      var cuota=de[2];
      var num=de[3];
      var idcompra=de[4];
      var fecha=de[5];
      var proveedor=de[6];
      var saldo=de[7];

    $("#modal_abono_ticket").modal({
            show:true,
            backdrop: 'static',
            keyboard: false
            });
    $("#abono").val("");

    $('#modal_abono_ticket').on('shown.bs.modal', function () {
        $('#abono').focus();
    });

    $("#mon_cuota").val(parseInt(monto).toLocaleString());
    $("#nrocuota").val(cuota);
    $("#fecha").val(fecha);
    $("#numcuota").val(num);
    $("#idcredito").val(idcredito);
    $("#idproveedor").val(proveedor);
    $("#idcompra").val(idcompra);
    $("#sal").val(saldo);
}



/***********************************************************************************/

function procesa_abono(){

                var opcion=$("#opcion").val();
                var idcredito=$("#idcredito").val();
                var idcompra=$("#idcompra").val();
                var nrocuota=$("#nrocuota").val();
                var fecha=$("#fecha").val();
                var monto=$("#mon_cuota").val().replace(/[^\d]/g, '');
                $.ajax({
                url: 'guardarPagos.php',
                type: 'POST',
                data: 'opcion='+opcion+'&idcompra='+idcompra+'&idcredito='+idcredito+'&nrocuota='+nrocuota+'&monto='+monto+'&fecha='+fecha
                 }).done( function( info ){
                var json_info = JSON.parse( info );
                 mostrar_mensaje( json_info );
                 busca_cuentas_proveedor();
                 $('#modal_abono_ticket').modal('hide');
              });

}

//************************************************************************************************
function revisa_pagos(id){
  $(document).ready(function(){
    var idcredito = id;
      $.ajax({
          beforeSend: function(){
            $("#pagos_realizados").html("Buscando...");
           },
          url: 'busca_abono_cliente.php',
          type: 'POST',
          data: 'idcredito='+idcredito,
          success: function(x1){
            $("#pagos_realizados").html(x1);
            $('#modal_revisa_pagos').modal('show');
           },
           error: function(jqXHR,estado,error){
            alert("Ocurrio un error, reporte a soporte..." +estado+"     "+error);
           }
           });
    })
    }


/**************************************************************************************/
function print_pagos(){
  $(".print_abonos").printArea();
}

var mostrar_mensaje = function( informacion ){
     if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'El pago ha sido registrado!',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "PROMESA" ){
         swal({
              title: 'Bien!',
              text: 'Promesa de Pago agendada!',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atenci¨®n!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADOYA" ){
          swal({
              title: 'Atenci¨®n!',
              text: 'La venta ya se anulo',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      } 
    }

//funcion para promesa de pago