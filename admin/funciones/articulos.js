var listar = function(){
        var table = $("#tablaarticulos").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
        "ajax" : {
            "url": "listarArticulos.php",
            "type": "POST" 
        },
        "columnDefs": [
         {
                "targets": [ 4 ],
                "visible": false,
                "searchable": false
            },
            {
                      "targets": [9], // El objetivo de la columna de posición, desde cero.
                      "data": "estado", // La inclusión de datos
                      "render": function(data, type, full) { // Devuelve el contenido personalizado
                            if (data==1) {
                                return "<span style='color:red;'><i class='fa fa-ban'></i> AGOTADO</span>";
                            }else{
                                return "<span style='color:green;'><i class='fa fa-check'></i> DISPONIBLE</span>";
                            }
                      }
                  },
        ],
        "columns":[
            { "data": "idarticulo"},
            { "data": "proveedor" },
            { "data": "nombreart" },
            { "data": "nombrecla" },
            { "data": "descripcion" },
            { "data": "costo" , "render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
            { "data": "iva" },
            { "data": "fecha_alta" , "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')},
            { "data": "fec_ult_compra" , "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')},
            { "data": "estado" },
            { "data": "codigobarras" },
            {"defaultContent": "<button type='button' class='editar btn btn-primary' title='editar'><i class='fa fa-pencil-square-o'></i></button> <button type='button' class='detalle btn btn-success' data-toggle='modal' data-target='#modalDetalle' title='Vista Previa'><i class='fa fa-eye'></i></button> <button type='button' title='deshabilitar' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar'><i class='fa fa-ban'></i></button>"}
        ],
        "language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
        });
    obtener_data_editar("#tablaarticulos tbody", table);
    obtener_id_eliminar("#tablaarticulos tbody", table);
    obtener_data_detalles("#tablaarticulos tbody", table);
}

var agregar_nuevo_cliente = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }