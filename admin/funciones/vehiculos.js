var listar = function(){
        var table = $("#tablavehiculos").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
        "ajax" : {
            "url": "listarVehiculos.php",
            "type": "POST" 
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
             {
                      "targets": [7], // El objetivo de la columna de posición, desde cero.
                      "data": "estado", // La inclusión de datos
                      "render": function(data, type, full) { // Devuelve el contenido personalizado
                            if (data==1) {
                                return "<span> EN PROCESO ENTREGA</span>";
                            }else{
                                return "<span> DISPONIBLE</span>";
                            }
                      }
                  }
        ],
        "columns":[
            { "data": "idvehiculo"},
            { "data": "idsucursal"},
            { "data": "desc_suc"},
            { "data": "marca"},
            { "data": "modelo" },
            { "data": "año" },
            { "data": "matricula" },
            { "data": "estado" },
            {"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#modalNuevo'><i class='fa fa-pencil-square-o'></i></button> <button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar'><i class='fa fa-trash'></i></button>"}
        ],
        "language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
        });
    obtener_data_editar("#tablavehiculos tbody", table);
    obtener_id_eliminar("#tablavehiculos tbody", table);
}

var agregar_nuevo_cliente = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }