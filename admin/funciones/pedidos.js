var listar = function(){
        var table = $("#tablapedidos").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
        "ajax" : {
            "url": "listarPedidos.php",
            "type": "POST" 
        },
        "columnDefs": [
             {
                      "targets": [6], // El objetivo de la columna de posición, desde cero.
                      "data": "estado", // La inclusión de datos
                      "render": function(data, type, full) { // Devuelve el contenido personalizado
                            if (data==4) {
                                return "<span style='color:green;'><i class='fa fa-check-circle'> </i> Facturado</span>";
                            }else if (data==5){
                                return "<span style='color:blue;'><i class='fa fa-cubes'> </i> Preparado</span>";
                            }else if (data==6){
                                return "<span style='color:blue;'><i class='fa fa-truck'> </i> En proceso de entrega</span>";   
                            }

                      }
                  }
        ],
        "columns":[
            { "data": "idpedido"},
            { "data": "razonsocial"},
            { "data": "barrio_fac"},
            { "data": "desc_tip_ped" },
            { "data": "fecha" , "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')},
            { "data": "hora" },
            { "data": "estado" },
            {"defaultContent": "<button type='button' title='Ver Detalles' class='detalles btn btn-success'><i class='fa fa-eye'></i></button> <button type='button' tilte='Ver ubicacion' class='ubicacion btn btn-primary'><i class='fa fa-map-marker'></i></button> <button type='button' title='Ver Ruta' class='ruta btn btn-warning'><i class='fa fa-road'></i></button> <button type='button' title='Ver Posicion' class='posicion btn btn-danger'><i class='fa fa-truck'></i></button>"}
        ],
        "language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
        });
    obtener_data_detalles("#tablapedidos tbody", table);
    obtener_data_ruta("#tablapedidos tbody", table);
    obtener_data_ubicacion("#tablapedidos tbody", table);
    obtener_data_ruta_logistica("#tablapedidos tbody", table);
    obtener_data_posicion("#tablapedidos tbody", table);
}

var agregar_nuevo_cliente = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }