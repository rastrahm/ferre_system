<header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35" id="mainNav">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="menu.php">
                            <h3 class="text-white">FS</h3>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-shopping-cart"></i>
                                    <span class="bot-line"></span>Pedidos</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="pedidos.php">Pedidos</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="content">
                                    <a class="js-acc-btn" href="#"><i class="fa fa-user"></i>  <?php
                                        if (isset($_SESSION['nombreUsuario'])){
                                            echo $_SESSION['nombreUsuario'];
                                        }
                                      ?> </a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
                                        <a onclick="cerrarsesion();">
                                            <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

<header class="header-mobile header-mobile-2 d-block d-lg-none" id="mainNav">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="menu.php">
                            <h3 class="text-white">FS</h3>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-shopping-cart"></i>Pedidos</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="pedidos.php">Ver Pedidos</a>
                                </li>
                            </ul>
                        </li>
                         <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i> <?php
                                if (isset($_SESSION['nombreUsuario'])){
                                    echo $_SESSION['nombreUsuario'];
                                }
                              ?> </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a onclick="cerrarsesion();" href="#">
                                            <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>


<div class="modal fade" id="cerrarsesion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Estas seguro?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Cerrar sesión" para finalizar su sesión actual.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="./seguridad/salir.php">Cerrar Sesión</a>
          </div>
        </div>
      </div>
    </div>