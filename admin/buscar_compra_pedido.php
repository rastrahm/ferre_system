<div class="modal fade" id="modal_busqueda_pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Busqueda de Pedido:</h4>
          </div>
          <div class="modal-body">
          <div class='input-group'>
          <span class='input-group-addon bg-blue'><b>Proveedor:</b></span>
          <input type='text' id='pedido_buscar' class='form-control' onkeyup="busca_ped();" placeholder='Nombre del proveedor...'>
          </div>
          <br>
            <div id='lista_pedidos'></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

