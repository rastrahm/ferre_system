<?php

include ("conexion.php");
include ("./seguridad/formatos.php");

if (isset($_POST["proveedor"])=='') {
  $proveedor = "Todos";
}else{
  $proveedor = $_POST["proveedor"];
}

$proveedores = "Todos";
$total = 0;
$nro_cuota = 0;
$hoy = date("Y-m-d");

if (strcmp($proveedor, $proveedores)==0) {

$sql="SELECT cc.idcredito,cc.compra,cc.proveedor,p.proveedor,cc.fecha_credito,cc.total,cc.abonado,cc.saldo,cc.estado from credito_compra cc
join proveedores p on p.idproveedor=cc.proveedor
where cc.estado=0
GROUP BY cc.idcredito";

$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){

       foreach ($res as $row) { 

          echo "<br><div class='col-md-12'>";
           echo "<div class='box box-primary print1'><h4>Proveedor: ".$row['proveedor']."";
           echo "<div class='box-header with-border'><br>";
           echo "<h4>Total: ".formatearNumero($row['total'])." | Total Abonado: ".formatearNumero($row['abonado'])."</h4><br>";
       
           echo "</div>";
           echo "<div class='box-body'>";
           echo "<div class='table-responsive table--no-card m-b-30'>";
           echo "<table class='table table-borderless table-striped table-earning' width='100%' cellspacing='0'>";
           echo "<thead>";
           echo "<tr>";
           echo "<th>N° Cuota</th>";
           echo "<th>Vencimiento</th>";
           echo "<th>Monto</th>";
           echo "<th>Estado</th>";
           echo "<th>Fecha Pago</th>";
           echo "</tr>";
           echo "</thead>";
           echo "<tbody>";

              $sql2="SELECT cc.idcredito,cc.compra,cc.proveedor,p.proveedor,cc.fecha_credito,cc.total,cc.abonado,cc.saldo,pcv.nro_cuota,pcv.monto,pcv.fecha_pago,pcv.fecha,pcv.estado from credito_compra cc
              join proveedores p on p.idproveedor=cc.proveedor
              join credito_compra_pagos pcv on pcv.credito=cc.idcredito
              where cc.idcredito=".$row['idcredito']." and cc.estado=0  order by pcv.nro_cuota";

              $res2 = mysqli_query($conexion,$sql2);

               $total = 0;
               $nro_cuota = 0;

              foreach ($res2 as $row2) {
                 
                    $nro_cuota += 1;

                    $idcredito = $row2['idcredito'];
                     
                     //fila de color amarillo
                    if ($row2['fecha']==$hoy && $row2['estado']==2) {
                        echo "<tr class='bg-warning' style='font-color:red'>";
                        echo "<td><p class='text-white'>".$nro_cuota."</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha'])."</p></td>";
                        echo "<td><p class='text-white'>".formatearNumero($row2['monto'])."</p></td>";


                        if ($row2['estado']==0) {
                          echo "<td><p class='text-white'>Pendiente</p></td>";
                          $total+=$row2['monto'];


                         $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>
                               ";
                        //fila de color rojo
                        }else if ($row2['estado']==2) {
                          echo "<td><p class='text-white'>Vencida</p></td>";
                          $total+=$row2['monto'];


                        $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                          
                        }else{
                          echo "<td>Pagado</td>";
                          echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                        }

                        echo "</tr>";
                        //fila de color rojo
                    }else if ($row2['fecha']!=$hoy && $row2['estado']==2) {
                        echo "<tr class='bg-danger'>";
                        echo "<td><p class='text-white'>".$nro_cuota."</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha'])."</p></td>";
                        echo "<td><p class='text-white'>".formatearNumero($row2['monto'])."</p></td>";


                        if ($row2['estado']==0) {
                          echo "<td><p class='text-white'>Pendiente</p></td>";
                          $total+=$row2['monto'];


                         $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>
                               ";
                        //mostrar color rojo cuando estado es igual a vencido
                        }else if ($row2['estado']==2) {
                          echo "<td><p class='text-white'>Vencida</p></td>";
                          $total+=$row2['monto'];


                         $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                          
                        }else{
                          echo "<td>Pagado</td>";
                          echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                        }
                        echo "</tr>";

                    }else if($row2['estado']==1){

                       echo "<tr class='bg-success'>";
                        echo "<td><p class='text-white'>".$nro_cuota."</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha'])."</p></td>";
                        echo "<td><p class='text-white'>".formatearNumero($row2['monto'])."</p></td>";
                        echo "<td><p class='text-white'>Pagado</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                        
                        echo "</tr>";
                    }else{
                          echo "<tr>";
                          echo "<td>".$nro_cuota."</td>";
                          echo "<td>".formatearFecha($row2['fecha'])."</td>";
                          echo "<td>".formatearNumero($row2['monto'])."</td>";


                          if ($row2['estado']==0) {
                            echo "<td>Pendiente</td>";
                            $total+=$row2['monto'];


                           $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                          
                          echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                            
                          }else if ($row2['estado']==2) {
                            echo "<td>Vencida</td>";
                            $total+=$row2['monto'];


                           $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                          
                          echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                            
                          }else{
                            echo "<td>Pagado</td>";
                            echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                          }

                          echo "</tr>";

                        }

              }
          echo "<tr>";
          echo "<td><b>SALDO: <b></td>";
          echo "<td><b>".formatearNumero($total)." Gs.</b></td>";
          echo "</tr>";

   echo '</tbody>';
   echo "</table>"; 

   echo "</div>";
   echo "</div>";
   echo "</div>";
   echo "</div>";
  
  }

     
  
 }else{
   echo '<script>alertify.error("Ninguna compra crédito registrada!!!");</script>';
 }
}else{
  $sql="SELECT cc.idcredito,cc.compra,cc.proveedor,p.proveedor,cc.fecha_credito,cc.total,cc.abonado,cc.saldo,cc.estado from credito_compra cc
join proveedores p on p.idproveedor=cc.proveedor
where  cc.proveedor like '%$proveedor%' and cc.estado=0
GROUP BY cc.idcredito";

$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){

       foreach ($res as $row) { 

          echo "<br><div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
          echo "<div class='box box-primary print1'><br>";
          echo "<div class='box-header with-border'><br>";
          echo "<h4>Total: ".formatearNumero($row['total'])." | Abonado: ".formatearNumero($row['abonado'])."<br>";
       
           echo "</div>";
           echo "<div class='box-body'>";
           echo "<div class='table-responsive table--no-card m-b-30'>";
           echo "<table class='table table-borderless table-striped table-earning' width='100%' cellspacing='0'>";
           echo "<thead>";
           echo "<tr>";
           echo "<th>N° Cuota</th>";
           echo "<th>Vencimiento</th>";
           echo "<th>Monto</th>";
           echo "<th>Estado</th>";
           echo "<th>Fecha Pago</th>";
           echo "</tr>";
           echo "</thead>";
           echo "<tbody>";

              $sql2="SELECT cc.idcredito,cc.compra,cc.proveedor,p.proveedor,cc.fecha_credito,cc.total,cc.abonado,cc.saldo,pcv.nro_cuota,pcv.monto,pcv.fecha_pago,pcv.fecha,pcv.estado from credito_compra cc
              join proveedores p on p.idproveedor=cc.proveedor
              join credito_compra_pagos pcv on pcv.credito=cc.idcredito
              where cc.idcredito=".$row['idcredito']." and cc.estado=0  order by pcv.nro_cuota";

              $res2 = mysqli_query($conexion,$sql2);

               $total = 0;
               $nro_cuota = 0;

              foreach ($res2 as $row2) {
                 
                    $nro_cuota += 1;

                    $idcredito = $row2['idcredito'];
                     
                     //fila de color amarillo
                    if ($row2['fecha']==$hoy && $row2['estado']==2) {
                        echo "<tr class='bg-warning'>";
                        echo "<td><p class='text-white'>".$nro_cuota."</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha'])."</p></td>";
                        echo "<td><p class='text-white'>".formatearNumero($row2['monto'])."</p></td>";


                        if ($row2['estado']==0) {
                          echo "<td><p class='text-white'>Pendiente</p></td>";
                          $total+=$row2['monto'];


                         $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>
                               ";
                        //fila de color rojo
                        }else if ($row2['estado']==2) {
                          echo "<td><p class='text-white'>Vencida</p></td>";
                          $total+=$row2['monto'];


                        $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                          
                        }else{
                          echo "<td>Pagado</td>";
                          echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                        }

                        echo "</tr>";
                        //fila de color rojo
                    }else if ($row2['fecha']!=$hoy && $row2['estado']==2) {
                        echo "<tr class='bg-danger'>";
                        echo "<td><p class='text-white'>".$nro_cuota."</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha'])."</p></td>";
                        echo "<td><p class='text-white'>".formatearNumero($row2['monto'])."</p></td>";


                        if ($row2['estado']==0) {
                          echo "<td><p class='text-white'>Pendiente</p></td>";
                          $total+=$row2['monto'];


                         $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>
                               ";
                        //mostrar color rojo cuando estado es igual a vencido
                        }else if ($row2['estado']==2) {
                          echo "<td><p class='text-white'>Vencida</p></td>";
                          $total+=$row2['monto'];


                         $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                        
                        echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                          
                        }else{
                          echo "<td>Pagado</td>";
                          echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                        }
                        echo "</tr>";

                    }else if($row2['estado']==1){

                       echo "<tr class='bg-success'>";
                        echo "<td><p class='text-white'>".$nro_cuota."</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha'])."</p></td>";
                        echo "<td><p class='text-white'>".formatearNumero($row2['monto'])."</p></td>";
                        echo "<td><p class='text-white'>Pagado</p></td>";
                        echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                        
                        echo "</tr>";
                    }else{
                          echo "<tr>";
                          echo "<td>".$nro_cuota."</td>";
                          echo "<td>".formatearFecha($row2['fecha'])."</td>";
                          echo "<td>".formatearNumero($row2['monto'])."</td>";


                          if ($row2['estado']==0) {
                            echo "<td>Pendiente</td>";
                            $total+=$row2['monto'];


                           $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                          
                          echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                            
                          }else if ($row2['estado']==2) {
                            echo "<td>Vencida</td>";
                            $total+=$row2['monto'];


                           $data=$row2['idcredito']."|".$row2['monto']."|".$row2['nro_cuota']."|".$nro_cuota."|".$row2['compra']."|".$row2['fecha']."|".$row2['proveedor']."|".$row2['saldo'];
                          
                          echo "<td><button id='".$data."' class='btn btn-xs btn-success' title='Registrar Pago' onclick='abona_ticket(this.id);'><i class='fa  fa-arrow-right'></i></button>";
                            
                          }else{
                            echo "<td>Pagado</td>";
                            echo "<td><p class='text-white'>".formatearFecha($row2['fecha_pago'])."</p></td>";
                          }

                          echo "</tr>";

                        }

              }
          echo "<tr>";
          echo "<td><b>SALDO: <b></td>";
          echo "<td><b>".formatearNumero($total)." Gs.</b></td>";
          echo "</tr>";

   echo '</tbody>';
   echo "</table>"; 

   echo "</div>";
   echo "</div>";
   echo "</div>";
   echo "</div>";
  
  }
 }else{
   echo '<script>alertify.error("Ninguna compra crédito registrada!!!");</script>';
 }
}

?>