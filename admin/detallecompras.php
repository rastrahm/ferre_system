<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

    include("modal_ingrediente.php");
    include ("consulta_factura.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
           <div class="main-content">
                <div class="container-fluid">
        <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-share"></i> Compras
        </div>
        <div class="card-body">
          <div class="col-sm-offset-6 col-sm-12">
            <h3 class="text-center"> <small class="mensaje"></small></h3>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered" id="detallecompras" width="100%" cellspacing="0">
              <thead>
        <tr> 
            <th class="hidden">IDCOMPRA</th>
            <th>PROVEEDOR</th> 
            <th>N° FACTURA</th>
            <th>FECHA</th>
            <th>HORA</th>               
            <th>TOTAL</th>
            <th>ESTADO</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div>
    <form id="frmEliminarCompra" action="" method="POST">
      <input type="hidden" id="idcompra" name="idcompra" value="">
      <input type="hidden" id="opcion" name="opcion" value="modificar">
      <!-- Modal -->
      <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modalEliminarLabel">Anular Compra</h4>
            </div>
            <div class="modal-body">              
              ¿Está seguro de anular la factura?<strong data-name=""></strong>
            </div>
            <div class="modal-footer">
              <button type="button" id="eliminar-venta" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
    </form>
  </div>
            </div>
            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <script src="js/detallecompras.js"></script>
    <?php include("scripts.php");  ?>

</body>

</html>
<!-- end document-->

<script type="text/javascript">
      $("#seguridad").hide(0);

  $("#ocultar").click(function(){
    $('#modalDetalles').modal('hide');
  });
  
  $(document).on("ready", function(){
      listar();
      eliminar();
    });

    var eliminar = function(){
      $("#eliminar-venta").on("click", function(){
        var idcompra = $("#frmEliminarCompra #idcompra").val(),
          opcion = $("#frmEliminarCompra #opcion").val();
        $.ajax({
          method:"POST",
          url: "anularcompra.php",
          data: {"idcompra": idcompra, "opcion": opcion}
        }).done( function( info ){
         var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          $('#detallecompras').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
       var texto = "", color = "";
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADO"){
          swal({
              title: 'Atención!',
              text: 'Compra Anulada',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADOYA" ){
           swal({
              title: 'Atención!',
              text: 'Compra ya se anuló',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      } 
    }


  var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.detalles", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idcompra = data.idcompra,
        estado = data.estado
          $.ajax({
          method:"POST",
          url: "busca_compras_detalle.php",
          data: 'idcompra='+idcompra+'&estado='+estado,
           success: function(x1){
            $("#detallesfactura").html(x1);
            $("#modalDetalles").modal({
             show:true,
             backdrop: 'static',
             keyboard: false
            });
           },
        })
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.anular", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idcompra = $("#frmEliminarCompra #idcompra").val( data.idcompra );
      });
    }

   var usuValido = "<?php echo isset($_SESSION['usuarioValido']) ? $_SESSION['usuarioValido'] : 'X' ?>";
  verificarLogueo(usuValido);
</script>
