<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<?php 
    include("header.php");
     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;


    include("conexion.php");
    include ("seguridad/formatos.php");

    $hoy=date("Y-m-d");

     $sql = "select count(idpedido) from pedido where estado=1";
    $res = mysqli_query($conexion,$sql);

    foreach ($res as $row) {
        $pedidospen = $row['count(idpedido)'];
    }
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbarcocinero.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
                    <div class="container-fluid"><br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1"><?php echo $fecha; ?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c1">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                 <a href="ingredientes2.php">
                                               <i class="zmdi zmdi-truck"></i></a>
                                            </div>
                                            <div class="text">
                                                <h2></h2>
                                                <span>Ingredientes</span>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <a href="verpedidos.php">
                                                <i class="zmdi zmdi-alert-circle-o"></i></a>
                                            </div>
                                            <div class="text">
                                                <h2><?php echo $pedidospen;?></h2>
                                                <span>Pedidos Pendientes</span>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
            <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   

</body>

</html>
<!-- end document-->

<script type="text/javascript">
    $("#seguridad").hide(0);
    var usuValido = "<?php echo isset($_SESSION['usuarioValido']) ? $_SESSION['usuarioValido'] : 'X' ?>";
    verificarLogueo(usuValido);
</script>