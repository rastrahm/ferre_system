<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");

?>
<link rel="stylesheet" href="css/leaflet.css" />
  <script src="js/leaflet.js"></script>
  <script src='js/leaflet-src.js'></script>
  <link rel="stylesheet" href="css/leaflet-routing-machine.css" />
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <link rel="stylesheet" href="css/Control.FullScreen.css" />
  <script src="js/Control.FullScreen.js"></script>

<link href="css/loader.css" rel="stylesheet">
<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
            include("modal_ubicacion.php");
            include("pedidos_detalle.php");
            include("consulta_stock_pedido.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <div class="container-fluid"><br>
              <div align="center">
                <div class="card-body">
                   <div align="left">
                  <?php //include("mostrar_pedidos.php");?>
                  <div id="mostrar_pedidos" class="row">
                    
                  </div>

                  </div><br>
                </div>
               </div>
            </div>
            <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idarticulo" name="idarticulo" value="">
              <input type="hidden" id="idpedid" name="idpedid" value="">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Articulo</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el articulo?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" onclick="eliminar_articulo();" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   

</body>

</html>
<!-- end document-->

<script type="text/javascript">

    $(document).on("ready", function(){
      buscar_pedidos();
    });

   //mostrar reservas de parque

    function buscar_pedidos(){

              $.ajax({
              url: 'mostrar_pedidos_pendientes.php',
              type: 'POST',
              success: function(data){
      
                $("#mostrar_pedidos").html(data).fadeIn('slow');
               },
               error: function(jqXHR,estado,error){
                 $("#mostrar_pedidos").html(estado+"    "+error);
               }
               });
    }

    function hablar(){

        var Jarvis = new Artyom();
 
        Jarvis.say("¡Hay nuevos pedidos!");

  }

    setInterval('buscar_pedidos()',5000);

     function facturar(id){
        var de=id.split("|");
        var idpedido=de[0];
        var color=de[1];
        var opcion = 'facturar';
        var cargo = 2;
          $.ajax({
              url: 'accionespedidos.php',
              type: 'POST',
              data: 'idpedido='+idpedido+'&color='+color+'&opcion='+opcion+'&cargo='+cargo
              }).done( function( info ){   
                var json_info = JSON.parse( info );
                mostrar_mensaje( json_info );
                $('#mostrar_pedidos').load('mostrar_pedidos_pendientes.php');
                 window.open("ventas.php?idpedido="+idpedido);
              });
    }

      function verificar(id){
        var de=id.split("|");
        var idpedido=de[0];
        var color=de[1];
        var opcion = 'verificar';
        var cargo = 2;
          $.ajax({
              url: 'accionespedidos.php',
              type: 'POST',
              data: 'idpedido='+idpedido+'&color='+color+'&opcion='+opcion+'&cargo='+cargo
              }).done( function( info ){   
                var json_info = JSON.parse( info );
                mostrar_mensaje( json_info );
                $('#mostrar_pedidos').load('mostrar_pedidos_pendientes.php');
              });
        }

        function anular(id){
        var de=id.split("|");
        var idpedido=de[0];
        var color=de[1];
        var opcion = 'anular';
        var cargo = 2;
          $.ajax({
              url: 'accionespedidos.php',
              type: 'POST',
              data: 'idpedido='+idpedido+'&color='+color+'&opcion='+opcion+'&cargo='+cargo
             }).done( function( info ){   
                var json_info = JSON.parse( info );
                mostrar_mensaje( json_info );
                $('#mostrar_pedidos').load('mostrar_pedidos_pendientes.php');
              });
        }

      function consultar_stock(idarticulo){
          $.ajax({
          method:"POST",
          url: "busca_stock_articulo_pedido.php",
          data: 'idarticulo='+idarticulo,
           success: function(x1){
            $("#detallestock").html(x1);
            $("#modalStock").modal("show");
           },
        })
    }

  function modificar(idpedido){
          $("#idped").val(idpedido);
          $.ajax({
          method:"POST",
          url: "busca_pedido_detalle_modificar.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallespedido").html(x1);
            $("#modalDetalles").modal("show");
           },
        })
    }

    function eliminar(id){
        var de=id.split("|");
        var idpedido=de[0];
        var articulo=de[1];
        $("#modalEliminar").modal("show");

        $("#idarticulo").val(articulo);
        $("#idpedid").val(idpedido);
    }

    function eliminar_articulo() {
        var idarticulo = $("#idarticulo").val();
        var idpedido = $("#idpedid").val();
        var opcion = "eliminar";
        $.ajax({
                      url: 'accionespedidos.php',
                      type: 'POST',
                      data: 'idpedido='+idpedido+'&idarticulo='+idarticulo+'&opcion='+opcion
                     }).done( function( info ){   
                        var json_info = JSON.parse( info );
                        mostrar_mensaje( json_info );
                        $("#modalEliminar").modal("hide");
                        $("#modalDetalles").modal("hide");
                      });
    }

function editar(nodo){

       var tr = nodo.parentNode.parentNode;
    var valPorDef = tr.querySelectorAll('td');
    tr.innerHTML = '<td><input type="text" class="form-control" name="art" id="art" size="3" disabled value="' + valPorDef[0].textContent +
     '"></td><td><input type="text" class="form-control" name="nom" id="nom" size="15" disabled value="' + valPorDef[1].textContent +
      '"></td><td><input type="number" class="form-control" style="text-align:center" onchange="validar_can(this.value)" onkeyup="validar_can(this.value)" name="can" id="can" size="1" value="' + valPorDef[2].textContent +
       '"></td> <td><input type="text" class="form-control" name="prec" disabled id="prec" size="5" value="' + valPorDef[3].textContent +
        '"></td><td><input type="text" class="form-control" name="sub" disabled id="sub" size="5" value="' + valPorDef[4].textContent +
        '"></td><td><button title="Guardar Cambios" class="btn btn-success" onclick="guardar_editar();"><i class="fa fa-check-circle"></i></button></td>';
}

function validar_can(can) {
    if (can<0) {
        alertify.error("La cantidad debe ser mayor a 0");
        $("#can").val("1");
    }else{
    }
}

function guardar_editar() {

  var idarticulo = $("#art").val();
  var cantidad = $("#can").val();
  var precio = $("#prec").val().replace(/(?!-)[^\d]/g, '');
  var idpedido = $("#idped").val();
  var opcion = "editar";

   $.ajax({
              url: 'accionespedidos.php',
              type: 'POST',
              data: 'idpedido='+idpedido+'&idarticulo='+idarticulo+'&opcion='+opcion+'&cantidad='+cantidad+'&precio='+precio
             }).done( function( info ){   
                var json_info = JSON.parse( info );
                mostrar_mensaje( json_info );
                $("#modalDetalles").modal("hide");
              });

}

function ubicacion(idpedido) {

          $.ajax({
          method:"POST",
          url: "ver_ubicacion_pedido.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallesubicacion").html(x1);
            $("#modalUbicacion").modal("show");
           },
        })
    }

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADO"){
          swal({
              title: 'Atención!',
              text: 'El pedido ha sido anulado!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ABIERTA"){
          swal({
              title: 'Atención!',
              text: 'La caja ya esta abierta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
      else if( informacion.respuesta == "MODIFICADO"){
          swal({
              title: 'Bien!',
              text: 'El pedido ha sido modificado!',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }


</script>
