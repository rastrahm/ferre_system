<?php
	session_start();
    session_unset();	//Elimina informaciones de todas las sesiones
	session_destroy();	//Cierra la sesion
	header("Location:../../index.php");
?>