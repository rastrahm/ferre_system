<?php

include ("conexion.php");
include ("seguridad/formatos.php");

$idproveedor = $_POST['idproveedor'];
$nrofactura = $_POST['nrofactura'];
$sql = "SELECT cc.idcredito,c.tipo_comprobante,c.nrofactura,cc.saldo,cc.fecha_credito from credito_compra cc
join compra_cab c on c.idcompra=cc.compra
where cc.proveedor=$idproveedor and c.nrofactura like '%$nrofactura%' and cc.estado=0";
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){
    echo "<div class='table-responsive'>";
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Tipo</th>";
    echo "<th>Fecha</th>";
    echo "<th>Nro Factura</th>";
    echo "<th>Saldo</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($res)){

    echo "<tr>";
    echo "<td>".$row['tipo_comprobante']."</td>";
    echo "<td>".formatearFecha($row['fecha_credito'])."</td>";
    echo "<td>".$row['nrofactura']."</td>";
    echo "<td>".formatearNumero($row['saldo'])."</td>";
    echo "<td><button type='button' id='".$row['nrofactura']."' class='btn btn-primary btn-xs' onclick='add_art(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
  echo "</div>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}

?>