
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     include ("conexion.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>
               
                             <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Empleados</strong> 
                                    </div>
                                     <form name="formulario" id="guardarDatos" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="card-body card-block">

                                      <div class="row">

                                          <input type="hidden" id="idempleado" name="idempleado" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                             <div class="col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nombre Completo</label>
                                                
                                                    <input type="text" id="nombres" name="nombres" class="form-control" required="">
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Cedula de Identidad</label>
                                                
                                                    <input type="text" id="documento" name="documento" class="form-control" required="">
                                            </div>
                                           
                                            <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Numero de Contacto</label>
                                                    <input type="text" id="telefono" name="telefono" class="form-control">
                                             </div>
                                              <div class="col-md-3">
                                                    <label for="textarea-input" class=" form-control-label">Direccion</label>
                                                    <input type="text" id="direccion" name="direccion" class="form-control">
                                             </div>
                                              <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Cargo</label>
                                                    <select class="form-control" name="cargo" id="cargo" class="form-control" required="">
                                                      <option>Seleccione un cargo</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from cargos";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idcargo"]."'>".$row["desc_cargo"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                             <div class="col-md-3">
                                                    <label for="text-input" class=" form-control-label">Ciudad</label>
                                                    <select class="form-control" name="ciudad" id="ciudad" class="form-control" required="">
                                                      <option>Seleccione una ciudad</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from ciudades";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idciudad"]."'>".$row["ciudad"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                            </div>
                                          </div>
                                            <div class="card-footer" align="right">
                                                          <button type="submit" class="btn btn-success">
                                                              <i class="fa fa-check"></i> Guardar
                                                          </button>    
                                                  </div> 
                                            </form>
                                            </div><br>

                                        
                   

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablaempleados" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th class="hidden">ID</th>
                                            <th>DOCUMENTO</th>
                                            <th>EMPLEADO</th>
                                            <th>TELEFONO</th>
                                            <th>DIRECCION</th>
                                            <th>CARGO</th>
                                            <th>CIUDAD</th>
                                            <th class="hidden">IDCARGO</th>
                                            <th class="hidden">IDCIUDAD</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div> 

                               </div>

                            </div>
        </div>     

          <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idempleado" name="idempleado" value="">
              <input type="hidden" id="opcion2" name="opcion2" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Empleado</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar al empleado?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
            
            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/empleados.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">


    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarEmpleados.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablaempleados').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
                   limpiar_datos();
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula o Ruc ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idempleado = $("#frmEliminarArticulo #idempleado").val(),
          opcion = $("#frmEliminarArticulo #opcion2").val();
        $.ajax({
          method:"POST",
          url: "guardarEmpleados.php",
          data: {"idempleado": idempleado, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablaempleados').DataTable().ajax.reload();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idcliente").val("");
      $("#nombres").val("");
      $("#direccion").val("");
      $("#documento").val("");
      $("#telefono").val("");
      $("#ciudad").val("Seleccione una ciudad");
      $("#cargo").val("Seleccione un cargo");
    }


    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var documento = $("#documento").val( data.documento),
            idempleado = $("#idempleado").val( data.idempleado ),
            nombres = $("#nombre").val( data.nombre ),
            direccion = $("#direccion").val( data.direccion ),
            telefono = $("#telefono").val( data.telefono ),
            cargo = $("#cargo").val( data.idcargo ),
            ciudad = $("#ciudad").val( data.idciudad ),
            opcion = $("#opcion").val("modificar");
             window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idempleado = $("#frmEliminarArticulo #idempleado").val( data.idempleado );
      });
    }

    
</script>
