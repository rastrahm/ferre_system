<?php
  include ("conexion.php");

  $sql = "SELECT cc.idcompra,p.proveedor,cc.nrofactura,cc.fecha,cc.hora,cc.total,cc.estado from compra_cab cc
      join proveedores p on p.idproveedor=cc.proveedor";
  $res = mysqli_query($conexion,$sql);
  
  if(!$res){
    die("Error");
  }else{
    while( $data = mysqli_fetch_assoc($res)) {
      $arreglo["data"][] = $data;
    }
    echo json_encode($arreglo);
  } 

  mysqli_free_result($res);
  mysqli_close($conexion);
?>