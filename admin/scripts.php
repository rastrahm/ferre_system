    <script src="js/jquery-1.12.3.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <script src="js/bloquear.js"></script>
    <script src="alertifyjs/alertify.js"></script>
    <script src="js/artyom.window.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap4.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <!--Libreria para exportar Excel-->
    <script src="js/jszip.min.js"></script>
    <!--Librerias para exportar PDF-->
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!--Librerias para botones de exportación-->
    <script src="js/buttons.html5.min.js"></script>
<!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Vendor JS       -->
     <script src="js/datetime.js"></script>
    <script src="moment/moment.js"></script>
    <script src="moment/moment.min.js"></script>
    <script src="daterangepicker/daterangepicker.js"></script>
    <script src="printarea/jquery.printarea.js"></script>

    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js"></script>

    <script src="js/main.js"></script>
    <script src="js/seguridad.js"></script>

    <script type="text/javascript">
        
        function cerrarsesion(){
        $('#cerrarsesion').modal('show');
    }

    $("#seguridad").hide(0);

     var usuValido = "<?php echo isset($_SESSION['usuarioValido']) ? $_SESSION['usuarioValido'] : 'X' ?>";
      verificarLogueo(usuValido);
  
    </script>