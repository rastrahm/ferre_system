<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
             <div class="container-fluid"><br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Reporte de Caja</h2>
                                </div>
                            </div>
                        </div><br>
                    <div class="card mb-3">
                      <div class="card-body">
                           <div class="row">
                            <div class="col-md-2"><br>
                              <div class='box box-primary'>
                                  <div class='box-body'>
                                    <div class="form-group">
                                            <label>Fechas:</label>
                                            <div class="input-group">
                                              <button class="btn btn-default pull-left" id="daterange-btn">
                                                <i class="fa fa-calendar"></i> Click para seleccionar.
                                                <i class="fa fa-caret-down"></i>
                                              </button>
                                            </div><br>
                                            <span class='fe'></span>
                                            <input type='hidden'  class='form-control' id='fi' value=''>
                                            <input type="hidden"  class='form-control' id='ff' value=''>
                                    </div><!-- /.form group -->
                                     
                                  </div>
                                  <div class='box-footer'>
                                 <button class='btn btn-primary pull-right' onclick='trae_venta();' id='btn-sig'><i class='fa fa-search'></i> Generar Reporte</button>
                                </div>
                              </div>
                            </div><br>
                              <div class="col-md-8"><br>
                                <div id='data'>
                              </div>
                              </div>
                          </div>
                        </div>
                      </div>
                                   <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
    <?php include("seguridad.php");  ?>
    <?php include("scripts.php");  ?>
   

</body>

</html>
<!-- end document-->

<script type="text/javascript">
    
    $("#seguridad").hide(0);
    var usuValido = "<?php echo isset($_SESSION['usuarioValido']) ? $_SESSION['usuarioValido'] : 'X' ?>";
    verificarLogueo(usuValido);

    $(function(){
      $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Este dia': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Los ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Los ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('.fe').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          var xstart=start.format('YYYY-MM-DD');
          var xend=end.format('YYYY-MM-DD');
          $("#fi").val(xstart);
          $("#ff").val(xend);
          //alert(start.format('YYYY-MM-DD')+'    '+end.format('YYYY-MM-DD'));
         }
        );
       });

function trae_venta(){
  $(document).ready(function(){
    if($("#fi").val()==""){
      alertify.error("Indica un rango de fechas valido...");
      $("#daterange-btn").focus();
    } else{
      var data_enviar='fechai='+$("#fi").val()+'&fechaf='+$("#ff").val();
    $.ajax({
          beforeSend: function(){
            $("#data").html("Buscando ventas, un momento...");
           },
          url: 'busca_salidas_reporte.php',
          type: 'POST',
          data: data_enviar,
          success: function(datares){
             $("#data").html(datares);
           },
           error: function(jqXHR,estado,error){
             $("#data").html(error+"    "+estado);
           }
    });
    }
  })
}

function print1(){
   $(".print1").printArea();
}


</script>
