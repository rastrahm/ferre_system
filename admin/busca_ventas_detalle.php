<?php

include ("conexion.php");
include ("./seguridad/formatos.php");


$idventa = $_POST["idventa"];
$estado = $_POST["estado"];

$sql="SELECT vd.idventa,c.idcombo,c.combo,vd.cantidad,vd.subtotal from ventas_cab vc 
            join ventas_det vd on vd.idventa=vc.idventa 
            join combos c on c.idcombo=vd.articulo 
            where vc.idventa='$idventa'";

$res = mysqli_query($conexion,$sql);

 if($estado==0){
   echo "<div class='box box-primary'>";
   echo "<div class='box-header with-border'>";
   echo "</div><br>";
   echo "<div class='box-body'>";
   echo "<table class='table table-bordered table-hover' id='tabla_ventas_detalle'>";
   echo "<thead>";
   echo "<tr>";
   echo "<th>Articulo</th>";
   echo "<th>Cantidad</th>";
   echo "<th>Subtotal</th>";
   echo "</tr>";
   echo "</thead>";
   echo "<tbody>";
    while($row = mysqli_fetch_array($res)){
      echo "<tr>";
      echo "<td>".$row['combo']."</td>";
      echo "<td>".$row['cantidad']."</td>";
      echo "<td>".formatearNumero($row['subtotal'])."</td>";
      echo "</tr>";
    }
   echo '</tbody>';
   echo "</table>";
   echo "</div>";
   echo "</div>";
 }else{
   echo "Venta Anulada";
 }

?>