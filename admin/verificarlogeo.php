<form method="post" name="verificar" action="">
<!-- Modal -->
<div id="verificar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Confirmar Contraseña</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="contraseña" class="col-sm-2 control-label">Contraseña</label>
            <div class="col-sm-6">
              <input type="password" name="contrasena" class="form-control" id="contrasena" value="">
            </div>
          </div>
          <input type="hidden" name="opcion" id="opcion" value="registrar">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		    <button type="button" class="btn btn-primary" onclick="procesarapertura();" id="ocultar" >Procesar</button>
      </div>
    </div>

  </div>
</div> 
 </form>