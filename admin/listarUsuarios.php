<?php
	include ("conexion.php");

	$sql = "SELECT u.idusuario,e.idempleado,e.nombre,u.nivel,u.idsucursal,s.desc_suc,u.nrocaja,n.desnivel,u.usuario,u.contrasena,u.estado from usuarios u
			JOIN sucursal s on s.idsucursal=u.idsucursal
			join niveles n on n.idnivel=u.nivel
			join empleados e on e.idempleado=u.empleado";
	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>