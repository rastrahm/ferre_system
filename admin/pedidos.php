<!DOCTYPE html>
<html lang="es">

<?php 
    include("header2.php");
?>

<link href="css/loader.css" rel="stylesheet">
<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            $nivel = $_SESSION['nivel'];

            if ($nivel==1) {
              include("navbar.php");
            }else{
              include("navbar_logistica.php");
            }
            
            include("pedidos_detalle.php");
            include("modal_ubicacion.php");
            include("conexion.php");

            $usuario = $_SESSION['nombreUsuario'];
            
            $idsucursal = $_SESSION['idsucursal'];

            $sql = "SELECT empleado from usuarios where usuario='$usuario'";
            $res = mysqli_query($conexion,$sql);
            foreach ($res as $row) {
                $idempleado = $row["empleado"];
            }

        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <div class="container-fluid"><br>
              <input type="hidden" id="empleado" value="<?php echo $idempleado ?>">
                <div class="card-body">
                  <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning" id="tablapedidos" width="100%" cellspacing="0">
                      <thead>
                        <?php  if ($nivel==1) {
                            echo "  <tr>
                                    <th>N° Pedido</th>
                                    <th>Cliente</th>
                                    <th>Barrio</th>
                                    <th>Tipo Pedido</th>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Estado</th>
                                    <th></th>
                                   </tr>" ;
                          }else{
                            echo "  <tr>
                                    <th></th>
                                    <th>Ruta</th>
                                    <th>N° Orden</th>
                                    <th>N° Pedido</th>
                                    <th>Distancia</th>
                                    <th>Vehiculo</th>
                                    <th>Fecha Salida</th>
                                    <th>Hora</th>
                                    <th>Estado</th>
                                   </tr>";
                                      } ?>
                      

                       </thead>
                       <tbody>
                     </tbody>
                      </table>
                  </div>
                  </div><br>
            </div>
                                   <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php"); ?>
    <?php include("scripts.php");  ?>

   <?php
       if ($nivel==1) {
                  ?>
                  <script src="funciones/pedidos.js"></script>
                  <?php
              }else if ($nivel==5){
                  ?>
                  <script src="funciones/pedidos_logistica.js"></script>
                  <?php

              }else if ($nivel==3){
                  ?>
                  <script src="funciones/pedidos.js"></script>
                  <?php

              }
    ?>

    <script src="js/leaflet.js"></script>
    <script src='js/leaflet-src.js'></script>
    <script src="js/leaflet-routing-machine.js"></script>
    <script src="js/Control.Geocoder.js"></script>
    <script src="js/Control.FullScreen.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

    $(document).on("ready", function(){
      listar();
    });

    var obtener_data_detalles_logistica = function(tbody, table){
      $(tbody).on("click", "button.detalles2", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idpedido = data.pedido
          $.ajax({
          method:"POST",
          url: "busca_pedido_detalle.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallespedido").html(x1);
            $("#modalDetalles").modal("show");
           },
        })
      });
    }

    var obtener_data_detalles = function(tbody, table){
      $(tbody).on("click", "button.detalles", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idpedido = data.idpedido
          $.ajax({
          method:"POST",
          url: "busca_pedido_detalle.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallespedido").html(x1);
            $("#modalDetalles").modal("show");
           },
        })
      });
    }

    var obtener_data_ruta = function(tbody, table){
      $(tbody).on("click", "button.ruta", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idpedido = data.idpedido
          window.open("mostrar_ruta_pedido.php?idpedido="+idpedido);
      });
    }

    var obtener_data_posicion = function(tbody, table){
      $(tbody).on("click", "button.posicion", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idpedido = data.idpedido
          window.open("mostrar_viaje.php?idpedido="+idpedido);
      });
    }

    var obtener_data_ruta_logistica = function(tbody, table){
      $(tbody).on("click", "button.ruta2", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idpedido = data.pedido,
        idruta = data.ruta
          window.open("mostrar_ruta_logistica.php?idpedido="+idpedido+"&idruta="+idruta);
      });
    }

    var obtener_data_ubicacion = function(tbody, table){
      $(tbody).on("click", "button.ubicacion", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idpedido = data.idpedido
          $.ajax({
          method:"POST",
          url: "ver_ubicacion_pedido.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallesubicacion").html(x1);
            $("#modalUbicacion").modal("show");
           },
        })
      });
    }

var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADO"){
          swal({
              title: 'Atención!',
              text: 'El pedido ha sido anulado!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ABIERTA"){
          swal({
              title: 'Atención!',
              text: 'La caja ya esta abierta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }


</script>
