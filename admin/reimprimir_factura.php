<?php
// Queremos hacer en pdf la factura numero 1 de la tipica BBDD de facturacion
require('fpdf/fpdf.php');
require_once("conexion.php");
require_once("./seguridad/formatos.php");
require_once('CifrasEnLetras.php');
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',6);

// Imprimimos el logo a 300 ppp
// Consulta a la base de datos para sacar cosas de la factura 1
	$sumiva10 = 0;
	$sumiva5 = 0;
	$subtotal = 0;
	$subtotal5 = 0;
	$subtotal10 = 0;
	$margenSuperior = 71;
	$altoFila = 5;
	$nrofactura = $_GET["nrofactura"];

	$sql = "select * from ventas_cab where nrofactura like '%$nrofactura%'";
			$res = mysqli_query($conexion, $sql);
			foreach ($res as $row) {
				$idventa = $row['idventa'];
	        }

	$sql = "SELECT vc.idventa,vc.nrofactura,c.documento,c.razonsocial,vc.tipofactura,vc.fecha,vc.hora,vc.total_comprobante_c_iva,vc.estado,c.direccion
      from ventas_cab vc
      join clientes c on c.idcliente=vc.cliente
      where vc.idventa='$idventa'";
			$result = mysqli_query($conexion, $sql);
			$res=mysqli_fetch_array($result);

	date_default_timezone_set('America/Asuncion');
	$fecha = date("d/m/Y");
	$hora = date("H:i:s");
	$pdf->Cell(110,22, $pdf->Image("img/logo.png", $pdf->GetX(), $pdf->GetY(),40,20),1); 
	$pdf->SetXY(10, 31);

	$texto2 = "MATERIALES DE CONSTRUCCION\nCONCEPCION - PARAGUAY";
	$pdf->SetXY(50, 10);
	$pdf->MultiCell(70,5.5,utf8_decode($texto2),0,"C");

	$texto3 = "TIMBRADO N°: 12345678                RUC: 856343-4"."\nFECHA INICIO VIGENCIA: 01/12/2020            		"."\nFECHA INICIO VIGENCIA: 01/12/2021"."\n FACTURA N° 001-001-".utf8_decode($res[1])."";
	$pdf->SetXY(120, 10);
	$pdf->MultiCell(80,5.5,utf8_decode($texto3),1,"L");

	$texto4 = "FECHA DE EMISIÓN: ".$fecha."";
	$pdf->SetXY(10, 32);
	$pdf->MultiCell(90,8,utf8_decode($texto4),1,"L");


		$texto4 = "COND. DE VENTA: 	CONTADO 	              X																															CREDITO";
		$pdf->SetXY(100, 32);
		$pdf->MultiCell(100,8,utf8_decode($texto4),1,"L");
		# code...

	$texto4 = "RUC o CI: ".utf8_decode($res[2])."\nNOMBRE O RAZÓN SOCIAL: ".utf8_decode($res[3])."\nDIRECCIÓN: ".utf8_decode($res[9]);
	$pdf->SetXY(10, 40);
	$pdf->MultiCell(190,8,utf8_decode($texto4),1,"L");

	$pdf->SetXY(10, 64);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(15,7,"COD",1,0,"C",true);
	$pdf->Cell(15,7,"CANT",1,0,"C",true);
	$pdf->Cell(70,7,"ARTICULOS",1,0,"C",true);
	$pdf->Cell(15,7,"P.UNIT",1,0,"C",true);
	$pdf->Cell(75,3.5,"VALOR DE VENTAS",1,0,"C",true);

	$pdf->SetXY(125, 67.5);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(28,3.5,"EXENTAS",1,0,"C",true);

	$pdf->SetXY(153, 67.5);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(23,3.5,"5%",1,0,"C",true);

	$pdf->SetXY(176, 67.5);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(24,3.5,"10%",1,0,"C",true);

	//bordes de factura

	$pdf->SetXY(10, 71);
	$pdf->Cell(15,57,"",1,0,"C",true);
	$pdf->Cell(15,57,"",1,0,"C",true);
	$pdf->Cell(70,57,"",1,0,"C",true);
	$pdf->Cell(15,57,"",1,0,"C",true);
	$pdf->Cell(75,57,"",1,0,"C",true);

	//sector valor de ventas

	$pdf->SetXY(125, 71);
	$pdf->Cell(28,57,"",1,0,"C",true);
	$pdf->SetXY(153, 71);
	$pdf->Cell(23,57,"",1,0,"C",true);

	$sql2 = "SELECT vd.articulo, vd.cantidad, a.nombreart, vd.precio, vd.subtotal_c_iva, a.iva
			FROM ventas_det vd
			JOIN articulos a on a.idarticulo=vd.articulo
			WHERE vd.idventa ='$idventa'";
	$result2 = mysqli_query($conexion, $sql2);
	// Los datos (en negro)
	$pdf->SetTextColor(0,0,0);

	while($res2=mysqli_fetch_array($result2)){
		$pdf->SetY($margenSuperior);	//Margen superior
		$pdf->SetX(10);
		$pdf->Cell(15,5,$res2[0],0,0,"L");
		$pdf->Cell(15,5,$res2[1],0,0,"L");
		$pdf->Cell(70,5,$res2[2],0,0,"L");
		$pdf->Cell(15,5,formatearNumero($res2[3]),0,0,"R");

		if ($res2[5]==10) {

			$iva10 = $res2[4]/11;
			$sumiva10 += $iva10;
			$subtotal10 += $res2[4];

		$pdf->Cell(75,5,formatearNumero($res2[4]),0,0,"R");
			
		}else{

			$iva5 = $res2[4]/21;
			$sumiva5 += $iva5;
			$subtotal5 += $res2[4];

		$pdf->Cell(90,5,formatearNumero($res2[4]),0,0,"C");
		}

	
		$margenSuperior = $margenSuperior + $altoFila;
	}


	$pdf->SetXY(10, 128);				//borde	
	$pdf->Cell(190,25,"",1,0,"C",true);
	$pdf->SetXY(170, 133);				//Margen izquierdo	
	$pdf->Cell(20,5,"TOTAL:",0,0,"C");
	$pdf->SetXY(182, 133);
	$pdf->Cell(20,5,formatearNumero($subtotal5+$subtotal10),0,0,"C");

	//subtotal exenta
	$pdf->SetXY(9, 128);	
	$pdf->Cell(20,5,"SUBTOTALES:",0,0,"C");

	//subtotal iva 5
	$pdf->SetXY(160, 128);
	$pdf->Cell(20,5,formatearNumero($subtotal5),0,0,"C");
	//subtotal iva 10
	$pdf->SetXY(185, 128);
	$pdf->Cell(20,5,formatearNumero($subtotal10),0,0,"C");

	//total
	$pdf->SetXY(10, 133);
	$pdf->Cell(20,5,"TOTAL A PAGAR:",0,0,"C");
	$pdf->SetXY(50, 133);
	$pdf->Cell(20,5,utf8_decode(CifrasEnLetras::convertirNumeroEnLetras($subtotal5+$subtotal10)),0,0,"C");
	//IVA
	$pdf->SetXY(55, 141);
	$pdf->Cell(10,5,"(10%):",0,0,"C");
	$pdf->SetXY(60, 141);
	$pdf->Cell(20,5,formatearNumero($sumiva10),0,0,"C");

	$pdf->SetXY(20, 141);
	$pdf->Cell(13,5,"LIQUIDACION DEL IVA:  (5%):",0,0,"C");
	$pdf->SetXY(40, 141);
	$pdf->Cell(20,5,formatearNumero($sumiva5),0,0,"C");

	$pdf->SetXY(80, 141);
	$pdf->Cell(13,5,"TOTAL IVA:",0,0,"C");
	//TOTALIVA
	$pdf->SetXY(90, 141);
	$pdf->Cell(20,5,formatearNumero($sumiva10+$sumiva5),0,0,"C");

			  	
	mysqli_close($conexion);
	// El documento enviado al navegador
	
	$pdf->Output();

?>