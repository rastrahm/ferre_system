
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     include ("conexion.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>
               
                             <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Usuarios</strong> 
                                    </div>
                                     <form name="formulario" id="guardarDatos" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="card-body card-block">
                                      <div class="row">
                                          <input type="hidden" id="idusuario" name="idusuario" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                           <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Empleado</label>
                                                    <select class="form-control" name="empleado" id="empleado" class="form-control" required="">
                                                      <option>Seleccione un empleado</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from empleados";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idempleado"]."'>".$row["nombre"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Sucursal</label>
                                                    <select class="form-control" name="sucursal" id="sucursal" class="form-control" required="">
                                                      <option>Seleccione un sucursal</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from sucursal";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idsucursal"]."'>".$row["desc_suc"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Nivel</label>
                                                    <select class="form-control" name="nivel" id="nivel" class="form-control" required="">
                                                      <option>Seleccione un nivel</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from niveles";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["idnivel"]."'>".$row["desnivel"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                             <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">Nro caja</label>
                                                
                                                    <input type="text" id="nrocaja" name="nrocaja" class="form-control" required="">
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Usuario</label>
                                                
                                                    <input type="text" id="usuario" name="usuario" class="form-control" required="">
                                            </div>
                                            <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Contraseña</label>
                                                    <input type="password" id="contrasena" name="contrasena" class="form-control">
                                             </div>
                                              <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Estado</label>
                                                    <select class="form-control" name="estado" id="estado" class="form-control" required="">
                                                    <option value="0">Activo</option>
                                                    <option value="1">Inactivo</option>
                                                  </select>

                                            </div>
                                            </div>
                                          </div>
                                            <div class="card-footer" align="right">
                                                          <button type="submit" class="btn btn-success">
                                                              <i class="fa fa-check"></i> Guardar
                                                          </button>    
                                                  </div> 
                                            </form>
                                            </div><br>

                                        
                   

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablausuarios" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th class="hidden">ID</th>
                                            <th>ID SUCURSAL</th>
                                            <th>IDNIVEL</th>
                                            <th>ID EMPLEADO</th>
                                            <th>EMPLEADO</th>
                                            <th>SUCURSAL</th>
                                            <th>NRO CAJA</th>
                                            <th>NIVEL</th>
                                            <th>USUARIO</th>
                                            <th>ESTADO</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div> 

                               </div>

                            </div>
        </div>     

          <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idusuario" name="idusuario" value="">
              <input type="hidden" id="opcion2" name="opcion2" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Usuario</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el usuario?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
            
            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/usuarios.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">


    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarUsuarios.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablausuarios').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula o Ruc ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idusuario = $("#frmEliminarArticulo #idusuario").val(),
          opcion = $("#frmEliminarArticulo #opcion2").val();
        $.ajax({
          method:"POST",
          url: "guardarUsuarios.php",
          data: {"idusuario": idusuario, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablausuarios').DataTable().ajax.reload();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idusuario").val("");
      $("#sucursal").val("");
      $("#empleado").val("");
      $("#nrocaja").val("");
      $("#nivel").val("");
      $("#usuario").val("");
      $("#contrasena").val("");
    }


    var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idusuario = $("#idusuario").val( data.idusuario ),
            sucursal = $("#sucursal").val( data.idsucursal ),
            empleado = $("#empleado").val( data.idempleado ),
            nrocaja = $("#nrocaja").val( data.nrocaja ),
            nivel = $("#nivel").val( data.nivel ),
            usuario = $("#usuario").val( data.usuario ),
            estado = $("#estado").val( data.estado ),
            opcion = $("#opcion").val("modificar");
             window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idusuario = $("#frmEliminarArticulo #idusuario").val( data.idusuario );
      });
    }

    
</script>
