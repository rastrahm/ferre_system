<form name="formulario" id="guardarDatos" method="POST">
<div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Formulario de Proveedores</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
      <div class="modal-body">
                    <input type="hidden" id="id_proveedor" name="id_proveedor" value="">
                    <input type="hidden" id="opcion" name="opcion" value="registrar">
                    <div class="form-group">
                        <label for="proveedor">Nombre del Proveedor</label>
                        <input type="text" class="form-control" id="proveedor" name="proveedor" placeholder="Ingrese nombre del Proveedor" onkeypress="return soloLetras(event)"/>
                    </div>
                    <div class="form-group">
                        <label for="ruc">Ruc</label>
                        <input type="text" class="form-control" id="ruc" name="ruc" placeholder="Ingrese Ruc"/>
                    </div>
                     <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese dirección" onkeypress="return soloLetras(event)"/>
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese teléfono"/>
                    </div>
                    <div class="form-group">
                        <label for="ciudad">Ciudad</label>
                        <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ingrese ciudad"/>
                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" id="ocultar" class="btn btn-primary">Guardar datos</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">

  function soloLetras(e){
                       key = e.keyCode || e.which;
                       tecla = String.fromCharCode(key).toLowerCase();
                       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz-";
                       especiales = "8-37-39-46";

                       tecla_especial = false
                       for(var i in especiales){
                            if(key == especiales[i]){
                                tecla_especial = true;
                                break;
                            }
                        }

                        if(letras.indexOf(tecla)==-1 && !tecla_especial){
                            return false;
                        }
                    }

</script>