<?php
	include ("conexion.php");

	$sql = "SELECT v.idvehiculo,s.idsucursal,s.desc_suc,v.sucursal,v.marca,v.modelo,v.año,v.matricula,v.estado from vehiculos v
			join sucursal s on s.idsucursal=v.sucursal";
	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>