<div class="modal fade" id ="modal_tabla_proveedores" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Busqueda de Proveedores:</h4>
          </div>
          <div class="modal-body">
          <div class='input-group'>
          <span class='input-group-addon bg-blue'><b>Ingrese RUC:</b></span>
          <input type='text' id='proveedor_buscar' class='form-control' onkeyup="busca_p();" placeholder='Numero de cedula'>
          </div>
          <br>
            <div id='lista_proveedores'></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->