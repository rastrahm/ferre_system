<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">

        <?php
                  include("navbar.php");

          ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
            <div class="container-fluid"><br>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Lista de Precios</strong> 
                                    </div>
                                <form name="formulario" id="guardarDatos" method="post" class="form-horizontal">
                                    <div class="card-body card-block">
                                      
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                          <input type="hidden" id="idlista" name="idlista">
                                          <div class="row">         
                                          <div class="col-md-4">
                                                    <label for="text-input" class=" form-control-label">DESCRIPCION</label>
                                                
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control" required="">
                                            </div>                               
                                            <!--<div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">PORCENTAJE</label>
                                                
                                                    <input type="text" id="porcentaje" name="porcentaje" class="form-control" required="">
                                            </div>-->

                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">ESTADO</label>
                                                
                                                    <select id="estado" name="estado" class="form-control">
                                                      <option value="0">ACTIVO</option>
                                                      <option value="1">INACTIVO</option>
                                                    </select>
                                            </div>
                                           
                                         
                                          </div>
                                          
                                            
                                    </div>
                                    <div class="card-footer" align="right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            
                                            </div>
                                        </form>
                                </div>

                                <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning" id="tablaprecioslista" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th>ID</th>
                                            <th>DESCRIPCION</th>
                                            <th>FECHA ALTA</th>
                                            <th>ESTADO</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>

                            </div>
            
        <?php  
            include("footer.php");
        ?>
            <!-- END PAGE CONTAINER-->
        </div>

        <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idlistaprecio" name="idlistaprecio" value="">
              <input type="hidden" id="opcion" name="opcion" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Deshabilitar Lista</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de deshabilitar la lista de precio?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>

    </div>
  </div>
    <?php include("scripts.php");  ?>
   <script src="funciones/lista_precios.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarPrecios_lista.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablaprecioslista').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'LISTA ELIMINADA',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "NUEVO" ){
          swal({
              title: 'Bien!',
              text: 'Un producto nuevo agregado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula o Ruc ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idlista = $("#frmEliminarArticulo #idlistaprecio").val(),
          opcion = $("#frmEliminarArticulo #opcion").val();
        $.ajax({
          method:"POST",
          url: "guardarPrecios_lista.php",
          data: {"idlista": idlista, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          listar();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#descripcion").val("");
      $("#porcentaje").val("");
    }


   var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idlista = $("#idlista").val( data.idlista),
            descripcion = $("#descripcion").val( data.descripcion ),
            estado = $("#estado").val( data.estado ),
            opcion = $("#opcion").val("modificar");
            window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idlista = $("#frmEliminarArticulo #idlistaprecio").val( data.idlista );
      });
    }
    
</script>
