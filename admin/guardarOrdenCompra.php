<?php
include("conexion.php");


switch ($_POST["opcion"]) {
    case 'nuevo':

        $sucursal = $_POST["sucursal"];
        $idpresupuesto = $_POST["idpresupuesto"];
        $concepto = $_POST["concepto"];
        $idproveedor = $_POST["proveedor"];
        $fecha = date("Y-m-d");
        $hora = strftime("%H:%M:%S");
        $total = $_POST["total"];

        mysqli_autocommit($conexion, FALSE);

        $existe = existe_orden_compra($idpresupuesto, $conexion);

        if ($existe > 0) {
            $informacion["respuesta"] = "EXISTE";
            echo json_encode($informacion);
        } else {

            $query = "INSERT INTO orden_compra_cab (sucursal, idpresupuesto, proveedor, observacion, fecha_alta, total) 
							VALUES ('$sucursal', '$idpresupuesto', '$idproveedor', '$concepto', '$fecha $hora', '$total');";
            $resultado = mysqli_query($conexion, $query);
            verificar_resultado($resultado);

            $sqlid = "SELECT idordencompra from orden_compra_cab
            order by idordencompra desc LIMIT 1";
            $resid = mysqli_query($conexion, $sqlid);

            foreach ($resid as $rowid) {
                $idordencompra = $rowid["idordencompra"];
            }

            $data = json_decode($_POST['articulos']);

            foreach ($data as $value) {

                $idarticulo = $value->idarticulo;
                $cantidad = $value->cantidad;
                $costo = $value->costo;
                $subtotal = $value->subtotal;

                if ($idarticulo == '') {
                } else {

                    $query = "INSERT INTO orden_compra_det (idordencompra, articulo, cantidad, costo, subtotal) 
                        VALUES ('$idordencompra', '$idarticulo', '$cantidad', '$costo', '$subtotal');";
                    $resultado = mysqli_query($conexion, $query);
                }
            }

            if (!mysqli_commit($conexion)) {
                echo "Commit transaction failed";
                exit();
            }
        }

        break;
    case 'modificar':

        $idordencompra = $_POST["idordencompra"];
        $sucursal = $_POST["sucursal"];
        $idproveedor = $_POST["proveedor"];
        $idpresupuesto = $_POST["idpresupuesto"];
        $concepto = $_POST["concepto"];

        $query = "UPDATE orden_compra_cab set idpresupuesto=$idpresupuesto,proveedor=$idproveedor,observacion='$concepto'
				WHERE idordencompra=$idordencompra";
        $resultado = mysqli_query($conexion, $query);
        verificar_resultado($resultado);

        break;
    case 'eliminar':

        $idordencompra = $_POST["idordencompra"];

        mysqli_autocommit($conexion, FALSE);

        $query = "DELETE FROM orden_compra_cab where idordencompra=$idordencompra";
        $resultadoquery = mysqli_query($conexion, $query);
        verificar_resultado2($resultadoquery);

        if (!mysqli_commit($conexion)) {
            echo "Commit transaction failed";
            exit();
        }

        break;
    default:
        $informacion["respuesta"] = "OPCION_VACIA";
        echo json_encode($informacion);
        break;
}


$informacion = [];

function existe_orden_compra($idpresupuesto, $conexion)
{
    $query = "SELECT * FROM orden_compra_cab 
	          WHERE idordencompra='$idpresupuesto'";
    $resultado = mysqli_query($conexion, $query);
    $existe = mysqli_num_rows($resultado);
    return $existe;
}


function verificar_resultado($resultado)
{
    if (!$resultado)
        $informacion["respuesta"] = "ERROR";
    else
        $informacion["respuesta"] = "BIEN";

    echo json_encode($informacion);
}

function verificar_resultado2($resultado)
{
    if (!$resultado)
        $informacion["respuesta"] = "ERROR";
    else
        $informacion["respuesta"] = "ELIMINADO";

    echo json_encode($informacion);
}

function cerrar($conexion)
{
    mysqli_close($conexion);
}
