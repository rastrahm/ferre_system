<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Acceso a Ferresystem</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="alertifyjs/css/alertify.css">
  <link rel="stylesheet" type="text/css" href="alertifyjs/css/themes/default.css">
  <link rel="stylesheet" type="text/css" href="css/fondo.css">
  
  <script src="alertifyjs/alertify.js"></script>
</head>

<body>
  <?php
    if (isset($_SESSION["exito"])){
     if ($_SESSION['exito']==3){
                echo '
                    <script>
                        alertify.error("No esta registrado en el sistema!!!");
                    </script>
                ';
            }
            unset($_SESSION["exito"]);
    }
  ?>
  <div class="container"><br>
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Identificarse</div><br>
      <img id="profile-img" class="profile-img-card" src="img/login2.jpg"/>
      <div class="card-body">
        <form action="./seguridad/control.php" method="post" id="form">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
               <input class="form-control" name="usuario" id="usuario" type="text" placeholder="Ingrese usuario">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input class="form-control" name="contrasena" id="contrasena" type="password" placeholder="ingrese contraseña">
            </div>
          </div>
          <button type="submit" class="btn btn-success btn-block">Iniciar Sesión</button>
        </form><br>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/bloquear.js"></script>
</body>

</html>
<script type="text/javascript">

  function soloLetras(e){
                       key = e.keyCode || e.which;
                       tecla = String.fromCharCode(key).toLowerCase();
                       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                       especiales = "8-37-39-46";

                       tecla_especial = false
                       for(var i in especiales){
                            if(key == especiales[i]){
                                tecla_especial = true;
                                break;
                            }
                        }

                        if(letras.indexOf(tecla)==-1 && !tecla_especial){
                            return false;
                        }
                    }

</script>
