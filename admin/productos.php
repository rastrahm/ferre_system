<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
    include("conexion.php");
    include("consulta_producto.php");
?>

<style>
.imgWrapper3 {
    font-size: 0;
    text-align: center;
}
.imgWrapper3 .imgResponsiva {
    display: inline-block;
    font-size: inherit;
}

.imgWrapper3 .imgResponsiva {width: 33.3%; max-width: 200px;}

.imgResponsiva img {
    height: auto;
    width: 100%;
}
</style>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->

       
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
          <?php
                  include("navbar.php");

          ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>
               <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Producto</strong> 
                                    </div>
                                     <form name="formulario" id="guardarDatos" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="card-body card-block">
                                       
                                          <input type="hidden" id="idarticulo" name="idarticulo" value="">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                          <div class="row">
                                            <div class="col-md-3">
                                                    <label for="text-input" class=" form-control-label">Proveedor</label>
                                                    <select class="proveedor form-control" name="proveedor" id="proveedor" class="form-control" required="">
                                                      <option value="">Seleccione un proveedor</option>
                                                      <?php
                                                          include("conexion.php");
                                                          $sql = "SELECT * from proveedores";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["proveedor"]."'>".$row["proveedor"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                            <div class="col-md-4">
                                                    <label for="text-input" class=" form-control-label">Producto</label>
                                                
                                                    <input type="text" id="nombreart" name="nombreart" class="form-control" required="">
                                            </div>
                                              <div class="col-md-1">
                                                    <label for="textarea-input" class=" form-control-label">Costo</label>
                                                    <input type="text" id="costo" name="costo" onkeyup="format(this)" class="form-control">
                                            </div>

                                             <div class="col-md-3">
                                                    <label for="text-input" class=" form-control-label">Categoria</label>
                                                    <select class="form-control" name="clasificacion" id="clasificacion" class="form-control" required="">
                                                      <option>Seleccione una categoria</option>
                                                      <?php
                                                        
                                                          $sql = "SELECT * from clasificaciones";
                                                          $res = mysqli_query($conexion,$sql);
                                                          while($row = mysqli_fetch_array($res)){
                                                              echo "<option value='".$row["nombrecla"]."'>".$row["nombrecla"]."</option>";
                                                          }
                                                      ?>
                                                  </select>
                                            </div>
                                          
                                          
                                          </div><br>
                                            <div class="row">
                                            
                                             <div class="col-md-1">
                                                    <label for="textarea-input" class=" form-control-label">Iva</label>
                                                    <input type="number" id="iva" name="iva" class="form-control" required="">
                                             </div>
                                            <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Cod Barra</label>
                                                    <input type="text" id="codbarra" name="codbarra" class="form-control" required="">
                                             </div>
                                             <div class="col-md-4">
                                                    <label for="textarea-input" class=" form-control-label">Descripcion</label>
                                                    <textarea id="descripcion" name="descripcion" class="form-control" required=""></textarea>
                                             </div>
                                             <div class="col-md-3">
                                                    <label for="textarea-input" class=" form-control-label">Imagenes</label>
                                                    <input type="file" id="imagen[]" name="imagen[]" class="form-control" multiple="">
                                            </div>
                                            </div><br>
                                           
                                        
                                    </div>
                                      <div class="card-footer" align="right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            
                                            </div>
                                   </form>
                                </div>

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablaarticulos" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th>ID</th>
                                            <th>PROVEEDOR</th>
                                            <th>NOMBRE</th>
                                            <th>CATEGORIA</th>  
                                            <th>DESCRIPCION</th>
                                            <th>COSTO</th>
                                            <th>IVA</th>
                                            <th>ALTA</th>
                                            <th>ULT COMPRA</th>
                                            <th>ESTADO</th>
                                            <th>COD BARRA</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>

                            </div>
        </div>     
            
           
  <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idarticulo" name="idarticulo" value="">
              <input type="hidden" id="opcion" name="opcion" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Articulo</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el articulo?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/articulos.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

  function porc(str){
    var xmlhttp;
     
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("porcentaje").innerHTML=xmlhttp.responseText;
    }
    }
    xmlhttp.open("POST","filtro_porcentaje.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("clasificacion="+str);
    $("#precio").val("");
    calcular();
}


function format(input) {
  var num = input.value.replace(/\./g, '');
  if (!isNaN(num)) {
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/, '');
    input.value = num;
  }
  else {
    alertify.error("Solo se permiten numeros!");
    input.value = input.value.replace(/[^\d\.]*/g, '');
  }
}
    
    $(document).ready(function() {
              $('.proveedor').select2();
          });
    
    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

   var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("guardarDatos"));
        $.ajax({
          url: "guardarArticulos.php",
          type: "POST",
          dataType: "HTML",
          data: formData,
          cache: false,
          contentType: false,
          processData: false
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          listar();
          $('#tablaarticulos').DataTable().ajax.reload();
      })
      });
    }

     var obtener_data_detalles = function(tbody, table){
      $(tbody).on("click", "button.detalle", function(){
        var data = table.row( $(this).parents("tr") ).data();
        idarticulo = data.idarticulo
          $.ajax({
          method:"POST",
          url: "detalle_producto.php",
          data: 'idarticulo='+idarticulo,
           success: function(x1){
            $("#detallesproducto").html(x1);
            $("#modalDetalles").modal("show");
           },
        })
      });
    }


    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "IMGDELETE" ){
          swal({
              title: 'Atención!',
              text: 'Imagen Eliminada',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'Cedula ya existe',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

    function eliminarimagen(id){
        var opcion = "eliminar";
        $.ajax({
          method:"POST",
          url: "guardarImagen.php",
          data: {"idimagen": id, "opcion2": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          $('#modalDetalles').modal("hide");
          $('#tablaarticulos').DataTable().ajax.reload();
        });
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idarticulo = $("#frmEliminarArticulo #idarticulo").val(),
          opcion = $("#frmEliminarArticulo #opcion").val();
        $.ajax({
          method:"POST",
          url: "guardarArticulos.php",
          data: {"idarticulo": idarticulo, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          listar();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#nombreart").val("");
      $("#nombrecla").val("");
      $("#iva").val("");
      $("#costo").val("");
      $("#clasificacion").val("");
      $("#descripcion").val("");
      $("#codbarra").val("");
      $("#imagen").val("");
    }

  var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var codigo = $("#idarticulo").val( data.idarticulo ),
            proveedor = $("#proveedor").val(data.proveedor).trigger('change'),
            articulo = $("#nombreart").val( data.nombreart ),
            descripcion = $("#descripcion").val( data.descripcion ),
            nombrecat = $("#clasificacion").val( data.nombrecla),
            codbarra = $("#codbarra").val( data.codigobarras),
            iva = $("#iva").val( data.iva),
            costo = $("#costo").val( parseInt(data.costo).toLocaleString()),
            opcion = $("#opcion").val("modificar");
            window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idarticulo = $("#frmEliminarArticulo #idarticulo").val( data.idarticulo );
      });
    }
    
</script>
