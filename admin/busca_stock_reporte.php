<?php

include("conexion.php");
include("./seguridad/formatos.php");

//$articulo=$_POST['articulo'];
$articulo = isset($_POST["articulo"]) && trim($_POST["articulo"]) != "";
$sucursal = isset($_POST["sucursal"]) && trim($_POST["sucursal"]) != "";
$bodega = isset($_POST["bodega"]) && trim($_POST["bodega"]) != "";

/*$sucursal=$_POST['sucursal'];
$bodega=$_POST['bodega'];*/

$costototal = 0;

$sql = "SELECT b.descbodega, s.desc_suc, a.nombreart, bd.stock, a.costo FROM `bodegas_det` bd
join bodegas b on b.idbodega=bd.bodega
join articulos a on a.idarticulo=bd.articulo
join sucursal s on s.idsucursal=b.sucursal
-- where a.idarticulo=$articulo and s.idsucursal=$sucursal and b.idbodega=$bodega";

//echo $sql;
$res = mysqli_query($conexion, $sql);
$resul = mysqli_num_rows($res);

if ($resul > 0) {
  echo "<div class='col-md-12'><button class='btn btn-primary no-print' id='imprimir' onclick='print1();'><i class='fa fa-print'></i> Imprimir</button>";
  echo "<div class='box box-danger print1'>";
  echo "<div class='box-header with-border'><br>";
  echo "</div>";
  echo "<div class='box-body'>";
  echo "<div class='box-body table-responsive'>";
  echo "<table id='tabla_ventas_credito' class='table table-bordered table-hover'>";
  echo "<thead>";
  echo "<tr>";
  echo "<th>Bodega</th>";
  echo "<th>Sucursal</th>";
  echo "<th>Articulo</th>";
  echo "<th>Stock</th>";
  echo "<th>Costo</th>";
  echo "</tr>";
  echo "</thead>";
  echo "<tbody>";

  foreach ($res as $detalle) {
    $costototal += $detalle['costo'] * $detalle['stock'];
    echo "<tr>";
    echo "<td>" . $detalle['descbodega'] . "</td>";
    echo "<td>" . $detalle['desc_suc'] . "</td>";
    echo "<td>" . $detalle['nombreart'] . "</td>";
    echo "<td>" . $detalle['stock'] . "</td>";
    echo "<td>" . formatearNumero($detalle['costo'] * $detalle['stock']) . "</td>";
    echo "</tr>";
  }

  echo "<tr>";
  echo "<td><b>TOTAL: <b></td>";
  echo "<td><b>" . formatearNumero($costototal) . " Gs.</b></td>";
  echo "</tr>";
  echo "</tbody>";
  echo "</table><br>";

  echo "</div>";
  echo "</div>";
  echo "</div>";
  echo "</div><br><br>";
} else {
  echo '<script>alertify.error("Ninguna venta registrada!!!");</script>';
}
