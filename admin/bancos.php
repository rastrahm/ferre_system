
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
    $idusuario = $_SESSION['idusuario'];
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbar.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->

            <!-- STATISTIC-->
            <div class="container-fluid"><br>
                <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Bancos</strong> 
                                    </div>
                                    <form name="formulario" id="guardarDatos" method="post" class="form-horizontal">
                                    <div class="card-body card-block">
                                        
                                          <input type="hidden" id="idbanco" name="idbanco" value="">
                                          <input type="hidden" id="usuario" name="usuario" value="<?php echo $idusuario ?>">
                                          <input type="hidden" id="opcion" name="opcion" value="registrar">
                                          <div class="row">
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Descripcion</label>
                                                
                                                    <input type="text" id="desbanco" name="desbanco" class="form-control" required="">
                                            </div>
                                          
                                             <div class="col-md-3">
                                                    <label for="textarea-input" class=" form-control-label">Direccion</label>
                                                    <input type="text" id="direccion" name="direccion" class="form-control">
                                             </div>
                                              <div class="col-md-2">
                                                    <label for="textarea-input" class=" form-control-label">Telefono</label>
                                                    <input type="text" id="telefono" name="telefono" class="form-control">
                                             </div>
                                              <div class="col-md-2">
                                              <label for="textarea-input" class=" form-control-label">Ciudad</label>
                                                <select class="cliente form-control" name="ciudad" id="ciudad" required="">
                                                    <?php
                                                        include("conexion.php");
                                                        $sql = "SELECT * from ciudades";
                                                        $res = mysqli_query($conexion,$sql);
                                                        while($row = mysqli_fetch_array($res)){
                                                          echo "<option value='".$row["idciudad"]."'>".$row["ciudad"]."</option>";
                                                       }
                                                    ?>
                                                </select>
                                            </div>
                                           
                                            </div><br>
                                           
                                             
                                        
                                    </div>
                                    <div class="card-footer" align="right">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            
                                            </div>
                                   </form>
                                </div>

                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablabancos" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                            <th class="hidden">ID</th>
                                            <th>DESCRIPCION</th>
                                            <th>TELEFONO</th>     
                                            <th>DIRECCION</th>
                                            <th>CIUDAD</th>  
                                            <th>ID</th>  
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>

                            </div>
        </div>     
          <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idbanco" name="idbanco" value="">
              <input type="hidden" id="opcion" name="opcion" value="eliminar">
              <!-- Modal -->
              <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Eliminar Banco</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de eliminar el banco?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="eliminar-articulo" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
            
          

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
     <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   <script src="funciones/bancos.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">

    $(document).on("ready", function(){
      listar();
      guardar();
      eliminar();
    });

    var guardar = function(){
      $("form").on("submit", function(e){
        e.preventDefault();
        var frm = $(this).serialize();
        $.ajax({
          method: "POST",
          url: "guardarBancos.php",
          data: frm
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablabancos').DataTable().ajax.reload();
        });
      });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "VACIO" ){
          swal({
              title: 'Atención!',
              text: 'No se cargo ninguna imagen',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "EXISTE"){
           swal({
              title: 'Atención!',
              text: 'El usuario ya existe!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

  var eliminar = function(){
      $("#eliminar-articulo").on("click", function(){
        var idbanco = $("#frmEliminarArticulo #idbanco").val(),
          opcion = $("#frmEliminarArticulo #opcion").val();
        $.ajax({
          method:"POST",
          url: "guardarBancos.php",
          data: {"idbanco": idbanco, "opcion": opcion}
        }).done( function( info ){
          var json_info = JSON.parse( info );
          mostrar_mensaje( json_info );
          limpiar_datos();
          $('#tablabancos').DataTable().ajax.reload();
        });
      });
    }


  var limpiar_datos = function(){
      $("#opcion").val("registrar");
      $("#idbanco").val("");
      $("#desbanco").val("");
      $("#telefono").val("");
      $("#direccion").val("");
    }


   var obtener_data_editar = function(tbody, table){
      $(tbody).on("click", "button.editar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idbanco = $("#idbanco").val( data.idbanco),
            desbanco = $("#desbanco").val( data.desbanco ),
            ciudad = $("#ciudad").val( data.idciudad ),
            telefono = $("#telefono").val( data.telefono ),
            direccion = $("#direccion").val( data.direccion ),
            opcion = $("#opcion").val("modificar");
            window.scrollTo(0,0);
      });
    }

  var obtener_id_eliminar = function(tbody, table){
      $(tbody).on("click", "button.eliminar", function(){
        var data = table.row( $(this).parents("tr") ).data();
        var idbanco = $("#frmEliminarArticulo #idbanco").val( data.idbanco );
      });
    }

    
</script>
