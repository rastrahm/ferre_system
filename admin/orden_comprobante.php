<?php
// Queremos hacer en pdf la factura numero 1 de la tipica BBDD de facturacion
require('fpdf/fpdf.php');
require_once("conexion.php");
require_once("./seguridad/formatos.php");
require_once('CifrasEnLetras.php');
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',6);

// Imprimimos el logo a 300 ppp
// Consulta a la base de datos para sacar cosas de la factura 1
	$total = 0;
	$margenSuperior = 71;
	$altoFila = 5;

	$sql = "SELECT max(idorden) from orden_entrega";
			$res = mysqli_query($conexion, $sql);
			foreach ($res as $row) {
				$idorden = $row['max(idorden)'];
	        }

	$sql = "SELECT o.idorden,e.nombre,e.documento,c.desc_cargo,o.fecha_salida,o.hora_salida,v.matricula FROM orden_entrega o
			join orden_entrega_det_ped op on op.idorden=o.idorden
			join vehiculos v on v.idvehiculo=o.vehiculo
			join pedido p on p.idpedido=op.pedido
            join orden_entrega_det_emp oe on oe.idorden=o.idorden
            join empleados e on e.idempleado=oe.empleado
            join cargos c on c.idcargo=e.cargo
			where o.idorden=$idorden";
			$result = mysqli_query($conexion, $sql);
			$res=mysqli_fetch_array($result);

	date_default_timezone_set('America/Asuncion');
	$pdf->Cell(110,22, $pdf->Image("img/logo.png", $pdf->GetX(), $pdf->GetY(),40,20),1); 
	$pdf->SetXY(10, 31);

	$texto2 = "MATERIALES DE CONSTRUCCION\nCONCEPCION - PARAGUAY";
	$pdf->SetXY(50, 10);
	$pdf->MultiCell(70,5.5,utf8_decode($texto2),0,"C");

	$texto3 = "COMPROBANTE N°".$res[0]."";
	$pdf->SetXY(120, 10);
	$pdf->MultiCell(80,5.5,utf8_decode($texto3),1,"C");

	$texto4 = "FECHA/HORA SALIDA: ".formatearFecha($res[4])." - ".$res[5]."";
	$pdf->SetXY(10, 32);
	$pdf->MultiCell(90,8,utf8_decode($texto4),1,"L");

	$texto4 = "VEHICULO:  ".$res[6]."";
	$pdf->SetXY(100, 32);
	$pdf->MultiCell(100,8,utf8_decode($texto4),1,"L");

	$texto4 = "RUC o CI: ".utf8_decode($res[2])."\nCHOFER: ".utf8_decode($res[1]);
	$pdf->SetXY(10, 40);
	$pdf->MultiCell(190,8,utf8_decode($texto4),1,"L");

	$pdf->SetXY(10, 64);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(15,7,"COD PEDIDO",1,0,"C",true);
	$pdf->Cell(15,7,"CANT",1,0,"C",true);
	$pdf->Cell(70,7,"ARTICULOS",1,0,"C",true);
	$pdf->Cell(15,7,"P.UNIT",1,0,"C",true);
	$pdf->Cell(75,7,"TOTAL",1,0,"C",true);


	//bordes de factura

	$pdf->SetXY(10, 71);
	$pdf->Cell(15,57,"",1,0,"C",true);
	$pdf->Cell(15,57,"",1,0,"C",true);
	$pdf->Cell(70,57,"",1,0,"C",true);
	$pdf->Cell(15,57,"",1,0,"C",true);
	$pdf->Cell(75,57,"",1,0,"C",true);

	//sector valor de ventas



	$sql2 = "SELECT op.pedido,pd.cantidad,a.nombreart,pd.precio,pd.subtotal FROM orden_entrega o
			join orden_entrega_det_ped op on op.idorden=o.idorden
			join pedido p on p.idpedido=op.pedido
			join pedido_det pd on pd.idpedido=p.idpedido
			join articulos a on a.idarticulo=pd.articulo
			where o.idorden=$idorden";
	$result2 = mysqli_query($conexion, $sql2);
	// Los datos (en negro)
	$pdf->SetTextColor(0,0,0);

	while($res2=mysqli_fetch_array($result2)){
		$pdf->SetY($margenSuperior);	//Margen superior
		$pdf->SetX(10);
		$pdf->Cell(15,5,$res2[0],0,0,"C");
		$pdf->Cell(15,5,formatearNumero($res2[1]),0,0,"C");
		$pdf->Cell(70,5,$res2[2],0,0,"C");
		$pdf->Cell(15,5,formatearNumero($res2[3]),0,0,"C");
		$pdf->Cell(80,5,formatearNumero($res2[4]),0,0,"C");
		$total += $res2[4];
		$margenSuperior = $margenSuperior + $altoFila;
	}


	$pdf->SetXY(10, 128);				//borde	
	$pdf->Cell(190,25,"",1,0,"C",true);
	$pdf->SetXY(170, 133);				//Margen izquierdo	
	$pdf->Cell(20,5,"TOTAL:",0,0,"C");
	$pdf->SetXY(182, 133);
	$pdf->Cell(20,5,formatearNumero($total),0,0,"C");
	
	//total
	$pdf->SetXY(10, 133);
	$pdf->Cell(20,5,"TOTAL A PAGAR:",0,0,"C");
	$pdf->SetXY(50, 133);
	$pdf->Cell(20,5,utf8_decode(CifrasEnLetras::convertirNumeroEnLetras($total)),0,0,"C");
	//IVA


			  	
	mysqli_close($conexion);
	// El documento enviado al navegador
	
	$pdf->Output();

?>