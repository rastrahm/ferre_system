<?php
	include ("conexion.php");

	$eliminar = "eliminar";

	if (strcmp($eliminar, $_POST["opcion"])==0) {
		$id_proveedor = $_POST["idproveedor"];
		$opcion = $_POST["opcion"];
	}else{
		$id_proveedor = $_POST["idproveedor"];
		$proveedor = $_POST["proveedor"];
		$direccion = $_POST["direccion"];
		$ruc = $_POST["ruc"];
		$telefono = $_POST["telefono"];
		$ciudad = $_POST["ciudad"];
		$opcion = $_POST["opcion"];
	}
	$informacion = [];

	switch ($opcion) {
		case 'registrar':
			if($ruc != "" ){
				$existe = existe_proveedor($ruc, $conexion);
				if ($existe>0) {
					$informacion["respuesta"] = "EXISTE";
					echo json_encode($informacion);
				}else{
					registrar($proveedor, $direccion, $ruc, $telefono, $ciudad, $conexion);
				}					
			}else{
				$informacion["respuesta"] = "VACIO";
				echo json_encode($informacion);
			}
			break;

		case 'modificar':
			modificar($id_proveedor, $proveedor, $direccion, $ruc, $telefono, $ciudad, $conexion);
			break;
		case 'eliminar':
			eliminar($id_proveedor,$conexion);
			break;
		default:
			$informacion["respuesta"] = "OPCION_VACIA";
			echo json_encode($informacion);
			break;
	}

	function existe_proveedor($ruc, $conexion){
		$query = "SELECT ruc FROM proveedores WHERE ruc = '$ruc';";
		$resultado = mysqli_query($conexion, $query);
		$existe_proveedor = mysqli_num_rows( $resultado );
		return $existe_proveedor;
	}

	function registrar($proveedor, $direccion, $ruc, $telefono, $ciudad, $conexion){
		$query = "INSERT INTO proveedores (ruc, proveedor, direccion, telefono, ciudad) 
		VALUES ('$ruc', '$proveedor', '$direccion', '$telefono', '$ciudad');";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado($resultado);
		cerrar($conexion);
	}

	function modificar($id_proveedor, $proveedor, $direccion, $ruc, $telefono, $ciudad,  $conexion){
		$query = "UPDATE proveedores SET proveedor='$proveedor',
									direccion='$direccion',
									ruc='$ruc', 
									telefono='$telefono',
									ciudad='$ciudad' 
									WHERE idproveedor='$id_proveedor'";
		$resultado = mysqli_query($conexion, $query);	
		verificar_resultado($resultado);
		cerrar($conexion);
	}

	function verificar_resultado($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "BIEN";
			
			echo json_encode($informacion);
		}

		function eliminar($id_proveedor, $conexion){
		$query = "DELETE from proveedores
					where idproveedor='$id_proveedor'";
		$resultado = mysqli_query($conexion, $query);		
		verificar_resultado2($resultado);
		cerrar($conexion);
	}
	
	function verificar_resultado2($resultado){
		if (!$resultado)
			$informacion["respuesta"] = "ERROR";
		else
			$informacion["respuesta"] = "ELIMINADO";
			
			echo json_encode($informacion);
		}

	function cerrar($conexion){
		mysqli_close($conexion);
	}
?>