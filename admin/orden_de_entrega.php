<!DOCTYPE html>
<html>
<meta charset=utf-8 />
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui' />
<?php 
  include("header.php");
  include ("conexion.php");
  $idsucursal = $_SESSION['idsucursal'];
?>
 
<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <!-- BREADCRUMB-->
            <?php 
              include("navbar.php");
              include ("buscar_empleado.php");
              include ("buscar_mpedido.php");
            ?>
            <!-- STATISTIC-->
            <div class="container-fluid"><br>

               <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Orden de Entrega</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                      <input type="hidden" id="nombre" name="nombre" class="form-control" required="">
                                      <input type="hidden" id="cargo" name="cargo" class="form-control" required="">
                                      <input type="hidden" id="idempleado" name="idempleado" class="form-control" required="">

                                      <input type="hidden" id="razonsocial_fac" name="razonsocial_fac" class="form-control" required="">
                                      <input type="hidden" id="barrio_fac" name="barrio_fac" class="form-control" required="">
                                      <div class="row">
                                         <div class="col-md-1">
                                            <label for="text-input" class=" form-control-label">N° ORDEN</label>
                                            <input type="text" id="idorden" name="idorden" class="form-control" required="" disabled="">
                                          </div>
                                          <div class="col-md-2">   
                                            <label for="text-input" class=" form-control-label">Pedido</label>
                                              <div class="input-group">
                                                <div class="input-group-btn">
                                                  <button class="btn btn-primary" onclick="busqueda_ped()"><i class="fa fa-search"></i>
                                                  </button>
                                                </div>
                                                  <input type="text" class="form-control" onchange="busca_pedido();" name="pedido" id="idpedido" style="text-align:center;">
                                                </div>    
                                            </div> 
                                            <div class="col-md-2">   
                                              <label for="text-input" class=" form-control-label">Empleado</label>
                                            <div class="input-group">
                                              <div class="input-group-btn">
                                                <button class="btn btn-primary" onclick="busqueda_emp()"><i class="fa fa-search"></i>
                                                </button>
                                              </div>
                                                <input type="text" class="form-control" onchange="busca_empleado();" name="documento" id="documento" style="text-align:center;" required="">
                                              </div>    
                                              </div> 
                                          
                                            <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Vehiculo</label>
                                                
                                                     <select id="vehiculo" name="vehiculo" class="form-control">
                                                  <?php
                                                    $res = mysqli_query($conexion, "SELECT * FROM vehiculos where sucursal=$idsucursal");

                                                    foreach ($res as $row) {
                                                      echo "<option value='".$row["idvehiculo"]."'> ".$row["marca"]." - ".$row["modelo"]."</option>";
                                                    }
                                                   

                                                  ?>

                                               </select>
                                            </div>
                                           <div class="col-md-2">
                                                    <label for="text-input" class=" form-control-label">Fecha Salida</label>
                                                
                                                    <input type="date" id="fecha_salida" name="fecha_salida" class="form-control" required="">
                                            </div>
                                           <div class="col-md-1">
                                                    <label for="text-input" class=" form-control-label">Hora Salida</label>
                                                
                                                    <input type="time" id="hora_salida" name="hora_salida" class="form-control" required="">
                                            </div>
    
                              <div class="col-md-6" align="center"><br>
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablaempleados" width="100%" cellspacing="0">
                                    <thead>
                                         <tr align="center">
                                            <th>CI</th>
                                            <th>NOMBRE</th>
                                            <th>ROL</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>
                              </div>

                               <div class="col-md-6" align="center"><br>
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning" id="tablapedidos" width="100%" cellspacing="0">
                                    <thead>
                                         <tr align="center">
                                            <th>N° PEDIDO</th>
                                            <th>CLIENTE</th>
                                            <th>BARRIO</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                              </div>
                              </div>

                            </div>
                                      </div>
                                      <div class="card-footer" align="right">
                                                <button type="button" onclick="guardar()" class="btn btn-success"><i class="fa fa-check"></i> Guardar</button>
                                      </div>
                                    </div>
                                </div>
                </div>

             
          </div>
      </div>

      <?php include("footer.php"); ?>
    </div>


    <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>

 </body> 

 </html>

<script type="text/javascript">

  function busca_nro_comprobante() {
          $.ajax({
          url: 'busca_ultimo_orden_entrega.php',
          dataType: 'json',
          type: 'POST',
          success: function(data){

            if (data=='0') {
             var idorden = 1;
            }else{
             var idorden = parseInt(data[0].idorden)+1;
            }

            $("#idorden").val(idorden);

           },
          });
}

  function agregar(){
         
            var documento = $("#documento").val();
            var nombre = $("#nombre").val();
            var cargo = $("#cargo").val();
            var idempleado = $("#idempleado").val();

              $("#tablaempleados > tbody").prepend("<tr style='text-align:center'><td class='documento'>"+documento+
                        "</td><td class='nombre'>"+nombre+
                        "</td><td class='cargo'>"+cargo+
                        "</td><td class='idempleado' style='display:none'>"+idempleado+
                        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
              
    }

     function agregar_p(){
         
            var razonsocial_fac = $("#razonsocial_fac").val();
            var barrio_fac = $("#barrio_fac").val();
            var idpedido = $("#idpedido").val();

              $("#tablapedidos > tbody").prepend("<tr style='text-align:center'><td class='idpedido'>"+idpedido+
                        "</td><td class='cliente'>"+razonsocial_fac+
                        "</td><td class='barrio'>"+barrio_fac+
                        "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
              
    }

      $(function(){
         // Evento que selecciona la fila y la elimina
        $(document).on("click",".delete",function(){
            var parent = $(this).parents().parents().get(0);
          $(parent).remove();
        });
       });

  function busca_pedido(){
          $.ajax({
          url: 'busca_data_pedido.php',
          dataType: 'json',
          type: 'POST',
          data: 'idpedido='+$("#idpedido").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el pedido!!!");
            $("#idpedido").val("");
            $("#idpedido").focus();
            }else{
            $("#idpedido").val(data[0].idpedido);
            $("#razonsocial_fac").val(data[0].razonsocial_fac);
            $("#barrio_fac").val(data[0].barrio_fac);
            agregar_p();
            }
           },
          });
}

function busqueda_ped(){
   $("#modal_pedidos").modal("show");
   $('#modal_pedidos').on('shown.bs.modal', function () {
   $("#lista_pedidos").html("");
   $("#pedido_buscar").val("");
   $("#pedido_buscar").focus();
   busca_p();
   });
}

function busca_p(){
    $.ajax({
        beforeSend: function(){
          $("#lista_pedidos").html("");
          },
        url: 'busca_pedidos_ayuda.php',
        type: 'POST',
        data: 'idpedido='+$("#pedido_buscar").val(),
        success: function(x){
         $("#lista_pedidos").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_pedidos").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
function add_ped(ped){
  //alert(art);
  $("#modal_pedidos").modal("toggle");
  $("#idpedido").val(ped);
  busca_pedido();
}

function busca_empleado(){
          $.ajax({
          url: 'busca_data_empleado.php',
          dataType: 'json',
          type: 'POST',
          data: 'documento='+$("#documento").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el empleado!!!");
            $("#documento").val("");
            $("#documento").focus();
            }else{
            $("#documento").val(data[0].documento);
            $("#idempleado").val(data[0].idempleado);
            $("#nombre").val(data[0].nombre);
            $("#cargo").val(data[0].desc_cargo);
            agregar();
            }
           },
          });
}

function busqueda_emp(){
   $("#modal_empleados").modal("show");
   $('#modal_empleados').on('shown.bs.modal', function () {
   $("#lista_empleados").html("");
   $("#empleado_buscar").val("");
   $("#empleado_buscar").focus();
   busca_e();
   });
}

function busca_e(){
    $.ajax({
        beforeSend: function(){
          $("#lista_empleados").html("");
          },
        url: 'busca_empleados_ayuda.php',
        type: 'POST',
        data: 'idempleado='+$("#empleado_buscar").val(),
        success: function(x){
         $("#lista_empleados").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_empleados").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
function add_emp(cli){
  //alert(art);
  $("#modal_empleados").modal("toggle");
  $("#documento").val(cli);
  busca_empleado();
}

		function guardar() {

         var vehiculo = $("#vehiculo").val();
         var fecha_salida = $("#fecha_salida").val();
         var hora_salida = $("#hora_salida").val();

          let empleados = [];
          let pedidos = [];

                    document.querySelectorAll('#tablaempleados tbody tr').forEach(function(e){
                      let fila = {
                        idempleado: e.querySelector('.idempleado').innerText
                      };
                      empleados.push(fila);
                    });

                    document.querySelectorAll('#tablapedidos tbody tr').forEach(function(e){
                      let fila = {
                        idpedido: e.querySelector('.idpedido').innerText
                      };
                      pedidos.push(fila);
                    });

			             $.ajax({
                      url: 'guardarOrdenEntrega.php',
                      dataType: 'json',
                      type: 'POST',
                     data: {'pedidos': JSON.stringify(pedidos), 'empleados': JSON.stringify(empleados), 'vehiculo': vehiculo, 'fecha_salida': fecha_salida, 'hora_salida': hora_salida}
                  }).done( function( info ){ 
                    var json_info = JSON.parse( JSON.stringify(info) );
          			    mostrar_mensaje( json_info );
                    busca_nro_comprobante();
                    imprimir_comprobante();
                  });
		}

function imprimir_comprobante(){
   window.open("orden_comprobante.php"); 
}


 var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "MODIFICADO" ){
         
      }
    }



		

</script>
