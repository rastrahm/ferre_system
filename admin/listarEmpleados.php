<?php
	include ("conexion.php");

	$sql = "SELECT e.idempleado,e.documento,e.nombre,c.idcargo,c.desc_cargo,e.telefono,e.direccion,cc.idciudad,cc.ciudad from empleados e
		join cargos c on c.idcargo=e.cargo
		join ciudades cc on cc.idciudad=e.ciudad";
	$res = mysqli_query($conexion,$sql);
	
	if(!$res){
		die("Error");
	}else{
		while( $data = mysqli_fetch_assoc($res)) {
			$arreglo["data"][] = $data;
		}
		echo json_encode($arreglo);
	}	

	mysqli_free_result($res);
	mysqli_close($conexion);
?>