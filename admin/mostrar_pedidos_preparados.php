<?php

include ("conexion.php");
include ("seguridad/formatos.php");
        
        $sql = "SELECT p.idpedido, p.tipo_pedido, tp.desc_tip_ped, p.barrio_fac, p.direccion_fac, p.razonsocial_fac, p.telefono_fac, p.fecha, p.hora, minute(hora) as minutos, hour(hora) as horas, p.estado,p.total from pedido p
          join tipo_pedido tp on tp.idtipo_ped=p.tipo_pedido where p.estado in (5)";

        $resultado = mysqli_query($conexion,$sql);
        foreach ($resultado as $row) {

        $fecha=date("Y-m-d");
          
        $hor = strftime("%H");
        $min = strftime("%M");

        $hora = $row['horas'];
        $minutos = $row['minutos'];
              
        $resta = $min-$minutos;

         if($resta<10){

          $color = 'Verde';

          echo '<div class="card-body">';
              echo '<div class="card" style="width: 30rem;">
                     <div class="card-header bg-success" align="center">
                        <strong class="card-title text-light"> PEDIDO: '.$row["idpedido"].' ('.$row["desc_tip_ped"].')</strong>';
              if ($row["estado"]==3) {
                   echo '<div class="bt-spinner"></div> <strong style="color:white">En proceso de facturacion</strong>';
              }else if ($row["estado"]==4) {
                   echo '<br><i style="color:white" class="fa fa-check-square"></i> <strong style="color:white"> Facturado</strong>';
              }

              echo '</div>';
                    

            $sql2 = "SELECT pd.idpedido,a.nombreart,pd.cantidad,pd.articulo,pd.subtotal FROM pedido_det pd
                   join articulos a on a.idarticulo=pd.articulo 
                   WHERE pd.idpedido=".$row["idpedido"];

                $subtotal = 0;
                $resultado2 = mysqli_query($conexion,$sql2);

                 echo "<div class='card-body'>";

                  echo '<div align="left"><strong class="card-title text-black">CLIENTE: '.$row["razonsocial_fac"].' <br>BARRIO: '.$row["barrio_fac"].' <br>DIRECCION: '.$row["direccion_fac"].'<br>TELEFONO: '.$row["telefono_fac"].' <a href="tel: '.$row["telefono_fac"].'"> <i class="fa fa-phone-square"></i> Llamar</a> <br>FECHA: '.formatearFecha($row["fecha"]).' <br>HORA: '.$row["hora"].'</strong></div>';

                echo "<div class='table-responsive'><br>";
                  echo "<table class='table table-bordered table-hover'>";
                  echo "<thead>";
                  echo "<tr>";
                  echo "<th>Codigo</th>";
                  echo "<th>Articulo</th>";
                  echo "<th>Cantidad</th>";
                  echo "<tbody>";

                foreach ($resultado2 as $row2) {

                    echo "<tr>";
                    echo "<td>".$row2['articulo']."</td>";
                    echo "<td>".$row2['nombreart']."</td>";
                    echo "<td>".$row2['cantidad']."</td>";
                    echo "</tr>";

                  $subtotal += $row2["subtotal"];
                                       
                }

                echo "<tr>";
                  echo "<td><b>TOTAL: <b></td>";
                  echo "<td><b>".formatearNumero($subtotal)." Gs.</b></td>";
                  echo "</tr>";

                echo "</tbody>";
                echo "</table>";
                echo "</div>
                </div>";

                $datos=$row['idpedido']."|".$color;

                  echo '<br><div class="card-footer" align="center">
                          <button class="btn btn-success" id="'.$datos.'" onclick="entregar(this.id)"><i class="fa fa-check"></i> Entregado </button>
                      </div>';


              echo '</div>
                  </div>';
          } if($resta>=10 && $resta<=20){

            $color = 'Amarillo';

            echo '<div class="card-body">';
              echo '<div class="card" style="width: 30rem;">
                     <div class="card-header bg-warning" align="center">
                        <strong class="card-title text-light"> PEDIDO: '.$row["idpedido"].' ('.$row["desc_tip_ped"].')</strong>';
             if ($row["estado"]==3) {
                   echo '<div class="bt-spinner"></div> <strong style="color:white">En proceso de facturacion</strong>';
              }else if ($row["estado"]==4) {
                   echo '<br><i style="color:white" class="fa fa-check-square"></i> <strong style="color:white"> Facturado</strong>';
              }

              echo '</div>';
                    

            $sql2 = "SELECT a.nombreart,pd.cantidad,pd.articulo,pd.subtotal FROM pedido_det pd
                   join articulos a on a.idarticulo=pd.articulo 
                   WHERE pd.idpedido=".$row["idpedido"];

                $subtotal = 0;
                $resultado2 = mysqli_query($conexion,$sql2);

                 echo "<div class='card-body'>";

                 echo '<div align="left"><strong class="card-title text-black">CLIENTE: '.$row["razonsocial_fac"].' <br>BARRIO: '.$row["barrio_fac"].' <br>DIRECCION: '.$row["direccion_fac"].'<br>TELEFONO: '.$row["telefono_fac"].' <a href="tel: '.$row["telefono_fac"].'"> <i class="fa fa-phone-square"></i> Llamar</a> <br>FECHA: '.formatearFecha($row["fecha"]).' <br>HORA: '.$row["hora"].'</strong></div>';

                 echo "<div class='table-responsive'><br>";
                  echo "<table class='table table-bordered table-hover'>";
                  echo "<thead>";
                  echo "<tr>";
                  echo "<th>Codigo</th>";
                  echo "<th>Articulo</th>";
                  echo "<th>Cantidad</th>";
                  echo "<tbody>";

                foreach ($resultado2 as $row2) {

                    echo "<tr>";
                    echo "<td>".$row2['articulo']."</td>";
                    echo "<td>".$row2['nombreart']."</td>";
                    echo "<td>".$row2['cantidad']."</td>";
                    echo "</tr>";

                  $subtotal += $row2["subtotal"];
                                       
                }

                echo "<tr>";
                  echo "<td><b>TOTAL: <b></td>";
                  echo "<td><b>".formatearNumero($subtotal)." Gs.</b></td>";
                  echo "</tr>";

                echo "</tbody>";
                echo "</table>";
                echo "</div>
                </div>";

                  $datos=$row['idpedido']."|".$color;

                  echo '<br><div class="card-footer" align="center">
                          <button class="btn btn-success" id="'.$datos.'" onclick="entregar(this.id)"><i class="fa fa-check"></i> Entregado </button>
                      </div>';


              echo '</div>
                  </div>';
          }if($resta>20){

          $color = 'Rojo';

           echo '<div class="card-body">';
              echo '<div class="card" style="width: 30rem;">
                     <div class="card-header bg-danger" align="center">
                        <strong class="card-title text-light"> PEDIDO: '.$row["idpedido"].' ('.$row["desc_tip_ped"].')</strong>';
              if ($row["estado"]==3) {
                   echo '<div class="bt-spinner"></div> <strong style="color:white">En proceso de facturacion</strong>';
              }else if ($row["estado"]==4) {
                   echo '<br><i style="color:white" class="fa fa-check-square"></i> <strong style="color:white"> Facturado</strong>';
              }

              echo '</div>';
                    

            $sql2 = "SELECT a.nombreart,pd.cantidad,pd.articulo,pd.subtotal FROM pedido_det pd
                   join articulos a on a.idarticulo=pd.articulo 
                   WHERE pd.idpedido=".$row["idpedido"];

                $subtotal = 0;
                $resultado2 = mysqli_query($conexion,$sql2);

                 echo "<div class='card-body'>";

                 echo '<div align="left"><strong class="card-title text-black">CLIENTE: '.$row["razonsocial_fac"].' <br>BARRIO: '.$row["barrio_fac"].' <br>DIRECCION: '.$row["direccion_fac"].'<br>TELEFONO: '.$row["telefono_fac"].' <a href="tel: '.$row["telefono_fac"].'"> <i class="fa fa-phone-square"></i> Llamar</a> <br>FECHA: '.formatearFecha($row["fecha"]).' <br>HORA: '.$row["hora"].'</strong></div>';

                 echo "<div class='table-responsive'><br>";
                  echo "<table class='table table-bordered table-hover'>";
                  echo "<thead>";
                  echo "<tr>";
                  echo "<th>Codigo</th>";
                  echo "<th>Articulo</th>";
                  echo "<th>Cantidad</th>";
                  echo "<tbody>";

                foreach ($resultado2 as $row2) {

                    echo "<tr>";
                    echo "<td>".$row2['articulo']."</td>";
                    echo "<td>".$row2['nombreart']."</td>";
                    echo "<td>".$row2['cantidad']."</td>";
                    echo "</tr>";

                  $subtotal += $row2["subtotal"];
                                       
                }

                echo "<tr>";
                  echo "<td><b>TOTAL: <b></td>";
                  echo "<td><b>".formatearNumero($subtotal)." Gs.</b></td>";
                  echo "</tr>";

                echo "</tbody>";
                echo "</table>";
                echo "</div>
                </div>";

                   $datos=$row['idpedido']."|".$color;

                  echo '<br><div class="card-footer" align="center">
                          <button class="btn btn-success" id="'.$datos.'" onclick="entregar(this.id)"><i class="fa fa-check"></i> Entregado </button>
                      </div>';


              echo '</div>
                  </div>';
          }

        }
    

    ?>