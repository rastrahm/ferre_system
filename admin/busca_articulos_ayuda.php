<?php

include ("conexion.php");
include ("seguridad/formatos.php");

$articulo = $_POST['articulo'];
$idsucursal = $_POST['idsucursal'];
$idbodega=$_POST['idbodega'];
$sql = "SELECT a.idarticulo, a.nombreart, a.costo, bd.stock from articulos a
    join bodegas_det bd on bd.articulo=a.idarticulo
    join bodegas b on b.idbodega=bd.bodega
    where a.nombreart like '%$articulo%' and b.sucursal=$idsucursal and b.idbodega=$idbodega";
$res = mysqli_query($conexion,$sql);
$resul = mysqli_num_rows($res);

if($resul>0){
    echo "<div class='table-responsive'>";
    echo "<table class='table table-bordered table-hover'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Codigo</th>";
    echo "<th>Articulo</th>";
    echo "<th>Costo</th>";
    echo "<th>Agregar</th>";
    echo "<tbody>";

  while($row = mysqli_fetch_array($res)){

    echo "<tr>";
    echo "<td>".$row['idarticulo']."</td>";
    echo "<td>".$row['nombreart']."</td>";
    echo "<td>".formatearNumero($row['costo'])."</td>";
    echo "<td><button type='button' id='".$row['idarticulo']."' class='btn btn-primary btn-xs' onclick='add_art(this.id);'><i class='fa fa-reply'></i></button></td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
  echo "</div>";
}else{
  echo "<div class='callout callout-danger'>No se encontraron coincidencias...</div>";
}

?>