<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<?php 
    include("header.php");
    include ("consulta_pedido.php");

     $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $fecha=$dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
?>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">
        <!-- HEADER DESKTOP-->
        <?php 
            include("navbarrepartidor.php");
        ?>
        <!-- PAGE CONTENT-->
        <div class="page-content">
            <div class="container-fluid"><br>
              <div align="center">
                <div class="card-body">
                  <div class='row'>
                  <div class="col-md-4">
                   <label>Filtrar por codigo de pedido</label>
                    <input type="text" class="form-control" id="codigo" placeholder="" onkeyup='load(1);'><br>
                    
                      <div class='outer_div'></div><!-- Carga los datos ajax -->
                    </div>
                    </div>
                  </div>
               </div>
            </div>
                                   <!-- END STATISTIC-->

            <!-- COPYRIGHT-->
           <?php include("footer.php"); ?>
            <!-- END COPYRIGHT-->
        </div>

    </div>
    <?php  include ("seguridad.php") ?>
    <?php include("scripts.php");  ?>
   

</body>

</html>
<!-- end document-->

<script type="text/javascript">

   function cambiarestado(id){
      $(document).ready(function(){
        var idpedido=id;
        var opcion = 'entregar';
          $.ajax({
              url: 'combolisto.php',
              type: 'POST',
              data: 'idpedido='+idpedido+'&opcion='+opcion,
              success: function(x1){
                location.reload(true);    
                },
            });
        })
        }

    $(document).on("ready", function(){
      load(1);
    });

    function load(){
      var codigo= $("#codigo").val();
      var parametros={'idpedido':codigo};
      $("#loader").fadeIn('slow');
      $.ajax({
        type: "POST",
        url:'buscar_pedido.php',
        data: parametros,
         beforeSend: function(objeto){
         $('#loader').html('');
        },
        success:function(data){
          $(".outer_div").html(data).fadeIn('slow');
          $('#loader').html('');
          
        }
      })
    }

 function detalles(id){
        var idpedido = id;
    $.ajax({
          method:"POST",
          url: "busca_pedido_detalle2.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallespedido").html(x1);
            $("#modalDetalles").modal("show");
           },
        })

 }    

  setTimeout('location.reload(true)',10000);
</script>