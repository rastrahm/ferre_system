<!DOCTYPE html>
<html lang="en">

<?php
include("header.php");
?>

<style type="text/css">
    .table-wrapper {
        width: 100%;
        height: 500px;
        /* Altura de ejemplo */
        overflow: auto;
    }

    .table-wrapper table {
        border-collapse: separate;
        border-spacing: 0;
    }

    .table-wrapper table thead {
        position: -webkit-sticky;
        /* Safari... */
        position: sticky;
        top: 0;
        left: 0;
    }

    .table-wrapper table thead th,
    .table-wrapper table tbody td {
        border: 1px solid #000;
        /*background-color: #000aff;*/
    }

    .derecha {
        float: right;
    }
</style>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">

        <?php
        include("conexion.php");
        include("./seguridad/formatos.php");
        include("buscar_articulo.php");
        include("navbar.php");
        ?>

        <!-- PAGE CONTAINER-->
        <div class="page-content">
            <!-- STATISTIC-->
            <div class="container-fluid" align="center">
                <!-- HEADER DESKTOP-->
                <div class="card-body">
                    <div class="card">
                        <div class="card-header">
                            <strong>Pedidos</strong>
                            <button class="btn btn-success" onclick="nuevo();"><i class="fa fa-plus-circle"></i> Nuevo</button>
                            <button class="btn btn-primary" onclick="consultar();"><i class="fa fa-search"></i> Consultar</button>
                            <button class="btn btn-warning" id="botonmodificar" onclick="modificar();" disabled=""><i class="fa fa-edit"></i> Modificar</button>
                            <button class="btn btn-danger" id="botoneliminar" onclick="eliminar()" disabled=""><i class="fa fa-trash"></i> Eliminar</button>
                        </div>
                        <div class="card-body card-block">
                            <div class="row">
                                <input type="hidden" name="opcion" id="opcion" value="nuevo">
                                <div class='col-md-1'>
                                    <label>Comprobante:</label>
                                    <input type="number" name="idpedido" id="idpedido" class="form-control" onchange="buscar_mov()" disabled="">
                                </div>

                                <div class='col-md-2'>
                                    <label>Sucursal:</label>
                                    <select id="sucursal" name="sucursal" class="form-control" disabled="">
                                        <option>Seleccione la sucursal</option>
                                        <?php
                                        $res = mysqli_query($conexion, "SELECT * FROM sucursal");

                                        foreach ($res as $row) {
                                            echo "<option value='" . $row["idsucursal"] . "'> " . $row["desc_suc"] . "</option>";
                                        }


                                        ?>

                                    </select><br>
                                </div>


                                <div class="col-md-3">
                                    <label for="text-input" class=" form-control-label">Proveedor</label>
                                    <select class="proveedor form-control" name="proveedor" id="proveedor" class="form-control" required="" disabled="">
                                        <option>Seleccione un proveedor</option>
                                        <?php
                                        $sql = "SELECT * from proveedores";
                                        $res = mysqli_query($conexion, $sql);
                                        while ($row = mysqli_fetch_array($res)) {
                                            echo "<option value='" . $row["idproveedor"] . "'>" . $row["proveedor"] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="pago">Fecha alta</label>
                                    <input type="text" class="form-control" id="fecha_alta" name="fecha_alta" value="<?php echo date('Y-m-d H:m:s'); ?>" disabled="">
                                </div>
                            </div><br>
                            <div class="row" align="left">
                                <div class="col-md-6">
                                    <label for="pago">Concepto</label>
                                    <input type="text" class="form-control" id="concepto" name="concepto" disabled="">
                                </div>
                            </div>

                        </div>
                        <div class="card-footer" align="right">

                            <button type="button" id="guardar" class="btn btn-success" onclick="procesa_pedido();" disabled=""><i class="fa fa-check"></i> Guardar
                            </button>
                        </div>
                    </div>

                    <div align="center">
                        <div class="col-md-10">
                            <div class="table-wrapper table-responsive table--no-card m-b-30">
                                <table class="table table-borderless table-striped table-earning" id="tabla_articulos" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th class='center'>Codigo</th>
                                            <th class='center'>Articulo</th>
                                            <th class='center'>Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <?php
                include("footer.php");
                ?>
                <!-- END PAGE CONTAINER-->
            </div>
        </div>
    </div>

    <?php include("seguridad.php") ?>
    <?php
    include("scripts.php");
    ?>

</body>

</html>
<!-- end document-->
<script type="text/javascript">
    function nuevo() {
        $("#opcion").val("nuevo");
        $("#idpedido").attr("disabled", true);
        $("#guardar").attr("disabled", false);
        $("#sucursal").attr("disabled", false);
        $("#concepto").attr("disabled", false);
        $("#proveedor").attr("disabled", false);

        limpiar_campos();

        $("#tabla_articulos>tbody").append('<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
            '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
            'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" size="5" onkeypress="saltar(event)"' +
            '></td><td style="width:100px"><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

        $("#sucursal").focus();
    }

    function buscar_mov() {
        $("#tabla_articulos > tbody:last").children().remove();
        $.ajax({
            url: 'busca_data_compra_pedido.php',
            dataType: 'json',
            type: 'POST',
            data: 'idpedido=' + $("#idpedido").val(),
            success: function(data) {

                if (data == 0) {
                    alertify.error("No existe el numero de comprobante!!!");
                } else {

                    $("#idpedido").val(data[0].idcab_pedido);
                    $("#sucursal").val(data[0].sucursal);
                    $("#fecha_alta").val(data[0].fecha_alta);
                    $("#concepto").val(data[0].concepto);
                    $("#proveedor").val(data[0].proveedor).trigger('change');

                    for (var i = 0; i < data.length; i++) {
                        $("#tabla_articulos > tbody").append("<tr><td class='idarticulo'>" + data[i].articulo +
                            "</td><td class='nombreart'>" + data[i].nombreart +
                            "</td><td class='cantidad'>" + data[i].cantidad +
                            "</td></tr>");
                    }
                }
            },
        });
    }

    function consultar() {
        $("#idpedido").attr("disabled", false);
        $("#idpedido").val("");
        $("#botoneliminar").attr("disabled", false);
        $("#botonmodificar").attr("disabled", false);
        deshabilitar_campos();
        $("#idpedido").focus();
    }

    function modificar() {
        $("#idpedido").attr("disabled", true);
        $("#guardar").attr("disabled", false);
        $("#concepto").attr("disabled", false);
        $("#proveedor").attr("disabled", false);
        $("#opcion").val("modificar");
    }

    function agregar() {

        var idarticulo = $("#idart").val();
        var nombreart = $("#nom").val();
        var cantidad = parseInt($("#cant").val());

        $("#tabla_articulos > tbody").prepend("<tr style='font-weight:bold;color:black;font-size:20px;text-align:center'><td        class='idarticulo'>" + idarticulo +
            "</td><td class='nombreart'>" + nombreart +
            "</td><td class='cantidad'>" + cantidad +
            "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");

        agregar_fila();

    }

    function agregar_fila() {

        $("#tabla_articulos>tbody").prepend('<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
            '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
            'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" size="5" onkeypress="saltar(event)"' +
            '></td><td style="width:100px"><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

        $("#idart").focus();

    }

    function saltar(e) {
        (e.keyCode) ? k = e.keyCode: k = e.which;
        if (k == 13) {
            $("#confirmar").focus();
        }
    }

    function saltar2(e, id) {
        (e.keyCode) ? k = e.keyCode: k = e.which;
        if (k == 13) {
            document.getElementById(id).focus();
        }
    }

    function busca_articulo() {
        $.ajax({
            url: 'busca_data_articulo_com.php',
            dataType: 'json',
            type: 'POST',
            data: 'idarticulo=' + $("#idart").val() + '&idsucursal=' + $("#sucursal").val(),
            success: function(data) {
                if (data == 0) {
                    alertify.error("No existe el articulo!!!");
                    $("#idart").val("");
                    $("#idart").focus();
                } else {
                    $("#cant").focus();
                    $("#idart").val(data[0].idarticulo);
                    $("#nom").val(data[0].nombreart);
                }
            },
        });
    }

    function busqueda_art() {
        $("#modal_busqueda_arts").modal("show");
        $('#modal_busqueda_arts').on('shown.bs.modal', function() {
            $("#lista_articulos").html("");
            $("#articulo_buscar").val("");
            $("#articulo_buscar").focus();
            busca();
        });
    }


    function busca() {
        $.ajax({
            beforeSend: function() {
                $("#lista_articulos").html("");
            },
            url: 'busca_articulos_ayuda_com.php',
            type: 'POST',
            data: 'articulo=' + $("#articulo_buscar").val() + '&idsucursal=' + $("#sucursal").val(),
            success: function(x) {
                $("#lista_articulos").html(x);
            },
            error: function(jqXHR, estado, error) {
                $("#lista_articulos").html("Error en la peticion AJAX..." + estado + "      " + error);
            }
        });
    }


    function add_art(art) {
        //alert(art);
        $("#modal_busqueda_arts").modal("toggle");
        $("#idart").val(art);
        busca_articulo();
    }

    $(function() {
        // Evento que selecciona la fila y la elimina
        $(document).on("click", ".delete", function() {
            var parent = $(this).parents().parents().get(0);
            $(parent).remove();
        });
    });

    $(document).ready(function() {
        $('.proveedor').select2();
    });

    function format(input) {
        var num = input.value.replace(/\./g, '');
        if (!isNaN(num)) {
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/, '');
            input.value = num;
        } else {
            alertify.error('Solo se permiten numeros!');
            input.value = input.value.replace(/[^\d\.]*/g, '');
        }
    }


    function procesa_pedido() {

        var idpedido = $("#idpedido").val();
        var sucursal = $("#sucursal").val();
        var concepto = $("#concepto").val();
        var proveedor = $("#proveedor").val();
        var fecha_alta = $("#fecha_alta").val();
        var opcion = $("#opcion").val();

        let articulos = [];

        document.querySelectorAll('#tabla_articulos tbody tr').forEach(function(e) {
            let fila = {
                idarticulo: e.querySelector('.idarticulo').innerText,
                cantidad: e.querySelector('.cantidad').innerText
            };
            articulos.push(fila);
        });

        $.ajax({
            url: 'guardarCompraPedido.php',
            dataType: 'json',
            type: 'POST',
            data: {
                'articulos': JSON.stringify(articulos),
                'idpedido': idpedido,
                'sucursal': sucursal,
                'concepto': concepto,
                'proveedor': proveedor,
                'fecha_alta': fecha_alta,
                'opcion': opcion
            }
        }).done(function(info) {
            var json_info = JSON.parse(JSON.stringify(info));
            mostrar_mensaje(json_info);
            deshabilitar_campos();
            limpiar_campos();
            busca_nro_comprobante();
        });
    }

    function busca_nro_comprobante() {
        $.ajax({
            url: 'busca_ultimo_comp_pedido.php',
            dataType: 'json',
            type: 'POST',
            success: function(data) {

                $("#idpedido").val(data[0].idpedido);

            },
        });
    }

    function eliminar() {
        var idpedido = $("#idpedido").val();
        var sucursal = $("#sucursal").val();
        var opcion = "eliminar";
        $.ajax({
            url: 'guardarCompraPedido.php',
            dataType: 'json',
            type: 'POST',
            data: {
                'idpedido': idpedido,
                'sucursal': sucursal,
                'opcion': opcion
            }
        }).done(function(info) {
            var json_info = JSON.parse(JSON.stringify(info));
            mostrar_mensaje(json_info);
            deshabilitar_campos();
            limpiar_campos();
        });

    }

    function limpiar_campos(argument) {
        $("#tabla_articulos > tbody:last").children().remove();
        $("#idpedido").val("");
        $("#sucursal").val("Seleccione la sucursal");
        $("#concepto").val("");
        $("#proveedor").val('Seleccione un proveedor').trigger('change');
    }

    function deshabilitar_campos() {
        $("#tabla_articulos > tbody:last").children().remove();
        $("#guardar").attr("disabled", true);
        $("#sucursal").attr("disabled", true);
        $("#concepto").attr("disabled", true);
        $("#proveedor").attr("disabled", true);
    }

    var mostrar_mensaje = function(informacion) {
        if (informacion.respuesta == "BIEN") {
            swal({
                title: 'Bien!',
                text: 'Registro Guardado',
                type: 'success',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "ERROR") {
            swal({
                title: 'Error!',
                text: 'No se ejecuto la consulta',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "ELIMINADO") {
            swal({
                title: 'Atención!',
                text: 'Registro Eliminado',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "VACIO") {
            swal({
                title: 'Atención!',
                text: 'No se cargo ninguna imagen',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "EXISTE") {
            swal({
                title: 'Atención!',
                text: 'Cedula ya existe',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "HABILITADO") {
            swal({
                title: 'Bien!',
                text: 'Ingrediente Disponible',
                type: 'success',
                timer: 2000,
                showConfirmButton: false
            })
        }
    }
</script>