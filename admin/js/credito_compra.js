function busca_proveedor(){
     $(document).ready(function(){
      var cod=$("#proveedor").val();
          if(cod!=""){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_proveedor.php',
          dataType: 'json',
          type: 'POST',
          data: 'id_proveedor='+$("#proveedor").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el proveedor!!!");
            $("#proveedor").val("");
            $("#proveedor").focus();
            }else{
            $("#proveedor").val(data[0].proveedor);
            $("#idproveedor").val(data[0].ruc);
            }
           },
          });
        });
      }
    });
}
function busqueda_pro(){
   $("#modal_tabla_proveedores").modal({
             show:true,
             backdrop: 'static',
             keyboard: false
            });
   $('#modal_tabla_proveedores').on('shown.bs.modal', function () {
   $("#lista_proveedores").html("");
   $("#proveedor_buscar").val("");
   $("#proveedor_buscar").focus();
   });
}

function busca_p(){
    $.ajax({
        beforeSend: function(){
          $("#lista_proveedores").html("");
          },
        url: 'busca_proveedores_ayuda.php',
        type: 'POST',
        data: 'cedula='+$("#proveedor_buscar").val(),
        success: function(x){
         $("#lista_proveedores").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_proveedores").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
function add_pro(pro){
  //alert(art);
  $("#modal_tabla_proveedores").modal("toggle");
  $("#proveedor").val(pro);
  busca_proveedor();
}

function busca_cuentas_cliente(){
  $(document).ready(function() {
          if ($("#idproveedor").val()!='') {
          $.ajax({
          beforeSend: function(){
            $("#cartera_clientes").html("Consultando informacion...");
           },
          url: 'consulta_cuenta_proveedor.php',
          type: 'POST',
          data: 'ruc='+$("#idproveedor").val(),
          success: function(x){
            $("#cartera_clientes").html(x);
           },
           error: function(jqXHR,estado,error){
             $("#cartera_clientes").html(estado+"    "+error);
           }
           });
          }else{
            alertify.error("Debe elegir un proveedor!!!");
          }
          });
}

/****************************************************************************/
function abona_ticket(ti){
      var de=ti.split("|");
      var cli=de[0];
      var nro=de[1];
      var mon=de[2];
      var abn=de[3];
      var sal=de[4];

      $('#modal_abono_ticket').modal('show');

    $('#modal_abono_ticket').on('shown.bs.modal', function () {
        $('#abono').focus();
    });

    $("#nombre_c").val(cli);
    $("#n_ticket").val(nro);
    $("#total_de_ticket").val(mon);
    $("#abonado").val(abn);
    $("#resto").val(sal);

  calcula_resto();
}

/***********************************************************************************/
function verifica_abono(){
  var m4=$("#abono").val();
  var m5=$("#resto").val();
  var dif=parseInt(m5)-parseInt(m4);
  if(dif>=0){
    $("#btn-procesa-abono").attr("disabled", true);
  }else{
    $("#btn-procesa-abono").attr("disabled", true)
  }
}

/**********************************************************************************/
function calcula_resto(){
   var m1=$("#total_de_ticket").val();
   var m3=$("#abonado").val();
   var change=parseInt(m1)-parseInt(m3);
   $("#resto").val(change);
}

/***********************************************************************************/

function procesa_abono(){
   $(document).ready(function(){
          var idcompra=$("#n_ticket").val();
          var monto=$("#abono").val();
          var total=$("#total_de_ticket").val();
          var id_cliente=$("#id_cliente_credito").val();
          var opcion=$("#opcion").val();
          var fec=$("#fecha").val();
          var sal=$("#resto").val();
          $.ajax({
          url: 'procesa_abono_compra.php',
          type: 'POST',
          data: 'idcompra='+idcompra+'&abono='
          +monto+'&total='+total+'&cedula='
          +id_cliente+'&opcion='+opcion+'&fecha='+fec
           }).done( function( info ){
          var json_info = JSON.parse( info );
           mostrar_mensaje( json_info );
           busca_cuentas_cliente();
           $('#modal_abono_ticket').modal('hide');
           //ticket_credito(numero,monto,total,id_cliente,sal);
        });
    })
}
//************************************************************************************************
function revisa_pagos(id){
  $(document).ready(function(){
    var idcredito = id;
      $.ajax({
          beforeSend: function(){
            $("#pagos_realizados").html("Buscando...");
           },
          url: 'busca_abono_proveedor.php',
          type: 'POST',
          data: 'idcredito='+idcredito,
          success: function(x1){
            $("#pagos_realizados").html(x1);
             $('#modal_revisa_pagos').modal('show');
           },
           error: function(jqXHR,estado,error){
            alert("Ocurrio un error, reporte a soporte..." +estado+"     "+error);
           }
           });
    })
    }

/**************************************************************************************/
function print_pagos(){
  $(".print_abonos").printArea();
}

function ticket_credito(p1,p2,p3,p4,p5){
      var num = p1;
      var mon = p2;
      var tot = p3;
      var cli = p4;
      var sal = p5;
      window.open("ticket_credito.php?nro_factura="+num+"&monto="+mon+"&total="+tot+"&cedula="+cli+"&saldo="+sal);
}

var mostrar_mensaje = function( informacion ){
     if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'El pago ha sido registrado!',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR"){
           swal({
              title: 'Error!',
              text: 'No se ejecuto la consulta',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ANULADOYA" ){
          swal({
              title: 'Atención!',
              text: 'La venta ya se anulo',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      } 
    }