function busca_articulo(){
     $(document).ready(function(){
      var cod=$("#idarticulo").val();
          if(cod!=""){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_articulo2.php',
          dataType: 'json',
          type: 'POST',
          data: 'idingrediente='+cod,
          success: function(data){
            if(data==0){
            alertify.error("No existe el articulo!!!");
            $("#idarticulo").val("");
            $("#idarticulo").focus();
            $("#cantidad").attr("disabled", true);
            $("#precio").attr("disabled", true);
            }else{
            $("#cantidad").attr("disabled", false);
            $("#cantidad").val("");
            $("#cantidad").focus();
            $("#articulo").val(data[0].nombreing);
            $("#costo").val(data[0].costo);
            $("#existencia").val(data[0].stock);
            $("#precio").select();
            $("#precio").focus();
            }
           },
          });
        });
      }
    });
  }
function busqueda_art(){
   
   $('#modal_busqueda_arts').modal('show');

   $('#modal_busqueda_arts').on('shown.bs.modal', function () {
   $("#lista_articulos").html("");
   $("#articulo_buscar").val("");
   $("#articulo_buscar").focus();
   });
}
function busca(){
    $.ajax({
        beforeSend: function(){
          $("#lista_articulos").html("");
          },
        url: 'busca_articulos_ayuda2.php',
        type: 'POST',
        data: 'idingrediente='+$("#articulo_buscar").val(),
        success: function(x){
         $("#lista_articulos").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_articulos").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
function add_art(art){
  //alert(art);
  $("#modal_busqueda_arts").modal("toggle");
  $("#idarticulo").val(art);
  busca_articulo();
}
//*******************************************************************************
function busca_proveedor(){
     $(document).ready(function(){
      var cod=$("#proveedor").val();
          if(cod!=""){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_proveedor.php',
          dataType: 'json',
          type: 'POST',
          data: 'id_proveedor='+$("#proveedor").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el proveedor!!!");
            $("#proveedor").val("");
            $("#proveedor").focus();
            }else{
            $("#proveedor").val(data[0].proveedor);
            $("#id_proveedor").val(data[0].ruc);
            }
           },
          });
        });
      }
    });
}

function busqueda_pro(){

   $('#modal_tabla_proveedores').modal('show');

   $('#modal_tabla_proveedores').on('shown.bs.modal', function () {
   $("#lista_proveedores").html("");
   $("#proveedor_buscar").val("");
   $("#proveedor_buscar").focus();
   });
}

function busca_p(){
    $.ajax({
        beforeSend: function(){
          $("#lista_proveedores").html("");
          },
        url: 'busca_proveedores_ayuda.php',
        type: 'POST',
        data: 'cedula='+$("#proveedor_buscar").val(),
        success: function(x){
         $("#lista_proveedores").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_proveedores").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
function add_pro(pro){
  //alert(art);
  $("#modal_tabla_proveedores").modal("toggle");
  $("#proveedor").val(pro);
  busca_proveedor();
}

 function agrega_a_lista(){
   $(document).ready(function(){
         if($("#cantidad").val()>0){
            var articulo=$("#idarticulo").val();
            var nombre=$("#articulo").val();
            var cantidad=$("#cantidad").val();
            var precio=$("#costo").val();
            var monto=cantidad*precio;
            var total=$("#total").val(monto);
            $("#tabla_articulos > tbody").append("<tr><td class='center'>"+articulo+"</td><td class='center'>"+nombre+"</td><td class='center'>"+cantidad+"</td><td class='center'>"+precio+"</td><td class='center'>"+monto+"</td><td class='center'><button class='btn btn-block btn-danger btn-xs delete'><i class='fa fa-trash'></i></button></td></tr>");
            $("#idarticulo").val("");
            $("#cantidad").attr("disabled", true);
            $("#precio").attr("disabled", true);
            $("#idarticulo").focus();
            resumen();
            }else{
             alertify.error("Cantidad requerida!");
            }
            })
         }

$(function(){
         // Evento que selecciona la fila y la elimina
        $(document).on("click",".delete",function(){
        var parent = $(this).parents().parents().get(0);
      $(parent).remove();
           resumen();
        });
       });


function resumen(){
  $(document).ready(function(){
            var articulos=0;
            var monto=0;
            $('#tabla_articulos > tbody > tr').each(function(){
            articulos +=parseInt($(this).find("td").eq(2).html());
            monto+=parseInt($(this).find('td').eq(4).html());
            });
            $("#total").val(monto);
            if(articulos>0){
              $("#btn-procesa").prop('disabled', false);
              $("#btn-cancela").prop('disabled', false);
            }else{
              $("#btn-procesa").prop('disabled', true);
              $("#btn-cancela").prop('disabled', true);
            }
            })
          }

function cancela_compra(){
  $("#btn_cancela").prop("disabled", true);
         $("#tabla_articulos > tbody:last").children().remove();
            resumen();
            cancela_codigo();
            $("#idarticulo").focus();
}

function cancela_codigo(){
   $("#precioventa").val("");
   $("#cantidad").val("");
   $("#precio").attr("disabled", true);
   $("#cantidad").attr("disabled", true);
   $("#articulo").val("");
   $("#precio").val("");
   $("#costo").val("");
   $("#porcentaje").val("");
   $("#precioventa").val("");
   $("#clasificacion").val("");
   $("#existencia").val("");
   $("#total").val("");
   $("#nrofactura").val("");
   $("#proveedor").val("xxxx");
   $("#tipofactura").val("Seleccionar");
   $("#tipopago").val("Seleccionar");
   $("#idarticulo").val("");
   $("#idarticulo").focus();
}