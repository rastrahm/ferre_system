/*********************************************************************************************/
 //seguridad de la pagina

  function verificarLogueo(usuValido) {
  if (usuValido != "si"){
    $("#cuerpo").hide(0);
    $("#mainNav").hide(0);
    $("#seguridad").show("slow");
    timer(7000, // milisegundos
        function(seg) {
          $("#segundos").html("En " + seg + " segundos será redirigido...");
        },
        function() {
          location.href="./seguridad/salir.php";
        }
    );
  }
}
/*********************************************************************************************/
function timer(time,update,complete) {
    var start = new Date().getTime();
    var interval = setInterval(function() {
        var now = time-(new Date().getTime()-start);
        if( now <= 0) {
            clearInterval(interval);
            complete();
        }
        else update(Math.floor(now/1000));
    },100);
}