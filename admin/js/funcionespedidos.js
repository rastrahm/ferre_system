var listar = function(){
		var table = $("#clientes").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
		"ajax" : {
			"url": "listar.php",
			"type": "POST" 
		},
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
		"columns":[
            { "data": "id_cliente"},
			      { "data": "cedula" },
            { "data": "nombres" },
            { "data": "apellidos" },
            { "data": "teléfono" },
            { "data": "direccion" },
            { "data": "ciudad" },
            {"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#modalNuevo'><i class='fa fa-pencil-square-o'></i></button> <button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}
		],
		"language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        {
                        "text":      '<i class="fa fa-user-plus"></i>',
                        "titleAttr": 'Agregar Cliente',
                        "className": 'btn btn-success',
                        action:     function(){
                            $(document).ready(function(){
                            $("#modalNuevo").modal("show");
                            });
                            agregar_nuevo_cliente();
                        }
                    },
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
		});
    obtener_data_editar("#clientes tbody", table);
    obtener_id_eliminar("#clientes tbody", table);
}

var agregar_nuevo_cliente = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

/*********************************************************************************************/
//busqueda desde el input

function busca_articulo(){
     $(document).ready(function(){
      var cod=$("#codigo").val();
          if(cod!=""){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_articulo.php',
          dataType: 'json',
          type: 'POST',
          data: 'codigo='+$("#codigo").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el articulo!!!");
            $("#codigo").val("");
            $("#codigo").focus();
            $("#cantidad").attr("disabled", true);
            $("#precio").attr("disabled", true);
            }else{
            $("#cantidad").attr("disabled", false);
            $("#cantidad").focus();
            $("#preciou").val(data[0].precioventa);
            $("#articulo").val(data[0].nombre);
            $("#existencia").val(data[0].existencia);
            $("#clasificacion").val(data[0].tipo);
            $("#precio").select();
            $("#precio").focus();
            }
           },
          });
        });
      }
    });
  }
/*********************************************************************************************/
function busca_cliente(){
     $(document).ready(function(){
      var cod=$("#cliente").val();
        if(cod!=""){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_cliente.php',
          dataType: 'json',
          type: 'POST',
          data: 'id_cliente_credito='+$("#cliente").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el cliente!!!");
            $("#cliente").val("");
            $("#cliente").focus();
            }else{
            $("#cliente").val(data[0].nombre);
            $("#id_cliente_credito").val(data[0].cedula);
            $("#saldo_cuenta").val(data[0].saldo);
            $("#nro_tarjeta").val(data[0].nro_tarjeta);
            $("#cliente").select();
            $("#cliente").focus();
            }
           },
          });
        });
      }
    });
  }
//******************************************************************************************
function busca_vendedor(){
     $(document).ready(function(){
      var cod=$("#vendedor").val();
          if(cod!=""){
         $(document).ready(function(){
          $.ajax({
          url: 'busca_data_vendedor.php',
          dataType: 'json',
          type: 'POST',
          data: 'id_vendedor='+$("#vendedor").val(),
          success: function(data){
            if(data==0){
            alertify.error("No existe el vendedor!!!");
            $("#vendedor").val("");
            $("#vendedor").focus();
            }else{
            $("#vendedor").val(data[0].vendedor);
            $("#id_vendedor").val(data[0].cedula);
            }
           },
          });
        });
      }
    });
  }

/*********************************************************************************************/
//Busqueda de articulo
function busqueda_art(){
   $("#modal_busqueda_arts").modal({
             show:true,
             backdrop: 'static',
             keyboard: false
            });
   $('#modal_busqueda_arts').on('shown.bs.modal', function () {
   $("#lista_articulos").html("");
   $("#articulo_buscar").val("");
   $("#articulo_buscar").focus();
   });
}
/*********************************************************************************************/
function busqueda_cli(){
   $("#modal_tabla_clientes").modal({
             show:true,
             backdrop: 'static',
             keyboard: false
            });
   $('#modal_tabla_clientes').on('shown.bs.modal', function () {
   $("#lista_clientes").html("");
   $("#cliente_buscar").val("");
   $("#cliente_buscar").focus();
   });
}
/*********************************************************************************************/
function busqueda_pro(){
   $("#modal_tabla_vendedores").modal({
             show:true,
             backdrop: 'static',
             keyboard: false
            });
   $('#modal_tabla_vendedores').on('shown.bs.modal', function () {
   $("#lista_vendedores").html("");
   $("#vendedor_buscar").val("");
   $("#vendedor_buscar").focus();
   });
}
/*********************************************************************************************/
//filtrar tabla
function busca(){
    $.ajax({
        beforeSend: function(){
          $("#lista_articulos").html("");
          },
        url: 'busca_articulos_ayuda.php',
        type: 'POST',
        data: 'articulo='+$("#articulo_buscar").val(),
        success: function(x){
         $("#lista_articulos").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_articulos").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
function busca_c(){
    $.ajax({
        beforeSend: function(){
          $("#lista_clientes").html("");
          },
        url: 'busca_clientes_ayuda.php',
        type: 'POST',
        data: 'cedula='+$("#cliente_buscar").val(),
        success: function(x){
         $("#lista_clientes").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_clientes").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}

/*********************************************************************************************/
function busca_p(){
    $.ajax({
        beforeSend: function(){
          $("#lista_vendedores").html("");
          },
        url: 'busca_vendedores_ayuda.php',
        type: 'POST',
        data: 'cedula='+$("#vendedor_buscar").val(),
        success: function(x){
         $("#lista_vendedores").html(x);
         },
        error: function(jqXHR,estado,error){
          $("#lista_vendedores").html("Error en la peticion AJAX..."+estado+"      "+error);
        }
       });
}
/*********************************************************************************************/
//agregar a datos de articulos
function add_art(art){
  //alert(art);
  $("#modal_busqueda_arts").modal("toggle");
  $("#codigo").val(art);
  busca_articulo();
}

function add_cli(cli){
  //alert(art);
  $("#modal_tabla_clientes").modal("toggle");
  $("#cliente").val(cli);
  busca_cliente();
}

function add_pro(pro){
  //alert(art);
  $("#modal_tabla_vendedores").modal("toggle");
  $("#vendedor").val(pro);
  busca_vendedor();
}

/*********************************************************************************************/
//agregar a lista de articulos

function agrega_a_listav(){
   $(document).ready(function(){
         if(parseInt($('#cantidad').val())<=parseInt($('#existencia').val())){
            var articulo=$("#codigo").val();
            var nombre=$("#articulo").val();
            var cantidad=$("#cantidad").val();
            var precio=$("#preciou").val();
            var monto=cantidad*precio;
            var total=$("#total").val(monto);
            $("#tabla_articulos > tbody").append("<tr><td class='center'>"+articulo+"</td><td class='center'>"+nombre+"</td><td class='center'>"+cantidad+"</td><td class='center'>"+precio+"</td><td class='center'>"+monto+"</td><td class='center'><button class='btn btn-block btn-danger btn-xs delete'><i class='fa fa-trash'></i></button></td></tr>");
            $("#codigo").val("");
            $("#cantidad").attr("disabled", true);
            $("#precio").attr("disabled", true);
            $("#codigo").focus();
            resumen();
            }else{
             alertify.error("Cantidad excede el stock!");
            }
            })
         }
/*********************************************************************************************/
//para eliminar articulo de la tabla
$(function(){
         // Evento que selecciona la fila y la elimina
        $(document).on("click",".delete",function(){
        var parent = $(this).parents().parents().get(0);
      $(parent).remove();
           resumen();
        });
       });
/*********************************************************************************************/
//completar cabecera venta
function resumen(){
  $(document).ready(function(){
            var articulos=0;
            var monto=0;
            $('#tabla_articulos > tbody > tr').each(function(){
            articulos+=parseInt($(this).find("td").eq(2).html());
            monto+=parseInt($(this).find('td').eq(4).html());
            });
            $("#total").val(monto);
            if(articulos>0){
              $("#btn-procesa").prop('disabled', false);
              $("#btn-cancela").prop('disabled', false);
            }else{
              $("#btn-procesa").prop('disabled', true);
              $("#btn-cancela").prop('disabled', true);
            }
            })
          }
/*********************************************************************************************/
//para cancelar la venta
function cancela_venta(){
  $("#btn_cancela").prop("disabled", true);
         $("#tabla_articulos > tbody:last").children().remove();
            resumen();
            cancela_codigo();
            $("#codigo").focus();
}
/*********************************************************************************************/
//para borrar los campos
function cancela_codigo(){
   $("#precio").val("");
   $("#cantidad").val("");
   $("#precio").attr("disabled", true);
   $("#cantidad").attr("disabled", true);
   $("#articulo").val("");
   $("#preciou").val("");
   $("#costo").val("");
   $("#porcentaje").val("");
   $("#venta").val("");
   $("#clasificacion").val("");
   $("#existencia").val("");
   $("#vendedor").val("");
   $("#cliente").val("");
   $("#saldo_cuenta").val("");
   $("#total").val("");
   $("#codigo").val("");
   $("#codigo").focus();
}

/*********************************************************************************************/
//para procesar la venta
function procesa_venta(){
  $(document).ready(function(){
    $('#modal_prepara_venta').modal('toggle');
         var cliente='0';
         var fac='0';
         var fec='0';
         var tot='0';
           if(parseInt($('#total').val())<=parseInt($('#saldo_cuenta').val())){
             cliente=$("#cliente").val();
             tarjeta=$("#tarjeta").val();
             vende=$("#id_vendedor").val();
             ped=$("#nro_pedido").val();
             fec=$("#fecha").val();
             tot=$("#total").val();
             exi=$("#existencia").val();
             pag=$("#paga_con").val();
             opc=$("#opcion").val();
             est=$("#estado").val();

           $('#tabla_articulos > tbody > tr').each(function(){
                var cod = $(this).find('td').eq(0).html();
                var art = $(this).find('td').eq(1).html();
                var can = $(this).find('td').eq(2).html();
                var pre = $(this).find('td').eq(3).html();
                var sub = $(this).find('td').eq(4).html();
                $.ajax({
                  beforeSend: function(){
                    },
                      url: 'guardarpedido.php',
                      type: 'POST',
                     data: 'codigo='+cod+'&cantidad='+can+'&precio='
                     +pre+'&subtotal='+sub+'&cedula='+cliente+'&tarjeta='+tarjeta+'&vendedor='+vende+'&nro_pedido='
                     +ped+'&fecha='+fec+'&total='+tot+'&opcion='+opc+'&estado='+est
                   });
                  });
           $("#tabla_articulos > tbody:last").children().remove();
                alertify.success("Pedido registrado!!!");
                cancela_codigo();
          }else{
            alertify.error("El saldo de la cuenta no es suficiente!!!");
          }
    })
}