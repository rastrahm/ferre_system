var listar = function(){
		var table = $("#articulos").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
		"ajax" : {
			"url": "listarA.php",
			"type": "POST" 
		},
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
             {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
            }
        ],
		"columns":[
			{ "data": "id_producto" },
            { "data": "nombre" },
            { "data": "tipo" },
            { "data": "precioventa", "render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
            { "data": "existencia" },
            { "data": "imagen" },
            {"defaultContent": "<button type='button' class='detalles btn btn-success' title='ver detalles' data-toggle='modal' data-target='#modalDetalles' ><i class='fa fa-eye'></i></button> <button type='button' class='editar btn btn-primary' title='editar' data-toggle='modal' data-target='#modalNuevo'><i class='fa fa-pencil-square-o'></i></button> <button type='button' class='eliminar btn btn-danger' title='eliminar' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}
		],
		"language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        {
                        "text":      '<i class="fa fa-plus"></i>',
                        "titleAttr": 'Agregar Articulo',
                        "className": 'btn btn-success',
                        action:     function(){
                            $(document).ready(function(){
                            $("#modalNuevo").modal("show");
                            });
                            agregar_nuevo_articulo();
                        }
                    },
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
		});

    obtener_data_editar("#articulos tbody", table);
    obtener_data_detalles("#articulos tbody", table);
    obtener_id_eliminar("#articulos tbody", table);
}

var agregar_nuevo_articulo = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
