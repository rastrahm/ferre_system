function busca_articulo() {
  $(document).ready(function () {
    var cod = $("#codigo").val();
    if (cod != "") {
      $(document).ready(function () {
        $.ajax({
          url: 'busca_data_articulo.php',
          dataType: 'json',
          type: 'POST',
          data: 'idpedido=' + $("#codigo").val(),
          success: function (data) {
            if (data == 0) {
              alertify.error("No existe el pedido!!!");
              $("#codigo").val("");
              $("#codigo").focus();
              $("#cantidad").attr("disabled", true);
              $("#precio").attr("disabled", true);
            } else {

              $("#total").val(data[0].total);

              for (var i = 0; i < data.length; i++) {
                $("#tabla_articulos > tbody").append("<tr><td class='datos'>" + data[i].idcombo +
                  "</td><td>" + data[i].combo +
                  "</td><td>" + data[i].cantidad +
                  "</td><td>" + data[i].subtotal +
                  "</td></tr>");
              }
              $("#pagar").focus();
            }
          },
        });
      });
    }
  });
}
function busqueda_art() {
  $("#modal_busqueda_arts").modal({
    show: true,
    backdrop: 'static',
    keyboard: false
  });
  $('#modal_busqueda_arts').on('shown.bs.modal', function () {
    $("#lista_articulos").html("");
    $("#articulo_buscar").val("");
    $("#articulo_buscar").focus();
  });
}
function busca() {
  $.ajax({
    beforeSend: function () {
      $("#lista_articulos").html("");
    },
    url: 'busca_articulos_ayuda.php',
    type: 'POST',
    data: 'idpedido=' + $("#articulo_buscar").val(),
    success: function (x) {
      $("#lista_articulos").html(x);
    },
    error: function (jqXHR, estado, error) {
      $("#lista_articulos").html("Error en la peticion AJAX..." + estado + "      " + error);
    }
  });
}
function add_art(art) {
  //alert(art);
  $("#modal_busqueda_arts").modal("toggle");
  $("#codigo").val(art);
  busca_articulo();
}
//*******************************************************************************

function agrega_a_listav() {
  $(document).ready(function () {
    var m1 = $("#existencia").val();
    var m2 = $("#cantidad").val();

    if (parseInt(m2) <= parseInt(m1)) {
      var articulo = $("#codigo").val();
      var nombre = $("#articulo").val();
      var cantidad = $("#cantidad").val();
      var precio = $("#preciou").val();
      var monto = cantidad * precio;
      var total = $("#total").val(monto);
      $("#tabla_articulos > tbody").append("<tr><td class='center'>" + articulo + "</td><td class='center'>" + nombre + "</td><td class='center'>" + cantidad + "</td><td class='center'>" + precio + "</td><td class='center'>" + monto + "</td><td class='center'><button class='btn btn-block btn-danger btn-xs delete'><i class='fa fa-trash'></i></button></td></tr>");
      $("#codigo").val("");
      $("#cantidad").attr("disabled", true);
      $("#precio").attr("disabled", true);
      $("#codigo").focus();
      resumen();
    } else {
      alertify.error("Cantidad es mayor al stock!");
    }
  })
}

$(function () {
  // Evento que selecciona la fila y la elimina
  $(document).on("click", ".delete", function () {
    var parent = $(this).parents().parents().get(0);
    $(parent).remove();
    resumen();
  });
});



function resumen() {
  $(document).ready(function () {
    var articulos = 0;
    var monto = 0;
    $('#tabla_articulos > tbody > tr').each(function () {
      articulos += parseInt($(this).find("td").eq(2).html());
      monto += parseInt($(this).find('td').eq(4).html());
    });
    $("#total").val(monto);
    if (articulos > 0) {
      $("#btn-procesa").prop('disabled', false);
      $("#btn-cancela").prop('disabled', false);
    } else {
      $("#btn-procesa").prop('disabled', true);
      $("#btn-cancela").prop('disabled', true);
    }
  })
}

function cancela_venta() {
  $("#btn_cancela").prop("disabled", true);
  $("#tabla_articulos > tbody:last").children().remove();
  resumen();
  cancela_codigo();
  $("#codigo").focus();
}

function prepara_venta() {
  $(document).ready(function () {
    $("#modal_prepara_venta").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
    });
    $('#modal_prepara_venta').on('shown.bs.modal', function () {
      $("#paga_con").focus();
      $("#total_de_venta").val($("#total").val());
    });
  })
}

function calcula_cambio() {
  var m1 = $("#total").val();
  var m2 = $("#paga_con").val();

  if (parseInt(m2) >= parseInt(m1)) {
    $("#btn-termina").attr("disabled", false);
    var change = parseInt(m2) - parseInt(m1);
    $("#cambio").val("Gs. " + change);
  } else {
    $("#btn-termina").attr("disabled", true);
  }

}

function cancela_codigo() {
  $("#precioventa").val("");
  $("#cantidad").val("");
  $("#precio").attr("disabled", true);
  $("#cantidad").attr("disabled", true);
  $("#articulo").val("");
  $("#preciou").val("");
  $("#costo").val("");
  $("#porcentaje").val("");
  $("#venta").val("");
  $("#clasificacion").val("");
  $("#existencia").val("");
  $("#id_cliente_credito").val("");
  $("#id_proveedor").val("");
  $("#total").val("");
  $("#nro_factura1").val("");
  $("#proveedor").val("");
  $("#tipoventa").val("Contado");
  $("#cliente").val("xxxx");
  $("#codigo").val("");
  $("#codigo").focus();
}



var mostrar_mensaje = function (informacion) {
  if (informacion.respuesta == "BIEN") {
    swal({
      title: 'Bien!',
      text: 'Registro Guardado',
      type: 'success',
      timer: 2000,
      showConfirmButton: false
    })
  } else if (informacion.respuesta == "ERROR") {
    swal({
      title: 'Error!',
      text: 'No se ejecuto la consulta',
      type: 'warning',
      timer: 2000,
      showConfirmButton: false
    })
  } else if (informacion.respuesta == "ELIMINADO") {
    swal({
      title: 'Atención!',
      text: 'Ingrediente Anulado',
      type: 'warning',
      timer: 2000,
      showConfirmButton: false
    })
  } else if (informacion.respuesta == "DONACION") {
    swal({
      title: 'Bien!',
      text: 'Producto Donado',
      type: 'success',
      timer: 2000,
      showConfirmButton: false
    })
  }
}

function procesa_venta() {
  $(document).ready(function () {
    $('#modal_prepara_venta').modal('toggle');
    var tipofactura = $("#tipofactura").val();
    if ($("#tipoventa").val() == "Contado") {
      total = $("#total_de_venta").val();
      tipoventa = $("#tipoventa").val();
      cliente = $("#cliente").val();
      opcion = $("#opcion").val();

      $.ajax({
        url: 'procesa_venta.php',
        type: 'POST',
        data: 'total=' + total + "&tipoventa=" + tipoventa + "&cliente=" + cliente + "&opcion=" + opcion
      }).done(function (info) {
        var json_info = JSON.parse(info);
        mostrar_mensaje(json_info);
        detalle();
        location.reload(true);
      });
    } else if ($("#tipoventa").val() == "Credito" && $("#cliente").val() != "xxxx") {
      total = $("#total_de_venta").val();
      tipoventa = $("#tipoventa").val();
      cliente = $("#cliente").val();
      opcion = $("#opcion").val();

      $.ajax({
        url: 'procesa_venta.php',
        type: 'POST',
        data: 'total=' + total + "&tipoventa=" + tipoventa + "&cliente=" + cliente +"&tipocomprobante="+ tipofactura +"&opcion=" + opcion
      }).done(function (info) {
        var json_info = JSON.parse(info);
        mostrar_mensaje(json_info);
        detalle();
        location.reload(true);
      });
    } else {
      alertify.error("Seleccione un cliente!");
    }
  })
}

function detalle() {
  var opcion = "detalle";
  var idventa = $("#idventa").val();
  $('#tabla_articulos > tbody > tr').each(function () {
    var cod = $(this).find('td').eq(0).html();
    var can = $(this).find('td').eq(2).html();
    var sub = $(this).find('td').eq(3).html();
    $.ajax({
      url: 'procesa_venta.php',
      type: 'POST',
      data: 'idventa' + idventa + '&idarticulo=' + cod + '&cantidad=' + can + '&subtotal=' + sub + '&opcion=' + opcion
    });
  });
  cancela_codigo();
  $("#tabla_articulos > tbody:last").children().remove();
}

function busca_cliente() {
  $(document).ready(function () {
    var cod = $("#id_cliente_credito").val();
    if (cod != "") {
      $(document).ready(function () {
        $.ajax({
          url: 'busca_data_cliente.php',
          dataType: 'json',
          type: 'POST',
          data: 'cedula=' + $("#id_cliente_credito").val(),
          success: function (data) {
            if (data == 0) {
              alertify.error("No existe el proveedor!!!");
              $("#id_cliente_credito").val("");
              $("#id_cliente_credito").focus();
            } else {
              $("#id_cliente_credito").val(data[0].idcliente);
              $("#cliente").val(data[0].razonsocial);
            }
          },
        });
      });
    }
  });
}
function busqueda_cli() {
  $("#modal_tabla_clientes").modal({
    show: true,
    backdrop: 'static',
    keyboard: false
  });
  $('#modal_tabla_clientes').on('shown.bs.modal', function () {
    $("#lista_clientes").html("");
    $("#cliente_buscar").val("");
    $("#cliente_buscar").focus();
  });
}

function busca_c() {
  $.ajax({
    beforeSend: function () {
      $("#lista_clientes").html("");
    },
    url: 'busca_clientes_ayuda.php',
    type: 'POST',
    data: 'cedula=' + $("#cliente_buscar").val(),
    success: function (x) {
      $("#lista_clientes").html(x);
    },
    error: function (jqXHR, estado, error) {
      $("#lista_clientes").html("Error en la peticion AJAX..." + estado + "      " + error);
    }
  });
}
/*********************************************************************************************/
function add_cli(cli) {
  //alert(art);
  $("#modal_tabla_clientes").modal("toggle");
  $("#id_cliente_credito").val(cli);
  busca_cliente();
}