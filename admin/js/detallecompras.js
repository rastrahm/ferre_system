var listar = function(){
		var table = $("#detallecompras").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
		"ajax" : {
			"url": "listarcompras.php",
			"type": "POST" 
		},
     "columnDefs": [
                {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
                },
                 {
                      "targets": [6], // El objetivo de la columna de posición, desde cero.
                      "data": "estado", // La inclusión de datos
                      "render": function(data, type, full) { // Devuelve el contenido personalizado
                            if (data==0) {
                                return "<span style='color:green;'><i class='fa fa-check'></i> Facturado</span>";
                            }else{
                                return "<span style='color:red;'><i class='fa fa-clock-o'></i> Anulado</span>";
                            }
                      }
                  }
              ],
		"columns":[
			{ "data": "idcompra" },
            { "data": "proveedor" },
            { "data": "nrofactura" },
            { "data": "fecha" , "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY') },
            { "data": "hora" },
            { "data": "total" , "render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
            { "data": "estado" },
            {"defaultContent": "<button type='button' class='detalles btn btn-success' title='ver detalles' data-toggle='modal' data-target='#modalDetalles' ><i class='fa fa-eye'></i></button> <button type='button' class='anular btn btn-danger' title='Anular' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-ban'></i></button>"}
		],
        "language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
		});
    obtener_data_editar("#detallecompras tbody", table);
    obtener_id_eliminar("#detallecompras tbody", table);
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }