var listar = function(){
		var table = $("#ingredientes").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
		"ajax" : {
			"url": "listaringredientes.php",
			"type": "POST" 
		},
        "columnDefs": [
            {
                      "targets": [7], // El objetivo de la columna de posición, desde cero.
                      "data": "estado", // La inclusión de datos
                      "render": function(data, type, full) { // Devuelve el contenido personalizado
                            if (data==0) {
                                return "<span style='color:green;'><i class='fa fa-check'> </i> Disponible</span>";
                            }else{
                                return "<span style='color:red;'><i class='fa fa-clock-o'> </i> Agotado</span>";
                            }
                      }
                  }
        ],
		"columns":[
			{ "data": "idingrediente" },
            { "data": "nombreing" },
            { "data": "precio" },
            { "data": "costo" },
            { "data": "stock" },
            { "data": "nombrecat" },
            { "data": "nombrepro" },
            { "data": "estado" },
            {"defaultContent": "<button type='button' class='editar btn btn-primary' title='editar' data-toggle='modal' data-target='#modalNuevo' ><i class='fa fa-edit'></i></button> <button type='button' class='habilitar btn btn-success' title='Habilitar' data-toggle='modal' data-target='#modalHabilitar'><i class='fa fa-check'></i></button> <button type='button' class='eliminar btn btn-danger' title='Anular' data-toggle='modal' data-target='#modalEliminar'><i class='fa fa-ban'></i></button> "}
		],
		"language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
         {
                        "text":      '<i class="fa fa-plus"></i>',
                        "titleAttr": 'Agregar Ingrediente',
                        "className": 'btn btn-success',
                        action:     function(){
                            $(document).ready(function(){
                            $("#modalNuevo").modal("show");
                            });
                            agregar_nuevo_ingrediente();
                        }
                    },
        {
                extend:    'pdfHtml5',
                "text":      "<i class='fa fa-file-pdf-o'></i>",
                "titleAttr": 'Generar PDF',
                "className": 'btn btn-danger'
        },
         {
                        extend:    'excelHtml5',
                        "text":      '<i class="fa fa-file-excel-o"></i>',
                        "titleAttr": 'Reporte Excel',
                        "className": 'btn btn-primary'
                    },
        ],
		});

    obtener_data_editar("#ingredientes tbody", table);
    obtener_id_eliminar("#ingredientes tbody", table);
    obtener_id_habilitar("#ingredientes tbody", table);
}

var agregar_nuevo_ingrediente = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }