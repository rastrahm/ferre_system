var listar = function(){
		var table = $("#pedidos").DataTable({
        "deferRender": true,
        "destroy": true,
        "sPaginationType": "full_numbers",
		"ajax" : {
			"url": "listarpedidosrepartidor.php",
			"type": "POST" 
		},
        "columnDefs": [
            {
                      "targets": [1], // El objetivo de la columna de posición, desde cero.
                      "data": "estado", // La inclusión de datos
                      "render": function(data, type, full) { // Devuelve el contenido personalizado
                            if (data==0) {
                                return "<span style='color:blue;'><i class='fa fa-clock-o'> </i> Pendiente</span>";
                            }else if (data==2) {
                                return "<span style='color:red;'><i class='fa fa-clock-o'> </i> Anulado</span>";
                            }else{
                                return "<span style='color:green;'><i class='fa fa-check'> </i> Entregado</span>";
                            }
                      }
                  }
        ],
		"columns":[
			      { "data": "idpedido" },
            { "data": "estado" },
            {"defaultContent": "<button type='button' class='confirmar btn btn-primary' title='confirmar' data-toggle='modal' data-target='#modalConfirmar' ><i class='fa fa-thumbs-up'></i></button> <button type='button' class='entregar btn btn-success' title='Entregar' data-toggle='modal' data-target='#modalVerificar'><i class='fa fa-check-circle'></i></button> <button type='button' class='anular btn btn-danger' title='Anular' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-ban'></i></button>"}
		],
		"language": idioma_espanol,
        "dom" : "B"
                     +"fl"
                     +"r"
                     +"t"
                     +"i"
                     +"p",//'Bfrtip',
        "buttons":[
        
        ],
		});

    obtener_data_editar("#pedidos tbody", table);
    obtener_data_verificar("#pedidos tbody", table);
    obtener_id_eliminar("#pedidos tbody", table);
}

var agregar_nuevo_vendedor = function(){
    limpiar_datos();
}

var idioma_espanol = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }