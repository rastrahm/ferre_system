<!DOCTYPE html>
<html lang="en">

<?php
include("header.php");
?>

<style type="text/css">
    .table-wrapper {
        width: 100%;
        height: 500px;
        /* Altura de ejemplo */
        overflow: auto;
    }

    .table-wrapper table {
        border-collapse: separate;
        border-spacing: 0;
    }

    .table-wrapper table thead {
        position: -webkit-sticky;
        /* Safari... */
        position: sticky;
        top: 0;
        left: 0;
    }

    .table-wrapper table thead th,
    .table-wrapper table tbody td {
        border: 1px solid #000;
        /*background-color: #000aff;*/
    }

    .derecha {
        float: right;
    }
</style>

<body class="animsition">
    <div class="page-wrapper" id="cuerpo">

        <?php
        include("conexion.php");
        include("./seguridad/formatos.php");
        include("buscar_articulo.php");
        include("buscar_compra_presupuesto.php");
        include("navbar.php");
        ?>

        <!-- PAGE CONTAINER-->
        <div class="page-content">
            <!-- STATISTIC-->
            <div class="container-fluid" align="center">
                <!-- HEADER DESKTOP-->
                <div class="card-body">
                    <div class="card">
                        <div class="card-header">
                            <strong>Orden Compra</strong>
                            <button class="btn btn-success" onclick="nuevo();"><i class="fa fa-plus-circle"></i> Nuevo</button>
                            <button class="btn btn-primary" onclick="consultar();"><i class="fa fa-search"></i> Consultar</button>
                            <button class="btn btn-warning" id="botonmodificar" onclick="modificar();" disabled=""><i class="fa fa-edit"></i> Modificar</button>
                            <button class="btn btn-danger" id="botoneliminar" onclick="eliminar()" disabled=""><i class="fa fa-trash"></i> Eliminar</button>
                        </div>
                        <div class="card-body card-block">
                            <div class="row">
                                <input type="hidden" name="opcion" id="opcion" value="nuevo">
                                <div class='col-md-1'>
                                    <label>Comprobante:</label>
                                    <input type="number" name="idordencompra" id="idordencompra" class="form-control" onchange="buscar_mov()" disabled="">
                                </div>
                                <div class="col-md-2">
                                    <label for="text-input" class=" form-control-label">Presupuesto</label>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" onclick="busqueda_presupuesto()" id="boton_buscar_presupuesto" disabled><i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <input type="number" class="form-control" onchange="busca_presupuesto();" name="idpresupuesto" id="idpresupuesto" style="text-align:center;" disabled>
                                    </div>
                                </div>
                                <div class='col-md-2'>
                                    <label>Sucursal:</label>
                                    <select id="sucursal" name="sucursal" class="form-control" disabled="">
                                        <option>Seleccione la sucursal</option>
                                        <?php
                                        $res = mysqli_query($conexion, "SELECT * FROM sucursal");

                                        foreach ($res as $row) {
                                            echo "<option value='" . $row["idsucursal"] . "'> " . $row["desc_suc"] . "</option>";
                                        }


                                        ?>

                                    </select><br>
                                </div>


                                <div class="col-md-3">
                                    <label for="text-input" class=" form-control-label">Proveedor</label>
                                    <select class="proveedor form-control" name="proveedor" id="proveedor" class="form-control" required="" disabled="">
                                        <option>Seleccione un proveedor</option>
                                        <?php
                                        $sql = "SELECT * from proveedores";
                                        $res = mysqli_query($conexion, $sql);
                                        while ($row = mysqli_fetch_array($res)) {
                                            echo "<option value='" . $row["idproveedor"] . "'>" . $row["proveedor"] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="pago">Fecha alta</label>
                                    <input type="text" class="form-control" id="fecha_alta" name="fecha_alta" value="<?php echo date('Y-m-d H:m:s'); ?>" disabled="">
                                </div>
                            </div><br>
                            <div class="row" align="left">
                                <div class="col-md-6">
                                    <label for="pago">Concepto</label>
                                    <input type="text" class="form-control" id="concepto" name="concepto" disabled="">
                                </div>
                                <div class="col-md-2" align="center">
                                    <h3><label for="textarea-input" class=" form-control-label">Total</label></h3>
                                    <input type="text" id="total" name="total" class="form-control" onkeyup="format(this);" onchange="format(this);" readonly="readonly" style="font-size:30px; text-align:center; color:red; font-weight: bold;">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer" align="right">

                            <button type="button" id="guardar" class="btn btn-success" onclick="procesa_orden_compra();" disabled=""><i class="fa fa-check"></i> Guardar
                            </button>
                        </div>
                    </div>

                    <div align="center">
                        <div class="col-md-10">
                            <div class="table-wrapper table-responsive table--no-card m-b-30">
                                <table class="table table-borderless table-striped table-earning" id="tabla_articulos" width="100%" cellspacing="0">
                                    <thead>
                                        <tr align="center">
                                            <th class='center'>Codigo</th>
                                            <th class='center'>Articulo</th>
                                            <th class='center'>Cantidad</th>
                                            <th class='center'>Costo</th>
                                            <th class='center'>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <?php
                include("footer.php");
                ?>
                <!-- END PAGE CONTAINER-->
            </div>
        </div>
    </div>

    <?php include("seguridad.php") ?>
    <?php
    include("scripts.php");
    ?>

</body>

</html>
<!-- end document-->
<script type="text/javascript">
    function nuevo() {
        $("#opcion").val("nuevo");
        $("#idordencompra").attr("disabled", true);
        $("#guardar").attr("disabled", false);
        $("#sucursal").attr("disabled", false);
        $("#concepto").attr("disabled", false);
        $("#proveedor").attr("disabled", false);
        $("#idpresupuesto").attr("disabled", false);
        $("#boton_buscar_presupuesto").attr("disabled", false);
        limpiar_campos();

        /*$("#tabla_articulos>tbody").append('<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
            '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
            'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" size="5" ' +
            '></td><td class="costo" style="width:200px"><input type="text" class="form-control" name="cos" id="cos" size="6" onkeyup="format(this);"' +
            '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"' +
            'disabled></td><td style="width:100px"><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');*/

        $("#idpresupuesto").focus();
    }

    function buscar_mov() {
        $("#tabla_articulos > tbody:last").children().remove();
        $.ajax({
            url: 'busca_data_orden_compra.php',
            dataType: 'json',
            type: 'POST',
            data: 'idordencompra=' + $("#idordencompra").val(),
            success: function(data) {

                if (data == 0) {
                    alertify.error("No existe el numero de comprobante!!!");
                } else {

                    $("#idordencompra").val(data[0].idordencompra);
                    $("#idpresupuesto").val(data[0].idpresupuesto);
                    $("#sucursal").val(data[0].sucursal);
                    $("#fecha_alta").val(data[0].fecha_alta);
                    $("#concepto").val(data[0].observacion);
                    $("#proveedor").val(data[0].proveedor).trigger('change');
                    $("#total").val(parseInt(data[0].total).toLocaleString());

                    for (var i = 0; i < data.length; i++) {
                        $("#tabla_articulos > tbody").append("<tr><td class='idarticulo'>" + data[i].articulo +
                            "</td><td class='nombreart'>" + data[i].nombreart +
                            "</td><td class='cantidad'>" + data[i].cantidad +
                            "</td><td class='costo'>" + parseInt(data[i].costo).toLocaleString() +
                            "</td><td class='subtotal'>" + parseInt(data[i].subtotal).toLocaleString() +
                            "</td></tr>");
                    }
                }
            },
        });
    }

    function consultar() {
        $("#idordencompra").attr("disabled", false);
        $("#idordencompra").val("");
        $("#botoneliminar").attr("disabled", false);
        $("#botonmodificar").attr("disabled", false);
        deshabilitar_campos();
        $("#idordencompra").focus();
    }

    function modificar() {
        $("#idordencompra").attr("disabled", true);
        $("#boton_buscar_presupuesto").attr("disabled", false);
        $("#guardar").attr("disabled", false);
        $("#concepto").attr("disabled", false);
        $("#proveedor").attr("disabled", false);
        $("#idpresupuesto").attr("disabled", false);
        $("#opcion").val("modificar");
    }

    function agregar() {

        var idarticulo = $("#idart").val();
        var nombreart = $("#nom").val();
        var cantidad = parseInt($("#cant").val());
        var costo = $("#cos").val().replace(/(?!-)[^\d]/g, '');
        var subtotal = $("#sub").val().replace(/(?!-)[^\d]/g, '');

        $("#tabla_articulos > tbody").prepend("<tr style='font-weight:bold;color:black;font-size:20px;text-align:center'><td class='idarticulo'>" + idarticulo +
            "</td><td class='nombreart'>" + nombreart +
            "</td><td class='cantidad'>" + cantidad +
            "</td><td class='costo'>" + parseInt(costo).toLocaleString() +
            "</td><td class='subtotal'>" + parseInt(subtotal).toLocaleString() +
            "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
        resumen();
        agregar_fila();

    }

    function agregar_fila() {

        $("#tabla_articulos>tbody").prepend('<tr style="text-align:center" id="fila1"><td class="idarticulo" style="width:200px"><div class="input-group"><div class="input-group-btn"><button class="btn btn-primary" onclick="busqueda_art()"><i class="fa fa-search"></i></button></div><input type="text" class="form-control" onchange="busca_articulo();" name="idart" id="idart" size="1"></div>' +
            '</td><td class="nombreart" style="width:400px"><input type="text" class="form-control" name="nom" id="nom" size="5"' +
            'disabled></td><td class="cantidad" style="width:150px"><input type="text" class="form-control" name="cant" id="cant" size="5" ' +
            '></td><td class="costo" style="width:200px"><input type="text" class="form-control" name="cos" id="cos" size="6" onkeyup="format(this);"' +
            '></td><td class="subtotal" style="width:200px"><input type="text" class="form-control" name="sub" id="sub" size="5"' +
            'disabled></td><td style="width:100px"><button class="btn btn-success delete" id="confirmar" onclick="agregar()"><i class="fa fa-check-circle"></i></button></td></tr>');

        $("#idart").focus();

    }

    function resumen() {
        var monto = 0;
        var subtotal = 0;
        $('#tabla_articulos > tbody > tr').each(function() {
            monto += parseInt($(this).find('td').eq(4).html().replace(/(?!-)[^\d]/g, ''));
            subtotal += parseInt($(this).find('td').eq(4).html().replace(/(?!-)[^\d]/g, ''));
        });

        $("#total").val(parseInt(monto).toLocaleString());
    }

    function saltar(e) {
        (e.keyCode) ? k = e.keyCode: k = e.which;
        if (k == 13) {
            $("#confirmar").focus();
        }
    }

    function saltar2(e, id) {
        (e.keyCode) ? k = e.keyCode: k = e.which;
        if (k == 13) {
            document.getElementById(id).focus();
        }
    }

    function busca_articulo() {
        $.ajax({
            url: 'busca_data_articulo_com.php',
            dataType: 'json',
            type: 'POST',
            data: 'idarticulo=' + $("#idart").val() + '&idsucursal=' + $("#sucursal").val(),
            success: function(data) {
                if (data == 0) {
                    alertify.error("No existe el articulo!!!");
                    $("#idart").val("");
                    $("#idart").focus();
                } else {
                    $("#cant").focus();
                    $("#idart").val(data[0].idarticulo);
                    $("#nom").val(data[0].nombreart);
                }
            },
        });
    }

    function busca_presupuesto() {
        $.ajax({
            url: 'busca_data_presupuesto_com.php',
            dataType: 'json',
            type: 'POST',
            data: 'idpresupuesto=' + $("#idpresupuesto").val(),
            success: function(data) {
                $("#tabla_articulos > tbody:last").children().remove();
                if (data == 0) {
                    alertify.error("No existe el presupuesto!!!");
                } else {
                    $("#sucursal").val(data[0].sucursal);
                    $("#proveedor").val(data[0].proveedor).trigger('change');
                    $("#concepto").val(data[0].observacion);

                    for (let i = 0; i < data.length; i++) {
                        $("#tabla_articulos > tbody").prepend("<tr style='font-weight:bold;color:black;font-size:20px;text-align:center'><td class='idarticulo'>" + data[i].articulo +
                            "</td><td class='nombreart'>" + data[i].nombreart +
                            "</td><td class='cantidad'>" + data[i].cantidad +
                            "</td><td class='costo'>" + parseInt(data[i].costo).toLocaleString() +
                            "</td><td class='subtotal'>" + parseInt(data[i].subtotal).toLocaleString() +
                            "</td><td><button class='btn btn-danger delete'><i class='fa fa-trash'></i></button></td></tr>");
                    }

                    resumen()
                }
            },
        });
    }

    function busqueda_art() {
        $("#modal_busqueda_arts").modal("show");
        $('#modal_busqueda_arts').on('shown.bs.modal', function() {
            $("#lista_articulos").html("");
            $("#articulo_buscar").val("");
            $("#articulo_buscar").focus();
            busca();
        });
    }

    function busqueda_presupuesto() {
        $("#modal_busqueda_presupuesto").modal("show");
        $('#modal_busqueda_presupuesto').on('shown.bs.modal', function() {
            $("#lista_presupuestos").html("");
            $("#presupuesto_buscar").val("");
            $("#presupuesto_buscar").focus();
            busca_ped();
        });
    }


    function busca() {
        $.ajax({
            beforeSend: function() {
                $("#lista_articulos").html("");
            },
            url: 'busca_articulos_ayuda_com.php',
            type: 'POST',
            data: 'articulo=' + $("#articulo_buscar").val() + '&idsucursal=' + $("#sucursal").val(),
            success: function(x) {
                $("#lista_articulos").html(x);
            },
            error: function(jqXHR, estado, error) {
                $("#lista_articulos").html("Error en la peticion AJAX..." + estado + "      " + error);
            }
        });
    }

    function busca_ped() {
        $.ajax({
            beforeSend: function() {
                $("#lista_presupuestos").html("");
            },
            url: 'busca_presupuestos_ayuda_com.php',
            type: 'POST',
            data: 'proveedor=' + $("#presupuesto_buscar").val(),
            success: function(x) {
                $("#lista_presupuestos").html(x);
            },
            error: function(jqXHR, estado, error) {
                $("#lista_presupuestos").html("Error en la peticion AJAX..." + estado + "      " + error);
            }
        });
    }


    function add_art(art) {
        //alert(art);
        $("#modal_busqueda_arts").modal("toggle");
        $("#idart").val(art);
        busca_articulo();
    }

    function add_ped(ped) {
        //alert(art);
        $("#modal_busqueda_presupuesto").modal("toggle");
        $("#idpresupuesto").val(ped);
        busca_presupuesto();
    }

    $(function() {
        // Evento que selecciona la fila y la elimina
        $(document).on("click", ".delete", function() {
            var parent = $(this).parents().parents().get(0);
            $(parent).remove();
            resumen()
        });
    });

    $(document).ready(function() {
        $('.proveedor').select2();
    });

    function format(input) {
        var num = input.value.replace(/\./g, '');
        if (!isNaN(num)) {
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/, '');
            input.value = num;
        } else {
            alertify.error('Solo se permiten numeros!');
            input.value = input.value.replace(/[^\d\.]*/g, '');
        }
        var cantidad = $("#cant").val().replace(/(?!-)[^\d]/g, '')
        var costo = $("#cos").val().replace(/(?!-)[^\d]/g, '')
        var subtotal = cantidad * costo
        $("#sub").val(parseInt(subtotal).toLocaleString())
    }


    function procesa_orden_compra() {

        var idordencompra = $("#idordencompra").val();
        var idpresupuesto = $("#idpresupuesto").val();
        var sucursal = $("#sucursal").val();
        var concepto = $("#concepto").val();
        var proveedor = $("#proveedor").val();
        var fecha_alta = $("#fecha_alta").val();
        var total = $("#total").val().replace(/[^\d]/g, '')
        var opcion = $("#opcion").val();

        let articulos = [];

        document.querySelectorAll('#tabla_articulos tbody tr').forEach(function(e) {
            let fila = {
                idarticulo: e.querySelector('.idarticulo').innerText,
                cantidad: e.querySelector('.cantidad').innerText,
                costo: e.querySelector('.costo').innerText.replace(/[^\d]/g, ''),
                subtotal: e.querySelector('.subtotal').innerText.replace(/[^\d]/g, '')
            };
            articulos.push(fila);
        });

        $.ajax({
            url: 'guardarOrdenCompra.php',
            dataType: 'json',
            type: 'POST',
            data: {
                'articulos': JSON.stringify(articulos),
                'idordencompra': idordencompra,
                'idpresupuesto': idpresupuesto,
                'sucursal': sucursal,
                'concepto': concepto,
                'proveedor': proveedor,
                'fecha_alta': fecha_alta,
                'total': total,
                'opcion': opcion
            }
        }).done(function(info) {
            var json_info = JSON.parse(JSON.stringify(info));
            mostrar_mensaje(json_info);
            deshabilitar_campos();
            limpiar_campos();
            busca_nro_comprobante();
        });
    }

    function busca_nro_comprobante() {
        $.ajax({
            url: 'busca_ultimo_comp_presupuesto.php',
            dataType: 'json',
            type: 'POST',
            success: function(data) {

                $("#idordencompra").val(data[0].idordencompra);

            },
        });
    }

    function eliminar() {
        var idordencompra = $("#idordencompra").val();
        var opcion = "eliminar";
        $.ajax({
            url: 'guardarOrdenCompra.php',
            dataType: 'json',
            type: 'POST',
            data: {
                'idordencompra': idordencompra,
                'opcion': opcion
            }
        }).done(function(info) {
            var json_info = JSON.parse(JSON.stringify(info));
            mostrar_mensaje(json_info);
            deshabilitar_campos();
            limpiar_campos();
        });

    }

    function limpiar_campos(argument) {
        $("#tabla_articulos > tbody:last").children().remove();
        $("#idordencompra").val("");
        $("#idpresupuesto").val("");
        $("#sucursal").val("Seleccione la sucursal");
        $("#concepto").val("");
        $("#total").val("");
        $("#proveedor").val('Seleccione un proveedor').trigger('change');
    }

    function deshabilitar_campos() {
        $("#tabla_articulos > tbody:last").children().remove();
        $("#guardar").attr("disabled", true);
        $("#sucursal").attr("disabled", true);
        $("#concepto").attr("disabled", true);
        $("#proveedor").attr("disabled", true);
        $("#idpresupuesto").attr("disabled", true);
        $("#boton_buscar_presupuesto").attr("disabled", true);
    }

    var mostrar_mensaje = function(informacion) {
        if (informacion.respuesta == "BIEN") {
            swal({
                title: 'Bien!',
                text: 'Registro Guardado',
                type: 'success',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "ERROR") {
            swal({
                title: 'Error!',
                text: 'No se ejecuto la consulta',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "ELIMINADO") {
            swal({
                title: 'Atención!',
                text: 'Registro Eliminado',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "VACIO") {
            swal({
                title: 'Atención!',
                text: 'No se cargo ninguna imagen',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "EXISTE") {
            swal({
                title: 'Atención!',
                text: 'Ya existe una orden con el presupuesto informado',
                type: 'warning',
                timer: 2000,
                showConfirmButton: false
            })
        } else if (informacion.respuesta == "HABILITADO") {
            swal({
                title: 'Bien!',
                text: 'Ingrediente Disponible',
                type: 'success',
                timer: 2000,
                showConfirmButton: false
            })
        }
    }
</script>