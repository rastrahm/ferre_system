<?php

include ("conexion.php");
include ("./seguridad/formatos.php");


$idcompra = $_POST["idcompra"];
$estado = $_POST["estado"];

$sql="SELECT cc.idcompra,i.idingrediente,i.nombreing,cd.cantidad,cd.subtotal from compra_cab cc join compra_det cd on cd.idcompra=cc.idcompra join ingredientes i on i.idingrediente=cd.articulo where cc.idcompra='$idcompra'";

$res = mysqli_query($conexion,$sql);

 if($estado==0){
   echo "<div class='box box-primary'>";
   echo "<div class='box-header with-border'>";
   echo "</div><br>";
   echo "<div class='box-body'>";
   echo "<div class='table-responsive'>";
   echo "<table class='table table-bordered table-hover' id='tabla_ventas_detalle'>";
   echo "<thead>";
   echo "<tr>";
   echo "<th>Articulo</th>";
   echo "<th>Cantidad</th>";
   echo "<th>Subtotal</th>";
   echo "</tr>";
   echo "</thead>";
   echo "<tbody>";
    while($row = mysqli_fetch_array($res)){
      echo "<tr>";
      echo "<td>".$row['nombreing']."</td>";
      echo "<td>".$row['cantidad']."</td>";
      echo "<td>".formatearNumero($row['subtotal'])."</td>";
      echo "</tr>";
    }
   echo '</tbody>';
   echo "</table>";
   echo "</div>";
   echo "</div>";
   echo "</div>";
 }else{
   echo "Compra Anulada";
 }

?>