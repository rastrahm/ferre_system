<!DOCTYPE html>
<html lang="en">
	
	<?php include("header.php"); ?>

<style type="text/css">


.hide {
    max-height: 0 !important;
}

.dropdown{
	border: 0.1em solid black;
	width: 10em;
	margin-bottom: 1em;
}

.dropdown .title{
	margin: .3em .3em .3em .3em;	
	width: 100%;
}

.dropdown .title .fa-angle-right{
	float: right;
	margin-right: .7em;
	transition: transform .3s;
}

.dropdown .menu{
	transition: max-height .5s ease-out;
	max-height: 20em;
	overflow: hidden;
}

.dropdown .menu .option{
	margin: .3em .3em .3em .3em;
	margin-top: 0.3em;
}

.dropdown .menu .option:hover{
	background: rgba(0,0,0,0.2);
}

.pointerCursor:hover{
	cursor: pointer;
}

.rotate-90{
	transform: rotate(90deg);
}

</style>

	<body>
		<!-- HEADER -->
		<?php include("header_top.php"); ?>
		<!-- /HEADER -->
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- ASIDE -->
					<div id="aside" class="col-md-3">
						<!-- aside Widget -->
						<!--<div class="aside">
							<div class='dropdown'>
								<div class='menu pointerCursor hide'>
								<?php 
								include ("conexion.php");

								$i = 0;
								
								$sqlcat = mysqli_query($conexion,"SELECT a.clasificacion,c.nombrecla, count(a.idarticulo) as cantidad FROM clasificaciones c
										JOIN articulos a on a.clasificacion=c.idclasificacion
										group by c.idclasificacion");
								foreach ($sqlcat as $row) {

									$i++;

									$data = $row["clasificacion"]."|".$i;

									echo '<div class="" id="'.$row["clasificacion"].'"> '.$row["nombrecla"].'</div>';
									
								}

								?>
							</div>
							</div>
						</div><br><br>-->
						<!-- /aside Widget -->

						<!-- aside Widget 
						<div class="aside">
							<h3 class="aside-title">Precio</h3>
							<div class="price-filter">
								<div id="price-slider"></div>
								<div class="input-number price-min">
									<input id="precio-min" min="0" type="number">
									<span class="qty-up">+</span>
									<span class="qty-down">-</span>
								</div>
								<span>-</span>
								<div class="input-number price-max">
									<input id="precio-max" max="100000" type="number">
									<span class="qty-up">+</span>
									<span class="qty-down">-</span>
								</div>
							</div>
						</div> /aside Widget -->		
					</div>
					<!-- /ASIDE -->

					<!-- STORE -->
					<div id="store" class="col-md-9">

						<!-- store products -->
						<div class="row">

							<div class="clearfix visible-lg visible-md"></div>

							<?php

								if (isset($_GET["id"])) {
									$id = $_GET["id"];
								}else{
									$id = "";
								}

								if (isset($_GET["search"])) {
									$buscar = $_GET["search"];
								}else{
									$buscar = "";
								}

								define('NUM_ITEMS_BY_PAGE', 10);
								$result = mysqli_query($conexion,'SELECT * FROM articulos where nombreart like "%'.$buscar.'%" and clasificacion like "%'.$id.'%"');
								$num_total_rows = mysqli_num_rows($result);
								if ($num_total_rows > 0) {
								    $page = false;

								    //examino la pagina a mostrar y el inicio del registro a mostrar
								    if (isset($_GET["page"])) {
								        $page = $_GET["page"];
								    }

								    if (!$page) {
								        $start = 0;
								        $page = 1;
								    } else {
								        $start = ($page - 1) * NUM_ITEMS_BY_PAGE;
								    }
								    //calculo el total de paginas
								    $total_pages = ceil($num_total_rows / NUM_ITEMS_BY_PAGE);


								    $sql = "SELECT a.idarticulo,a.nombreart,p.precio,a.clasificacion,cl.nombrecla FROM articulos a
								            join clasificaciones cl on cl.idclasificacion=a.clasificacion
								            join precios p on p.articulo=a.idarticulo
								             where a.nombreart like '%".$buscar."%' and cl.idclasificacion like '%".$id."%'
								            ORDER BY nombreart DESC LIMIT 20";
								    $listarart = mysqli_query($conexion,$sql);
								    foreach($listarart as $row){
								        $sqlimg = mysqli_query($conexion,"SELECT imagen from imagenes where articulo='".$row["idarticulo"]."' and tipo=1");
								        foreach ($sqlimg as $fila) {
								            ?>
                            <!-- product -->
                            <div class="col-md-4 col-xs-6">
                                <div class="product">
                                    <div class="product-img">
                                        <?php 
                                            echo '<img src="./img/'.$fila["imagen"].'" alt="">';
                                         ?>
                                        
                                    </div>
                                    <div class="product-body">
                                        <h3 class="product-name"><a href="detail_product.php?id=<?php echo $row["idarticulo"]; ?>"><?php echo $row["nombreart"];?></a></h3>
                                        <h4 class="product-price"><?php echo formatearNumero($row["precio"]);?> Gs.</h4>
                                    </div>
                                    <form method="POST" action="cargar_carrito3.php">
                                            <input type="hidden" id="idarticulo" name="idarticulo" value="<?php echo $row["idarticulo"]; ?>">
                                            <input type="hidden" class="form-control" name="cantidad" id="cantidad" value="1">
                                    <div class="add-to-cart">
                                        <button type="submit" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar al carrito</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /product -->
            <?php  
             }
            }
            
  			?>

						<!--para los botones de paginacion-->
						<div class="clearfix visible-sm visible-xs"></div>

						
						<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>


						<div class="clearfix visible-sm visible-xs"></div>

						<div class="product-rating">
				
						</div>
						<!-- /store products -->

						<?php
						    echo '<div class="store-filter clearfix">
							<span class="store-qty">Mostrando '.$page.' de ' .$total_pages.' paginas</span>
							<ul class="store-pagination">';

						    if ($total_pages > 1) {
						        if ($page != 1) {
						            echo '<li><a href="store.php?search='.$buscar.'&page='.($page-1).'"><i class="fa fa-angle-left"></i></a></li>';
						        }

						        for ($i=1;$i<=$total_pages;$i++) {
						            if ($page == $i) {
						                echo '<li class="active"><a href="#">'.$page.'</a></li>';
						            } else {
						                echo '<li><a href="store.php?search='.$buscar.'&page='.$i.'">'.$i.'</a></li>';
						            }
						        }

						        if ($page != $total_pages) {
						            echo '<li><a href="store.php?search='.$buscar.'&page='.($page+1).'"><i class="fa fa-angle-right"></i></a></li>';
						        }
						    }
						    echo '</ul>
						    </div>';
						}else{
							echo "Ningun producto relacionado...";
						}

							?>

					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
	
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
			<?php include("footer.php");?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php");?>

	</body>
</html>

<script type="text/javascript">
	

</script>