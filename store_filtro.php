<div id="store" class="col-md-9">

						<!-- store products -->
						<div class="row">

							<div class="clearfix visible-lg visible-md"></div>

							<?php

								include ("conexion.php");
								include ("formatos.php");

								if (isset($_GET["id"])) {
									$id = $_GET["id"];
								}else{
									$id = "";
								}

								$sql = "SELECT a.idarticulo,a.nombreart,a.precio,a.clasificacion,cl.nombrecla FROM articulos a
									join clasificaciones cl on cl.idclasificacion=a.clasificacion where cl.idclasificacion like '%".$id."%' limit 12";
									$listarart = mysqli_query($conexion,$sql);
									foreach($listarart as $row){

									$sqlimg = mysqli_query($conexion,"SELECT imagen from imagenes where articulo='".$row["idarticulo"]."' and tipo=1");
									foreach ($sqlimg as $fila) {
													
												
							?>

							<!-- product -->
							<div class="col-md-4 col-xs-6">
								<div class="product">
									<div class="product-img">
										<?php 
											echo '<img src="./img/'.$fila["imagen"].'" alt="">';
										 ?>
										
									</div>
									<div class="product-body">
										<h3 class="product-name"><a href="#"><?php echo $row["nombreart"];?></a></h3>
										<h4 class="product-price"><?php echo formatearNumero($row["precio"]);?> Gs.</h4>
									</div>
									<form method="POST" action="cargar_carrito3.php">
											<input type="hidden" id="idarticulo" name="idarticulo" value="<?php echo $row["idarticulo"]; ?>">
											<input type="hidden" class="form-control" name="cantidad" id="cantidad" value="1">
									<div class="add-to-cart">
										<button type="submit" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar al carrito</button>
									</div>
									</form>
								</div>
							</div>
							<!-- /product -->
							<?php  
							}
							}
							?>

						<!--para los botones de paginacion-->
						<div class="clearfix visible-sm visible-xs"></div>

						
						<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>


						<div class="clearfix visible-sm visible-xs"></div>

						<div class="product-rating">
				
						</div>
						<!-- /store products -->

						<!-- store bottom filter -->
						<div class="store-filter clearfix">
							<span class="store-qty">Showing 20-100 products</span>
							<ul class="store-pagination">
								<li class="active">1</li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div>
						<!-- /store bottom filter -->
					</div>
					<!-- /STORE -->
				</div>