<!DOCTYPE html>
<html lang="en">
	<?php include("header.php"); ?>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<?php 

			include("header_top.php"); 
			include("modal_detalles.php");

			?>

			<?php  

				include("conexion.php");

				$usuario = $_SESSION['nombreUsuario'];

				$sql = "SELECT * from clientes where usuario='$usuario'";
				$res = mysqli_query($conexion,$sql);

				foreach ($res as $row) {
					$idcliente = $row["idcliente"];
				}

			?>
			<!-- /TOP HEADER -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container" id="pedido">
				<!-- row -->
			<?php


								define('NUM_ITEMS_BY_PAGE', 10);

								$result = mysqli_query($conexion,'SELECT p.idpedido,tp.desc_tip_ped,p.fecha,p.hora,p.estado FROM pedido p
								   join tipo_pedido tp on tp.idtipo_ped=p.tipo_pedido
								   WHERE p.cliente="'.$idcliente.'"');

								$num_total_rows = mysqli_num_rows($result);

								if ($num_total_rows > 0) {
								    $page = false;

								    //examino la pagina a mostrar y el inicio del registro a mostrar
								    if (isset($_GET["page"])) {
								        $page = $_GET["page"];
								    }

								    if (!$page) {
								        $start = 0;
								        $page = 1;
								    } else {
								        $start = ($page - 1) * NUM_ITEMS_BY_PAGE;
								    }
								    //calculo el total de paginas
								    $total_pages = ceil($num_total_rows / NUM_ITEMS_BY_PAGE);


									$sql = "SELECT p.idpedido,tp.desc_tip_ped,p.fecha,p.hora,p.estado FROM pedido p
										   join tipo_pedido tp on tp.idtipo_ped=p.tipo_pedido
										   WHERE p.cliente=$idcliente
								           ORDER BY p.idpedido DESC LIMIT 10";
								    $listarart = mysqli_query($conexion,$sql);

									   echo "<div class='table_responsive'>";
									   echo "<table class='table table-bordered table-hover'>";
									   echo "<thead>";
									   echo "<tr>";
									   echo "<th>Nro Pedido</th>";
									   echo "<th>Tipo</th>";
									   echo "<th>Fecha</th>";
									   echo "<th>Estado</th>";
									   echo "</tr>";
									   echo "</thead>";
									   echo "<tbody>";

								    foreach($listarart as $row){
								        
								          echo "<tr>";
		      							  echo "<td>".$row['idpedido']."</td>";
									      echo "<td>".$row['desc_tip_ped']."</td>";
									      echo "<td>".formatearFecha($row['fecha'])."</td>";

									      if ($row['estado']==0) {
									      	echo "<td style='color:blue;font-weight: bold;'><i class='fa fa-clock-o'></i> Pendiente</td>";
									      }else if ($row['estado']==1) {
									      	echo "<td style='color:red;font-weight: bold;'><i class='fa fa-ban'></i> Anulado</td>";
									      }else if ($row['estado']==2) {
									      	echo "<td style='color:yellow;font-weight: bold;'>En proceso de verificacion</td>";
									      }else if ($row['estado']==3) {
									      	echo "<td style='color:yellow;font-weight: bold;'><i class='fa fa-print'></i> En proceso de facturacion</td>";
									      }else if ($row['estado']==4) {
									      	echo "<td style='color:green;font-weight: bold;'><i class='fa fa-check-circle'></i> Facturado</td>";
									      }else if ($row['estado']==5) {
									      	echo "<td style='color:blue;font-weight: bold;'><i class='fa fa-cubes'></i> Preparado</td>";
									      }else if ($row['estado']==6) {
									      	echo "<td style='color:blue;font-weight: bold;'><i class='fa fa-truck'></i> En proceso de entrega</td>";
									      }else if ($row['estado']==7) {
									      	echo "<td style='color:green;font-weight: bold;'><i class='fa fa-check-circle'></i> Entregado</td>";
									      }

									      if ($row['estado']==6) {
									      	 echo '<td><button class="btn btn-success" title="Confirmar" id="'.$row['idpedido'].'" onclick="entregado(this.id)"><i class="fa fa-check-circle"></i></button> <button class="btn btn-primary" id="'.$row['idpedido'].'" title="Ver detalles" onclick="detalles(this.id)"><i class="fa fa-eye"></i></button></td>';
									      }else{
									      	echo '<td><button class="btn btn-primary" id="'.$row['idpedido'].'" title="Ver detalles" onclick="detalles(this.id)"><i class="fa fa-eye"></i></button></td>';
									      }

									      
									      echo "</tr>";

								    }

								       echo '</tbody>';
									   echo "</table>";
									   echo "</div>";
            
  			?>

						<!--para los botones de paginacion-->
						<div class="clearfix visible-sm visible-xs"></div>

						
						<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>


						<div class="clearfix visible-sm visible-xs"></div>

						<div class="product-rating">
				
						</div>
						<!-- /store products -->

						<?php
						    echo '<div class="store-filter clearfix">
							<span class="store-qty">Mostrando '.$page.' de ' .$total_pages.' paginas</span>
							<ul class="store-pagination">';

						    if ($total_pages > 1) {
						        if ($page != 1) {
						            echo '<li><a href="mis_pedidos.php?page='.($page-1).'"><i class="fa fa-angle-left"></i></a></li>';
						        }

						        for ($i=1;$i<=$total_pages;$i++) {
						            if ($page == $i) {
						                echo '<li class="active"><a href="#">'.$page.'</a></li>';
						            } else {
						                echo '<li><a href="mis_pedidos.php?page='.$i.'">'.$i.'</a></li>';
						            }
						        }

						        if ($page != $total_pages) {
						            echo '<li><a href="mis_pedidos.php?page='.($page+1).'"><i class="fa fa-angle-right"></i></a></li>';
						        }
						    }
						    echo '</ul>
						    </div>';
						}else{
							echo "Ningun pedido encontrado...";
						}

							?>
				<!-- /row -->
			</div>
			<!-- /container -->
			 <form id="frmEliminarArticulo" action="" method="POST">
              <input type="hidden" id="idpedido" name="idpedido" value="">
              <!-- Modal -->
              <div class="modal fade" id="modalConfirmar" role="dialog" aria-labelledby="modalEliminarLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span></button>
                      <h4 class="modal-title" id="modalEliminarLabel">Pedido Recibido</h4>
                    </div>
                    <div class="modal-body">              
                      ¿Está seguro de que su pedido esta completo?<strong data-name=""></strong>
                    </div>
                    <div class="modal-footer">
                      <button type="button" onclick="procesar_pedido();" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal -->
            </form>
		</div>
		<!-- /SECTION -->
		<!-- FOOTER -->
		<?php include("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php"); ?>

	</body>
</html>

<script type="text/javascript">

function detalles(idpedido){
          $.ajax({
          method:"POST",
          url: "busca_pedido_detalle.php",
          data: 'idpedido='+idpedido,
           success: function(x1){
            $("#detallespedido").html(x1);
            $("#modalDetalles").modal("show");
           },
        })
    }

    function entregado(idpedido){

    	$("#modalConfirmar").modal("show");
    	$("#idpedido").val(idpedido);
      
    }

    function procesar_pedido() {
    	 $.ajax({
          method:"POST",
          url: "accionespedidos.php",
          data: 'idpedido='+$("#idpedido").val()
          }).done( function( info ){   
                var json_info = JSON.parse( info );
                mostrar_mensaje( json_info );
                location.reload(true);
           });
    }

    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "BIEN" ){
         swal({
              title: 'Bien!',
              text: 'Registro Guardado',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

</script>