<?php
// Include FB config file && User class
/*require_once 'fbConfig.php';
require_once 'usuarios.php';

if(isset($accessToken)){
    if(isset($_SESSION['facebook_access_token'])){
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }else{
        // Token de acceso de corta duración en sesión
        $_SESSION['facebook_access_token'] = (string) $accessToken;
        
          // Controlador de cliente OAuth 2.0 ayuda a administrar tokens de acceso
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Intercambia una ficha de acceso de corta duración para una persona de larga vida
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
        
        // Establecer token de acceso predeterminado para ser utilizado en el script
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }
    
    // Redirigir el usuario de nuevo a la misma página si url tiene "code" parámetro en la cadena de consulta
    if(isset($_GET['code'])){
        header('Location: ./');
    }
    
    // Obtener información sobre el perfil de usuario facebook
    try {
        $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
        $fbUserProfile = $profileRequest->getGraphNode()->asArray();
    } catch(FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        session_destroy();
        // Redirigir usuario a la página de inicio de sesión de la aplicación
        header("Location: ./");
        exit;
    } catch(FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    
    // Inicializar clase "user"
    $user = new User();
    
    // datos de usuario que iran a  la base de datos
    $fbUserData = array(
        'oauth_provider'=> 'facebook',
        'oauth_uid'     => $fbUserProfile['id'],
        'first_name'    => $fbUserProfile['first_name'],
        'last_name'     => $fbUserProfile['last_name'],
        'email'         => $fbUserProfile['email'],
        'picture'       => $fbUserProfile['picture']['url']
    );
    $userData = $user->checkUser($fbUserData);
    
    // Poner datos de usuario en variables de Session
    $_SESSION['userData'] = $userData;
    
    // Obtener el url para cerrar sesión
    $logoutURL = $helper->getLogoutUrl($accessToken, $redirectURL.'cerrar.php');
    
    // imprimir datos de usuario
    if(!empty($userData)){

        $userInfo= 
        '<div class="col-md-offset-3 col-md-6">
        <table class="table table-responsive" style="background-color:rgba(255, 255, 255, 0.3); border: 2px #a0bbe8 solid;">
            <h4 class="bg-primary text-center pad-basic">INFORMACIÓN DEL USUARIO</h4>
            <tr><th>Miniatura de Perfil:</th><td><img src="'.$userData['picture'].'"></td></tr>
            <tr><th>Nombre:</th><td>' . $userData['first_name'].' '.$userData['last_name'].'</td></tr>
            <tr><th>Correo:</th><td>' . $userData['email'].'</td></tr>
            <tr><th>Logueado con: </th><td> Facebook </td></tr>
            <tr><th>Cerrar Sesión de:</th><td><a class="btn btn-primary" href="cerrar.php"> Facebook</a></td></tr>
        </table>
        </div>';


    }else{
        $output = '<h3 style="color:red">Ocurrió algún problema, por favor intenta nuevamente.</h3>';
    }
    
}else{
    // Obtener la liga de inicio de sesión
    $loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);
    
    // imprimir botón de login
    $output = '<a href="'.htmlspecialchars($loginURL).'"><img class="img-responsive" src="img/fblogin-btn.png"></a>';
}*/
?>

<?php include ("login.php"); ?>

<style type="text/css">

.resultado {
	background-color: #FFFFFF;
  	color: #FFFFFF;
  	 width: 65%;
}

#texto{
  display: inline-block;
  vertical-align: middle;
}

.input {
  height: 40px;
  padding: 0px 15px;
  border: 1px solid #E4E7ED;
  background-color: #FFF;
  width: 100%;
}

</style>

<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="tel:+595971752130"><i class="fa fa-phone"></i> +595971-752-130</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> jenniferferreira296@gmail.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> Luque-Paraguay</a></li>
					</ul>
					<ul class="header-links pull-right">
					
						 <?php
                                        if (isset($_SESSION['nombreUsuario'])){

                                            echo "<li><a href='#' </a> ".$_SESSION['nombreUsuario']." </li>";
                                        	echo "<li><a href='mis_pedidos.php' </a> Mis Pedidos </li>";
                                            echo '<li><a href="admin/seguridad/salir_cliente.php"><i class="fa fa-sign-out"></i> Salir</a></li>';

                                        }/*else if(isset($output)){

                                        	if ($output=='') {
                                        		
                                        		echo "<li><a href='#' </a><img width='40px' width='40px' src='img/facebook_logo.png'>  ".utf8_decode($userData['first_name']).' '.utf8_decode($userData['last_name'])."</li>";
                                            	echo '<li><a href="cerrar.php"><i class="fa fa-sign-out"></i> Salir</a></li>';
                                        	}else{
                                        		 echo "<li><a href='#' data-toggle='modal' data-target='#modalSesion' ><i class='fa fa-user-o'></i>Iniciar Sesión</a></li>";
                                        	echo "<li><a href='#' data-toggle='modal' data-target='#modalRegistro' ><i class='fa fa-user-plus'></i> Registrarme</a></li>";
                                        	}

                                           

                                        }*/else{
                                            echo "<li><a href='#' data-toggle='modal' data-target='#modalSesion' ><i class='fa fa-user-o'></i>Iniciar Sesión</a></li>";
                                        	echo "<li><a href='#' data-toggle='modal' data-target='#modalRegistro' ><i class='fa fa-user-plus'></i> Registrarme</a></li>";
                                        }
                        ?>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="index.php" class="logo">
									<img src="./img/logo.png" alt="" width="200" height="100">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-8">
							<div class="header-search">
							<form method="GET" action="store.php">
								<input class="input" name="search" id="buscar_product" placeholder="Buscar Producto" onkeyup="buscar_producto();" value="<?php
																			if(isset($_GET["search"])){
																				echo $_GET["search"];
																			}else{
																				echo "";
																			}
																		?>">
								<button type="submit" class="search-btn">Buscar</button>
									<div id="mostrar_resultado" class="resultado" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: block;">
									</div>									
							</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-1 clearfix">
							<div class="header-ctn">

								<?php include ("carrito.php");?>

								<!-- Menu Toogle -->
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
<?php include ("registrarse.php"); ?>

<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li class="active"><a href="index.php">INICIO</a></li>

							<?php 
								include ("conexion.php");
								
								$sqlcat = mysqli_query($conexion,"SELECT idclasificacion, nombrecla FROM clasificaciones");
								
								foreach ($sqlcat as $row) {

									echo '<li><a href="store.php?id='.$row["idclasificacion"].'">'.$row["nombrecla"].'</a></li>';
									
								}

								?>

						
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>