<!DOCTYPE html>
<html lang="en">
<?php include ("header.php"); ?>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<?php include ("header_top.php") ?>
			<!-- /TOP HEADER -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- Product main img -->
					<div class="col-md-5 col-md-push-2">
						<div id="product-main-img">

							<?php 

								include ("conexion.php");

								$sqlimg = mysqli_query($conexion,"SELECT * from imagenes where articulo='".$_GET["id"]."' and tipo=0");

								foreach ($sqlimg as $row) {

									echo '<div class="product-preview">
											<img src="./img/'.$row["imagen"].'" alt="">
										</div>';
									
								}
							?>
						</div>
					</div>
					<!-- /Product main img -->

					<!-- Product thumb imgs -->
					<div class="col-md-2  col-md-pull-5">
						<div id="product-imgs">
							

							<?php 
								$sqlimg = mysqli_query($conexion,"SELECT * from imagenes where articulo='".$_GET["id"]."' and tipo=0");

								foreach ($sqlimg as $row) {

									echo '<div class="product-preview">
											<img src="./img/'.$row["imagen"].'" alt="">
										</div>';
									
								}

							?>

						</div>
					</div>
					<!-- /Product thumb imgs -->

					<!-- Product details -->
					<div class="col-md-5">
						<div class="product-details">
							<h2 class="product-name"><?php

								$sqlproduct = mysqli_query($conexion,"SELECT a.idarticulo,a.descripcion,a.nombreart,p.precio,c.nombrecla from articulos a
									join precios p on p.articulo=a.idarticulo
									join clasificaciones c on c.idclasificacion=a.clasificacion
									where p.idlista=1 and a.idarticulo=".$_GET["id"]);

								foreach ($sqlproduct as $row) {

									echo $row["nombreart"];

									$idarticulo = $row["idarticulo"];
									$precio = $row["precio"];
									$categoria = $row["nombrecla"];
									$descripcion = $row["descripcion"];
									
								}

							 ?>
							 	
							 </h2>

							<div>
								<h3 class="product-price"><?php echo formatearNumero($precio); ?> Gs.</h3>

								<?php 	

								$sqlstock = mysqli_query($conexion,"SELECT sum(bd.stock) as stock from bodegas_det bd
								join bodegas b on b.idbodega=bd.bodega
								where b.descbodega='DEPOSITO' and bd.articulo=".$_GET["id"]." group by b.descbodega");
								$stocknum = mysqli_num_rows($sqlstock);

								if ($stocknum>0) {
									foreach ($sqlstock as $st) {

										if ($st["stock"]>0) {
											echo '<span class="product-available">En Stock</span>';
										}else{
											echo '<span class="product-available">Agotado</span>';
										}
									}
			
								}else{
									echo '<span class="product-available">Agotado</span>';
								}
								?>
								


							</div><br>

							<form method="POST" action="cargar_carrito2.php">

								<?php 	

								$sqlstock = mysqli_query($conexion,"SELECT sum(bd.stock) as stock from bodegas_det bd
								join bodegas b on b.idbodega=bd.bodega
								where b.descbodega='DEPOSITO' and bd.articulo=".$_GET["id"]." group by b.descbodega");
								$stocknum = mysqli_num_rows($sqlstock);

								if ($stocknum>0) {
									
								?>

								<div class="add-to-cart">
								<div class="qty-label">
									<label>CANT.</label>
									<input type="number" class="form-control" name="cantidad" value="1" style="text-align:center">
									<input type="hidden" name="idarticulo" value="<?php echo $idarticulo; ?>">
								</div>
								<div class="row"><br>
									<button type="submit" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar al carrito</button>
								</div>
								
								</div>

								<?php
			
								}else{
								}

								?>
							
							</form>

							<!--<ul class="product-links">
								<li><h4>Categoria:</h4></li>
								<li><a href="#"><?php echo $categoria ?></a></li>
							</ul>

							<ul class="product-links">
								<li>Share:</li>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i></a></li>
							</ul>-->

						</div>
					</div>
					<!-- /Product details -->

					<!-- Product tab -->
					<div class="col-md-12">
						<div id="product-tab">
							<!-- product tab nav -->
							<ul class="tab-nav">
								<li class="active"><a data-toggle="tab" href="#tab1">Descripcion</a></li>
								<li><a data-toggle="tab" href="#tab2">Especificaciones</a></li>
							</ul>
							<!-- /product tab nav -->

							<!-- product tab content -->
							<div class="tab-content">
								<!-- tab1  -->
								<div id="tab1" class="tab-pane fade in active">
									<div class="row">
										<div class="col-md-12">
											<p><?php echo $descripcion;  ?></p>
										</div>
									</div>
								</div>
								<!-- /tab1  -->

								<!-- tab2  -->
								<div id="tab2" class="tab-pane fade in">
									<div class="row">
										<div class="col-md-12">
											<?php 

											$sqlesp = mysqli_query($conexion,"SELECT * from especificaciones where articulo=".$_GET["id"]);
											foreach ($sqlesp as $fila) {
												echo "<strong>".$fila["descrip"].":  ".$fila["detalle"]." </strong><br>";
											}

											?>
										</div>
									</div>
								</div>
								<!-- /tab2  -->
							</div>
							<!-- /product tab content  -->
						</div>
					</div>
					<!-- /product tab -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- Section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<div class="col-md-12">
						<div class="section-title text-center">
							<h3 class="title">Productos Relacionados</h3>
						</div>
					</div>

					<!-- product -->
					
						<?php
											$sql = "SELECT a.idarticulo,a.nombreart,p.precio,a.clasificacion,cl.nombrecla FROM articulos a
												join clasificaciones cl on cl.idclasificacion=a.clasificacion
												join precios p on p.articulo=a.idarticulo
												 where cl.nombrecla like '%".$categoria."%' order by a.nombreart desc limit 4";
											$listarart2 = mysqli_query($conexion,$sql);
											foreach($listarart2 as $row2){

												$sqlimg = mysqli_query($conexion,"SELECT imagen from imagenes where articulo='".$row2["idarticulo"]."' and tipo=1");
												foreach ($sqlimg as $fila) {
													
												
										?>


										<!-- product -->
								<div class="col-md-3 col-xs-6">
										<div class="product">
											<div class="product-img">
												<?php 
												echo '<img src="./img/'.$fila["imagen"].'" id="'.$row2["idarticulo"].'">'
												?>
											</div>
											<div class="product-body">
												<h3 class="product-name"><a href="detail_product.php?id=<?php echo $row2["idarticulo"]; ?>"><?php echo $row2["nombreart"];?></a></h3>
												<h4 class="product-price"><?php echo formatearNumero($row2["precio"]);?> Gs.</h4>
												
												<!--<h4 class="product-price"><del class="product-old-price">$990.00</del></h4> para precio anterior-->
												<!--<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>-->
											</div>
										<form method="POST" action="cargar_carrito2.php">
											<input type="hidden" id="idarticulo" name="idarticulo" value="<?php echo $row2["idarticulo"]; ?>">
											<input type="hidden" class="form-control" name="cantidad" id="cantidad" value="1">
											<div class="add-to-cart">
												<button type="submit" class="add-to-cart-btn" ><i class="fa fa-shopping-cart"></i> Agregar al carrito</button>
											</div>
										</form>
										</div>
										</div>
										<!-- /product -->
										<?php
											}
										}
										?>
					
					<!-- /product -->


					<div class="clearfix visible-sm visible-xs"></div>

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /Section -->


		<!-- FOOTER -->
		<?php include("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php"); ?>

	</body>
</html>
