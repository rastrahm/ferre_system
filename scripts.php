	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/nouislider.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/main.js"></script>
	<script src="admin/alertifyjs/alertify.js"></script>
	<script src="admin/js/sweetalert.min.js"></script>
<!--===============================================================================================-->
	<script src="vendorlogin/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<!--<script src="vendorlogin/bootstrap/js/popper.js"></script>
	<script src="vendorlogin/bootstrap/js/bootstrap.min.js"></script>
===============================================================================================-->
<!--===============================================================================================-->
<!--===============================================================================================-->
	<script src="vendorlogin/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main2.js"></script>


	<script type="text/javascript">
		$(document).bind("contextmenu",function(e) {
		     e.preventDefault();
		    });

		        $(document).keydown(function(e){
		            if(e.which === 123){
		               return false;
		            }
		        });

	function add(id,cantidad){
        $.ajax({
          url: 'aumentar.php',
          type: 'POST',
          data: {"id": id, "cantidad": cantidad}
          }).done( function( info ){
          $("#carrito_act").html(info);
          });
        };

    function borrar_elemento(id){
         $.ajax({
          url: 'borrar.php',
          type: 'POST',
          data: 'idarticulo='+id,
          }).done( function( info ){
          $("#carrito_act").html(info);
          //alertify.error("Producto Eliminado");
          location.reload(true);
        });
          
      };


    function guardar_cli(){

    	if ($("#contra1").val()==$("#contra2").val()) {

    		var documento = $("#document").val();
	    	var razonsocial = $("#razonsoc").val();
	    	var telefono = $("#telef").val();
	    	var correo = $("#correoele").val();
	    	var ciudad = $("#ciu").val();
	    	var direccion = $("#direc").val();
	    	var contrasena = $("#contra1").val();

	    	$.ajax({
	          method: "POST",
	          url: "guardarClientes.php",
	          data: {'documento': documento, 'razonsocial': razonsocial, 'telefono': telefono, 'correo': correo, 'ciudad': ciudad, 'direccion': direccion, 'contrasena': contrasena}
	        }).done( function( info ){
	          var json_info = JSON.parse( info );
	          mostrar_mensaje( json_info );
	          limpiar_datos();
	        });

    	}else{
    		alertify.error("Las contraseñas no coinciden");
    	}

    }

    function iniciar_sesion() {

        var usuario = $("#usuario").val();
        var contrasena = $("#contrasena").val();

        $.ajax({
            method: "POST",
            url: "./admin/seguridad/control_clientes.php",
            data: {'usuario': usuario, 'contrasena': contrasena}
          }).done( function( info ){
            var json_info = JSON.parse( info );
            mostrar_mensaje( json_info );
          });
      
    }

        
    var mostrar_mensaje = function( informacion ){
      if( informacion.respuesta == "EXITO" ){

        location.reload(true);
         
      }else if( informacion.respuesta == "INCORRECTO"){
           swal({
              title: 'Error!',
              text: 'Datos Incorrectos!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ELIMINADO"){
          swal({
              title: 'Atención!',
              text: 'Registro Eliminado',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "ERROR" ){
          swal({
              title: 'Atención!',
              text: 'No se ejecuto la consulta!',
              type: 'warning',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "BIEN"){
           swal({
              title: 'Bien!',
              text: 'Registro Exitoso!',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }else if( informacion.respuesta == "HABILITADO"){
           swal({
              title: 'Bien!',
              text: 'Ingrediente Disponible',
              type: 'success',
              timer: 2000,
              showConfirmButton: false
            })
      }
    }

var limpiar_datos = function(){
      $("#razonsocial").val("");
      $("#documento").val("");
      $("#direccion").val("");
      $("#telefono").val("");
      $("#ciudad").val("");
      $("#correo").val("");
      $("#contrasena").val("");
      $("#contrasena2").val("");
    }

    function buscar_producto(){
      var codigo= $("#buscar_product").val();
      var parametros={'articulo':codigo};
      $.ajax({
        type: "POST",
        url:'buscar_producto.php',
        data: parametros,
        success:function(data){
          $("#mostrar_resultado").html(data).fadeIn('slow');
        }
      })
    }

    $('body').click(function() {
		 $('#mostrar_resultado').fadeOut();
		});

	</script>