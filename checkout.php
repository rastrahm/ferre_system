<!DOCTYPE html>
<html lang="en">
	<?php include("header.php"); ?>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<?php include("header_top.php"); ?>
			<!-- /TOP HEADER -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<form method="POST" action="guardar.php">
					<?php 

						include ("conexion.php");

						if (isset($_SESSION['nombreUsuario'])){
							$usuario = $_SESSION['nombreUsuario'];

							$sqlusu = mysqli_query($conexion,"SELECT * from clientes where usuario='$usuario'");
							foreach ($sqlusu as $row) {
								
					?>


					<div class="col-md-7">
						<!-- Billing Details -->
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Dirección de Facturación</h3>
							</div>
							<div class="form-group">
								<input class="input" type="text" name="razonsocial" placeholder="Nombre Completo o Razon Social" value="<?php echo $row["razonsocial"] ?>" required="">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="documento" placeholder="Ruc o Ci" value="<?php echo $row["documento"] ?>" required="">
							</div>
							<div class="form-group">
								<input class="input" type="email" name="email" placeholder="Correo" value="<?php echo $row["correo"] ?>" required="">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="barrio" placeholder="Barrio" value="<?php echo $row["barrio"] ?>" required="">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="direccion" placeholder="Direccion" value="<?php echo $row["direccion"] ?>" required="">
							</div>
							<div class="form-group">
								<input class="input" type="tel" name="telefono" placeholder="Telefono" value="<?php echo $row["telefono"] ?>" required="">
							</div>
							<select class="form-control" name="ciudad" required="">

								<?php 
									include("conexion.php");
									$sql = "SELECT * from ciudades where idciudad=".$row["ciudad"];
									$res = mysqli_query($conexion,$sql);
									foreach ($res as $row) {
										echo "<option value='".$row["idciudad"]."'>".$row["ciudad"]."</option>";
									}

								?>
								
							</select><br>
							<div class="col-md-10">
                                        <div class="card">
                                            <div class="card-body">
                                            		<h2>Seleccione pantalla completa</h2>
                                                    <div class="card-body card-block">
                                                        		
                                                              <div class='mostrar'></div><!-- Carga los datos ajax -->
                                                                <!-- END USER DATA-->
                                                    
                                                    </div><br>
                                                  </div>
                                                 
                                                  </div>
                                  </div>
						
						</div>
						<!-- /Billing Details -->
					</div>


					<?php 

							}
						}else{
					?>

					<div class="col-md-7">
						<!-- Billing Details -->
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Dirección de Facturación</h3>
							</div>
							<div class="form-group">
								<input class="input" type="text" name="razonsocial" placeholder="Nombre Completo o Razon Social" required="">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="documento" placeholder="Ruc o Ci" required="">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="barrio" placeholder="Barrio" required="">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="direccion" placeholder="Direccion" required="">
							</div>
							<div class="form-group">
								<input class="input" type="email" name="email" placeholder="Correo" required="">
								</div>
							<div class="form-group">
								<input class="input" type="tel" name="telefono" placeholder="Telefono" required="">
							</div>
							<select class="form-control" name="ciudad" required="">

								<?php 
									include("conexion.php");
									$sql = "SELECT * from ciudades";
									$res = mysqli_query($conexion,$sql);
									foreach ($res as $row) {
										echo "<option value='".$row["idciudad"]."'>".$row["ciudad"]."</option>";
									}

								?>
								
							</select><br>
							
							<div class="col-md-10">
                                        <div class="card">
                                            <div class="card-body">
                                            		<h2>Seleccione pantalla completa</h2>
                                                    <div class="card-body card-block">
                                                        		
                                                              <div class='mostrar'></div><!-- Carga los datos ajax -->
                                                                <!-- END USER DATA-->
                                                    
                                                    </div><br>
                                                  </div>
                                                 
                                                  </div>
                                  </div>
							
						</div>
						<!-- /Billing Details -->

						<!-- Shiping Details -->
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title">Dirección de envío</h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
								<label for="shiping-address">
									<span></span>
									Enviar a una ubicación diferente?
								</label>
								<div class="caption">
								<div class="form-group">
								<input class="input" type="text" name="razonsocial2" placeholder="Nombre Completo o Razon Social">
								</div>
								<div class="form-group">
								<input class="input" type="text" name="documento2" placeholder="Ruc o Ci">
								</div>
								<div class="form-group">
									<input class="input" type="text" name="barrio2" placeholder="Barrio">
								</div>
								<div class="form-group">
									<input class="input" type="text" name="direccion2" placeholder="Direccion">
								</div>
								
								<div class="form-group">
									<input class="input" type="tel" name="telefono2" placeholder="Telefono">
								</div>
								<select class="form-control" name="ciudad2" required="">

								<?php 
									include("conexion.php");
									$sql = "SELECT * from ciudades";
									$res = mysqli_query($conexion,$sql);
									foreach ($res as $row) {
										echo "<option value='".$row["idciudad"]."'>".$row["ciudad"]."</option>";
									}

								?>
								
							</select><br>
								</div>
							</div>
						</div>
						<!-- /Shiping Details -->
					</div>
					
					<?php 
						}
					?>


					

					<!-- Order Details -->
					<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Su Pedido</h3>
						</div>
						<div class="order-summary">
							<div class="order-col">
								<div><strong>CANT / PRODUCTO</strong></div>
								<div><strong>TOTAL / ELIMINAR</strong></div>
							</div>
						
							<div class="order-products">
								<?php 

								$total = 0;

								foreach ($_SESSION["Pedido_det"] as $i=>$valor) {
									$subtotal=$_SESSION["Pedido_det"][$i][2]*$_SESSION["Pedido_det"][$i][3];
									$total+=$subtotal;
								?>
								<div class="order-col">
									<div>(<?php echo $_SESSION["Pedido_det"][$i][3]?> ) <?php echo $_SESSION["Pedido_det"][$i][1]; ?></div>
									<div><?php echo formatearNumero($subtotal);    
									echo '&nbsp;&nbsp;&nbsp;<button type="button" name="ver_code" class="borrar btn btn-sm btn-danger" onClick="borrar_elemento('.($i).');"><i class="fa fa-times"></i> </button>';
									?></div>

								</div>
								<?php 
									} 

								 ?>
							</div>
							<div class="order-col">
								<div>Envío</div>
								<div><strong>GRATIS</strong></div>
							</div>
							<div class="order-col">
								<div><strong>TOTAL</strong></div>
								<div><strong class="order-total"><?php echo formatearNumero($total) ?> Gs.</strong></div>
							</div>
						</div>
						<div class="payment-method">
							<div class="input-radio">
								<input type="radio" name="payment" id="payment-1" value="Deposito o Transferencia Bancaria">
								<label for="payment-1">
									<span></span>
									Deposito o Transferencia Bancaria
								</label>
								<div class="caption">

									<p>IMPORTANTE: Indicar el número de pedido en el concepto de su transferencia</p>

									<p>Datos para depósito o transferencia</p>

									   Titular: ZAD S.A
									<p>RUC: 80099645-3</p>

									<p>Banco BBVA: Cta. Cte. Guaranies: 1501023411</p>

									<p>Una vez realizado el deposito o la transferencia, enviar una FOTO o CAPTURA DE PANTALLA del comprobante en el siguiente numero de Whatsapp +595971752130</p>

									<p>Pagos confirmados después de las 15:00 tendrán un retraso de un día hábil adicional para la entrega del producto.</p>

								</div>
							</div>
							<div class="input-radio">
								<input type="radio" name="payment" id="payment-2" value="Pagar al Recibir/Retirar mi Pedido">
								<label for="payment-2">
									<span></span>
									Pagar al Recibir/Retirar mi Pedido
								</label>
								<div class="caption">

								</div>
							</div>
							<div class="section-title text-center">
								<h4 class="title">Metodo de entrega</h4>
							</div>

							<select class="form-control" name="tipo_pedido" id="tipo_pedido" required="">

								<?php 
									include("conexion.php");
									$sql = "SELECT * from tipo_pedido";
									$res = mysqli_query($conexion,$sql);
									foreach ($res as $row) {
										echo "<option value='".$row["idtipo_ped"]."'>".$row["desc_tip_ped"]."</option>";
									}

								?>
								
							</select><br>
							<!--<div class="input-radio">
								<input type="radio" name="payment" id="payment-3">
								<label for="payment-3">
									<span></span>
									Paypal System
								</label>
								<div class="caption">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>-->
						</div>
						<div class="input-checkbox">
							<input type="checkbox" id="terms" name="terms">
							<label for="terms">
								<span></span>
								He leído y acepto los <a href="about.php">Términos & condiciones</a>
							</label>
						</div>
						<div align="center">
						<button type="submit" class="primary-btn order-submit">Realizar Pedido</button>
						</div>
					</div>
					<!-- /Order Details -->
				</form>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->
		<!-- FOOTER -->
		<?php include("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php"); ?>

	</body>
</html>

<script type="text/javascript">
	
	window.onload = function() {
          $.ajax({
            url:'ubicacion.php',
             beforeSend: function(objeto){
             $('#loader').html('');
            },
            success:function(data){
              $(".mostrar").html(data).fadeIn('slow');
              $('#loader').html('');
            }
          })
        };

</script>