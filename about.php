<!DOCTYPE html>
<html lang="en">
	<?php include("header.php"); ?>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<?php 

			include("header_top.php"); 

			?>

			
			<!-- /TOP HEADER -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
					<h2>Términos y Condiciones de Uso</h2><br>

<h4>Política de Privacidad</h4><br>

<h5>Sección 1: Manejo de la Información</h5><br>

<p>¿Qué hacemos con tu información? Cuando usted compra algo de nuestra tienda en línea, como parte del proceso de compra y venta, recopilamos la información personal que usted nos da, como su nombre, dirección física y dirección de correo electrónico.</p><br>

<p>Cuando navega por nuestra tienda en línea, también recibimos automáticamente la dirección del protocolo de Internet (IP) del equipo con el cual se está conectando con el fin de proporcionarnos información que nos ayuda a aprender acerca de su navegador y sistema operativo.</p>

<p>Marketing por correo electrónico: Con su permiso, podemos enviarle correos electrónicos acerca de nuestra tienda, nuevos productos, promociones y otras actualizaciones.</p><br>


<h5>Sección 2: Consentimiento</h5><br>
<p>¿Cómo consiguen mi consentimiento? Cuando usted nos proporciona información personal para completar una transacción, verificar su tarjeta de crédito, hacer un pedido, los arreglos para una entrega o devolución de una compra, asumimos que usted acepta que recolectemos sus datos para ser utilizados para las razones especificadas más arriba.</p><br>

<p>Si le pedimos su información personal por una razón secundaria, como actividades de marketing, le pediremos su expreso consentimiento para esto y proporcionaremos la oportunidad de decir que no.</p><br>

<h5>Sección 3: Divulgación</h5><br>
<p>Podemos revelar su información personal en caso que la ley nos obligue a hacerlo o si usted infringe nuestros Términos de Servicio.</p><br>


<h5>Sección 4: Proveedor del Servicio de Comercio Electrónico</h5><br>
<p>Nuestra tienda se encuentra alojado en nuestros propios servidores. Nosotros proporcionamos la plataforma de comercio electrónico que nos permite vender nuestros productos y servicios a usted.</p><br>

<p>Sus datos se almacenan a través de los sistemas de almacenamiento de datos de nuestra tienda en línea, así como nuestros propios datos y los de nuestros productos y servicios. Prestamos principal atención en almacenar sus datos en un servidor seguro protegido por firewall certificado.</p><br>


<h5>Sección 5: Servicios Proveídos por Terceros</h5><br>
<p>En general, los proveedores de servicios utilizados por nuestra plataforma sólo recopilan, utilizan y divulgan su información en la medida necesaria para que puedan realizar los servicios que prestan.</p><br>

<p>Sin embargo, algunos proveedores de servicios tales como pasarelas de pago y otros procesadores de transacciones de pago, tienen sus propias políticas de privacidad con respecto a la información que estamos obligados a proporcionar a ellos para sus transacciones relacionadas con su compra.</p><br>

<p>Para estos proveedores, le recomendamos que lea las políticas de privacidad para que pueda entender la manera en que su información personal será manejada por estos proveedores.</p><br>

<p>Debe considerar que algunos proveedores pueden estar ubicados físicamente y legalmente en una jurisdicción diferente a usted o nosotros. Así que si usted decide proceder con una transacción que involucra los servicios de un proveedor externo su información puede estar sujeta a las leyes de la jurisdicción en el que se encuentran el proveedor o sus instalaciones.</p><br>

<p>A modo de ejemplo, si se encuentra en Paraguay y su transacción es procesada por una pasarela de pago ubicado en los Estados Unidos, luego que su información personal haya sido utilizada en la realización de la transacción, puede ser objeto de divulgación de su información de conformidad con la legislación de los Estados Unidos.</p><br>

<p>Una vez que abandone el sitio web de nuestra empresa y se redirige a un sitio web o aplicación de terceros, ya no se rigen por la presente Política de Privacidad y Términos de Servicio.</p><br>

<p>Enlaces
Al hacer clic en enlaces en nuestra tienda, pueden dirigirlo fuera de nuestro sitio. No somos responsables de las prácticas de privacidad de otros sitios y animamos a leer sus declaraciones de privacidad.</p><br>


<h5>Sección 6: Seguridad</h5><br>
<p>Para proteger su información personal tomamos las precauciones razonables y seguimos las mejores prácticas de la industria para asegurarnos que no se pierde de manera inapropiada debido al mal uso, vulnerabilidad de acceso, divulgación errónea, alteración o destrucción.</p><br>

<p>Si usted nos proporciona su información de tarjeta de crédito, ésta información es encriptada utilizando tecnología de Capa de Conexión Segura (SSL) y se almacena bajo cifrado. Aunque ningún método de transmisión a través de Internet o de almacenamiento electrónico es 100% seguro, seguimos todos los requisitos de PCI-DSS y aplicamos adicionalmente las normas de la industria generalmente aceptados.</p><br>


<h5>Sección 7: Edad Mínima para el Uso de la Plataforma</h5><br>
<p>Al utilizar este sitio web, usted declara que tiene, al menos, la mayoría de edad legal en Paraguay, o que poseyendo esta mayoría de edad ha permitido a cualquiera de sus dependientes menores de edad usar bajo su tutela, cuidado y responsabilidad este sitio web.</p><br>


<h5>Sección 8: Cambios de la Presente Política de Privacidad</h5><br>
<p>Nos reservamos el derecho de modificar esta política de privacidad en cualquier momento, así que por favor revísela con frecuencia. Los cambios y aclaraciones entrarán en vigor inmediatamente después de su publicación en el sitio web. Si hacemos cambios a esta política, le notificaremos aquí que ha sido actualizada, por lo que usted está enterado de qué información recopilamos, cómo la usamos y bajo qué circunstancias, si las hubiere, la utilización y/o divulgación de ella.</p><br>

<p>Si nuestra tienda es adquirida o fusionada con otra empresa, su información puede ser transferida a los nuevos propietarios.</p><br>


<h3>Dudas e Información de Contacto</h3><br>
<p>Si quiere tener acceso a su información, rectificarla, modificarla, eliminarla, presentar una queja o simplemente quiere más información póngase en contacto con nosotros al correo electrónico jenniferferreira296@gmail.com o directamente en nuestras oficinas ubicadas en:</p><br>

<p><i class="fa fa-map-marker"></i> Avda. Artigas N° 988 c/ Tte. Cusmanich, Asunción Paraguay</p><br>
<p><i class="fa fa-phone-square"></i> (+595) 971 752-130</p><br>
<p><i class="fa fa-envelope"></i> jenniferferreira296@gmail.com</p><br>
<p><i class="fa fa-clock-o"></i> Lunes-Viernes: 7:30-18:00</p><br>
<p>Sábados: 7:30-12:00</p><br>
<p>Domingos: Cerrado.</p><br>
				<!-- /row -->
			</div>
			<!-- /container -->
			
		</div>
		<!-- /SECTION -->
		<!-- FOOTER -->
		<?php include("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php"); ?>

	</body>
</html>