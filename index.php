<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" type="text/css" href="admin/alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="admin/alertifyjs/css/themes/default.css">
<script src="admin/alertifyjs/alertify.js"></script>

	<?php 

		include ("header.php");
	    include ("conexion.php");
		  //include ("formatos.php");

		  if (isset($_SESSION["exito"])){
          if ($_SESSION['exito']==1){
                    echo '<script>
                        
                            alertify.success("¡El producto fue agregado al Carrito!");
                        </script>
                    ';
                }else if ($_SESSION['exito']==2){
                    echo '<script>
                            alertify.success("¡El pedido ha sido procesado! En unos momentos nos pondremos en contacto con usted!");
                        </script>
                    ';
                }else if ($_SESSION['exito']==3){
                    echo '<script>
                            alertify.success("¡El usuario y/o contraseña no existen!");
                        </script>
                    ';
                }
                unset($_SESSION['exito']);
            }

            /*$clasificacion = mysqli_query($conexion, 'SELECT * from clasificaciones');

            foreach ($clasificacion as $row) {

	            $insert = mysqli_query($conexion,"SELECT * from articulos where clasificacion = ".$row["idclasificacion"]." limit 20");

	            foreach ($insert as $value) {

	            	$insert2 = mysqli_query($conexion,"SELECT * from bodegas");

	            	foreach ($insert2 as $value2) {
	            		
		            	$bodega = mysqli_query($conexion, "INSERT INTO bodegas_det (bodega, articulo, stock)
		            		values (".$value2["idbodega"].", ".$value["idarticulo"].", 20) ");
		            }

	            }
        	}*/

        	/*$clasificacion = mysqli_query($conexion, 'SELECT * from articulos');

        	$precio = 0;

            foreach ($clasificacion as $row) {

            	$precio += 1000;

	            $insert = mysqli_query($conexion,"INSERT INTO precios (idlista, articulo, precio, fecha_alta)
	            	values (1, ".$row["idarticulo"].", ".$precio.", '2021-12-07')");
	        }*/

	?>
	<body>
		<!-- HEADER -->
		<?php include ("header_top.php") ?>
		<!-- /HEADER -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<a href="https://api.whatsapp.com/send?phone=+595971752130&text=Buenos dias!" class="float" target="_blank">
		<i class="fa fa-whatsapp my-float"></i>
		</a>
  
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<?php
						$sql = "SELECT * from clasificaciones";
						$listarart = mysqli_query($conexion,$sql);
						foreach($listarart as $row){
						?>

					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title"><?php echo $row["nombrecla"];?></h3>
						</div>
					</div>

					<!-- Products tab & slick -->
					<div class="col-md-12">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										
										<?php
											$sql = "SELECT a.idarticulo,a.nombreart,p.precio,a.clasificacion,cl.nombrecla FROM articulos a
												join clasificaciones cl on cl.idclasificacion=a.clasificacion
												join precios p on p.articulo=a.idarticulo
												where p.idlista=1 and cl.nombrecla like '%".$row["nombrecla"]."%' limit 10";
											$listarart2 = mysqli_query($conexion,$sql);
											foreach($listarart2 as $row2){

												$sqlimg = mysqli_query($conexion,"SELECT imagen from imagenes where articulo='".$row2["idarticulo"]."' and tipo=1");
												foreach ($sqlimg as $fila) {
													
												
										?>


										<!-- product -->
										
										<div class="product">
											<div class="product-img">
												<?php 
												echo '<a href="detail_product.php?id='.$row2["idarticulo"].'"><img width="255" height="240" src="./img/'.$fila["imagen"].'" id="'.$row2["idarticulo"].'"></a>'
												?>
											</div>
											<div class="product-body">
												<h3 class="product-name"><a href="detail_product.php?id=<?php echo $row2["idarticulo"]; ?>"><?php echo $row2["nombreart"];?></a></h3>
												<h4 class="product-price"><?php echo formatearNumero($row2["precio"]);?> Gs.</h4>
												
												<!--<h4 class="product-price"><del class="product-old-price">$990.00</del></h4> para precio anterior-->
												<!--<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>-->
											</div>
										<form method="POST" action="cargar_carrito.php">
											<input type="hidden" id="idarticulo" name="idarticulo" value="<?php echo $row2["idarticulo"]; ?>">
											<input type="hidden" class="form-control" name="cantidad" id="cantidad" value="1">
											<div class="add-to-cart">
												<button type="submit" class="add-to-cart-btn" ><i class="fa fa-shopping-cart"></i> Agregar al carrito</button>
											</div>
										</form>
										</div>
										
										<!-- /product -->
										<?php
											}

										?>
											
										<?php

										}
										?>
										</div>
										
									</div>
								</div>
						</div>

										<?php
										}
										?>

									
									
								<!-- /tab -->
					<!-- Products tab & slick -->
				</div>
				
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->




		<!-- FOOTER -->
		<?php include ("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
	<?php include("scripts.php") ?>



	</body>
</html>
<script type="text/javascript">



</script>