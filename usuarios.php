<?php

date_default_timezone_set('America/Asuncion');

include ("conexion.php");       

class User {
    private $dbHost     = "localhost";
    private $dbUsername = "root";
    private $dbPassword = "";
    private $dbName     = "ferresystem";
    private $userTbl    = 'usuarios_face';
    
    function __construct(){
        if(!isset($this->db)){
            // Conectar a la BD
            $conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
            if($conn->connect_error){
                die("Failed to connect with MySQL: " . $conn->connect_error);
            }else{
                $this->db = $conn;
            }
        }
    }

      
    
    function checkUser($userData = array()){
        if(!empty($userData)){
            // Revisar si la información de usuario ya existe
            $prevQuery = "SELECT * FROM ".$this->userTbl." WHERE oauth_provider = '".$userData['oauth_provider']."' AND oauth_uid = '".$userData['oauth_uid']."'";
            $prevResult = $this->db->query($prevQuery);


            if($prevResult->num_rows > 0){
                // actualizar información si es que existe

                $query = "UPDATE ".$this->userTbl." SET first_name = '".utf8_encode($userData['first_name'])."', last_name = '".utf8_encode($userData['last_name'])."', email = '".$userData['email']."', picture = '".$userData['picture']."', modified = '".date("Y-m-d H:i:s")."' WHERE oauth_provider = '".$userData['oauth_provider']."' AND oauth_uid = '".$userData['oauth_uid']."'";
                $update = $this->db->query($query);
            }else{

                $queryc = "INSERT INTO clientes (razonsocial,correo) VALUES (concat('".utf8_decode($userData['first_name'])."',' ', '".utf8_decode($userData['last_name'])."'), '".$userData['email']."')";
                $insertc = $this->db->query($queryc);

                 $prevQueryid = "SELECT idcliente FROM clientes WHERE correo = '".$userData['email']."'";
                $resultid = $this->db->query($prevQueryid);
                $userDataid = $resultid->fetch_assoc();

                // Insertar información del usuario
                $query = "INSERT INTO ".$this->userTbl." SET cliente='".$userDataid['idcliente']."', oauth_provider = '".$userData['oauth_provider']."', oauth_uid = '".$userData['oauth_uid']."', first_name = '".utf8_decode($userData['first_name'])."', last_name = '".utf8_decode($userData['last_name'])."', email = '".$userData['email']."', picture = '".$userData['picture']."', created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."'";
                $insert = $this->db->query($query);

            }
            
            // Tomar la información de la BD
            $result = $this->db->query($prevQuery);
            $userData = $result->fetch_assoc();
        }
        
        // return
        return $userData;
    }
}

?>