<!DOCTYPE html>
<html lang="en">
	<?php include("header.php"); ?>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<?php 

			include("header_top.php"); 

			?>

			
			<!-- /TOP HEADER -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
					<h2>Nosotros</h2><br>
<p>FERRESYSTEM es una empresa del Grupo Union. Grupo de empresas industriales, importadoras y comerciales.
Se encuentra en el negocio de BRINDAR SOLUCIONES relacionadas a la venta de productos eléctricos, de iluminación, electrónicos, herramientas y tecnologías para las obras, la industria, el comercio y el hogar.</p><br>

<p>LA UNION nació como una necesidad de llegada más directa de las industrias del grupo al mercado. En los inicios se dedicó exclusivamente a la promoción y venta de los productos fabricados por las demás empresas del grupo. Posteriormente adquirió personería jurídica propia, constituyéndose en una empresa dedicada al negocio corporativo y de retail, con especial énfasis en la venta de productos nacionales, incorporando luego una gama de productos de importación, generalmente de distribución exclusiva. La estrategia fue aprovechar los canales de venta existentes para ir incorporando productos relacionados con el área de la construcción.</p><br>

<p>LA UNION busca brindar cada día más y mejores servicios. Para atender con mayor profesionalismo las exigencias de sus clientes, la empresa se divide en seis grandes grupos de negocios: ELECTRICIDAD – ILUMINACIÓN – INDUSTRIA – TECNOLOGÍA – CLIMATIZACIÓN – HERRAMIENTAS.</p><br>

<p>De manera a optimizar la inversión del consumidor, ofrece para cada necesidad y en cada producto que comercializa MÁS DE UNA OPCIÓN logrando la mejor relación PRECIO-CALIDAD del mercado.
Los COLABORADORES de LA UNION, el capital MÁS VALIOSO de la empresa, pone todo su empeño, dedicación, conocimiento y compromiso para brindar una atención cada vez más eficiente.</p><br>

<p>Listos para enfrentar los retos de este NUEVO MUNDO, donde todo cambia cada vez más aceleradamente, en LA UNION aceptamos los desafíos de hoy y respondemos con TECNOLOGÍA DE PUNTA y CONOCIMIENTO PROFESIONAL.</p><br>

<p>CALIDAD CERTIFICADA: En el año 2009 hemos implementado el SISTEMA DE GESTIÓN DE CALIDAD conforme a la “Norma ISO 9001”.</p><br>

<h3>Documento de la ISO</h3><br>
<p>Nuestra empresa ha sorteado con éxito los exigentes procesos de auditoría externa de certificación llevados a cabo por la empresa SGS que culminaron con el otorgamiento del CERTIFICADO DE CONFORMIDAD de máximo renombre en el mundo.</p><br>

<p>Seguimos en la constante búsqueda, por ofrecer los mejores productos en materia de iluminación y electricidad, sumado al conocimiento y profesionalismo de nuestra GENTE, apostando a la capacitación y al crecimiento, para que la LUZ alumbre cada día con mayor fuerza.</p><br>
				<!-- /row -->
			</div>
			<!-- /container -->
			
		</div>
		<!-- /SECTION -->
		<!-- FOOTER -->
		<?php include("footer.php"); ?>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<?php include("scripts.php"); ?>

	</body>
</html>